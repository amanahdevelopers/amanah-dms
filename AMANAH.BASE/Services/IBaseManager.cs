﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.Repoistry
{
    public interface IBaseManager<TViewModel, TEntity>
    {
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>();
        Task<TViewModel> AddAsync(TViewModel entity);
        Task<List<TViewModel>> AddAsync(List<TViewModel> entityLst);
        Task<bool> UpdateAsync(TViewModel entity);
        Task<bool> DeleteAsync(TViewModel entity);
        Task<bool> SoftDeleteAsync(TViewModel entity);
        Task<bool> SoftDeleteAsync(List<TViewModel> ViewModelLst);
        Task<bool> DeleteAsync(List<TViewModel> entitylst);
        Task<bool> DeleteByIdAsync(params object[] id);
        Task<PagedResult<TViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
        //Task<PagedResult<TViewModel>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel);
      
        /// <summary>
        /// This Method for lookups usage if you need to get shared lookups 
        /// </summary>
        /// <typeparam name="TProjectedModel"></typeparam>
        /// <param name="IgnoreTenant">if true will get all lookups for the system if false get lookups for tenant . </param>
        /// <returns></returns>
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(bool IgnoreTenant);

        Task<PagedResult<TViewModel>> GetAllByPaginationAsync(bool IgnoreTenant, PaginatedItemsViewModel pagingparametermodel);


    }
}