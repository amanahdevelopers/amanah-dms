﻿using System.Collections.Generic;
using System.Linq;

namespace AMANAH.DMS.BASE.Authentication
{
    public interface ICurrentUser
    {
        //TODO: make sure all those data are stored in the token
        bool? IsAuthenticated { get; }

        string Id { get; }

        int? DriverId { get; }

        string UserName { get; }

        //string Email { get; }
        /// <summary>
        ///  Admin = 0,Agent = 2,Manager = 3,
        /// </summary>
        public RoleType? UserType { get; }

        public IReadOnlyCollection<string> UserPermissions { get; }

        string TenantId { get; }
    }

    public interface ICurrentUserDetailsStore
    {
        void Clear();

        void Deserialize(string details);

        string Serialize();
    }


    public static class Extensions
    {
        public static bool HasPermission(this ICurrentUser currentUser, string permission)
        {
            return currentUser.UserType == RoleType.Admin || currentUser.UserPermissions.Contains(permission);
        }

        public static bool IsDriver(this ICurrentUser currentUser) => currentUser.DriverId.HasValue;
    }
}