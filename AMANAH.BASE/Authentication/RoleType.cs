﻿namespace AMANAH.DMS.BASE.Authentication
{
    public enum RoleType : byte
    {
        Admin = 0,
        General = 1,
        Agent = 2,
        Manager = 3,
    }
}