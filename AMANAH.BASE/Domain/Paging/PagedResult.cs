﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities.Utilites.Paging
{
    public class PagedResult<T>
    {

       public List<T> Result { set; get; }
        
       public int TotalCount { set; get; }
     
    }
}
