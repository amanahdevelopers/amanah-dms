﻿namespace AMANAH.DMS.BASE.Domain.Entities
{
    public interface IBaseEntity:IBaseGlobalEntity, IHaveTenant
    {
    }
}
