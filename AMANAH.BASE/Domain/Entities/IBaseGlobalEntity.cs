﻿using System;

namespace AMANAH.DMS.BASE.Domain.Entities
{
    public interface IBaseGlobalEntity
    {
        string CreatedBy_Id { get; }

        string UpdatedBy_Id { get; }

        string DeletedBy_Id { get; }

        bool IsDeleted { get; }

        DateTime CreatedDate { get; }

        DateTime UpdatedDate { get; }

        DateTime DeletedDate { get; }
    }
}
