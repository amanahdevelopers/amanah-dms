﻿namespace AMANAH.DMS.BASE.Domain.Entities
{
    public interface IHaveTenant
    {
        string Tenant_Id { get; }

    }
}
