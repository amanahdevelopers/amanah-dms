﻿using System;

namespace AMANAH.DMS.BASE.Domain.Entities
{
    public class BaseGlobalEntity : IBaseGlobalEntity
    {

        public virtual string CreatedBy_Id { get; private set; }

        public virtual string UpdatedBy_Id { get; private set; }

        public virtual string DeletedBy_Id { get; private set; }

        public virtual bool IsDeleted { get; private set; }

        public virtual DateTime CreatedDate { get; private set; }

        public virtual DateTime UpdatedDate { get; private set; }

        public virtual DateTime DeletedDate { get; private set; }
    }
}
