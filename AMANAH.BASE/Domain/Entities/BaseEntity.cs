﻿namespace AMANAH.DMS.BASE.Domain.Entities
{
    public class BaseEntity : BaseGlobalEntity, IBaseEntity
    {
        public string Tenant_Id { get; protected set; }
    }
}
