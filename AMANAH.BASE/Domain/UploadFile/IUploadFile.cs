﻿namespace Utilites.UploadFile
{
    public interface IUploadFile
    {
        string FileContent { get; set; }

        string FileName { get; set; }

        string FileRelativePath { get; set; }
    }
}