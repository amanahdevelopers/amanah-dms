﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BASE.Domain.Repositories
{
    public interface IRepositry<TEntity>
    {

#warning cleanup- remove this instead use dbcontext if needed, repository exists to hide this
        IQueryable<TEntity> GetAll();

        Task<PagedResult<TEntity>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel);

        IQueryable<TEntity> GetAllByPagination(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel, out int totalNumbers);

        TEntity Get(params object[] id);

        TEntity Get(Expression<Func<TEntity, bool>> predicate);

        TEntity AddAsync(TEntity entity);

        List<TEntity> Add(IEnumerable<TEntity> entityLst);

        bool Update(TEntity entity);

        bool Delete(TEntity entity);

        bool Delete(IEnumerable<TEntity> entitylst);

        bool SoftDelete(TEntity entity);

        bool SoftDelete(IEnumerable<TEntity> entityLst);

        bool DeleteById(params object[] id);

        IQueryable<TEntity> GetAll(bool IgnoreTenant);

        int SaveChanges();
    }
}