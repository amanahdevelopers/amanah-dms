﻿using System;
using NUnit.Framework;
using Shouldly;
using Utilites;

namespace DMS.Tests.Utilities
{
    public class ReflectionHelperTest
    {
        [Test]
        public void IsDefaultValue_WorksAsExpected()
        {
            // Arrange
            var valueAndExpectedResults = new (object value, bool expectedResult)[]
            {
                (default(DateTime), true ),
                (DateTime.Now, false ),
                (default(int), true ),
                (int.MaxValue, false ),
                (string.Empty, false ),
                (null, true ),
                (new DateTime?(), true ),
                (new DateTime?(DateTime.Now), false)
            };

            // Act, Assert
            foreach (var (value, expectedResult) in valueAndExpectedResults)
            {
                ReflectionHelper.IsDefaultValue(value).ShouldBe(expectedResult);
            }
        }
    }
}
