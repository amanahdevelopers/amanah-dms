using AMANAH.DMS.API;
using AMANAH.DMS.ViewModel;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog.Extensions.Logging;
using System.IO;
using System.Net.Http;
using System.Text;

namespace DMS.Tests
{
    public class BaseControllerTests
    {
        public readonly TestServer _server;
        public readonly HttpClient _client;
        public string  AccsessToken;

        public BaseControllerTests()
        {
            string[] args = new string[0];
            
          var build = WebHost.CreateDefaultBuilder(args)
             .UseContentRoot(Directory.GetCurrentDirectory())
             .UseIISIntegration()
             .UseStartup<Startup>()
             .ConfigureLogging((hostingContext, builder) =>
             {
                 builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                 builder.AddConsole();
                 builder.AddDebug();
                 builder.AddNLog();

             });

            _server = new TestServer(build);
            _client = _server.CreateClient();

            TokenViewModel tokenView = new TokenViewModel();
            tokenView.Username = "admin";
            tokenView.Password = "P@ssw0rd";

            var json = JsonConvert.SerializeObject(tokenView);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response =  _client.PostAsync("/api/Token", data).Result;
            var responseStream =  response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<TokenResultViewModel>(responseStream);
            this.AccsessToken = result.token.accessToken;
            _client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.AccsessToken);
        }


        public  IWebHost BuildWebHost(string[] args) =>
         WebHost.CreateDefaultBuilder(args)
             .UseContentRoot(Directory.GetCurrentDirectory())
             .UseIISIntegration()
             .UseStartup<Startup>()
             .ConfigureLogging((hostingContext, builder) =>
             {
                 builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                 builder.AddConsole();
                 builder.AddDebug();
                 builder.AddNLog();

             })
             .Build();

    }
}