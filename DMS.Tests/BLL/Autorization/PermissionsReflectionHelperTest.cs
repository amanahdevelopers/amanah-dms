﻿using AMANAH.DMS.BLL.Authorization;
using NUnit.Framework;
using Shouldly;
using DescriptionAttribute = System.ComponentModel.DescriptionAttribute;

namespace DMS.Tests.BLL.Autorization
{
    public class PermissionsReflectionHelperTest
    {
        [Test]
        public void GetFeaturePermissions_ForPermissionsClass_WillReturnExactItems()
        {
            // Arrange
            var expectedPermissions = new (string Name, string[] Permissions)[]
            {
                (
                    nameof(DummyPermissionClass.FeatureWithoutDescription),
                    new[]{ DummyPermissionClass.FeatureWithoutDescription.Permission1}
                ),
                (
                    DummyPermissionClass.FeatureDescription,
                    new[]
                    {
                        DummyPermissionClass.FeatureWithDescription.Create,
                        DummyPermissionClass.FeatureWithDescription.Delete
                    }
                )
            };

            // Act
            var permissions = PermissionsReflectionHelper.GetFeaturePermissions(typeof(DummyPermissionClass));

            // Assert
            permissions.Length.ShouldBe(expectedPermissions.Length);
            for (int i = 0; i < expectedPermissions.Length; i++)
            {
                var featurePermission = permissions[i];
                var expectedFeaturePermission = expectedPermissions[i];
                featurePermission.Name.ShouldBe(expectedFeaturePermission.Name);
                featurePermission.Permissions.ShouldBe(expectedFeaturePermission.Permissions);
            }
        }
    }

    internal static class DummyPermissionClass
    {
        public const string FeatureDescription = "The description";

        public static class FeatureWithoutDescription
        {
            public const string Permission1 = "{32522320-0464-4210-A9CF-49178E1D680E}";
            public const int OtherValue = default;
        }

        [Description(FeatureDescription)]
        public static class FeatureWithDescription
        {
            public const string Create = "{3CE896BF-656E-4A77-BD3C-9DB54D940BF0}";
            public const string Delete = "{A3A23CA0-D766-40AD-9C9A-E00181C932B0}";
        }

        private static class PrivateFeature
        {
        }

        public class NonStaticFeature
        {
        }
    }
}
