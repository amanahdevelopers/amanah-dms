CREATE TABLE [dbo].[GeoFence] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (MAX) NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_GeoFences] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO


CREATE TABLE [dbo].[GeoFenceDriver] (
    [DriverId]   INT NOT NULL,
    [GeoFenceId] INT NOT NULL,
    CONSTRAINT [PK_GeoFenceDriver] PRIMARY KEY CLUSTERED ([DriverId] ASC, [GeoFenceId] ASC),
    CONSTRAINT [FK_GeoFenceDriver_Driver] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Driver] ([Id]),
    CONSTRAINT [FK_GeoFenceDriver_GeoFence] FOREIGN KEY ([GeoFenceId]) REFERENCES [dbo].[GeoFence] ([Id])
);
GO

CREATE TABLE [dbo].[GeoFenceLocation](
	[GeoFenceId] [int] NOT NULL,
	[Longitude] [nvarchar](100) NOT NULL,
	[Latitude] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_GeoFenceLocation] PRIMARY KEY CLUSTERED 
(
	[GeoFenceId] ASC,
	[Longitude] ASC,
	[Latitude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GeoFenceLocation]  WITH CHECK ADD  CONSTRAINT [FK_GeoFenceLocation_GeoFence] FOREIGN KEY([GeoFenceId])
REFERENCES [dbo].[GeoFence] ([Id])
GO

ALTER TABLE [dbo].[GeoFenceLocation] CHECK CONSTRAINT [FK_GeoFenceLocation_GeoFence]
GO

--alter table [Teams] 
--  add  [GeoFenceId] INT NULL     CONSTRAINT [FK_Teams_GeoFence] FOREIGN KEY ([GeoFenceId]) REFERENCES [dbo].[GeoFence] ([Id])
--  GO


 --alter table [Teams] 
 -- drop column [GeoFenceId]
 -- Go