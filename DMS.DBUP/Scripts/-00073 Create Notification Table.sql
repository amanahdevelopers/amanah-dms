BEGIN TRANSACTION
GO


CREATE TABLE [dbo].[Notifications] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Body]         NVARCHAR (MAX) NOT NULL,
    [Date]         DATETIME2 (7)  NOT NULL,
    [ToUserId]     NVARCHAR (450) NOT NULL,
    [FromUserId]   NVARCHAR (450) NOT NULL,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Notifications_AspNetUsers] FOREIGN KEY ([FromUserId]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_Notifications_AspNetUsers1] FOREIGN KEY ([ToUserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);
GO




COMMIT