BEGIN TRANSACTION
GO
--ALTER TABLE TaskRoute
--ALTER  Latitude float;
--Go

--ALTER TABLE TaskRoute
--ALTER  Longitude float;
--Go
ALTER TABLE Tasks
drop COLUMN   Latitude ;
Go

ALTER TABLE Tasks
drop COLUMN   Longitude ;
Go
ALTER TABLE Tasks
add  Latitude float;
Go

ALTER TABLE Tasks
add  Longitude float;
Go
ALTER TABLE TaskHistory
drop COLUMN   Latitude ;
Go

ALTER TABLE TaskHistory
drop COLUMN   Longitude ;
Go
ALTER TABLE TaskHistory
add  Latitude float;
Go

ALTER TABLE TaskHistory
add  Longitude float;
Go
ALTER TABLE GeoFenceLocation
drop COLUMN   Latitude ;
Go

ALTER TABLE GeoFenceLocation
drop COLUMN   Longitude ;
Go
ALTER TABLE GeoFenceLocation
add  Latitude float;
Go

ALTER TABLE GeoFenceLocation
add  Longitude float;
Go
ALTER TABLE DriverCurrentLocation
drop COLUMN   Latitude ;
Go

ALTER TABLE DriverCurrentLocation
drop COLUMN   Longitude ;
Go
ALTER TABLE DriverCurrentLocation
add  Latitude float;
Go

ALTER TABLE DriverCurrentLocation
add  Longitude float;
Go

ALTER TABLE Customers
drop COLUMN   Latitude ;
Go

ALTER TABLE Customers
drop COLUMN   Longitude ;
Go
ALTER TABLE Customers
add  Latitude float;
Go

ALTER TABLE Customers
add  Longitude float;
Go

COMMIT