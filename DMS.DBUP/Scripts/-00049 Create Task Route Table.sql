CREATE TABLE [dbo].[TaskRoute] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [DriverId]     INT NOT NULL,
    [TaskId]       INT NOT NULL,
    [TaskStatusId] INT  NULL,
	[Latitude]     float Not NULL,
	[Longitude]    float Not NULL,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_TaskRoute] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

ALTER TABLE dbo.TaskRoute ADD CONSTRAINT
	FK_Driver_TaskRoute FOREIGN KEY
	(DriverId) REFERENCES dbo.Driver (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.TaskRoute ADD CONSTRAINT
	FK_Task_TaskRoute FOREIGN KEY
	(TaskId) REFERENCES dbo.Tasks (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.TaskRoute ADD CONSTRAINT
	FK_TaskStatus_TaskRoute FOREIGN KEY
	(TaskStatusId) REFERENCES dbo.TaskStatus (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO



