SET IDENTITY_INSERT [dbo].[AgentStatus] ON 
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id]) VALUES (1, N'Offline', N'0', N'0', N'0', 0, CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'6929ddcc-7841-4c89-844a-2191d565ebe6')
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id]) VALUES (2, N'Available', N'0', N'0', N'0', 0, CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'6929ddcc-7841-4c89-844a-2191d565ebe6')
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id]) VALUES (3, N'Unavailable', N'0', N'0', N'0', 0, CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'6929ddcc-7841-4c89-844a-2191d565ebe6')
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id]) VALUES (4, N'Busy', N'0', N'0', N'0', 0, CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'6929ddcc-7841-4c89-844a-2191d565ebe6')
GO
SET IDENTITY_INSERT [dbo].[AgentStatus] OFF
GO