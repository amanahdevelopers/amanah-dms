ALTER TABLE [dbo].[UserDevice] DROP CONSTRAINT [FK_UserDevice_AspNetUsers_UserId]
GO

/****** Object:  Table [dbo].[UserDevice]    Script Date: 3/4/2020 12:26:55 PM ******/
DROP TABLE [dbo].[UserDevice]
GO

/****** Object:  Table [dbo].[UserDevice]    Script Date: 3/4/2020 12:26:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserDevice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FCMDeviceId] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NULL,
	[DeviceType] [nvarchar](450) NULL,
	[Version] [nvarchar](450) NULL,
	
 CONSTRAINT [PK_UserDevice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserDevice]  WITH CHECK ADD  CONSTRAINT [FK_UserDevice_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[UserDevice] CHECK CONSTRAINT [FK_UserDevice_AspNetUsers_UserId]
GO


