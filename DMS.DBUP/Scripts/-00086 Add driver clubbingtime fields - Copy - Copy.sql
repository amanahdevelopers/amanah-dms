BEGIN TRANSACTION
GO
ALTER TABLE dbo.Driver ADD
	IsInClubbingTime bit NOT NULL CONSTRAINT DF_Driver_IsInClubbingTime DEFAULT 0,
	ClubbingTimeExpiration datetime2(7) NULL
GO
COMMIT
