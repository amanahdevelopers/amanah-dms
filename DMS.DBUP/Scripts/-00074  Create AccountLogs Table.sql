CREATE TABLE [dbo].[AccountLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[ActivityType] [nvarchar](2000) NULL,
	[Description] [nvarchar](max) NULL,
	[TableName] [nvarchar](2000) NULL,
	[Record_Id] [int] NULL ,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.AccountLogs ADD CONSTRAINT
	PK_AccountLogs PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

