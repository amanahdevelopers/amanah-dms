BEGIN TRANSACTION
GO

ALTER TABLE MainTask
ADD IsCompleted bit NULL;
GO

ALTER TABLE MainTask
ADD IsDelayed bit NULL;
GO

ALTER TABLE Tasks
ADD StartDate [datetime2](7) NULL;
GO

ALTER TABLE Tasks
ADD DelayTime float NULL;
GO

COMMIT
