CREATE TABLE [dbo].[DriverCurrentLocation](
	[DriverId] [int] NOT NULL,
	[Latitude] [decimal](12, 9) NOT NULL,
	[Longitude] [decimal](12, 9) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
 CONSTRAINT [PK_DriverCurrentLocation] PRIMARY KEY CLUSTERED 
(
	[DriverId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[DriverCurrentLocation]  WITH CHECK ADD  CONSTRAINT [FK_DriverCurrentLocation_DriverCurrentLocation] FOREIGN KEY([DriverId])
REFERENCES [dbo].[Driver] ([Id])
GO

ALTER TABLE [dbo].[DriverCurrentLocation] CHECK CONSTRAINT [FK_DriverCurrentLocation_DriverCurrentLocation]
GO
