Begin Transaction 
Delete from [dbo].AspNetUsers where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


Delete from  [dbo].AccountLogs where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers]
)


Delete from  [dbo].TaskRoute where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers]
)

Delete from  [dbo].TaskHistory where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers]
)

Delete from  [dbo].TaskGallary where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 
Delete from  [dbo].Settings where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 Delete from  [dbo].TaskDriverRequests where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 Delete from  [dbo].Notifications where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 Delete from  [dbo].Notifications where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 Delete from  [dbo].MapRequestLogs where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


  Delete from  [dbo].ManagerDispatching where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 
   Delete from  [dbo].DriverRate where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


Delete from  [dbo].DriverloginTracking where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 
 
Delete from  [dbo].DriverLoginRequests where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 
 Delete from  [dbo].DriverCurrentLocation where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 Delete  from GeoFenceDriver
 where GeoFenceDriver.DriverId in (
 select distinct [Id] from [dbo].Driver where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )

  Delete  from GeoFenceDriver 
 where GeoFenceDriver.GeoFenceId in (
 select distinct [Id] from [dbo].GeoFence where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )
 

 
  Delete   from GeoFenceLocation
 where GeoFenceId in (
 select distinct [Id] from [dbo].GeoFence where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )


 Delete  from DriverDeliveryGeoFence
 where GeoFenceId in (
 select distinct [Id] from [dbo].GeoFence where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )

 
 
 
 Delete  from DriverDeliveryGeoFence
 where DriverId in (
 select distinct [Id] from [dbo].Driver where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )

 Delete  from DriverPickUpGeoFence
 where GeoFenceId in (
 select distinct [Id] from [dbo].GeoFence where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )

 
 
 
 Delete  from DriverPickUpGeoFence
 where DriverId in (
 select distinct [Id] from [dbo].Driver where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )



 
 Delete   from GeoFenceLocation
 where GeoFenceId in (
 select distinct [Id] from [dbo].GeoFence where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )




 Delete  from DriverloginTracking
 where DriverId in (
 select distinct [Id] from [dbo].Driver where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )


 
 Delete  from DriverLoginRequests
 where DriverId in (
 select distinct [Id] from [dbo].Driver where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )

 
 Delete  from TeamManager
 where TeamId in (
 select distinct [Id] from [dbo].Teams where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )

 
  
 Delete  from TeamManager
 where ManagerId in (
 select distinct [Id] from [dbo].Manager where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )


 Delete from  [dbo].Manager where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 Delete  from DriverCurrentLocation
 where DriverId in (
 select distinct [Id] from [dbo].Driver where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )


 Delete   from TaskHistory
 where TaskId in (
 select distinct [Id] from [dbo].Tasks where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )


 
 Delete   from TaskRoute
 where TaskId in (
 select distinct [Id] from [dbo].Tasks where [Tenant_Id] not in (
    SELECT distinct [Id]      
        FROM [dbo].[AspNetUsers])
 )


 
Delete from  [dbo].[Tasks] where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers]
)




Delete from  [dbo].Driver where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 Delete from TaskHistory
 where TaskId not in (
 select distinct [Id] from [dbo].Tasks where CustomerId not in (
        SELECT distinct  Id from [dbo].Customers where [Tenant_Id] not in (
			SELECT distinct [Id]      
			 FROM [dbo].[AspNetUsers])
	 ))

Delete  from TaskRoute
 where TaskId not in (
 select distinct [Id] from [dbo].Tasks where CustomerId not in (
        SELECT distinct  Id from [dbo].Customers where [Tenant_Id] not in (
			SELECT distinct [Id]      
			 FROM [dbo].[AspNetUsers])
	 ))


Delete  from TaskGallary
 where TaskId not in (
 select distinct [Id] from [dbo].Tasks where CustomerId not in (
        SELECT distinct  Id from [dbo].Customers where [Tenant_Id] not in (
			SELECT distinct [Id]      
			 FROM [dbo].[AspNetUsers])
	 ))



Delete from [dbo].Tasks where CustomerId  in (
        SELECT distinct  Id from [dbo].Customers where [Tenant_Id] not in (
			SELECT distinct [Id]      
			 FROM [dbo].[AspNetUsers])
	 )
 
Delete from [dbo].Customers where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


Delete from [dbo].Tasks where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])

 
Delete from [dbo].MainTask where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


Delete from  [dbo].Teams where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 

Delete from TaskHistory where TaskId in (
    SELECT distinct [Id] from  [dbo].Tasks where BranchId = 71
)

Delete from TaskGallary where TaskId in (
    SELECT distinct [Id] from  [dbo].Tasks where BranchId = 71
)

Delete from TaskRoute where TaskId in (
    SELECT distinct [Id] from  [dbo].Tasks where BranchId = 71
)

Delete  from  [dbo].Tasks where BranchId = 71



delete from Branch where Id = 71 

delete from Restaurant where Id = 37 


Delete from [dbo].Tasks where 
BranchId  in (
SELECT distinct [Id] from [dbo].Branch where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])
 )


Delete from [dbo].Branch where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])
 


Delete from [dbo].Branch where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])
 

Delete from [dbo].Restaurant where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


 Delete from  [dbo].DriverTaskNotification where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])


  Delete from  [dbo].GeoFence where [Tenant_Id] not in (
SELECT distinct [Id]      
 FROM [dbo].[AspNetUsers])



 DElete from [dbo].DriverRate where [CustomerId] not in (
SELECT distinct [Id]      
 FROM [dbo].Customers)


 
 Delete  from [dbo].DriverRate where TasksId not in (
SELECT distinct [Id]      
 FROM [dbo].Tasks)
Commit 


 
 
 







