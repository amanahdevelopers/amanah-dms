BEGIN TRANSACTION
GO


CREATE TABLE [dbo].[Restaurant] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (100)  NOT NULL,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Restaurant] PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO


CREATE TABLE [dbo].[Branch] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (100)  NOT NULL,
    [Phone]        VARCHAR (50)   NOT NULL,
    [RestaurantId] INT            NOT NULL,
    [GeoFenceId]   INT            NOT NULL,
    [Address]      VARCHAR (200)  NOT NULL,
    Latitude float(53) NOT NULL,
    Longitude float(53) NOT NULL,
    [ManagerId]    INT            NOT NULL,
    [IsActive]    BIT            NOT NULL default 1,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Branch_GeoFence] FOREIGN KEY ([GeoFenceId]) REFERENCES [dbo].[GeoFence] ([Id]),
    CONSTRAINT [FK_Branch_Restaurant] FOREIGN KEY ([RestaurantId]) REFERENCES [dbo].[Restaurant] ([Id]),
    CONSTRAINT [FK_Branch_Manager] FOREIGN KEY ([ManagerId]) REFERENCES [dbo].[Manager] ([Id])
);

GO

COMMIT
