INSERT [dbo].[MainTaskType] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Pickup & Delivery',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskType] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Pickup',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskType] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Delivery',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO 

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Unassigned',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Assigned',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Accepted',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO




INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Started',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'In progress',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Successful',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Failed',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Declined',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[TaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Cancelled',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO