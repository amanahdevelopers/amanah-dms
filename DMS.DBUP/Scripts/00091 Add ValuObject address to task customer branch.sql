BEGIN TRANSACTION
GO

Alter table [dbo].[Branch] ADD
	Location_Area Nvarchar(100)  NULL,
	Location_Block Nvarchar(100)  NULL,
	Location_Street Nvarchar(100)  NULL,
	Location_Building Nvarchar(100)  NULL,
	Location_Floor Nvarchar(100)  NULL,
	Location_Flat Nvarchar(100)  NULL
Go

Alter table [dbo].[Customers] ADD
	Location_Area Nvarchar(100)  NULL,
	Location_Block Nvarchar(100)  NULL,
	Location_Street Nvarchar(100)  NULL,
	Location_Building Nvarchar(100)  NULL,
	Location_Floor Nvarchar(100)  NULL,
	Location_Flat Nvarchar(100)  NULL
Go

Alter table [dbo].[Tasks] ADD
	Location_Area Nvarchar(100)  NULL,
	Location_Block Nvarchar(100)  NULL,
	Location_Street Nvarchar(100)  NULL,
	Location_Building Nvarchar(100)  NULL,
	Location_Floor Nvarchar(100)  NULL,
	Location_Flat Nvarchar(100)  NULL
Go

COMMIT