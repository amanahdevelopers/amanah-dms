BEGIN TRANSACTION
GO
   Alter table [dbo].[Tasks]
          Add IsTaskReached bit Null;

   Alter table [dbo].[Tasks]
          Add ReachedTime dateTime Null;

GO
COMMIT
