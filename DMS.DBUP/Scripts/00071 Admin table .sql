CREATE TABLE [dbo].[Admins](
	[CompanyName] [nvarchar](max) NULL,
	[CompanyAddress] [nvarchar](max) NULL,
	[DisplayImage] [int] NULL,
	[DashBoardLanguage] [nvarchar](100) NULL,
	[TrackingPanelLanguage] [nvarchar](100) NULL,
	[DeactivationReason] [nvarchar](max) NULL,
	[Id] [nvarchar](450) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](50) NULL,
 CONSTRAINT [PK_Admins] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Admins]  WITH CHECK ADD  CONSTRAINT [FK_Admins_Admins] FOREIGN KEY([Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

ALTER TABLE [dbo].[Admins] CHECK CONSTRAINT [FK_Admins_Admins]
GO