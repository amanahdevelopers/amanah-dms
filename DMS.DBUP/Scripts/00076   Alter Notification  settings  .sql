BEGIN TRANSACTION
GO
ALTER TABLE dbo.Notifications ADD
	Title nvarchar(50) NULL,
	IsSeen bit NULL
GO

COMMIT