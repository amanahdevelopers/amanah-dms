BEGIN TRANSACTION
GO
ALTER TABLE dbo.TaskDriverRequests ADD
	RetriesCount int NOT NULL CONSTRAINT DF_TaskDriverRequests_RetriesCount DEFAULT 1
GO
ALTER TABLE dbo.TaskDriverRequests SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
