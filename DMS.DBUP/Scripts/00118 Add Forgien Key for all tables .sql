Begin Transaction

ALTER TABLE dbo.AccountLogs ADD CONSTRAINT
	FK_AccountLogs_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


ALTER TABLE dbo.Admins ADD CONSTRAINT
	FK_Admins_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO




ALTER TABLE dbo.Branch ADD CONSTRAINT
	FK_Branchs_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO




ALTER TABLE dbo.Customers ADD CONSTRAINT
	FK_Customers_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO




ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.DriverCurrentLocation ADD CONSTRAINT
	FK_DriverCurrentLocation_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


ALTER TABLE dbo.DriverTaskNotification ADD CONSTRAINT
	FK_DriverTaskNotification_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.GeoFence ADD CONSTRAINT
	FK_GeoFence_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


ALTER TABLE dbo.MainTask ADD CONSTRAINT
	FK_MainTask_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.Manager ADD CONSTRAINT
	FK_Manager_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


ALTER TABLE dbo.ManagerDispatching ADD CONSTRAINT
	FK_ManagerDispatching_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO



ALTER TABLE dbo.Restaurant ADD CONSTRAINT
	FK_Restaurant_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.TaskGallary ADD CONSTRAINT
	FK_TaskGallary_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.TaskHistory ADD CONSTRAINT
	FK_TaskHistory_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


ALTER TABLE dbo.TaskRoute ADD CONSTRAINT
	FK_TaskRoute_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.Tasks ADD CONSTRAINT
	FK_Tasks_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO


ALTER TABLE dbo.Teams ADD CONSTRAINT
	FK_Teams_AspNetUsers FOREIGN KEY
	(
	Tenant_Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

Commit