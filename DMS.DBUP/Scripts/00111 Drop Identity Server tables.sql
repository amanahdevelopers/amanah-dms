/* Identity server tables */ 
DROP TABLE ApiClaims
DROP TABLE  ClientClaims 
DROP TABLE ClientCorsOrigins
DROP TABLE ClientGrantTypes
DROP TABLE ClientIdPRestrictions
DROP TABLE ClientPostLogoutRedirectUris
DROP TABLE ClientProperties
DROP TABLE ClientRedirectUris
DROP TABLE ClientScopes
DROP TABLE ClientSecrets
DROP TABLE Clients
DROP TABLE PersistedGrants
DROP TABLE IdentityClaims
DROP TABLE IdentityResources
DROP TABLE ApiScopeClaims
DROP TABLE ApiScopes
DROP TABLE ApiSecrets
DROP TABLE ApiResources

/**** deprecated tables ***/
DROP TABLE WebsiteSettings
