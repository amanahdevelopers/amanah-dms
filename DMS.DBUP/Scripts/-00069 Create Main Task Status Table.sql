
CREATE TABLE dbo.MainTaskStatus
	(
	Id int IDENTITY (1, 1) NOT NULL,
	Name nvarchar(100) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.MainTaskStatus ADD CONSTRAINT
	PK_MainTaskStatus PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.MainTaskStatus SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE [dbo].[MainTask]
ADD [MainTaskStatusId]  int NULL;
GO

ALTER TABLE [dbo].[MainTask] ADD CONSTRAINT
	FK_MainTask_MainTaskStatus FOREIGN KEY
	(MainTaskStatusId) REFERENCES dbo.MainTaskStatus (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
