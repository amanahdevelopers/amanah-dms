BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[DriverRate] (
    [Id]           INT             IDENTITY (1, 1) NOT NULL,
    [CustomerId]   INT             NOT NULL,
    [DriverId]     INT             NOT NULL,
    [TasksId]       INT             NOT NULL,
    [Rate]         FLOAT (53)      NOT NULL,
    [Note]         nvarchar(500) NULL,
    [CreatedBy_Id] NVARCHAR (MAX)  NULL,
    [UpdatedBy_Id] NVARCHAR (MAX)  NULL,
    [DeletedBy_Id] NVARCHAR (MAX)  NULL,
    [Tenant_Id]    NVARCHAR (MAX)  NULL,
    [IsDeleted]    BIT             NOT NULL,
    [CreatedDate]  DATETIME2 (7)   NULL,
    [UpdatedDate]  DATETIME2 (7)   NULL,
    [DeletedDate]  DATETIME2 (7)   NULL,

      CONSTRAINT [PK_DriverRate] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO



COMMIT
