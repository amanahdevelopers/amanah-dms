BEGIN TRANSACTION
GO


CREATE TABLE [dbo].[ManagerDispatching] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [DesignationName]  VARCHAR (100)  NOT NULL,
    [ManagerId]    INT   NOT NULL,
    [Zones]        NTEXT NULL,
    [Restaurants]  NTEXT NULL,
    [Branches]  NTEXT NULL,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_ManagerDispatching] PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO

ALTER TABLE dbo.ManagerDispatching ADD CONSTRAINT
	FK_ManagerDispatching_Manager FOREIGN KEY
	(ManagerId) REFERENCES dbo.Manager (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO


COMMIT
