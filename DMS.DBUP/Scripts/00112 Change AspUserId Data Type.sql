/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT FK_Driver_TransportType
GO
ALTER TABLE dbo.TransportType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT FK_Driver_Team
GO
ALTER TABLE dbo.Teams SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT FK_Driver_AgentType
GO
ALTER TABLE dbo.AgentType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AspNetUserRoles
	DROP CONSTRAINT FK_AspNetUserRoles_AspNetRoles_RoleId
GO
ALTER TABLE dbo.AspNetRoles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AspNetUsers
	DROP CONSTRAINT DF__AspNetUse__IsLoc__4BAC3F29
GO
ALTER TABLE dbo.AspNetUsers
	DROP CONSTRAINT DF_AspNetUsers_Country
GO
CREATE TABLE dbo.Tmp_AspNetUsers
	(
	Id varchar(36) NOT NULL,
	AccessFailedCount int NOT NULL,
	ConcurrencyStamp nvarchar(MAX) NULL,
	CreatedDate datetime2(7) NOT NULL,
	DeletedDate datetime2(7) NOT NULL,
	Email nvarchar(256) NULL,
	EmailConfirmed bit NOT NULL,
	CreatedBy_Id nvarchar(MAX) NULL,
	DeletedBy_Id nvarchar(MAX) NULL,
	UpdatedBy_Id nvarchar(MAX) NULL,
	FirstName nvarchar(MAX) NULL,
	IsAvailable bit NOT NULL,
	IsDeleted bit NOT NULL,
	LastName nvarchar(MAX) NULL,
	LockoutEnabled bit NOT NULL,
	LockoutEnd datetimeoffset(7) NULL,
	MiddleName nvarchar(MAX) NULL,
	NormalizedEmail nvarchar(256) NULL,
	NormalizedUserName nvarchar(256) NULL,
	PasswordHash nvarchar(MAX) NULL,
	PhoneNumber nvarchar(MAX) NULL,
	PhoneNumberConfirmed bit NOT NULL,
	SecurityStamp nvarchar(MAX) NULL,
	TwoFactorEnabled bit NOT NULL,
	UpdatedDate datetime2(7) NOT NULL,
	UserName nvarchar(256) NULL,
	IsLocked bit NOT NULL,
	Tenant_Id nvarchar(MAX) NULL,
	CountryId int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AspNetUsers SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_AspNetUsers ADD CONSTRAINT
	DF__AspNetUse__IsLoc__4BAC3F29 DEFAULT ((0)) FOR IsLocked
GO
ALTER TABLE dbo.Tmp_AspNetUsers ADD CONSTRAINT
	DF_AspNetUsers_Country DEFAULT ((1)) FOR CountryId
GO
IF EXISTS(SELECT * FROM dbo.AspNetUsers)
	 EXEC('INSERT INTO dbo.Tmp_AspNetUsers (Id, AccessFailedCount, ConcurrencyStamp, CreatedDate, DeletedDate, Email, EmailConfirmed, CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, FirstName, IsAvailable, IsDeleted, LastName, LockoutEnabled, LockoutEnd, MiddleName, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UpdatedDate, UserName, IsLocked, Tenant_Id, CountryId)
		SELECT CONVERT(varchar(36), Id), AccessFailedCount, ConcurrencyStamp, CreatedDate, DeletedDate, Email, EmailConfirmed, CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, FirstName, IsAvailable, IsDeleted, LastName, LockoutEnabled, LockoutEnd, MiddleName, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UpdatedDate, UserName, IsLocked, Tenant_Id, CountryId FROM dbo.AspNetUsers WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.Manager
	DROP CONSTRAINT FK_Manager_User
GO
ALTER TABLE dbo.AspNetUserClaims
	DROP CONSTRAINT FK_AspNetUserClaims_AspNetUsers_UserId
GO
ALTER TABLE dbo.AspNetUserLogins
	DROP CONSTRAINT FK_AspNetUserLogins_AspNetUsers_UserId
GO
ALTER TABLE dbo.AspNetUserRoles
	DROP CONSTRAINT FK_AspNetUserRoles_AspNetUsers_UserId
GO
ALTER TABLE dbo.AspNetUserTokens
	DROP CONSTRAINT FK_AspNetUserTokens_AspNetUsers_UserId
GO
ALTER TABLE dbo.Notifications
	DROP CONSTRAINT FK_Notifications_AspNetUsers
GO
ALTER TABLE dbo.Notifications
	DROP CONSTRAINT FK_Notifications_AspNetUsers1
GO
ALTER TABLE dbo.Admins
	DROP CONSTRAINT FK_Admins_Admins
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT FK_Driver_User
GO
ALTER TABLE dbo.UserDevice
	DROP CONSTRAINT FK_UserDevice_AspNetUsers_UserId
GO
DROP TABLE dbo.AspNetUsers
GO
EXECUTE sp_rename N'dbo.Tmp_AspNetUsers', N'AspNetUsers', 'OBJECT' 
GO
ALTER TABLE dbo.AspNetUsers ADD CONSTRAINT
	PK_AspNetUsers PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX EmailIndex ON dbo.AspNetUsers
	(
	NormalizedEmail
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX UserNameIndex ON dbo.AspNetUsers
	(
	NormalizedUserName
	) WHERE ([NormalizedUserName] IS NOT NULL)
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_UserDevice
	(
	Id int NOT NULL IDENTITY (1, 1),
	FCMDeviceId nvarchar(MAX) NULL,
	UserId varchar(36) NULL,
	DeviceType nvarchar(450) NULL,
	Version nvarchar(450) NULL,
	IsLoggedIn bit NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_UserDevice SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_UserDevice ON
GO
IF EXISTS(SELECT * FROM dbo.UserDevice)
	 EXEC('INSERT INTO dbo.Tmp_UserDevice (Id, FCMDeviceId, UserId, DeviceType, Version, IsLoggedIn)
		SELECT Id, FCMDeviceId, CONVERT(varchar(36), UserId), DeviceType, Version, IsLoggedIn FROM dbo.UserDevice WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_UserDevice OFF
GO
DROP TABLE dbo.UserDevice
GO
EXECUTE sp_rename N'dbo.Tmp_UserDevice', N'UserDevice', 'OBJECT' 
GO
ALTER TABLE dbo.UserDevice ADD CONSTRAINT
	PK_UserDevice PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.UserDevice ADD CONSTRAINT
	FK_UserDevice_AspNetUsers_UserId FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT DF_Driver_AgentStatusUpdateDate
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT DF_Driver_MaxOrdersWeightsCapacity
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT DF_Driver_MinOrdersNoWait
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT DF_Driver_IsInClubbingTime
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT DF__Driver__AllPicku__719CDDE7
GO
ALTER TABLE dbo.Driver
	DROP CONSTRAINT DF__Driver__AllDeliv__72910220
GO
CREATE TABLE dbo.Tmp_Driver
	(
	Id int NOT NULL IDENTITY (1, 1),
	ImageUrl nvarchar(MAX) NULL,
	Tags nvarchar(MAX) NULL,
	TransportDescription nvarchar(MAX) NULL,
	LicensePlate nvarchar(MAX) NULL,
	Color nvarchar(MAX) NULL,
	Role nvarchar(MAX) NULL,
	TeamId int NOT NULL,
	AgentTypeId int NULL,
	TransportTypeId int NULL,
	CountryId int NULL,
	UserId varchar(36) NULL,
	CreatedBy_Id nvarchar(MAX) NULL,
	DeletedBy_Id nvarchar(MAX) NULL,
	UpdatedBy_Id nvarchar(MAX) NULL,
	IsDeleted bit NOT NULL,
	UpdatedDate datetime2(7) NOT NULL,
	CreatedDate datetime2(7) NOT NULL,
	DeletedDate datetime2(7) NOT NULL,
	Tenant_Id nvarchar(450) NULL,
	AgentStatusId int NULL,
	DeviceType nvarchar(450) NULL,
	Version nvarchar(450) NULL,
	Reason nvarchar(MAX) NULL,
	AgentStatusUpdateDate datetime2(7) NOT NULL,
	MaxOrdersWeightsCapacity int NOT NULL,
	MinOrdersNoWait int NOT NULL,
	IsInClubbingTime bit NOT NULL,
	ClubbingTimeExpiration datetime2(7) NULL,
	ReachedTime datetime NULL,
	DriverAvgRate float(53) NULL,
	TokenTimeExpiration datetime2(7) NULL,
	AllPickupGeoFences bit NOT NULL,
	AllDeliveryGeoFences bit NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Driver SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Driver ADD CONSTRAINT
	DF_Driver_AgentStatusUpdateDate DEFAULT (getdate()) FOR AgentStatusUpdateDate
GO
ALTER TABLE dbo.Tmp_Driver ADD CONSTRAINT
	DF_Driver_MaxOrdersWeightsCapacity DEFAULT ((1)) FOR MaxOrdersWeightsCapacity
GO
ALTER TABLE dbo.Tmp_Driver ADD CONSTRAINT
	DF_Driver_MinOrdersNoWait DEFAULT ((1)) FOR MinOrdersNoWait
GO
ALTER TABLE dbo.Tmp_Driver ADD CONSTRAINT
	DF_Driver_IsInClubbingTime DEFAULT ((0)) FOR IsInClubbingTime
GO
ALTER TABLE dbo.Tmp_Driver ADD CONSTRAINT
	DF__Driver__AllPicku__719CDDE7 DEFAULT ((0)) FOR AllPickupGeoFences
GO
ALTER TABLE dbo.Tmp_Driver ADD CONSTRAINT
	DF__Driver__AllDeliv__72910220 DEFAULT ((0)) FOR AllDeliveryGeoFences
GO
SET IDENTITY_INSERT dbo.Tmp_Driver ON
GO
IF EXISTS(SELECT * FROM dbo.Driver)
	 EXEC('INSERT INTO dbo.Tmp_Driver (Id, ImageUrl, Tags, TransportDescription, LicensePlate, Color, Role, TeamId, AgentTypeId, TransportTypeId, CountryId, UserId, CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id, AgentStatusId, DeviceType, Version, Reason, AgentStatusUpdateDate, MaxOrdersWeightsCapacity, MinOrdersNoWait, IsInClubbingTime, ClubbingTimeExpiration, ReachedTime, DriverAvgRate, TokenTimeExpiration, AllPickupGeoFences, AllDeliveryGeoFences)
		SELECT Id, ImageUrl, Tags, TransportDescription, LicensePlate, Color, Role, TeamId, AgentTypeId, TransportTypeId, CountryId, CONVERT(varchar(36), UserId), CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id, AgentStatusId, DeviceType, Version, Reason, AgentStatusUpdateDate, MaxOrdersWeightsCapacity, MinOrdersNoWait, IsInClubbingTime, ClubbingTimeExpiration, ReachedTime, DriverAvgRate, TokenTimeExpiration, AllPickupGeoFences, AllDeliveryGeoFences FROM dbo.Driver WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Driver OFF
GO
ALTER TABLE dbo.Tasks
	DROP CONSTRAINT FK_Tasks_Driver
GO
ALTER TABLE dbo.GeoFenceDriver
	DROP CONSTRAINT FK_GeoFenceDriver_Driver
GO
ALTER TABLE dbo.DriverTaskNotification
	DROP CONSTRAINT FK_Driver_DriverTaskNotification
GO
ALTER TABLE dbo.DriverCurrentLocation
	DROP CONSTRAINT FK_DriverCurrentLocation_DriverCurrentLocation
GO
ALTER TABLE dbo.TaskRoute
	DROP CONSTRAINT FK_Driver_TaskRoute
GO
ALTER TABLE dbo.DriverPickUpGeoFence
	DROP CONSTRAINT FK_DriverPickUpGeoFence_Driver
GO
ALTER TABLE dbo.DriverDeliveryGeoFence
	DROP CONSTRAINT FK_DriverDeliveryGeoFence_Driver
GO
ALTER TABLE dbo.DriverLoginRequests
	DROP CONSTRAINT FK_DriverLoginRequests_Driver
GO
ALTER TABLE dbo.DriverloginTracking
	DROP CONSTRAINT FK_DriverloginTracking_Driver
GO
DROP TABLE dbo.Driver
GO
EXECUTE sp_rename N'dbo.Tmp_Driver', N'Driver', 'OBJECT' 
GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	PK_Driver PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_AgentType FOREIGN KEY
	(
	AgentTypeId
	) REFERENCES dbo.AgentType
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_Team FOREIGN KEY
	(
	TeamId
	) REFERENCES dbo.Teams
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_TransportType FOREIGN KEY
	(
	TransportTypeId
	) REFERENCES dbo.TransportType
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_User FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverloginTracking ADD CONSTRAINT
	FK_DriverloginTracking_Driver FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DriverloginTracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverLoginRequests ADD CONSTRAINT
	FK_DriverLoginRequests_Driver FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DriverLoginRequests SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverDeliveryGeoFence ADD CONSTRAINT
	FK_DriverDeliveryGeoFence_Driver FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DriverDeliveryGeoFence SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverPickUpGeoFence ADD CONSTRAINT
	FK_DriverPickUpGeoFence_Driver FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DriverPickUpGeoFence SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TaskRoute ADD CONSTRAINT
	FK_Driver_TaskRoute FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TaskRoute SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverCurrentLocation ADD CONSTRAINT
	FK_DriverCurrentLocation_DriverCurrentLocation FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DriverCurrentLocation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverTaskNotification ADD CONSTRAINT
	FK_Driver_DriverTaskNotification FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.DriverTaskNotification SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.GeoFenceDriver ADD CONSTRAINT
	FK_GeoFenceDriver_Driver FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.GeoFenceDriver SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tasks WITH NOCHECK ADD CONSTRAINT
	FK_Tasks_Driver FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tasks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Admins
	(
	CompanyName nvarchar(MAX) NULL,
	CompanyAddress nvarchar(MAX) NULL,
	DisplayImage int NULL,
	DashBoardLanguage nvarchar(100) NULL,
	TrackingPanelLanguage nvarchar(100) NULL,
	DeactivationReason nvarchar(MAX) NULL,
	Id varchar(36) NOT NULL,
	CreatedBy_Id nvarchar(MAX) NULL,
	DeletedBy_Id nvarchar(MAX) NULL,
	UpdatedBy_Id nvarchar(MAX) NULL,
	IsDeleted bit NULL,
	UpdatedDate datetime2(7) NOT NULL,
	CreatedDate datetime2(7) NOT NULL,
	DeletedDate datetime2(7) NOT NULL,
	Tenant_Id nvarchar(50) NULL,
	ResidentCountryId int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Admins SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Admins)
	 EXEC('INSERT INTO dbo.Tmp_Admins (CompanyName, CompanyAddress, DisplayImage, DashBoardLanguage, TrackingPanelLanguage, DeactivationReason, Id, CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id, ResidentCountryId)
		SELECT CompanyName, CompanyAddress, DisplayImage, DashBoardLanguage, TrackingPanelLanguage, DeactivationReason, CONVERT(varchar(36), Id), CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id, ResidentCountryId FROM dbo.Admins WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Admins
GO
EXECUTE sp_rename N'dbo.Tmp_Admins', N'Admins', 'OBJECT' 
GO
ALTER TABLE dbo.Admins ADD CONSTRAINT
	PK_Admins PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Admins ADD CONSTRAINT
	FK_Admins_Admins FOREIGN KEY
	(
	Id
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Notifications
	(
	Id int NOT NULL IDENTITY (1, 1),
	Body nvarchar(MAX) NOT NULL,
	Date datetime2(7) NOT NULL,
	ToUserId varchar(36) NOT NULL,
	FromUserId varchar(36) NOT NULL,
	CreatedBy_Id nvarchar(MAX) NULL,
	UpdatedBy_Id nvarchar(MAX) NULL,
	DeletedBy_Id nvarchar(MAX) NULL,
	Tenant_Id nvarchar(MAX) NULL,
	IsDeleted bit NOT NULL,
	CreatedDate datetime2(7) NULL,
	UpdatedDate datetime2(7) NULL,
	DeletedDate datetime2(7) NULL,
	Title nvarchar(50) NULL,
	IsSeen bit NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Notifications SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Notifications ON
GO
IF EXISTS(SELECT * FROM dbo.Notifications)
	 EXEC('INSERT INTO dbo.Tmp_Notifications (Id, Body, Date, ToUserId, FromUserId, CreatedBy_Id, UpdatedBy_Id, DeletedBy_Id, Tenant_Id, IsDeleted, CreatedDate, UpdatedDate, DeletedDate, Title, IsSeen)
		SELECT Id, Body, Date, CONVERT(varchar(36), ToUserId), CONVERT(varchar(36), FromUserId), CreatedBy_Id, UpdatedBy_Id, DeletedBy_Id, Tenant_Id, IsDeleted, CreatedDate, UpdatedDate, DeletedDate, Title, IsSeen FROM dbo.Notifications WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Notifications OFF
GO
DROP TABLE dbo.Notifications
GO
EXECUTE sp_rename N'dbo.Tmp_Notifications', N'Notifications', 'OBJECT' 
GO
ALTER TABLE dbo.Notifications ADD CONSTRAINT
	PK_Notifications PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Notifications ADD CONSTRAINT
	FK_Notifications_AspNetUsers FOREIGN KEY
	(
	FromUserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Notifications ADD CONSTRAINT
	FK_Notifications_AspNetUsers1 FOREIGN KEY
	(
	ToUserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AspNetUserTokens
	(
	UserId varchar(36) NOT NULL,
	LoginProvider nvarchar(450) NOT NULL,
	Name nvarchar(450) NOT NULL,
	Value nvarchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AspNetUserTokens SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.AspNetUserTokens)
	 EXEC('INSERT INTO dbo.Tmp_AspNetUserTokens (UserId, LoginProvider, Name, Value)
		SELECT CONVERT(varchar(36), UserId), LoginProvider, Name, Value FROM dbo.AspNetUserTokens WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.AspNetUserTokens
GO
EXECUTE sp_rename N'dbo.Tmp_AspNetUserTokens', N'AspNetUserTokens', 'OBJECT' 
GO
ALTER TABLE dbo.AspNetUserTokens ADD CONSTRAINT
	FK_AspNetUserTokens_AspNetUsers_UserId FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AspNetUserRoles
	(
	UserId varchar(36) NOT NULL,
	RoleId nvarchar(450) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AspNetUserRoles SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.AspNetUserRoles)
	 EXEC('INSERT INTO dbo.Tmp_AspNetUserRoles (UserId, RoleId)
		SELECT CONVERT(varchar(36), UserId), RoleId FROM dbo.AspNetUserRoles WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.AspNetUserRoles
GO
EXECUTE sp_rename N'dbo.Tmp_AspNetUserRoles', N'AspNetUserRoles', 'OBJECT' 
GO
CREATE NONCLUSTERED INDEX IX_AspNetUserRoles_RoleId ON dbo.AspNetUserRoles
	(
	RoleId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.AspNetUserRoles ADD CONSTRAINT
	FK_AspNetUserRoles_AspNetRoles_RoleId FOREIGN KEY
	(
	RoleId
	) REFERENCES dbo.AspNetRoles
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.AspNetUserRoles ADD CONSTRAINT
	FK_AspNetUserRoles_AspNetUsers_UserId FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AspNetUserLogins
	(
	LoginProvider nvarchar(450) NOT NULL,
	ProviderKey nvarchar(450) NOT NULL,
	ProviderDisplayName nvarchar(MAX) NULL,
	UserId varchar(36) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AspNetUserLogins SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.AspNetUserLogins)
	 EXEC('INSERT INTO dbo.Tmp_AspNetUserLogins (LoginProvider, ProviderKey, ProviderDisplayName, UserId)
		SELECT LoginProvider, ProviderKey, ProviderDisplayName, CONVERT(varchar(36), UserId) FROM dbo.AspNetUserLogins WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.AspNetUserLogins
GO
EXECUTE sp_rename N'dbo.Tmp_AspNetUserLogins', N'AspNetUserLogins', 'OBJECT' 
GO
ALTER TABLE dbo.AspNetUserLogins ADD CONSTRAINT
	PK_AspNetUserLogins PRIMARY KEY CLUSTERED 
	(
	LoginProvider,
	ProviderKey
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_AspNetUserLogins_UserId ON dbo.AspNetUserLogins
	(
	UserId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.AspNetUserLogins ADD CONSTRAINT
	FK_AspNetUserLogins_AspNetUsers_UserId FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AspNetUserClaims
	(
	Id int NOT NULL IDENTITY (1, 1),
	ClaimType nvarchar(MAX) NULL,
	ClaimValue nvarchar(MAX) NULL,
	UserId varchar(36) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AspNetUserClaims SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_AspNetUserClaims ON
GO
IF EXISTS(SELECT * FROM dbo.AspNetUserClaims)
	 EXEC('INSERT INTO dbo.Tmp_AspNetUserClaims (Id, ClaimType, ClaimValue, UserId)
		SELECT Id, ClaimType, ClaimValue, CONVERT(varchar(36), UserId) FROM dbo.AspNetUserClaims WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_AspNetUserClaims OFF
GO
DROP TABLE dbo.AspNetUserClaims
GO
EXECUTE sp_rename N'dbo.Tmp_AspNetUserClaims', N'AspNetUserClaims', 'OBJECT' 
GO
ALTER TABLE dbo.AspNetUserClaims ADD CONSTRAINT
	PK_AspNetUserClaims PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_AspNetUserClaims_UserId ON dbo.AspNetUserClaims
	(
	UserId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.AspNetUserClaims ADD CONSTRAINT
	FK_AspNetUserClaims_AspNetUsers_UserId FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Manager
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserId varchar(36) NULL,
	CreatedBy_Id nvarchar(MAX) NULL,
	DeletedBy_Id nvarchar(MAX) NULL,
	UpdatedBy_Id nvarchar(MAX) NULL,
	IsDeleted bit NOT NULL,
	UpdatedDate datetime2(7) NOT NULL,
	CreatedDate datetime2(7) NOT NULL,
	DeletedDate datetime2(7) NOT NULL,
	Tenant_Id nvarchar(450) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Manager SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Manager ON
GO
IF EXISTS(SELECT * FROM dbo.Manager)
	 EXEC('INSERT INTO dbo.Tmp_Manager (Id, UserId, CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id)
		SELECT Id, CONVERT(varchar(36), UserId), CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id FROM dbo.Manager WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Manager OFF
GO
ALTER TABLE dbo.TeamManager
	DROP CONSTRAINT FK_TeamManager_Manager
GO
ALTER TABLE dbo.ManagerDispatching
	DROP CONSTRAINT FK_ManagerDispatching_Manager
GO
DROP TABLE dbo.Manager
GO
EXECUTE sp_rename N'dbo.Tmp_Manager', N'Manager', 'OBJECT' 
GO
ALTER TABLE dbo.Manager ADD CONSTRAINT
	PK_Manager PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Manager ADD CONSTRAINT
	FK_Manager_User FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.AspNetUsers
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ManagerDispatching ADD CONSTRAINT
	FK_ManagerDispatching_Manager FOREIGN KEY
	(
	ManagerId
	) REFERENCES dbo.Manager
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ManagerDispatching SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TeamManager ADD CONSTRAINT
	FK_TeamManager_Manager FOREIGN KEY
	(
	ManagerId
	) REFERENCES dbo.Manager
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TeamManager SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
