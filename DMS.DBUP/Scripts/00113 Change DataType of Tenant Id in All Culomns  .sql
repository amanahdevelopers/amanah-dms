ALTER TABLE AccountLogs
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Admins
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE AgentStatus
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE AgentType
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE AgentType
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE AspNetUsers
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Branch
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Country
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Customers
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE Driver
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE DriverCurrentLocation
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE DriverLoginRequests
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE DriverloginTracking
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE DriverRate
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE DriverTaskNotification
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE GeoFence
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE LocationAccuracy
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE MainTask
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE MainTaskStatus
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE MainTaskType
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Manager
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE ManagerDispatching
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE MapRequestLogs
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Notifications
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE PACI
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE Restaurant
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Settings
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE TaskDriverRequests
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE TaskGallary
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE TaskHistory
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE TaskRoute
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE TaskStatus
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE Tasks
ALTER  COLUMN Tenant_Id  Varchar(36);


ALTER TABLE Teams
ALTER  COLUMN Tenant_Id  Varchar(36);

ALTER TABLE TransportType
ALTER  COLUMN Tenant_Id  Varchar(36);
