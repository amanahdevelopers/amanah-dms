BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverCurrentLocation ADD
	BranchId int NULL
GO
ALTER TABLE dbo.DriverCurrentLocation ADD CONSTRAINT
	FK_DriverCurrentLocation_Branch FOREIGN KEY
	(
	BranchId
	) REFERENCES dbo.Branch
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT