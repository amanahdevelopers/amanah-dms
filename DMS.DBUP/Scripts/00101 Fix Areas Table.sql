/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.Area.FK_UpdatedBy_Id', N'Tmp_UpdatedBy_Id', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Area.Tmp_UpdatedBy_Id', N'UpdatedBy_Id', 'COLUMN' 
GO
ALTER TABLE dbo.Area ADD
	Tenant_Id varchar(50) NULL
GO
ALTER TABLE dbo.Area SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
