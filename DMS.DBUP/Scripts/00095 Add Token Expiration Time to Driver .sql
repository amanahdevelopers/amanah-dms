BEGIN TRANSACTION
GO


ALTER TABLE dbo.Driver ADD
	[TokenTimeExpiration] [datetime2](7)  NULL
GO

COMMIT
