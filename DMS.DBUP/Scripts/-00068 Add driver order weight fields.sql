BEGIN TRANSACTION
GO
ALTER TABLE dbo.Driver ADD
	MaxOrdersWeightsCapacity int NOT NULL CONSTRAINT DF_Driver_MaxOrdersWeightsCapacity DEFAULT 1,
	MinOrdersNoWait int NOT NULL CONSTRAINT DF_Driver_MinOrdersNoWait DEFAULT 1
GO
ALTER TABLE dbo.Driver SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
