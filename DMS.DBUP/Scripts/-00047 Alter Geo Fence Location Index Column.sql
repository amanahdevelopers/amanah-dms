BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[GeoFenceLocation]
ADD [PointIndex] [int]  DEFAULT ((0))  NOT NULL;
GO

COMMIT