BEGIN TRANSACTION
GO


ALTER TABLE dbo.Driver ADD
	[AllPickupGeoFences] bit Not NULL DEFAULT  0,
	[AllDeliveryGeoFences] bit Not NULL DEFAULT  0
GO

COMMIT
