
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Manager
	(
	Id int IDENTITY (1, 1) NOT NULL,
	TeamId int NOT NULL,
	UserId [nvarchar](450)  NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Manager ADD CONSTRAINT
	PK_Manager PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


GO
ALTER TABLE dbo.Manager ADD CONSTRAINT
	FK_Manager_Team FOREIGN KEY
	(TeamId) REFERENCES dbo.Teams (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 

GO	
ALTER TABLE dbo.Manager ADD CONSTRAINT
	FK_Manager_User FOREIGN KEY
	(UserId) REFERENCES dbo.AspNetUsers (Id)
	ON UPDATE  NO ACTION 
	ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Manager SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Manager', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Manager', 'Object', 'VIEW DEFINITION') 
as View_def_Per, Has_Perms_By_Name(N'dbo.Manager', 'Object', 'CONTROL') as Contr_Per 

