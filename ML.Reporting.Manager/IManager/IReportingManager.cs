﻿using ML.Reporting.Manager.ReportModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.IManager
{
    public interface IReportingManager
    {
        byte[] Export(List<DataSetViewModel> DataSets, string templatePath, int FileType);
    }
}
