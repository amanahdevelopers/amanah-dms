﻿using AspNetCore.Reporting;
using ML.Reporting.Manager.IManager;
using ML.Reporting.Manager.ReportModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.Manager
{
    public  class ReportingManager  : IReportingManager
    {
         public byte[] Export(List<DataSetViewModel> DataSets, string templatePath, int FileType)
        {
            ReportResult result;

            LocalReport report = new LocalReport(templatePath);


           

                foreach (DataSetViewModel ds in DataSets)
                {
                    report.AddDataSource(ds.Name, ds.Data);
                }

                if(FileType == (int)FileTypes.PDF )
                {
                    result= report.Execute(RenderType.Pdf, 0, null, null);
                }
                else if(FileType == (int)FileTypes.Word)
                {
                    result = report.Execute(RenderType.WordOpenXml, 0, null, null);
                }
                else if (FileType == (int)FileTypes.Excel)
                {
                    result = report.Execute(RenderType.ExcelOpenXml, 0, null, null);
                }
                else
                {
                    result = report.Execute(RenderType.Pdf, 0, null, null);
                }

            return result.MainStream;

        }
    }
}
