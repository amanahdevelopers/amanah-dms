﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
   public class Calls
    {
        public String CallerName { get; set; }
        public String CID { get; set; }
        public String Address { get; set; }
        public String CallerNumber { get; set; }
        public String CallStatus {get; set; }
        public String CallDate { get; set; }

    }
}
