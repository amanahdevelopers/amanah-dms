﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
     public class Inventories
    {
        public string OrderNo { get; set; }

        public string ContractNo { get; set; }

        public string CustomerName { get; set; }

        public string Governrate { get; set; }

        public string Area { get; set; }
        public string ItemNmae { get; set; }
        public string ItemDescription { get; set; }
        public int Quantity { get; set; }
    }
}
