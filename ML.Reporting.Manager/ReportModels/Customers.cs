﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
   public class Customers
    {

        public String Name { set; get; }
        public String CID { set; get; }
        public String Phone { set; get; }
        public String Address { set; get; }
    }
}
