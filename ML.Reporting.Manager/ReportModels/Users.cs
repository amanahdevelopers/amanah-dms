﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting
{
    public  class Users
    {

        public String Name { set; get; }
        public String UserName { set; get; }
        public String Email { set; get; }
        public String Role { set; get; }
        public String Phone { set; get; }


    }
}
