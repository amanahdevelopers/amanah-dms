﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
   public class Orders
    {

        public String OrderNumber { set; get; }
        public String Name { set; get; }
        public String OrderProblem { set; get; }
        public String OrderType { set; get; }
        public String OrderPriority{ set; get; }
        public string StartDate { set; get; }
        public string EndDate { set; get; }
        public double Value { set; get; }
        public String OrderStatus { set; get; }
        public String DispatcherName { set; get; }
        public String TechnicianName { set; get; }
        public String Location { set; get; }
        public String Note { set; get; }




    }
}
