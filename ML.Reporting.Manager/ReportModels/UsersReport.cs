﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
   public class UsersReport
    {
        public int Users { get; set; }
        public int CallCenters { get; set; }
        public int Maintenances { get; set; }
        public int Technicans { get; set; }
        public int Admins { get; set; }
        public int Dispatchers { get; set; }
        public int Supervisors { get; set; }
        public int LockedUsers { get; set; }

    }
}
