﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
     public class DataSetViewModel
    {
       public string Name { set; get; }
       public IEnumerable<Object> Data { set; get; }
    }

}
