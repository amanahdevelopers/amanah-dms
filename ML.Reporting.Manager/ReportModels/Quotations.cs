﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
    public class Quotations
    {
        public string CustomerName { get; set; }
        public string ReferenceNo { get; set; }
        public string EstimationRefNo { get; set; }
        public string CustomerMobile { get; set; }
        public string Area { get; set; }


    }
}
