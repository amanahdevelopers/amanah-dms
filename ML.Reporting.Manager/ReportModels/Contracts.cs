﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ML.Reporting.Manager.ReportModels
{
    public class Contracts
    {
        public String CustomerName { set; get; }
        public String ContractType { set; get; }
        public String ContractNumber { set; get; }
        public String EndDate { set; get; }
        public double Value { set; get; }
        public String EstimationReference { set; get; }

        public String ProjectName { set; get; }
        public String BuildingType { set; get; }


    }
}
