﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class GeoFenceViewModelValidation : AbstractValidator<GeoFenceViewModel>
    {
        public GeoFenceViewModelValidation()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.Drivers).NotNull();
        }
    }
}
