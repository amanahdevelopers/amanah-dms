﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class DriverRateViewModelValidation : AbstractValidator<DriverRateViewModel>
    {
        public DriverRateViewModelValidation()
        {
            //RuleFor(x => x.Id).NotEmpty().NotNull() ;
            RuleFor(x => x.CustomerId).NotEmpty().NotNull() ;
            RuleFor(x => x.TasksId).NotEmpty().NotNull() ;
            RuleFor(x => x.Rate).NotEmpty().NotNull();
          

        }
    }
}
