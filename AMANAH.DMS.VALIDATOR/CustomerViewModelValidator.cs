﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;

namespace AMANAH.DMS.VALIDATOR
{
	public class CustomerViewModelValidator : AbstractValidator<CustomerViewModel>
    {
		public CustomerViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
			RuleFor(x => x.Email).EmailAddress().MaximumLength(250);
			RuleFor(x => x.Phone).NotEmpty().NotNull().MaximumLength(50);
			RuleFor(x => x.Address).NotEmpty().NotNull();
			RuleFor(x => x.Longitude).NotEmpty().NotNull();
			RuleFor(x => x.Latitude).NotEmpty().NotNull();

		}
	}
}
