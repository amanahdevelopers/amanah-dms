﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class CreateNotificationViewModelValidator : AbstractValidator<CreateNotificationViewModel>
    {
        public CreateNotificationViewModelValidator()
        {
            RuleFor(x => x.FromUserId).NotEmpty().NotNull().MaximumLength(450);
            RuleFor(x => x.ToUserId).NotEmpty().NotNull().MaximumLength(450);
            RuleFor(x => x.Date).NotEmpty().NotNull();
            RuleFor(x => x.Body).NotNull().NotEmpty();

        }
    }
}
