﻿using AMANAH.DMS.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class CreateRestaurantViewModelValidation : AbstractValidator<RestaurantViewModel>
    {
        public CreateRestaurantViewModelValidation()
        {

            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
        }

    }
}
