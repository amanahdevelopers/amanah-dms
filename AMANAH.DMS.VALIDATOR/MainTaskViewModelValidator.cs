﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class MainTaskViewModelValidator : AbstractValidator<MainTaskViewModel>
	{
		public MainTaskViewModelValidator()
		{
			RuleFor(x => x.MainTaskTypeId).NotEmpty().NotNull();
			RuleFor(x => x.Tasks).NotNull().NotEmpty();

		}
	}
}
