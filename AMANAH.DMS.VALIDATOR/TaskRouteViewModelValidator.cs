﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class TaskRouteViewModelValidator : AbstractValidator<TaskRouteViewModel>
	{
        public TaskRouteViewModelValidator()
		{
			RuleFor(x => x.TaskId).NotEmpty().NotNull();
			RuleFor(x => x.DriverId).NotEmpty().NotNull();
			RuleFor(x => x.Latitude).NotEmpty().NotNull();
			RuleFor(x => x.Longitude).NotEmpty().NotNull();
		}
	}
}
