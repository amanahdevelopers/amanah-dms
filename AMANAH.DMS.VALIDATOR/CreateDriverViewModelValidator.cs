﻿using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class CreateDriverViewModelValidator : AbstractValidator<CreateDriverViewModel>
    {
		public CreateDriverViewModelValidator()
		{
			RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
			RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();
			RuleFor(x => x.TeamId).NotEmpty().NotNull();
			RuleFor(x => x.UserName).NotEmpty().NotNull();
			//RuleFor(x => x.TransportTypeId).NotEmpty().NotNull();
		}
	}
}
