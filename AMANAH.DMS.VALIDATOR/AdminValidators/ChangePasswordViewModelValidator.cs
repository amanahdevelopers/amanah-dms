﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.VALIDATOR
{
    public class ChangePasswordViewModelValidator : AbstractValidator<ChangePasswordViewModel>
    {
        public ChangePasswordViewModelValidator(ILocalizer localizer)
        {
            RuleFor(x => x.newPassword).Password(localizer);
            RuleFor(x => x.confirmPassword).Equal(x => x.newPassword)
                .When(x => !string.IsNullOrWhiteSpace(x.newPassword))
                .WithMessage(localizer[Keys.Validation.RetypePasswordMismatch]);

        }
    }
}
