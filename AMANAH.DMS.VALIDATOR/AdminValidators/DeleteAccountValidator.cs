﻿using AMANAH.DMS.BLL.ViewModels.Admin;
using AMANAH.DMS.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.VALIDATOR
{
    public class DeleteAccountValidator : AbstractValidator<AccountToDeleteViewModel>
    {
        public DeleteAccountValidator(ILocalizer localizer)
        {
            RuleFor(x => x.deactivationReason).NotNull().NotEmpty();
            RuleFor(x => x.password).Password(localizer);
        }
    }
}
