﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class ManagerDispatchingViewModelValidator : AbstractValidator<ManagerDispatchingViewModel>
    {
		public ManagerDispatchingViewModelValidator()
		{
			RuleFor(x => x.DesignationName).NotEmpty().NotNull().MaximumLength(100);
		}
	}
}
