﻿using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class TeamViewModelValidator : AbstractValidator<TeamViewModel>
    {
		public TeamViewModelValidator()
		{
			RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
			RuleFor(x => x.LocationAccuracyId).NotEmpty().NotNull();
		}
	}
}
