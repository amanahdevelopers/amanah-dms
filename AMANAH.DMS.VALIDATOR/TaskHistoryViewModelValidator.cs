﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.VALIDATOR
{
    public class TaskHistoryViewModelValidator : AbstractValidator<TaskHistoryViewModel>
	{
        public TaskHistoryViewModelValidator()
		{
			RuleFor(x => x.TaskId).NotEmpty().NotNull();
			RuleFor(x => x.MainTaskId).NotEmpty().NotNull();
			RuleFor(x => x.ActionName).NotEmpty().NotNull();
        }
	}
}
