﻿namespace Utilities.Utilites.Localization
{
    public interface ILocalizer
    {
        string this[string key] { get; }

        string Format(string key, params object[] arguments);
    }
}
