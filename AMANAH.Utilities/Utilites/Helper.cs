﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Utilites
{
    public static class Helper
    {
        public static string GenerateUniqueId()
        {
            return DateTime.UtcNow.Ticks.ToString();
        }
        public static string CreateInfixDateCode()
        {
            string day = DateTime.UtcNow.Day.ToString();
            string month = DateTime.UtcNow.Month.ToString();
            string year = DateTime.UtcNow.Year.ToString().Substring(2);
            string orderdate = day + month + year;
            return orderdate;
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        public static string  GetValueFromRequestHeader(HttpRequest requst,string key="Authorization")
        {
             string authHeader = requst.Headers["Authorization"];
            authHeader=authHeader.Replace("bearer ", "");
            authHeader=authHeader.Replace("Bearer ", "");
            return authHeader;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}

