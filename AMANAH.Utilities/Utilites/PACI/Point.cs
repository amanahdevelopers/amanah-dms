﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Utilites.PACI
{
    public class Point
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
