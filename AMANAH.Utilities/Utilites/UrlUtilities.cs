﻿using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace Utilites
{
    public static class UrlUtilities
    {
        public static string ToQueryString(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            return "?" + string.Join("&", array);
        }

        public static async Task<string> GetServerResponseAsync(string url)
        {
            var request = WebRequest.Create(url);
            using (var response = await request.GetResponseAsync())
            {

                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);
                var responseFromServer = await reader.ReadToEndAsync();
                return responseFromServer;
            }
        }

        ///// <summary>
        ///// Downloads a file if it doesn't exist yet.
        ///// </summary>
        //public static async Task DownloadToFile(string url, string filename)
        //{
        //    if (!File.Exists(filename))
        //    {
        //        var client = new HttpClient();
        //        using (var stream = await client.GetStreamAsync(url))
        //        using (var outputStream = File.OpenWrite(filename))
        //        {
        //            stream.CopyTo(outputStream);
        //        }
        //    }
        //}
    }
}
