﻿using System;

namespace Utilities.Utilites.Exceptions
{
    /// <summary>
    /// Represents an exception due to a wrong domain flow and should be shown to user,
    /// It holds neutral strings not a specific language and end user of exception is responsible for localization.
    /// End user is place that catch exception and use its message
    /// </summary>
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
        }

        public DomainException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    //Any Exception otherthan DomainException is considered Application Exception like (ArgumentNullException, ....)
#if false
    /// <summary>
    /// Represents an exception due to an issue in the application. and it is actually a bug in the system that lead to such exception
    /// </summary>
    public class ApplicationException : Exception
    {
    }
#endif
}
