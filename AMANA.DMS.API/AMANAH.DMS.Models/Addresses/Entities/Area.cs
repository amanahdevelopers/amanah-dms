﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Area : BaseEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string FK_Governrate_Id { get; set; }

    }
}
