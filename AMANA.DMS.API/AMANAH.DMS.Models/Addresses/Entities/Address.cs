﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Address : ValueObject
    {

        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }

        public Address() { }

        public Address(string area, string block, string street, string bulding, string floor, string flat)
        {
            Area = area;
            Block = block;
            Street = street;
            Building = bulding;
            Floor = floor;
            Flat = flat;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            // Using a yield return statement to return each element one at a time
            yield return Area;
            yield return Block;
            yield return Street;
            yield return Building;
            yield return Floor;
            yield return Flat;
        }
    }
}
