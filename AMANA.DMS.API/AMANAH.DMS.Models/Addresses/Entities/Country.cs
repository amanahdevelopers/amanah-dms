﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{

    public class Country : BaseGlobalEntity
    {
        public int Id { set; get; }
        public string Name { set; get; }

        public string Code { set; get; }

        public string Flag { set; get; }
        public string TopLevel { set; get; }

    }
}
