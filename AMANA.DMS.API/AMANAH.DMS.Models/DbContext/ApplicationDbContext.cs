﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BASE.Domain.Entities;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.EntityConfigurations;
using AMANAH.DMS.DATA.Extensions;
using AMANAH.DMS.Entities;
using AMANAH.DMS.EntityConfigurations;
using AMANAH.DMS.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using TaskStatus = AMANAH.DMS.DATA.Entities.TaskStatus;

namespace AMANAH.DMS.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        private readonly ICurrentUser _currentUser;
#if false
        private static readonly object _lock = new object();
        private static int _referenceCount = 0;
#endif
        public DbSet<MapRequestLog> MapRequestLogs { get; set; }
        public DbSet<RolePrivilge> RolePrivilge { get; set; }
        public DbSet<Privilge> Privilge { get; set; }
        public DbSet<UserDevice> UserDevice { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<LocationAccuracy> LocationAccuracy { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<AgentType> AgentType { get; set; }
        public DbSet<TransportType> TransportType { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Driver> Driver { get; set; }
        public DbSet<DriverCurrentLocation> DriverCurrentLocation { get; set; }
        public DbSet<GeoFence> GeoFence { get; set; }
        public DbSet<GeoFenceLocation> GeoFenceLocation { get; set; }
        public DbSet<Manager> Manager { get; set; }
        public DbSet<AgentStatus> AgentStatus { get; set; }
        public DbSet<MainTaskStatus> MainTaskStatus { get; set; }
        public DbSet<MainTaskType> MainTaskType { get; set; }
        public DbSet<MainTask> MainTask { get; set; }
        public DbSet<TaskType> TaskType { get; set; }
        public DbSet<TaskStatus> TaskStatus { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<TaskHistory> TaskHistory { get; set; }
        public DbSet<TaskGallary> TaskGallary { get; set; }
        public DbSet<DriverTaskNotification> DriverTaskNotification { get; set; }
        public DbSet<TeamManager> TeamManager { get; set; }
        public DbSet<TaskRoute> TaskRoute { get; set; }
        public DbSet<TaskDriverRequests> TaskDriverRequests { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Restaurant> Restaurant { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<ManagerDispatching> ManagerDispatching { get; set; }
        public DbSet<DriverPickUpGeoFence> DriverPickUpGeoFence { get; set; }
        public DbSet<DriverDeliveryGeoFence> DriverDeliveryGeoFence { get; set; }
        public DbSet<DriverLoginRequest> DriverLoginRequests { get; set; }
        public DbSet<DriverRate> DriverRate { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<AccountLogs> AccountLogs { get; set; }
        public DbSet<DriverloginTracking> DriverloginTracking { get; set; }

        public DbSet<PACI> PACI { get; set; }

        /*
         * TODO: Tenant, Auditing writing(soft delete
         * 
         * Tenant, SoftDelete filtration
         *      By Default use current user tenant, but we should be able to override this
        */


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ICurrentUser currentUser)
            : base(options)
        {
            _currentUser = currentUser;
#if fasle
            lock(_lock)
            {
                _referenceCount++;
            }
#endif
        }

        public override void Dispose()
        {
            base.Dispose();
#if false
            lock(_lock)
            {
                _referenceCount--;
            }
#endif
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ApplicationUSerEntityTypeConfiguration());
            builder.ApplyConfiguration(new RolePrivilgeEntityTypeConfiguration());

            builder.ApplyConfiguration(new PrivilgeEntityTypeConfiguration());

            builder.ApplyConfiguration(new ApplicationRoleEntityTypeConfiguration());

            builder.ApplyConfiguration(new ApplicationRoleEntityTypeConfiguration());

            builder.ApplyConfiguration(new GeoFenceConfiguration());
            builder.ApplyConfiguration(new DriverConfiguration());

            builder.ApplyConfiguration(new DriverEntityTypeConfiguration());
            builder.ApplyConfiguration(new DriverTaskNotificationConfiguration());
            builder.ApplyConfiguration(new NotificationConfiguration());

            builder.ApplyConfiguration(new BranchConfiguration());
            builder.ApplyConfiguration(new CustomerEntityTypeConfiguration());
            builder.ApplyConfiguration(new TasksEntityTypeConfiguration());

            builder.ApplyConfiguration(new RestaurantConfiguration());
            builder.ApplyConfiguration(new ManagerConfiguration());
            builder.ApplyConfiguration(new DriverLoginRequestConfiguration());
            builder.ApplyConfiguration(new AdminEntityTypeConfiguration());


            base.OnModelCreating(builder);
            AddGlobalFilters(builder);

        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            HandleAuditing();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            HandleAuditing();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        #region Automatic Auditing & Global Query Filter
        private void HandleAuditing()
        {
            // TenantID for created
            var now = DateTime.UtcNow;
            var haveTenantAddedEntries = ChangeTracker.Entries<IHaveTenant>()
                .Where(entry => entry.State == EntityState.Added)
                .ToArray();
            foreach (var addedEntry in haveTenantAddedEntries)
            {
                addedEntry.SetPropertyIfNotSet(nameof(IHaveTenant.Tenant_Id), _currentUser.TenantId);
            }
            var auditedCreatedEntries = ChangeTracker.Entries<IBaseEntity>()
                .Where(entry => entry.State == EntityState.Added)
                .ToArray();
            foreach (var addedEntry in auditedCreatedEntries)
            {
                addedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.CreatedBy_Id), _currentUser.UserName);
                addedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.CreatedDate), now);
            }
            var auditedModifiedEntries = ChangeTracker.Entries<IBaseEntity>()
                .Where(entry => entry.State == EntityState.Modified)
                .ToArray();
            foreach (var modifiedEntry in auditedModifiedEntries)
            {
                modifiedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.UpdatedBy_Id), _currentUser.UserName);
                modifiedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.UpdatedDate), now);

                modifiedEntry.Property(nameof(IHaveTenant.Tenant_Id)).IsModified = false;
                modifiedEntry.Property(nameof(IBaseEntity.CreatedBy_Id)).IsModified = false;
                modifiedEntry.Property(nameof(IBaseEntity.CreatedDate)).IsModified = false;

            }
            var auditedDeletedEntries = ChangeTracker.Entries<IBaseEntity>()
                .Where(entry => entry.State == EntityState.Deleted)
                .ToArray();
            foreach (var deletedEntry in auditedDeletedEntries)
            {
                deletedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.IsDeleted), true);
                deletedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.DeletedBy_Id), _currentUser.UserName);
                deletedEntry.SetPropertyIfNotSet(nameof(IBaseEntity.DeletedDate), now);
                deletedEntry.State = EntityState.Modified;
                deletedEntry.Property(nameof(IHaveTenant.Tenant_Id)).IsModified = false;
                deletedEntry.Property(nameof(IBaseEntity.CreatedBy_Id)).IsModified = false;
                deletedEntry.Property(nameof(IBaseEntity.CreatedDate)).IsModified = false;
            }
        }

        private void AddGlobalFilters(ModelBuilder builder)
        {
            // Note: adding multiple global query filters is not supported, a single filter is allowed however you can add multiple conditions to it.
            static bool HasFilterInterface<T>(IMutableEntityType entityType)
            {
                return
                    typeof(T).IsAssignableFrom(entityType.ClrType) &&
                    (entityType.BaseType == null || !typeof(T).IsAssignableFrom(entityType.BaseType.ClrType));
            }

            var tenantFilteredEntityTypes = builder.Model
                .GetEntityTypes()
                .Where(entityType => HasFilterInterface<IHaveTenant>(entityType))
                .ToArray();
            var softDeleteFilteredEntityTypes = builder.Model
                .GetEntityTypes()
                .Where(entityType => HasFilterInterface<IBaseEntity>(entityType))
                .ToArray();
            foreach (var tenantEnityType in tenantFilteredEntityTypes)
            {
                tenantEnityType.AddQueryFilter(QueryFilterHelper.GetTenantQueryFilter(tenantEnityType.ClrType, this));
            }
            foreach (var softDeletedEntity in softDeleteFilteredEntityTypes)
            {
                softDeletedEntity.AddQueryFilter(QueryFilterHelper.GetSofDeleteQueryFilter(softDeletedEntity.ClrType));
            }
#if false
            Dictionary<IMutableEntityType, ICollection<LambdaExpression>> entityFilters = new Dictionary<IMutableEntityType, ICollection<LambdaExpression>>();
            foreach (var entityType in tenantFilteredEntityTypes)
            {
                if (!entityFilters.TryGetValue(entityType, out var filters))
                {
                    filters = new List<LambdaExpression>();
                    entityFilters[entityType] = filters;
                }
                filters.Add(QueryFilterHelper.GetTenantQueryFilter(entityType.ClrType, _currentUser));
            }
            foreach (var entityType in softDeleteFilteredEntityTypes)
            {
                if (!entityFilters.TryGetValue(entityType, out var filters))
                {
                    filters = new List<LambdaExpression>();
                    entityFilters[entityType] = filters;
                }
                filters.Add(QueryFilterHelper.GetSofDeleteQueryFilter(entityType.ClrType));
            }
            foreach (var (entityType, filters) in entityFilters)
            {
                var filter = filters.Aggregate((accumulated, current) => accumulated.And(current));
                entityType.SetQueryFilter(filter);
            } 
#endif
        }

        private static class QueryFilterHelper
        {
            private static readonly MethodInfo _getTenantQueryFilterMethodInfo = GetMethod(nameof(GetTenantQueryFilter));
            private static readonly MethodInfo _getSoftDeleteQueryFilterMethodInfo = GetMethod(nameof(GetSoftDeleteQueryFilter));

            public static LambdaExpression GetSofDeleteQueryFilter(Type entityType)
            {
                return _getSoftDeleteQueryFilterMethodInfo.MakeGenericMethod(entityType)
                    .Invoke(null, Array.Empty<object>()) as LambdaExpression;
            }

            public static LambdaExpression GetTenantQueryFilter(Type entityType, ApplicationDbContext dbContext)
            {
                return _getTenantQueryFilterMethodInfo.MakeGenericMethod(entityType)
                    .Invoke(null, new object[] { dbContext }) as LambdaExpression;
            }

            private static Expression<Func<TEntity, bool>> GetTenantQueryFilter<TEntity>(ApplicationDbContext dbContext) where TEntity : IHaveTenant
            {
                // Note: it is ment to use dbcontext instead of directly referencing currentuser.Tenant_Id to avoid tenant caching
                return entity =>
                    string.IsNullOrWhiteSpace(dbContext._currentUser.TenantId) ||
                    string.IsNullOrWhiteSpace(entity.Tenant_Id) ||
                    entity.Tenant_Id == dbContext._currentUser.TenantId;
            }

            private static Expression<Func<TEntity, bool>> GetSoftDeleteQueryFilter<TEntity>() where TEntity : IBaseEntity
            {
                return entity => !entity.IsDeleted;
            }

            private static MethodInfo GetMethod(string name)
            {
                return typeof(QueryFilterHelper)
                    .GetMethod(name, BindingFlags.NonPublic | BindingFlags.Static);
            }
        }
        #endregion
    }
}
