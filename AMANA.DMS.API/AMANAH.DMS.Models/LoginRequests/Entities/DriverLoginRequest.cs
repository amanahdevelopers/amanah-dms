﻿using System.ComponentModel.DataAnnotations;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class DriverLoginRequest : BaseEntity
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// DriverLoginRequestSatus enum type
        /// </summary>
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }

        public string ApprovedOrRejectBy { get; set; }

        public Driver Driver { get; set; }
        public string Reason { get; set; }

        private DriverLoginRequest()
        {
        }

        public DriverLoginRequest(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
