﻿using System;
using System.ComponentModel.DataAnnotations;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class DriverloginTracking : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int DriverId { get; set; }
        public DateTime? LoginDate { get; set; }
        public double? LoginLatitude { get; set; }
        public double? LoginLongitude { get; set; }
        public DateTime? LogoutDate { get; set; }
        public double? LogoutLatitude { get; set; }
        public double? LogoutLongitude { get; set; }
        public string LogoutActionBy { get; set; }
        public Driver Driver { get; set; }

        private DriverloginTracking()
        {
        }

        public DriverloginTracking(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
