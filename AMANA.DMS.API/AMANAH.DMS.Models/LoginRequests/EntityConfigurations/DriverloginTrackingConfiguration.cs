﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class DriverloginTrackingConfiguration
        : IEntityTypeConfiguration<DriverloginTracking>
    {
        public void Configure(EntityTypeBuilder<DriverloginTracking> driverloginTrackingConfiguration)
        {
            driverloginTrackingConfiguration.HasKey(x => x.Id);

        }
    }
}
