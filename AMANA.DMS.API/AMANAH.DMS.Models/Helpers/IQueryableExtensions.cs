﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;


namespace AMANAH.DMS.DATA.Helpers
{
    public static class IQueryableExtensions
    {
        public static async Task<PagedResult<T>> ToPagedResultAsync<T>(
            this IQueryable<T> source,
            PaginatedItemsViewModel pagingOptions)
        {
            var pagedResult = new PagedResult<T>
            {
                TotalCount = await source.CountAsync(),
                Result = await source.ApplyPaging(pagingOptions).ToListAsync()
            };
            return pagedResult;
        }

        public static async Task<PagedResult<TResult>> ToPagedResultAsync<TEntity, TResult>(
            this IQueryable<TEntity> source,
            PaginatedItemsViewModel pagingOptions,
            IConfigurationProvider config)
        {
            var totalCount = await source.CountAsync();
            var items = await source
                .ApplyPaging(pagingOptions)
                .ProjectTo<TResult>(config)
                .ToListAsync();
            //if(items.Count < pagingOptions.PageSize && totalCount >= pagingOptions.PageSize)
            //{
            //    Debugger.Break();
            //}
            var pagedResult = new PagedResult<TResult>
            {
                TotalCount = totalCount,
                Result = items
            };
            return pagedResult;
        }

        private static IQueryable<T> ApplyPaging<T>(this IQueryable<T> source, PaginatedItemsViewModel pagingOptions)
        {
            var pageNumber = Math.Max(pagingOptions.PageNumber, 1);
            var pageSize = pagingOptions.PageSize == 0
                ? 20
                : pagingOptions.PageSize;
            return source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

#if DEBUG
        /// <summary>
        /// Translate IQueryable to sql if relational database provider is configured Compatible with EF core 3.1
        /// To be used for testing and debugging purposes only
        /// </summary>
        public static string ToSql<TEntity>(this IQueryable<TEntity> query)
        {
            using var enumerator = query.Provider.Execute<IEnumerable<TEntity>>(query.Expression).GetEnumerator();
            var relationalCommandCache = enumerator.GetPrivateField("_relationalCommandCache");
            var selectExpression = relationalCommandCache.GetPrivateField<SelectExpression>("_selectExpression");
            var factory = relationalCommandCache.GetPrivateField<IQuerySqlGeneratorFactory>("_querySqlGeneratorFactory");
            var sqlGenerator = factory.Create();
            var command = sqlGenerator.GetCommand(selectExpression);
            string sql = command.CommandText;
            return sql;
        }

        private static object GetPrivateField(this object instance, string privateField) =>
            instance?.GetType()
                .GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)
                ?.GetValue(instance);

        private static T GetPrivateField<T>(this object instance, string privateField) =>
            (T)instance?.GetType()
                .GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)
                ?.GetValue(instance);
#endif
    }
}