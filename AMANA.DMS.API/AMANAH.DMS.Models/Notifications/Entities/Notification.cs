﻿using System;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Notification : BaseEntity
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public bool IsSeen { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }

        //public ApplicationUser FromUser { get; set; }
        //public ApplicationUser ToUser { get; set; }

    }
}
