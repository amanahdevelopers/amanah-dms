﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class NotificationConfiguration
        : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> notificationConfiguration)
        {

            notificationConfiguration.HasKey(x => x.Id);
            notificationConfiguration.Property(o => o.Body).IsRequired();
            notificationConfiguration.Property(o => o.Date).IsRequired();
            notificationConfiguration.Property(o => o.FromUserId).HasMaxLength(450);
            notificationConfiguration.Property(o => o.ToUserId).HasMaxLength(450);
        }
    }
}
