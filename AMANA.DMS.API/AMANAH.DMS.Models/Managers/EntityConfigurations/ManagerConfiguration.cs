﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class ManagerConfiguration
        : IEntityTypeConfiguration<Manager>
    {
        public void Configure(EntityTypeBuilder<Manager> ManagerConfiguration)
        {
            ManagerConfiguration.HasKey(x => x.Id);
            //ManagerConfiguration.HasMany(x => x.Branches).WithOne(x => x.Manager).HasForeignKey(x => x.ManagerId);


        }
    }
}
