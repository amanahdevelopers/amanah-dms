﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class ManagerDispatchingEntityTypeConfiguration
        : IEntityTypeConfiguration<ManagerDispatching>
    {
        public void Configure(EntityTypeBuilder<ManagerDispatching> ManagerDispatchingConfiguration)
        {
            ManagerDispatchingConfiguration.HasKey(x => x.Id);

            ManagerDispatchingConfiguration.Property(o => o.DesignationName)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
