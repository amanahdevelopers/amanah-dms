﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;
using AMANAH.DMS.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Manager : BaseEntity
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public ICollection<TeamManager> TeamManagers { get; set; }
        //public ICollection<Branch> Branches { get; set; }

    }
}
