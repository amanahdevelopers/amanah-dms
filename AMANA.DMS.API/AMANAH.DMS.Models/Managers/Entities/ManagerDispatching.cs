﻿using AMANAH.DMS.BASE.Domain.Entities;


namespace AMANAH.DMS.DATA.Entities
{
    public class ManagerDispatching : BaseEntity
    {
        public int Id { get; set; }
        public string DesignationName { get; set; }
        public string Zones { get; set; }
        public string Restaurants { get; set; }
        public string Branches { get; set; }

        public int ManagerId { get; set; }
        public Manager Manager { get; set; }
    }
}
