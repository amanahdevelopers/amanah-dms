﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Utilites;

namespace AMANAH.DMS.DATA.Extensions
{
    internal static class EntityFrameworkExtensions
    {
        public static void SetPropertyIfNotSet(this EntityEntry entityEntry, string propertyName, object value)
        {
            var property = entityEntry.Property(propertyName);
            if (ReflectionHelper.IsDefaultValue(property.CurrentValue))
            {
                property.CurrentValue = value;
            }
        }

        public static void AddQueryFilter(this IMutableEntityType target, LambdaExpression filter)
        {
            if (target.GetQueryFilter() == default)
            {
                target.SetQueryFilter(filter);
            }
            else
            {
                var parameter = target.GetQueryFilter().Parameters[0];
                var left = target.GetQueryFilter().Body;
                var right = filter.Body.ReplaceParameter(filter.Parameters[0], parameter);
                var body = Expression.AndAlso(left, right);
                target.SetQueryFilter(Expression.Lambda(body, parameter));
            }
        }

        private static Expression ReplaceParameter(this Expression expression, ParameterExpression source, Expression target)
        {
            return new ParameterReplacer { Source = source, Target = target }.Visit(expression);
        }

        private class ParameterReplacer : ExpressionVisitor
        {
            public ParameterExpression Source;

            public Expression Target;

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return node == Source ? Target : base.VisitParameter(node);
            }
        }
    }
}
