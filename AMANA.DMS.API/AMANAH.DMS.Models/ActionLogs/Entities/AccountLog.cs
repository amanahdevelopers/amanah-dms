﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class AccountLogs : BaseEntity
    {
        public int Id { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }
        public string TableName { get; set; }
        public int Record_Id { get; set; }

    }
}
