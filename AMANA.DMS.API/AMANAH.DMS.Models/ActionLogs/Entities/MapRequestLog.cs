﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class MapRequestLog : BaseEntity
    {
        public long Id { get; set; }

        public string Provider { get; set; }

        public string StackTrace { get; set; }

        public string ApiKey { get; set; }

        public string Request { get; set; }

        public string Response { get; set; }

        public bool Success { get; set; }

        public string Error { get; set; }

        public string ErrorMessage { get; set; }
    }
}
