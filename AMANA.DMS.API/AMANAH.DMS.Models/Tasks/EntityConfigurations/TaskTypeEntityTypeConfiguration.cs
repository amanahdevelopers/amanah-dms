﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class TaskTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<TaskType>
    {
        public void Configure(EntityTypeBuilder<TaskType> TaskTypeConfiguration)
        {
            TaskTypeConfiguration.HasKey(x => x.Id);
            TaskTypeConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
