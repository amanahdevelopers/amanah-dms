﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class TaskRouteEntityTypeConfiguration
        : IEntityTypeConfiguration<TaskRoute>
    {
        public void Configure(EntityTypeBuilder<TaskRoute> TaskRouteConfiguration)
        {
            TaskRouteConfiguration.HasKey(x => x.Id);
            TaskRouteConfiguration.Property(o => o.TaskId).IsRequired();
            TaskRouteConfiguration.Property(o => o.DriverId).IsRequired();
            TaskRouteConfiguration.Property(o => o.Latitude).IsRequired();
            TaskRouteConfiguration.Property(o => o.Longitude).IsRequired();
        }
    }
}
