﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class TasksEntityTypeConfiguration
        : IEntityTypeConfiguration<Tasks>
    {
        public void Configure(EntityTypeBuilder<Tasks> TasksConfiguration)
        {
            TasksConfiguration.HasKey(x => x.Id);
            TasksConfiguration.Property(o => o.MainTaskId).IsRequired();
            TasksConfiguration.Property(o => o.TaskTypeId).IsRequired();
            TasksConfiguration.Property(o => o.CustomerId).IsRequired();
            TasksConfiguration.Property(o => o.TaskStatusId).IsRequired();
            TasksConfiguration.Property(o => o.Address).IsRequired();
            TasksConfiguration.OwnsOne(o => o.Location);
        }
    }
}
