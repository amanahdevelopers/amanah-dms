﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class MainTaskEntityTypeConfiguration
        : IEntityTypeConfiguration<MainTask>
    {
        public void Configure(EntityTypeBuilder<MainTask> MainTaskConfiguration)
        {
            MainTaskConfiguration.HasKey(x => x.Id);

            MainTaskConfiguration.Property(o => o.MainTaskTypeId).IsRequired();
        }
    }
}
