﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class TaskHistoryEntityTypeConfiguration
        : IEntityTypeConfiguration<TaskHistory>
    {
        public void Configure(EntityTypeBuilder<TaskHistory> TaskHistoryConfiguration)
        {
            TaskHistoryConfiguration.HasKey(x => x.Id);

            TaskHistoryConfiguration.Property(o => o.TaskId).IsRequired();
            TaskHistoryConfiguration.Property(o => o.MainTaskId).IsRequired();
        }
    }
}
