﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class TaskStatusEntityTypeConfiguration
        : IEntityTypeConfiguration<TaskStatus>
    {
        public void Configure(EntityTypeBuilder<TaskStatus> TaskStatusConfiguration)
        {
            TaskStatusConfiguration.HasKey(x => x.Id);
            TaskStatusConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
