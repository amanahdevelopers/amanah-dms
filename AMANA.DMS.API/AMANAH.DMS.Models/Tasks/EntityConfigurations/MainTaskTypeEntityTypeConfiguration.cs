﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class MainTaskTypeEntityTypeConfiguration
        : IEntityTypeConfiguration<MainTaskType>
    {
        public void Configure(EntityTypeBuilder<MainTaskType> MainTaskTypeConfiguration)
        {
            MainTaskTypeConfiguration.HasKey(x => x.Id);
            MainTaskTypeConfiguration.Property(o => o.Name).IsRequired();
        }
    }
}
