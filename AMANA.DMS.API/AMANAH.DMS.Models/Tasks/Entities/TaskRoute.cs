﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class TaskRoute : BaseEntity
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public int? TaskStatusId { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public Tasks Task { set; get; }
        public Driver Driver { set; get; }
        public TaskStatus TaskStatus { set; get; }
    }
}
