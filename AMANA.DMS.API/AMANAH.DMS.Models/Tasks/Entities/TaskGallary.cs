﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class TaskGallary : BaseEntity
    {
        public int Id { set; get; }
        public int? MainTaskId { set; get; }
        public int TaskId { set; get; }
        public int? DriverId { set; get; }


        public string FileName { set; get; }
        public string FileURL { set; get; }
        public Tasks Task { set; get; }
    }
}
