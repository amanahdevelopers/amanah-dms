﻿using System;
using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class MainTask : BaseEntity
    {
        public int Id { set; get; }
        public int MainTaskTypeId { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }
        public MainTaskType MainTaskType { set; get; }
        public int AssignmentType { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? MainTaskStatusId { set; get; }
        public double? EstimatedTime { set; get; }

        public MainTaskStatus MainTaskStatus { set; get; }
        public ICollection<Tasks> Tasks { set; get; }
    }
}
