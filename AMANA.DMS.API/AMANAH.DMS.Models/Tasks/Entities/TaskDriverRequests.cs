﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class TaskDriverRequests : BaseEntity
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public MainTask MainTask { set; get; }
        public int? DriverId { set; get; }
        public int ResponseStatus { set; get; }
        public int RetriesCount { get; set; }
    }
}
