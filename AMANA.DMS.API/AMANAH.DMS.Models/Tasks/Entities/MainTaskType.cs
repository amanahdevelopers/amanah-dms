﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class MainTaskType : BaseGlobalEntity
    {
        public int Id { set; get; }
        public string Name { set; get; }

    }
}
