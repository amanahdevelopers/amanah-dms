﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class TaskHistory : BaseEntity
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public int? FromStatusId { set; get; }
        public int? ToStatusId { set; get; }
        public string Reason { set; get; }
        public string ActionName { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public Tasks Task { set; get; }
        public TaskStatus FromStatus { set; get; }
        public TaskStatus ToStatus { set; get; }
    }
}
