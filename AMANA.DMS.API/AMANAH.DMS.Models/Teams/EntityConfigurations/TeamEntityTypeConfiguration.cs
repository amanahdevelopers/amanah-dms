﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class TeamEntityTypeConfiguration
        : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> TeamConfiguration)
        {
            TeamConfiguration.HasKey(x => x.Id);

            TeamConfiguration.Property(o => o.Name)
                .HasMaxLength(100)
                .IsRequired();

            TeamConfiguration.Property(o => o.LocationAccuracyId).IsRequired();
        }
    }
}
