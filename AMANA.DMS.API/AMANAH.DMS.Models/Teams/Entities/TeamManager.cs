﻿namespace AMANAH.DMS.DATA.Entities
{
    public class TeamManager
    {
        public int Id { set; get; }
        public int TeamId { set; get; }
        public Team Team { get; set; }
        public int ManagerId { set; get; }
        public Manager Manager { get; set; }
    }
}
