﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Team : BaseEntity
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }
        public LocationAccuracy LocationAccuracy { get; set; }
        public virtual ICollection<TeamManager> TeamManagers { get; set; }
        public virtual ICollection<Driver> Drivers { get; set; }
    }
}
