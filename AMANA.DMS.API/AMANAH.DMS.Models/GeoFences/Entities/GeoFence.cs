﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class GeoFence : BaseEntity
    {
        public GeoFence()
        {
            GeoFenceLocations = new HashSet<GeoFenceLocation>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

       public virtual ICollection<GeoFenceLocation> GeoFenceLocations{ get; set; }

        public virtual ICollection<Branch> Branchs { get; set; }

    }
}
