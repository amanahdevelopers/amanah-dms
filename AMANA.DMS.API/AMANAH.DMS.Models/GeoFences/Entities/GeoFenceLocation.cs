﻿namespace AMANAH.DMS.DATA.Entities
{
    public class GeoFenceLocation
    {
        public int Id { set; get; }

        public int GeoFenceId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public virtual GeoFence GeoFence { get; set; }
        public int PointIndex { get; set; }
    }
}
