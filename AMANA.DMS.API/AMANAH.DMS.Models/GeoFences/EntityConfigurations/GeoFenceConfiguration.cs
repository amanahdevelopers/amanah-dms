﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class GeoFenceConfiguration
        : IEntityTypeConfiguration<GeoFence>
    {
        public void Configure(EntityTypeBuilder<GeoFence> geoFenceConfiguration)
        {
            geoFenceConfiguration.HasKey(x => x.Id);

            geoFenceConfiguration.Property(o => o.Name)
                .HasMaxLength(100)
                .IsRequired();

            geoFenceConfiguration.Property(o => o.Description)
               .HasMaxLength(100)
               .IsRequired();

            geoFenceConfiguration.HasMany(x => x.GeoFenceLocations).WithOne(x => x.GeoFence).HasForeignKey(x => x.GeoFenceId);
            geoFenceConfiguration.HasMany(x => x.Branchs).WithOne(x => x.GeoFence).HasForeignKey(x => x.GeoFenceId);



        }
    }
}
