﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class DriverEntityTypeConfiguration
        : IEntityTypeConfiguration<Driver>
    {
        public void Configure(EntityTypeBuilder<Driver> DriverConfiguration)
        {
            DriverConfiguration.HasKey(x => x.Id);

            DriverConfiguration.HasOne(x => x.AgentStatus).WithMany(x => x.Drivers);

            DriverConfiguration.HasOne(x => x.AgentType).WithMany(x => x.Drivers);


        }


    }
}
