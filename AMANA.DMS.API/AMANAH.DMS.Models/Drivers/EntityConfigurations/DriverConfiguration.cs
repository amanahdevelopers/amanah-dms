﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class DriverConfiguration
        : IEntityTypeConfiguration<Driver>
    {
        public void Configure(EntityTypeBuilder<Driver> geoFenceConfiguration)
        {
            geoFenceConfiguration.HasMany(x => x.DriverLoginRequests).WithOne(x => x.Driver).HasForeignKey(x => x.DriverId);
        }
    }
}
