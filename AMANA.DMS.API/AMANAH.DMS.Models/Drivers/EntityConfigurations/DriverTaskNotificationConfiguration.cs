﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class DriverTaskNotificationConfiguration
        : IEntityTypeConfiguration<DriverTaskNotification>
    {
        public void Configure(EntityTypeBuilder<DriverTaskNotification> driverTaskNotificationConfiguration)
        {
            driverTaskNotificationConfiguration.HasKey(x => x.Id);
            driverTaskNotificationConfiguration.Property(o => o.TaskId).IsRequired();
            driverTaskNotificationConfiguration.Property(o => o.DriverId).IsRequired();
        }
    }
}
