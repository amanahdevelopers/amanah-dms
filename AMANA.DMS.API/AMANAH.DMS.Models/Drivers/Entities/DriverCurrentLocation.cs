﻿using System.ComponentModel.DataAnnotations;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class DriverCurrentLocation : BaseEntity
    {

        [Key]
        public int DriverId { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Driver Driver { get; set; }
        public int? BranchId { get; set; }
        public Branch Branch { get; set; }
    }
}