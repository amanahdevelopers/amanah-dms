﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class DriverTaskNotification : BaseEntity
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public string Description { set; get; }

        public string ActionStatus { set; get; }



        public bool? IsRead { set; get; }


        public Driver Driver { set; get; }
        public Tasks Task { set; get; }
    }
}
