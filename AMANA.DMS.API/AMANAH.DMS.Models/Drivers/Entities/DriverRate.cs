﻿using System.ComponentModel.DataAnnotations;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class DriverRate : BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int DriverId { get; set; }
        public int TasksId { get; set; }
        public double Rate { get; set; }
        public string Note { get; set; }

        public virtual Driver Driver { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Tasks Tasks { get; set; }





    }
}
