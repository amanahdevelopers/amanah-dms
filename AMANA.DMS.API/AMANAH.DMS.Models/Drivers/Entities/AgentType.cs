﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class AgentType : BaseGlobalEntity
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public List<Driver> Drivers { set; get; }

    }
}