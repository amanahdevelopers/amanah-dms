﻿using AMANAH.DMS.DATA.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.DATA.EntityConfigurations
{
    class AdminEntityTypeConfiguration : IEntityTypeConfiguration<Admin>
    {

        public void Configure(EntityTypeBuilder<Admin> AdminConfiguration)
        {
            AdminConfiguration.HasKey(x => x.Id);   
        }
    }
}
