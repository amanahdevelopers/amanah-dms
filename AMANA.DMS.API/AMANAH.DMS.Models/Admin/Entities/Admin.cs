﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Admin : BaseEntity
    {
        public string Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public string DeactivationReason { get; set; }
        public int ResidentCountryId { get; set; }

        /// <summary>
        /// Default branch 
        /// </summary>
        public int? BranchId { set; get; }
        // public virtual ApplicationUser User { get; set; }
    }
}
