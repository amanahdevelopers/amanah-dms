﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Restaurant : BaseEntity
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string Reason { get; set; }

        public virtual ICollection<Branch> Branchs { get; set; }

    }
}
