﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Customer : BaseEntity
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        public Country Country { get; set; }
        public int? BranchId { set; get; }
        public Branch Branch { set; get; }
        public Address Location { set; get; }

    }
}
