﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class CustomerEntityTypeConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> CustomerConfiguration)
        {
            CustomerConfiguration.HasKey(x => x.Id);

            CustomerConfiguration.Property(o => o.Name)
                .HasMaxLength(250)
                .IsRequired();

            CustomerConfiguration.Property(o => o.Email).HasMaxLength(250);

            CustomerConfiguration.Property(o => o.Phone)
                .HasMaxLength(50)
                .IsRequired();

            CustomerConfiguration.Property(o => o.Address)
                .IsRequired();

            CustomerConfiguration.Property(o => o.Latitude).HasMaxLength(100);

            CustomerConfiguration.Property(o => o.Longitude).HasMaxLength(100);
            CustomerConfiguration.OwnsOne(x => x.Location);

        }
    }
}
