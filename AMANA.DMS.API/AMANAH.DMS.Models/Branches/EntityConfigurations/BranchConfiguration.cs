﻿using System;
using System.Collections.Generic;
using System.Text;
using AMANAH.DMS.DATA.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.DATA.EntityConfigurations
{
    public class BranchConfiguration : IEntityTypeConfiguration<Branch>
    {


        public void Configure(EntityTypeBuilder<Branch> BranchConfiguration)
        {
            BranchConfiguration.HasKey(x => x.Id);
            BranchConfiguration.Property(o => o.Name)
                .HasMaxLength(250)
                .IsRequired();
            
            BranchConfiguration.Property(o => o.Phone)
                .HasMaxLength(50)
                .IsRequired();
            
            BranchConfiguration.OwnsOne(x => x.Location);
         
            BranchConfiguration.Property(o => o.Address)
                .HasMaxLength(250)
                .IsRequired();

            BranchConfiguration.Property(o => o.Latitude)
                .HasMaxLength(250)
                .IsRequired();

            BranchConfiguration.Property(o => o.Longitude)
                .HasMaxLength(250)
                .IsRequired();
        }
    }
}
