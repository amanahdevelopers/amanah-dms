﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Branch : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public int RestaurantId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }

        public Address Location { set; get; }

        public virtual GeoFence GeoFence { get; set; }
        public virtual Restaurant Restaurant { get; set; }
        //public virtual Manager Manager { get; set; }
        public string Reason { get; set; }
        public List<Customer> Customers { set; get; }

    }

}
