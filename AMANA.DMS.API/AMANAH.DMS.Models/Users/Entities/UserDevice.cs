﻿using System;
using AMANAH.DMS.Entities;

namespace AMANAH.DMS.Models.Entities
{
    public class UserDevice
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string FCMDeviceId { get; set; }

        public Boolean? IsLoggedIn { get; set; }
    }
}
