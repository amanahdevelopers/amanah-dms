﻿using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;
using AMANAH.DMS.Entities;

namespace AMANAH.DMS.Models.Entities
{
    public class Privilge : BaseEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ApplicationRole> Roles { get; set; }
    }
}
