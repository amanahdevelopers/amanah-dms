﻿using System;
using System.Collections.Generic;
using AMANAH.DMS.BASE.Domain.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace AMANAH.DMS.Entities
{
    public class ApplicationRole : IdentityRole, IBaseEntity
    {
        private ApplicationRole()
        {

        }

        public ApplicationRole(string roleName)
          : base(roleName)
        {
        }

        public string CreatedBy_Id { get; private set; }

        public string UpdatedBy_Id { get; private set; }

        public string DeletedBy_Id { get; private set; }

        public bool IsDeleted { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public DateTime UpdatedDate { get; private set; }

        public DateTime DeletedDate { get; private set; }

        public string Tenant_Id { get; private set; }

        public List<Privilge> Privilges { get; set; }

        public int Type { get; set; }
    }
}
