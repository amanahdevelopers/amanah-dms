﻿using AMANAH.DMS.BASE.Domain.Entities;
using AMANAH.DMS.Entities;

namespace AMANAH.DMS.Models.Entities
{
    public class RolePrivilge : BaseEntity
    {
        public int Id { get; set; }

        public int FK_Privilge_Id { get; set; }

        public Privilge Privilge { get; set; }

        public string FK_Role_Id { get; set; }

        public ApplicationRole Role { get; set; }

    }
}
