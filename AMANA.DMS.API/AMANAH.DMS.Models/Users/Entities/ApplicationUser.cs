﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using AMANAH.DMS.BASE.Domain.Entities;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace AMANAH.DMS.Entities
{
    public class ApplicationUser : IdentityUser, IBaseEntity
    {
        public ApplicationUser()
        {
        }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public bool IsAvailable { get; set; }

        public int CountryId { set; get; }

        public bool IsLocked { get; set; }

        public string CreatedBy_Id { get; private set; }

        public string UpdatedBy_Id { get; private set; }

        public string DeletedBy_Id { get; private set; }

        public bool IsDeleted { get; private set; }
        public string Tenant_Id { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public DateTime UpdatedDate { get; private set; }

        public DateTime DeletedDate { get; private set; }

        [NotMapped]
        public List<string> RoleNames { get; set; }

        [NotMapped]
        public List<string> Permissions { get; set; }


        public List<UserDevice> UserDvices { get; set; }

        public Country Country { get; set; }

        //public virtual ICollection<Notification> FromUserNotifications { get; set; }
        //public virtual ICollection<Notification> ToUserNotifications { get; set; }
        public void SetTenant(string tenantId)
        {
            if (!string.IsNullOrWhiteSpace(Tenant_Id))
            {
                throw new InvalidOperationException("Tenant already set to a value.");
            }
            Tenant_Id = tenantId;
        }

    }
}
