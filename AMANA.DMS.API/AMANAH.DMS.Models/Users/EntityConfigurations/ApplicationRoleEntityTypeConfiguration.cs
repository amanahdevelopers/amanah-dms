﻿using AMANAH.DMS.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class ApplicationRoleEntityTypeConfiguration
        : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> ApplicationRoleConfiguration)
        {
            ApplicationRoleConfiguration.Ignore(o => o.Privilges);
        }
    }
}
