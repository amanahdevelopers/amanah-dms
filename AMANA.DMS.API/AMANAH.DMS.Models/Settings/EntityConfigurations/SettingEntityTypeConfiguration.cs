﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace AMANAH.DMS.EntityConfigurations
{
    public class SettingEntityTypeConfiguration
        : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> SettingConfiguration)
        {
            SettingConfiguration.HasKey(x => x.Id);
           

        }
    }
}
