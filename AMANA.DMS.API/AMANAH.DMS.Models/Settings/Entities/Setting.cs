﻿using AMANAH.DMS.BASE.Domain.Entities;

namespace AMANAH.DMS.DATA.Entities
{
    public class Setting : BaseEntity
    {
        public int Id { set; get; }

        public string SettingKey { set; get; }

        public string Value { set; get; }

        public string SettingDataType { set; get; }

        public string GroupId { set; get; }

        private Setting()
        {
        }

        public Setting(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
