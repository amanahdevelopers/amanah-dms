﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;

namespace AMANAH.DMS.BLL.Managers
{
    public class LocationAccuracyManager : BaseManager<LocationAccuracyViewModel, LocationAccuracy>, ILocationAccuracyManager
    {
        public LocationAccuracyManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<LocationAccuracy> repository)
            : base(context, repository, mapper)
        {
        }

    }
}
