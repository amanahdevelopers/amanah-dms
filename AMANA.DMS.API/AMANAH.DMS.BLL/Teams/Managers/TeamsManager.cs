﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class TeamsManager : BaseManager<TeamViewModel, Team>, ITeamsManager
    {
        private readonly ICurrentUser _currentUser;

        public TeamsManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Team> repository,
            ICurrentUser currentUser)
            : base(context, repository, mapper)
        {
            _currentUser = currentUser;
        }

        public async Task<TeamViewModel> Get(int id)
        {
            var team = await context.Teams
                .Include(x => x.Drivers)
                .ThenInclude(x => x.User)
                .Include(x => x.TeamManagers)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            return mapper.Map<Team, TeamViewModel>(team);

        }

        public async Task<List<LkpViewModel>> GetByKeywordAsync(string SearchKey)
        {
            return await context.Teams
                .Where(x =>
                    x.Name.ToLower().Contains(SearchKey.ToLower()))
                .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                .Take(5)
                .ToListAsync();
        }

        public override async Task<List<TeamViewModel>> GetAllAsync<TeamViewModel>()
        {
            var teams = await repository.GetAll()
                .Include(x => x.Drivers)
                .ThenInclude(x => x.User)
                .ToListAsync();
            return mapper.Map<List<Team>, List<TeamViewModel>>(teams);
        }

        public async Task<List<TeamViewModel>> GetAllAsync(List<int> managerTeamIds)
        {
            var teams = await repository.GetAll()
                .Include(x => x.Drivers)
                .ThenInclude(x => x.User)
                .Where(x => managerTeamIds.Contains(x.Id))
                .ToListAsync();
            return mapper.Map<List<Team>, List<TeamViewModel>>(teams);
        }

        public async Task<PagedResult<TeamViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel, List<int> managerTeamIds)
        {
            var pagedResult = new PagedResult<TeamViewModel>();
            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;
            var source = repository.GetAll()
                .Include(x => x.Drivers)
                .ThenInclude(x => x.User)
                .Where(x => managerTeamIds.Contains(x.Id));

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var teams = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            teams.ForEach(x => x.Drivers = x.Drivers.Where(d => d.IsDeleted == false).ToList());
            pagedResult.Result = mapper.Map<List<Team>, List<TeamViewModel>>(teams);

            return pagedResult;
        }

        public override async Task<PagedResult<TeamViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<TeamViewModel>();
#warning if global filter is used for IsDeleted this can be simplified to ToPagedResultAsync
            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;
            var source = repository.GetAll()
                .Include(x => x.Drivers)
                .ThenInclude(x => x.User);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var teams = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            teams.ForEach(x => x.Drivers = x.Drivers.Where(d => d.IsDeleted == false).ToList());
            pagedResult.Result = mapper.Map<List<Team>, List<TeamViewModel>>(teams);

            return pagedResult;
        }

        public async Task<List<int>> GetManagerTeamsAsync()
        {
            var userId = _currentUser.Id;
            if (string.IsNullOrEmpty(userId))
            {
                return new List<int>();
            }
            var query = context.Teams.AsQueryable();
            if (!_currentUser.HasPermission(ManagerPermissions.Team.ReadTeam))
            {
                query = query.Where(team =>
                    team.CreatedBy_Id == _currentUser.UserName ||
                    team.TeamManagers.Where(m => m.Manager.UserId == userId).Count() > 0);
            }
            return await query.Select(team => team.Id).ToListAsync();
        }
    }
}
