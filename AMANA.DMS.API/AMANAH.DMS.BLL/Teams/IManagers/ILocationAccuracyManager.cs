﻿using System.Threading.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ILocationAccuracyManager : IBaseManager<LocationAccuracyViewModel, LocationAccuracy>
    {
    }
}
