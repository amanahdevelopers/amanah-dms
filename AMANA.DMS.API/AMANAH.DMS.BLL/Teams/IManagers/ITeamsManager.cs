﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ITeamsManager : IBaseManager<TeamViewModel, Team>
    {
        Task<TeamViewModel> Get(int id);
        Task<List<LkpViewModel>> GetByKeywordAsync(string SearchKey);
        Task<List<TeamViewModel>> GetAllAsync(List<int> managerTeamIds);
        Task<PagedResult<TeamViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel, List<int> managerTeamIds);
        Task<List<int>> GetManagerTeamsAsync();
    }
}
