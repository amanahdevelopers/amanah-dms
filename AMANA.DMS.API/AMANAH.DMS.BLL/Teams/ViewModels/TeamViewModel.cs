﻿using AMANAH.DMS.BLL.ViewModels;
using System.Collections.Generic;

namespace AMANAH.DMS.ViewModels
{
    public class TeamViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }
        public string LocationAccuracyName { set; get; }
        public List<DriverTeamViewModel> Drivers { get; set; }

        public List<TeamManagerViewModel> TeamManagers { get; set; }
        
    }

}
