﻿namespace AMANAH.DMS.ViewModels
{
    public class LocationAccuracyViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int? Duration { set; get; }
    }
}