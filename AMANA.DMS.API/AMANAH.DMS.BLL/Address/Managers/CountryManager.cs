﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.Managers
{
    class CountryManager : BaseManager<CountryViewModel, Country>, ICountryManager
    {
        public CountryManager(
             ApplicationDbContext context,
             IMapper mapper,
             IRepositry<Country> repository)
             : base(context, repository, mapper)
        {
        }

        public async Task<CountryViewModel> Get(int id)
        {
            return await (context as ApplicationDbContext).Country
                .Where(x => x.Id == id)
                .ProjectTo<CountryViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
    }
}
