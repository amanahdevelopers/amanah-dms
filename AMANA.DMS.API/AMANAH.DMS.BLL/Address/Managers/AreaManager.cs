﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class AreaManager : BaseManager<AreaViewModel, Area> , IAreaManager
    {
        public AreaManager(ApplicationDbContext context, IMapper mapper, IRepositry<Area> repository)
            : base(context, repository, mapper)
        {
        }
    }
}
