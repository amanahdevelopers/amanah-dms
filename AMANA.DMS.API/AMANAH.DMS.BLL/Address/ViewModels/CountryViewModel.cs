﻿namespace AMANAH.DMS.BLL.ViewModels
{
    public class CountryViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string FlagUrl { set; get; }
        public string TopLevel { set; get; }
    }
}
