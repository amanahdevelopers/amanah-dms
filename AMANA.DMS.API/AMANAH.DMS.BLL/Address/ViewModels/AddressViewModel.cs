﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class AddressViewModel
    {
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get;set; }
        public string Floor { get; set; }
        public string Flat { get;set; }

    }
}
