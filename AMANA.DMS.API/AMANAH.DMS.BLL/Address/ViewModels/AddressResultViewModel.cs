﻿namespace AMANAH.DMS.BLL.ViewModels
{
    public class AddressResultViewModel
    {
        public string Address { set; get; }
        public decimal Longtiude { get; set; }
        public decimal Latitude { get; set; }
    }
}
