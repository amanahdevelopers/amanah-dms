﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace AMANAH.DMS.BLL.Managers
{
    public class CachedPACIProvider : IAddressProvider
    {
        private ApplicationDbContext _context { set; get; }
        public int Priority { get; set; }
        protected readonly IMapper _mapper;

        public CachedPACIProvider(
            ApplicationDbContext context,
            IConfiguration configuration,
            IMapper mapper)
        {
            _context = context;
            Priority = configuration.GetSection("Address").GetSection("CachedPACIProvider").GetValue<int>("Priority");
            _mapper = mapper;
        }

        public async Task<AddressResultViewModel> SearchForAddressAsync(AddressViewModel address)
        {
            var PACIResult = await _context.PACI
                .Where(x =>
                    x.AreaName.ToLower().Trim() == address.Area.ToLower().Trim() &&
                    x.StreetName.ToLower().Trim() == address.Street.ToLower().Trim() &&
                    x.BlockName.ToLower().Trim() == address.Block.ToLower().Trim() &&
                    x.BuildingNumber.ToLower().Trim() == address.Building.ToLower().Trim())
                .FirstOrDefaultAsync();
            if (PACIResult != null)
            {
                AddressResultViewModel result = new AddressResultViewModel();
                result.Latitude = PACIResult.Latitude;
                result.Longtiude = PACIResult.Longtiude;
                StringBuilder addressString = new StringBuilder();
                addressString.Append("Kwuit-");
                addressString.Append(PACIResult.GovernorateName);
                addressString.Append("-");
                addressString.Append(PACIResult.AreaName);
                addressString.Append("-Block");
                addressString.Append(PACIResult.BlockName);
                addressString.Append("-Building");
                addressString.Append(PACIResult.BuildingNumber);
                result.Address = addressString.ToString();
                return result;
            }
            return null;
        }
    }
}
