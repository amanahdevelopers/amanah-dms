﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.BLL.ViewModels;

namespace AMANAH.DMS.BLL.IManagers
{
    public class AddressService : IAddressService
    {
        private readonly IReadOnlyCollection<IAddressProvider> _addressProviders;

        public AddressService(IEnumerable<IAddressProvider> addressProviders)
        {
            _addressProviders = addressProviders.OrderBy(x => x.Priority).ToArray();
        }

        public async Task<AddressResultViewModel> SearchForAddressAsync(AddressViewModel address)
        {

            foreach (var addressProvider in _addressProviders)
            {
                var AddressResut = await addressProvider.SearchForAddressAsync(address);
                if (AddressResut != null)
                {
                    return AddressResut;
                }
            }
            return null;
        }
    }
}
