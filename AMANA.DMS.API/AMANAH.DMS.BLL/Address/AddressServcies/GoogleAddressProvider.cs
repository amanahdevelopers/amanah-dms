﻿using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AutoMapper;
using Microsoft.Extensions.Configuration;

namespace AMANAH.DMS.BLL.Managers
{
    public class GoogleAddressProvider : IAddressProvider
    {
        public int Priority { get; set; }

        public GoogleAddressProvider(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<PACI> repository,
            IConfiguration configuration
            )
        {
            Priority = configuration.GetSection("Address").GetSection("GoogleAddressProvider").GetValue<int>("Priority");


        }

        public Task<AddressResultViewModel> SearchForAddressAsync(AddressViewModel address)
        {
            return Task.FromResult<AddressResultViewModel>(null);
        }
    }
}
