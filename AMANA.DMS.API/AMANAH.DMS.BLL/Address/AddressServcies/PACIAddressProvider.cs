﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.AddressServcies;
using AMANAH.DMS.BLL.BLL.Settings;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Utilites.PACI;

namespace AMANAH.DMS.BLL.Managers
{
    public class PACIAddressProvider : IAddressProvider
    {
        private readonly PACIServiceSettings _settings;
        private readonly ApplicationDbContext _context;

        public int Priority { get; }

        public PACIAddressProvider(
            ApplicationDbContext context,
            IOptions<PACIServiceSettings> settings,
            IConfiguration configuration)
        {
            _settings = settings.Value;
            _context = context;
            Priority = configuration.GetValue<int>("Address:PACIAddressProvider:Priority");
        }


        public async Task<AddressResultViewModel> SearchForAddressAsync(AddressViewModel address)
        {
            if (string.IsNullOrWhiteSpace(address.Street) || string.IsNullOrWhiteSpace(address.Block))
            {
                return null;
            }
            var defaultValue = default(StreetPACIResultModel);
            var policy = PolicyHelper.TimeoutEachCallAndRetry(
                _settings.RequestTimeOut,
                _settings.NumberOfRetris,
                defaultValue,
                value => value == defaultValue);
            var paciResult = await policy.ExecuteAsync(() =>
                PACIHelper.GetStreetPACItModelAsync(
                    address.Street,
                    address.Block,
                    _settings.ProxyUrl,
                    _settings.StreetServiceUrl,
                    _settings.StreetFieldNameStreetService,
                    _settings.BlockNameFieldNameStreetService));
            var addressResult = Map(paciResult);
            if (addressResult != null)
            {
                await SaveResultToCacheAsync(address, addressResult);
            }
            return addressResult;
        }

        private Task SaveResultToCacheAsync(AddressViewModel address, AddressResultViewModel addressResult)
        {
            PACI paci = new PACI
            {
                AreaName = address.Area,
                BlockName = address.Block,
                StreetName = address.Street,
                BuildingNumber = address.Building,
                Latitude = addressResult.Latitude,
                Longtiude = addressResult.Longtiude
            };
            _context.PACI.Add(paci);
            return _context.SaveChangesAsync();
        }

        private AddressResultViewModel Map(StreetPACIResultModel streetPacItModel)
        {
            if (streetPacItModel != null && streetPacItModel.features.Any())
            {
                if (streetPacItModel.features[0] != null)
                {
                    return new AddressResultViewModel
                    {
                        Address = streetPacItModel.features[0].attributes.DetailsEnglish,
                        Latitude = streetPacItModel.features[0].attributes.CENTROID_Y.Value,
                        Longtiude = streetPacItModel.features[0].attributes.CENTROID_X.Value
                    };
                }
            }
            return null;
        }
    }
}

