﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.AddressServcies;
using AMANAH.DMS.BLL.BLL.Settings;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Utilites.PACI;

namespace AMANAH.DMS.BLL.IManagers
{
    public class PACIService : IPACIService
    {
        private readonly PACIServiceSettings _settings;
        private readonly ApplicationDbContext _context;

        public PACIService(
            IOptions<PACIServiceSettings> settings,
            ApplicationDbContext context)
        {
            _settings = settings.Value;
            _context = context;
        }

        public async Task<List<PACIHelper.DropPACI>> GetStreetsAsync(int? areaId, string blockName)
        {
            var area = await _context.Area.FirstOrDefaultAsync(x => x.Id == areaId.ToString());
            var defaultValue = default(List<PACIHelper.DropPACI>);
            var result = defaultValue;
            if (area != null)
            {
                int.TryParse(area.FK_Governrate_Id, out var governrateId);
                var policy = PolicyHelper.TimeoutEachCallAndRetry(
                    _settings.RequestTimeOut,
                    _settings.NumberOfRetris,
                    defaultValue,
                    result => result == defaultValue);
                result = await policy.ExecuteAsync(() =>
                    PACIHelper.GetStreetsAsync(
                        governrateId,
                        _settings.GovernorateIdFieldNameAreaService,
                        areaId, _settings.AreaIdFieldNameStreetService,
                        blockName,
                        _settings.BlockNameFieldNameStreetService,
                        _settings.ProxyUrl,
                        _settings.StreetServiceUrl));
                // TODO: Cache the result

            }
            return result;
        }

        public async Task<List<PACIHelper.DropPACI>> GetBlocksAsync(int? areaId)
        {
            var defaultValue = default(List<PACIHelper.DropPACI>);
            var policy = PolicyHelper.TimeoutEachCallAndRetry(
                _settings.RequestTimeOut,
                _settings.NumberOfRetris,
                defaultValue,
                result => result == defaultValue);
            var result = await policy.ExecuteAsync(() =>
                 PACIHelper.GetBlocksAsync(
                     areaId,
                     _settings.AreaIdFieldNameBlockService,
                     _settings.ProxyUrl,
                     _settings.BlockServiceUrl));
            // TODO: Cache the result
            return result;
        }
    }
}
