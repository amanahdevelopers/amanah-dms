﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.PACI;

namespace AMANAH.DMS.BLL.Providers
{
    public interface IPACIService
    {
        Task<List<PACIHelper.DropPACI>> GetStreetsAsync(int? areaId, string blockName);

        Task<List<PACIHelper.DropPACI>> GetBlocksAsync(int? areaId);
    }
}
