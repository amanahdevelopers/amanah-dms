﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;

namespace AMANAH.DMS.BLL.Providers
{
    public interface IAddressProvider
    {
        public int Priority { get; }

        Task<AddressResultViewModel> SearchForAddressAsync(AddressViewModel address);
    }
}
