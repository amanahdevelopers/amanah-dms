﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IAddressService 
    {
        Task<AddressResultViewModel> SearchForAddressAsync(AddressViewModel address);
    
    }
}
