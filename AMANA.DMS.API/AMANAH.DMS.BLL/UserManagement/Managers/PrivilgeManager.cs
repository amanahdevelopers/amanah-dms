﻿using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.Models.Entities;
using AMANAH.DMS.Repoistry;

namespace AMANAH.DMS.BLL.Managers
{
    // TODO: cleanup code this is a repository or a service? prefer the composition over inheritance, just inject the repo and use it
    public class PrivilgeManager : Repositry<Privilge>, IPrivilgeManager
    {
        public PrivilgeManager(ApplicationDbContext context)
            : base(context)
        {
        }

    }
}
