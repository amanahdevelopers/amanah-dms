﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AMANAH.DMS.BASE;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.Entities;
using Microsoft.EntityFrameworkCore;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers
{
    public class ManagerAccessControlManager : IManagerAccessControlManager
    {
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;

        public ManagerAccessControlManager(
            IApplicationRoleManager applicationRoleManager,
            ApplicationDbContext context,
            ILocalizer localizer)
        {
            _applicationRoleManager = applicationRoleManager;
            _context = context;
            _localizer = localizer;
        }

        public async Task<bool> Create(ManagerAccessControlViewModel input)
        {
            if (await _applicationRoleManager.IsRoleExistAsync(input.RoleName.Trim()))
            {
                throw new DomainException(Keys.Validation.RoleAlreadyExists);
            }
            else
            {
                var _role = new ApplicationRole(input.RoleName);
                _role.Type = (int)RoleType.Manager;
                await _applicationRoleManager.AddRoleAsync(_role);

                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, RoleType.Manager);
                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }

        }

        public async Task<bool> Delete(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, RoleType.Manager))
            {
                throw new NotFoundException(Keys.Validation.RoleNotFound);
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, RoleType.Manager);
                if (role == null)
                {
                    throw new NotFoundException(Keys.Validation.RoleNotFound);
                }
                bool isUsed = await _context.UserRoles.AnyAsync(x => x.RoleId == role.Id);
                if (isUsed)
                {
                    throw new DomainException(Keys.Validation.CanNotDeleteRoleAssignedToManagers);
                }
                return await _applicationRoleManager.DeleteRoleAsync(role);
            }
        }

        public async Task<ManagerAccessControlViewModel> Get(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, RoleType.Manager))
            {
                throw new NotFoundException(Keys.Validation.RoleNotFound);
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, RoleType.Manager);
                return new ManagerAccessControlViewModel
                {
                    Id = role.Id,
                    RoleName = role.Name,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList()
                };
            }
        }

        public async Task<List<ManagerAccessControlViewModel>> GetAll()
        {
            List<ManagerAccessControlViewModel> lst = new List<ManagerAccessControlViewModel>();

            var roles = _applicationRoleManager.GetAllRoles(RoleType.Manager);


            foreach (var r in roles)
            {
                lst.Add(new ManagerAccessControlViewModel
                {
                    RoleName = r.Name,
                    CreationDate = r.CreatedDate,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList()
                });


            }

            //roles.ForEach(async r =>
            //{
            //    lst.Add(new ManagerAccessControlViewModel
            //    {
            //        RoleName = r.Name,
            //        CreationDate = r.CreatedDate,
            //        Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList()
            //    });
            //});


            return lst.OrderByDescending(x => x.CreationDate).ToList();
        }

        public Task<List<FeaturePermissionViewModel>> GetAllPermissions()
        {
            var result = ManagerPermissions.FeaturePermissions
                .Select(feature =>
                    new FeaturePermissionViewModel
                    {
                        Name = _localizer[feature.Name],
                        Permissions = feature.Permissions
                            .Select(permission =>
                                new PermissionViewModel
                                {
                                    Value = permission,
                                    Name = _localizer[permission]
                                })
                            .ToList()
                    })
                .ToList();
            return Task.FromResult(result);
        }


        public async Task<bool> Update(ManagerAccessControlViewModel input)
        {

            //if (!await _applicationRoleManager.(input.RoleName, RoleType.Manager))
            //{

            //}


            if (!await _applicationRoleManager.IsRoleExistAsync(input.RoleName, RoleType.Manager))
            {
                throw new NotFoundException(Keys.Validation.RoleNotFound);
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, RoleType.Manager);
                var existsClaims = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();

                var addedPermissions = input.Permissions
                    .Except(existsClaims, StringComparer.OrdinalIgnoreCase)
                    .ToArray();
                var removedPermissions = existsClaims.Except(input.Permissions, StringComparer.OrdinalIgnoreCase)
                    .ToArray();
                foreach (var c in removedPermissions)
                {
                    await _applicationRoleManager.RemoveClaimAsync(role, c);
                }
                foreach (var permission in addedPermissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }
        }
    }
}
