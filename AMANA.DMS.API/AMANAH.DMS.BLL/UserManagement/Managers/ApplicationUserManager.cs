﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.BLL.IManagers;
using AMANAH.DMS.BLL.BLL.Settings;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.Settings.Resources;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Account;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using AMANAH.DMS.Settings;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers
{
    public class ApplicationUserManager : IApplicationUserManager
    {
        private readonly UserManager<ApplicationUser> _identityUserManager;
        private readonly RoleManager<ApplicationRole> _identityRoleManager;
        private readonly ApplicationDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly EmailSettings _emailSettings;
        private readonly IEmailSenderservice _emailsender;
        private readonly AppSettings _appSettings;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly ILocalizer _localizer;
        private readonly IdentityErrorDescriber _identityErrorDescriber;
        private readonly ITokenGeneratorService _tokenGeneratorService;
        private readonly IApplicationRoleManager _appllicationRoleManager;
        private readonly IConfiguration _configuration;


        private IdentityError UserNotFound =>
            new IdentityError
            {
                Code = "UserNotFound",
                Description = _localizer[Keys.Validation.UserNotFound]
            };

        private IdentityError UserDeleted =>
            new IdentityError
            {
                Code = "UserDeleted",
                Description = _localizer[Keys.Validation.UserIsDeleted]
            };

        private IdentityError InvalidLogin =>
            new IdentityError
            {
                Code = "InvalidLogin",
                Description = _localizer[Keys.Validation.InvalidUserNameOrPassword]
            };

        private IdentityError InvalidTokenError =>
            new IdentityError
            {
                Code = "InvalidToken",
                Description = _localizer[Keys.Validation.InvalidToken]
            };

        //private IdentityError GeneralError =>
        //    new IdentityError
        //    {
        //        Code = "GeneralError",
        //        Description = _localizer[Keys.Validation.GeneralError]
        //    };

        private IdentityError UserHasNoRole =>
            new IdentityError
            {
                Code = "UserNoRole",
                Description = _localizer[Keys.Validation.UserHasNoRole]
            };

        private IdentityError UserNotDriver =>
            new IdentityError()
            {
                Code = "NotDriver",
                Description = _localizer[Keys.Validation.UserIsNotRegisteredAsDriver]
            };

        private IdentityError BlockedDriver =>
            new IdentityError()
            {
                Code = "NotDriver",
                Description = _localizer[Keys.Validation.DriverAccountBlocked]
            };

        private IdentityError CanNotSendResetEmail =>
            new IdentityError
            {
                Code = "SendResetEmailError",
                Description = _localizer[Keys.Validation.SendResetEmailError]
            };

        public ApplicationUserManager(
            IApplicationRoleManager appllicationRoleManager,
            IConfiguration configuration,
            ApplicationDbContext dbContext,
            UserManager<ApplicationUser> identityUserManager,
            RoleManager<ApplicationRole> identityRoleManager,
            IMapper mapper,
            IOptions<EmailSettings> emailSettings,
            IEmailSenderservice emailsender,
            IOptions<AppSettings> appSettings,
            IUserDeviceManager userDeviceManager,
            ILocalizer localizer,
            IdentityErrorDescriber identityErrorDescriber,
            ITokenGeneratorService tokenGeneratorService)
        {
            _identityUserManager = identityUserManager;
            _identityRoleManager = identityRoleManager;
            _dbContext = dbContext;
            _mapper = mapper;
            _emailSettings = emailSettings.Value;
            _emailsender = emailsender;
            _appSettings = appSettings.Value;
            _configuration = configuration;
            _userDeviceManager = userDeviceManager;
            _localizer = localizer;
            _identityErrorDescriber = identityErrorDescriber;
            _tokenGeneratorService = tokenGeneratorService;
            _appllicationRoleManager = appllicationRoleManager;
        }


        /// <summary>
        /// Method to add user in the database using identity provider.
        /// </summary>
        /// <param name="user">user object to be added.</param>
        /// <returns>id of the added user in case of operation succeed. null in case of failure.</returns>

        public async Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users)
        {
            foreach (var usr in users)
            {
                await AddUserAsync(usr.Item1, usr.Item2);
            }

        }

        public async Task<UserManagerResult> AddTenantAsync(RegistrationViewModel model)
        {
            ApplicationUser applicationUser = new ApplicationUser()
            {
                FirstName = model.FullName,
                Email = model.Email,
                UserName = model.Email,
                RoleNames = new List<string> { StaticRoleNames.Tenant },
                PhoneNumber = model.PhoneNumber,
                CountryId = model.CountryId
            };
            var res = await AddUserAsync(applicationUser, model.Password);
            if (res.Succeeded)
            {
                applicationUser.SetTenant(res.User.Id);
                await _identityUserManager.UpdateAsync(applicationUser);

                var emailBody = MailTemplates.RegistrationMail;
                var FrontEndURL = _configuration.GetValue<string>("FrontEndURL");


                var body = string.Format(emailBody, applicationUser.Email, FrontEndURL);

                try
                {
                    await _emailsender.SendEmailAsync("Welcome to D-Hub", body, res.User.Email);
                }
                catch
                {
                }
            }
            return res;
        }

        public async Task<UserManagerResult> AddUserAsync(string email, string password)
        {
            ApplicationUser applicationUser = new ApplicationUser()
            {
                Email = email,
                UserName = email
            };
            return await AddUserAsync(applicationUser, password);
        }
        public async Task<UserManagerResult> AddUserAsync(ApplicationUser user, string password)
        {
            var result = new UserManagerResult();
            try
            {
                if (user.RoleNames != null && user.RoleNames.Count > 0)
                {
                    //user.SecurityStamp = Guid.NewGuid().ToString("D");
                    var identityResult = await _identityUserManager.CreateAsync(user, password);
                    result = _mapper.Map<UserManagerResult>(identityResult);
                    if (identityResult.Succeeded)
                    {
                        result.User = _mapper.Map<UserViewModel>(user);
                        await AddUserToRolesAsync(user);
                    }
                    return result;
                }
                result.Errors.Add(UserHasNoRole);

                return result;

            }
            catch (Exception)
            {
                result.Succeeded = false;
                //result. = ;
                return result;
            }

        }

        public async Task<LoginResult> LoginAsync(LoginViewModel model)
        {
            var result = new LoginResult();

            var user = await GetByEmailAsync(model.Email);
            if (user == null)
            {
                result.Errors.Add(UserNotFound);
                return result;
            }

            if (user.IsDeleted == true)
            {
                result.Errors.Add(UserDeleted);
                return result;
            }
            var isPasswordValid = await _identityUserManager.CheckPasswordAsync(user, model.Password);
            if (!isPasswordValid)
            {
                result.Errors.Add(_identityErrorDescriber.PasswordMismatch());
            }
            var tokenResponse = new DMSTokenResponse
            {
                AccessToken = await _tokenGeneratorService.GenerateJWTTokenAsync(user)
            };
            if (tokenResponse.AccessToken == null)
            {
                result.Errors.Add(InvalidLogin);
                return result;
            }
            user.RoleNames = (await GetRolesAsync(user)).ToList();

            // TODO: use direct relationship between user and userroles just add collection in user model
            var role = await _appllicationRoleManager.GetRoleAsyncByName(user.RoleNames.FirstOrDefault(), RoleType.Manager);
            if (role != null)
            {
                user.Permissions = (await _appllicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();
            }

            result.User = _mapper.Map<ApplicationUser, UserViewModel>(user);

            result.TokenResponse = tokenResponse;
            return result;
        }

        public async Task<ApplicationUser> GetByUserNameOrEmailAsync(string usernameOrEmail)
        {
            var user = await _identityUserManager.FindByNameAsync(usernameOrEmail);
            if (user == null)
            {
                user = await _identityUserManager.FindByEmailAsync(usernameOrEmail);
                if (user == null)
                {
                    return null;
                }
            }

            return user.IsDeleted ? null : user;
        }

        public async Task<AgentLoginResult> DriverLoginAsync(AgentLoginViewModel model)
        {
            var result = new AgentLoginResult();
            var user = await GetByUserNameOrEmailAsync(model.Email);

            if (user == null)
            {
                result.Succeeded = false;
                result.Errors.Add(UserNotFound);
                return result;
            }

            if (user.IsDeleted == true)
            {
                result.Succeeded = false;
                result.Errors.Add(UserDeleted);
                return result;
            }
            var isPasswordValid = await _identityUserManager.CheckPasswordAsync(user, model.Password);
            if (!isPasswordValid)
            {
                result.Errors.Add(_identityErrorDescriber.PasswordMismatch());
            }
            var driver = _dbContext.Driver.FirstOrDefault(x => x.UserId == user.Id);
            if (driver == null)
            {
                result.Succeeded = false;
                result.Errors.Add(UserNotDriver);
                return result;
            }

            if (driver.AgentStatusId == (int)Enums.AgentStatusesEnum.Blocked)
            {
                result.Succeeded = false;
                result.Errors.Add(BlockedDriver);
                return result;
            }


            var tokenResponse = new DMSTokenResponse
            {
                AccessToken = await _tokenGeneratorService.GenerateJWTTokenAsync(user, driver)
            };

            if (tokenResponse.AccessToken == null)
            {
                result.Errors.Add(InvalidLogin);
                return result;
            }
            user.RoleNames = (await GetRolesAsync(user)).ToList();
            var role = await _appllicationRoleManager.GetRoleAsyncByName(user.RoleNames.FirstOrDefault(), RoleType.Agent);
            if (role != null)
            {
                user.Permissions = (await _appllicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();
            }

            var userDevice = new UserDevice
            {
                FCMDeviceId = model.FCMDeviceId,
                UserId = user.Id,
                Version = model.Version,
                DeviceType = model.DeviceType,
                IsLoggedIn = true
            };

            await _userDeviceManager.AddIfNotExistAsync(userDevice);
            _userDeviceManager.SetUserDeviceLoggedIn(userDevice);
            result.User = _mapper.Map<ApplicationUser, UserViewModel>(user);
            result.TokenResponse = tokenResponse;
            return result;
        }


        public async Task<bool> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword)
        {
            bool result = false;
            IdentityResult identityResult = await _identityUserManager.ChangePasswordAsync(user, oldPassword, newPassword);
            if (identityResult.Succeeded)
            {
                result = true;
            }
            return result;

        }

        public async Task<bool> UpdateUserAsync(ApplicationUser user)
        {
            bool result = false;
            var olduser = await _identityUserManager.FindByIdAsync(user.Id);
            var oldRoles = (await GetRolesAsync(olduser.UserName)).ToList();
            var identityRMVRolResult = await _identityUserManager.RemoveFromRolesAsync(olduser, oldRoles);
            if (identityRMVRolResult.Succeeded)
            {
                CopyProps(olduser, user);
                IdentityResult identityResult = await _identityUserManager.UpdateAsync(olduser);
                if (identityResult.Succeeded)
                {
                    result = true;

                    if (olduser.RoleNames != null)
                    {
                        result = true;
                        var identityAddRolResult = await _identityUserManager.AddToRolesAsync(olduser, user.RoleNames);
                        if (identityAddRolResult.Succeeded)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            else
            {
                result = false;
            }
            return result;
        }

        public Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {



            return _identityUserManager.UpdateAsync(user);
        }

        public async Task<bool> ChangeUserAvailabiltyAsync(string userId, bool isAvailable)
        {
            bool result = false;
            var user = await _identityUserManager.FindByIdAsync(userId);
            user.IsAvailable = isAvailable;
            var identityResult = await _identityUserManager.UpdateAsync(user);
            if (identityResult.Succeeded)
            {
                result = true;
            }
            return result;
        }

        public string HashPassword(ApplicationUser applicationUser, string newPassword)
        {
            return _identityUserManager.PasswordHasher.HashPassword(applicationUser, newPassword);
        }

        public async Task<ApplicationUser> GetByEmailAsync(string email)
        {
            ApplicationUser user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                if (user.IsDeleted)
                {
                    return null;
                }
            }

            return user;
        }

        public async Task<ApplicationUser> GetByUserIdAsync(string userId)
        {
            ApplicationUser user = await _identityUserManager.FindByIdAsync(userId);

            if (user != null)
            {
                if (user.IsDeleted)
                {
                    return null;
                }
            }
            return user;
        }

        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            var result = false;
            user = await GetAsync(user);
            var callBack = await _identityUserManager.DeleteAsync(user);
            if (callBack.Succeeded)
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> SoftDeleteAsync(ApplicationUser user)
        {
            user = await GetAsync(user);
            user.Email = user.Email + "_Deleted";
            user.UserName = user.UserName + "_Deleted";
            var result = await _identityUserManager.DeleteAsync(user);
            return result.Succeeded;
        }

        public async Task<ApplicationUser> GetAsync(ApplicationUser user)
        {
            if (user.UserName != null)
            {
                user = await _identityUserManager.FindByNameAsync(user.UserName);
            }
            else if (user == null && user.Email != null)
            {
                user = await _identityUserManager.FindByEmailAsync(user.Email);
            }
            else if (user == null && user.Id != null)
            {
                user = await _identityUserManager.FindByIdAsync(user.Id);
            }

            if (user != null)
            {
                user.RoleNames = (await GetRolesAsync(user)).ToList();
            }
            if (user != null && user.IsDeleted)
            {
                return null;
            }
            return user;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var result = _identityUserManager.Users
                .OrderByDescending(x => x.CreatedDate)
                .ToList();
            // TODO: use roles directly from user model, use dbcontext to load users, include UserRoles then include Role
            foreach (var user in result)
            {
                if (user != null)
                {
                    var userRoles = (await GetRolesAsync(user)).ToList();
                    if (userRoles != null && userRoles.Count > 0)
                    {
                        user.RoleNames = userRoles;
                    }
                    // result.Add(user);
                }
            }
            return result;
        }

        public List<ApplicationUser> GetByUserIds(List<string> userIds)
        {
            return _identityUserManager.Users.Where(usr => userIds.Contains(usr.Id)).ToList();
        }

        public List<ApplicationUser> GetAllExcept(List<string> userIds)
        {
            return _identityUserManager.Users.Where(usr => !userIds.Contains(usr.Id) && usr.IsDeleted != true).ToList();
        }

        public async Task<List<ApplicationUser>> GetAdmins(List<string> userIdsExclude = null)
        {
            return await GetByRole(StaticRoleNames.Admin);
        }

        public async Task<List<ApplicationUser>> GetByRole(string roleName, List<string> userIdsExclude = null)
        {
            var result = (await _identityUserManager.GetUsersInRoleAsync(roleName)).ToList();
            if (userIdsExclude != null)
            {
                result = result.Where(usr => !userIdsExclude.Contains(usr.Id)).ToList();
            }
            foreach (var user in result)
            {
                if (user != null)
                {
                    var userRoles = (await GetRolesAsync(user)).ToList();
                    if (userRoles != null && userRoles.Count > 0)
                    {
                        user.RoleNames = userRoles;
                    }
                }
            }
            return result;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            return await _identityUserManager.GetClaimsAsync(user);
        }

        public async Task<IList<string>> GetRolesAsync(string userName)
        {

            var user = await GetByUserNameOrEmailAsync(userName);
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<bool> AddUserToRoleAsync(string userName, string roleName)
        {
            bool result = false;
            var applicationUser = await GetByEmailAsync(userName);
            if (applicationUser != null)
            {
                if (roleName != null)
                {
                    var role = await _identityRoleManager.FindByNameAsync(roleName);
                    IdentityResult roleResult = await _identityUserManager.AddToRoleAsync(applicationUser, role.Name);
                    if (roleResult.Succeeded)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }

            }
            return result;
        }

        public async Task AddUsersToRolesAsync(List<ApplicationUser> users)
        {
            foreach (var user in users)
            {
                await AddUserToRolesAsync(user);
            }
        }
        public async Task AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                foreach (var role in user.RoleNames)
                {
                    if (await _identityRoleManager.FindByNameAsync(role) == null)
                    {
                        await _identityRoleManager.CreateAsync(new ApplicationRole(role));
                    }
                }
                await _identityUserManager.AddToRolesAsync(user, user.RoleNames);
            }
        }
        public async Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole)
        {
            bool result = false;
            if (applicationUser != null)
            {
                IdentityResult roleResult = await _identityUserManager.AddToRoleAsync(applicationUser, applicationRole.Name);
                if (roleResult.Succeeded)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public async Task<bool> IsUserNameExistAsync(string userName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async Task<bool> IsEmailExistAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void CopyProps(ApplicationUser oldUser, ApplicationUser user)
        {
            oldUser.PhoneNumber = user.PhoneNumber;
            oldUser.RoleNames = user.RoleNames;
            oldUser.UserName = user.UserName;
            oldUser.Email = user.Email;
            oldUser.FirstName = user.FirstName;
            oldUser.MiddleName = user.MiddleName;
            oldUser.LastName = user.LastName;
            oldUser.RoleNames = user.RoleNames;
            oldUser.CountryId = user.CountryId;

        }
        public async Task<bool> IsAllowToDeleteAdmin()
        {
            try
            {
                // TODO: user repository or dbcontext instead, IQueryable.Where....CountAsync()
                var admincount = (await _identityUserManager.GetUsersInRoleAsync("Admin")).Count();

                if (admincount > 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception)
            {
                return false;
            }
        }

        public Task<bool> IsPhoneExist(string Phone)
        {
            return _dbContext.Users.AnyAsync(x => x.PhoneNumber == Phone);
        }

        public async Task<bool> IsUserNameExistAsync(string userName, string excludedUserId)
        {

            var user = await _identityUserManager.FindByNameAsync(userName);
            return user != null  && user.Id != excludedUserId;
        }

        public async Task<UserManagerResult> ForgotPassword(string email)
        {
            var result = new UserManagerResult();
            try
            {
                var user = await GetByEmailAsync(email);
                if (user == null)
                {
                    result.Errors.Add(UserNotFound);
                    return result;
                }

                var token = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
                token = WebUtility.UrlEncode(token);
                var resetUrl = string.Format(_emailSettings.ResetCallBackUrl, token, user.UserName);
                //resetUrl = WebUtility.UrlEncode(resetUrl);
                var callbackUrl = "<a href='" + resetUrl + "'>Reset Password</a>";
                var emailBody = MailTemplates.ForgetPasswordEmail;


                var body = string.Format(emailBody, email, resetUrl);


                await _emailsender.SendEmailAsync("D-Hub Password Reset", body, user.Email);
                return result;
            }
            catch (Exception)
            {
                result.Errors.Add(CanNotSendResetEmail);
                return result;
            }
        }
        public async Task<UserManagerResult> ResetPasswordAsync(string email, string token, string newPassword)
        {
            var result = new UserManagerResult();
            var user = await GetByEmailAsync(email);
            if (user == null)
            {
                result.Errors.Add(UserNotFound);
                return result;
            }
            var res = await _identityUserManager.ResetPasswordAsync(user, token, newPassword);
            result = _mapper.Map<UserManagerResult>(res);
            return result;
        }
        public async Task<string> GeneratePasswordResetTokenAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            return await _identityUserManager.GeneratePasswordResetTokenAsync(user);
        }
        public async Task<UserManagerResult> CheckResetToken(string email, string token)
        {
            var result = new UserManagerResult();
            var user = await GetByEmailAsync(email);
            if (user == null)
            {
                result.Errors.Add(InvalidTokenError);
                return result;
            }
            var valid = await _identityUserManager.VerifyUserTokenAsync(user, _identityUserManager.Options.Tokens.PasswordResetTokenProvider, "ResetPassword", token);
            if (!valid)
            {
                result.Errors.Add(InvalidTokenError);
            }
            return result;

        }
        public async Task<bool> ChangePasswordAsync(ApplicationUser user, string newPassword)
        {
            await _identityUserManager.RemovePasswordAsync(user);
            var res = await _identityUserManager.AddPasswordAsync(user, newPassword);
            return res.Succeeded;
        }


    }

    public interface ITokenGeneratorService
    {
        Task<string> GenerateJWTTokenAsync(ApplicationUser user, Driver driver = default);
    }
}
