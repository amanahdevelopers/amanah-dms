﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class UserDeviceManager : IUserDeviceManager
    {
        private readonly ApplicationDbContext _dbContext;
        public UserDeviceManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddIfNotExistAsync(UserDevice userDevice)
        {
            var count = await _dbContext.UserDevice.CountAsync(x => x.UserId == userDevice.UserId);
            if (count > 0)
            {
                var isDeviceExist = await _dbContext.UserDevice
                    .CountAsync(x =>
                        x.UserId == userDevice.UserId &&
                        x.FCMDeviceId == userDevice.FCMDeviceId) > 0;
                if (!isDeviceExist)
                {
                    _dbContext.UserDevice.Add(userDevice);
                }
            }
            else
            {
                _dbContext.UserDevice.Add(userDevice);
            }
            await _dbContext.SaveChangesAsync();
        }

        public bool SetUserDeviceLoggedIn(UserDevice userDevice)
        {
            var userDeviceFromDatabase = _dbContext.UserDevice
                .Where(x =>
                    x.UserId == userDevice.UserId &&
                    x.FCMDeviceId == userDevice.FCMDeviceId)
                .FirstOrDefault();
            if (userDeviceFromDatabase != null)
            {
                userDeviceFromDatabase.IsLoggedIn = userDevice.IsLoggedIn;
                _dbContext.SaveChanges();
                return true;

            }
            return false;
        }
        public List<UserDevice> GetByUserId(string userId)
        {
            var userDevices = _dbContext.UserDevice.Where(x => x.UserId == userId).ToList();
            return userDevices;
        }

        public bool DeleteDevice(string token)
        {
            var userDevice = _dbContext.UserDevice.FirstOrDefault(x => x.FCMDeviceId == token);
            if (userDevice != null)
            {
                _dbContext.Remove(userDevice);
                return _dbContext.SaveChanges() > 0;
            }
            return false;
        }
    }
}
