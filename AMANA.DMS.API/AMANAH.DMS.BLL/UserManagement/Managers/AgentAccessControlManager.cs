﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AMANAH.DMS.BASE;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Entities;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers
{
    public class AgentAccessControlManager : IAgentAccessControlManager
    {
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ILocalizer _localizer;

        public AgentAccessControlManager(
            IApplicationRoleManager applicationRoleManager,
            ILocalizer localizer)
        {
            _applicationRoleManager = applicationRoleManager;
            _localizer = localizer;
        }

        public async Task<bool> Create(AgentAccessControlViewModel input)
        {
            if (await _applicationRoleManager.IsRoleExistAsync(input.RoleName))
            {
                throw new DomainException(Keys.Validation.RoleAlreadyExists);
            }
            else
            {
                var _role = new ApplicationRole(input.RoleName);
                _role.Type = (int)RoleType.Agent;
                await _applicationRoleManager.AddRoleAsync(_role);

                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, RoleType.Agent);
                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }

        }

        public async Task<bool> Delete(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, RoleType.Agent))
            {
                throw new NotFoundException(Keys.Validation.RoleNotFound);
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, RoleType.Agent);
                if (role != null)
                    return await _applicationRoleManager.DeleteRoleAsync(role);
                return false;
            }
        }

        public async Task<AgentAccessControlViewModel> Get(string roleName)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(roleName, RoleType.Agent))
            {
                throw new NotFoundException(Keys.Validation.RoleNotFound);
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(roleName, RoleType.Agent);
                return new AgentAccessControlViewModel
                {
                    RoleName = role.Name,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList()
                };
            }
        }

        public async Task<List<AgentAccessControlViewModel>> GetAll()
        {
            List<AgentAccessControlViewModel> lst = new List<AgentAccessControlViewModel>();
            var roles = _applicationRoleManager.GetAllRoles(RoleType.Agent);

            foreach (var r in roles)
            {


                lst.Add(new AgentAccessControlViewModel
                {
                    RoleName = r.Name,
                    CreationDate = r.CreatedDate,
                    Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList()
                });


            }


            //roles.ForEach(async r =>
            //{
            //    lst.Add(new AgentAccessControlViewModel
            //    {
            //        CreationDate = r.CreatedDate,
            //        RoleName = r.Name, Permissions = (await _applicationRoleManager.GetClaimsAsync(r)).Select(t => t.Value).ToList() });
            //});
            return lst.OrderByDescending(x => x.CreationDate).ToList();
        }

        public Task<List<FeaturePermissionViewModel>> GetAllPermissions()
        {
            //TODO: need to reuse same code in ManagerPermissions...
            var result = AgentPermissions.FeaturePermissions
                .Select(feature =>
                    new FeaturePermissionViewModel
                    {
                        Name = _localizer[feature.Name],
                        Permissions = feature.Permissions
                            .Select(permission =>
                                new PermissionViewModel
                                {
                                    Value = permission,
                                    Name = _localizer[permission]
                                })
                            .ToList()
                    })
                .ToList();
            return Task.FromResult(result);
        }

        public async Task<bool> Update(AgentAccessControlViewModel input)
        {
            if (!await _applicationRoleManager.IsRoleExistAsync(input.RoleName, RoleType.Agent))
            {
                throw new NotFoundException(Keys.Validation.RoleNotFound);
            }
            else
            {
                var role = await _applicationRoleManager.GetRoleAsyncByName(input.RoleName, RoleType.Agent);
                var existsClaims = (await _applicationRoleManager.GetClaimsAsync(role)).Select(t => t.Value).ToList();

                foreach (var c in existsClaims)
                {
                    await _applicationRoleManager.RemoveClaimAsync(role, c);
                }
                foreach (var permission in input.Permissions)
                {
                    await _applicationRoleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                }
                return true;
            }
        }
    }
}
