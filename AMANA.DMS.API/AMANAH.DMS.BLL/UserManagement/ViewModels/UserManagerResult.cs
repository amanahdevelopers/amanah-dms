﻿using AMANAH.DMS.BLL.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Account
{
    public class UserManagerResult
    {
        public UserManagerResult()
        {
            Errors = new List<IdentityError>();
            Succeeded = true;
        }

        public ICollection<IdentityError> Errors { get; set; }

        private bool succeeded;
        public bool Succeeded {
            get => succeeded && !Errors.Any();
            set => succeeded = value;
        }
        public UserViewModel User { get; set; }

        public string Error => string.Join("-", Errors.Select(x => x.Description));
    }
}
