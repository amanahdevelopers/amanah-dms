﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Account
{
    public class RegistrationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }


        public string FullName { get; set; }


        [Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        public int CountryId { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
    }
}
