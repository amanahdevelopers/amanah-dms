﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.ViewModel
{
    public class FeaturePermissionViewModel
    {
        public string Name { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }
}
