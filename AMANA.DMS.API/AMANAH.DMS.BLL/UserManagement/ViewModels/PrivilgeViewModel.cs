﻿using AMANAH.DMS;
using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.ViewModel
{
    public class PrivilgeViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ApplicationRoleViewModel> Roles { get; set; }
    }
}
