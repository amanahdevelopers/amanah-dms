﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Account
{
    public class AgentLoginRequestViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string FCMDeviceId { get; set; }

    }
}
