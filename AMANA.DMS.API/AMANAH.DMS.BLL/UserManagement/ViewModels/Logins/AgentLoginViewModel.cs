﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Account
{
    public class AgentLoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string FCMDeviceId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
