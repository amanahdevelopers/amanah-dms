﻿namespace AMANAH.DMS.BLL.ViewModels.Account
{
    public class LoginResult : UserManagerResult
    {
        public DMSTokenResponse TokenResponse { get; set; }
    }

    public class DMSTokenResponse
    {
        public string AccessToken { get; set; }
        public string TokenType { get; } = "Bearer";
    }
}
