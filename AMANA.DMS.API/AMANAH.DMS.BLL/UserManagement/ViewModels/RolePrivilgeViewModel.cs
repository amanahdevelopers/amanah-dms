﻿
using AMANAH.DMS;
using AMANAH.DMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using AMANAH.DMS.ViewModel;

namespace AMANAH.DMS.Entities
{
    public class RolePrivilgeViewModel 
    {
        public int Id { get; set; }
        public int FK_Privilge_Id { get; set; }
        public PrivilgeViewModel Privilge { get; set; }
        public string FK_Role_Id { get; set; }
        public ApplicationRoleViewModel Role { get; set; }
    }
}
