﻿using System;
using System.Collections.Generic;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class ManagerAccessControlViewModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }
        public DateTime? CreationDate { set; get; }
    }
}
