﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.ViewModel
{
    public class AssignRoleViewModel
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}
