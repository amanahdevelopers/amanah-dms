﻿using System.ComponentModel;
using System.Linq;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Authorization
{
    public static class ManagerPermissions
    {
        public static (string Name, string[] Permissions)[] FeaturePermissions { get; } =
            PermissionsReflectionHelper.GetFeaturePermissions(typeof(ManagerPermissions));

        public static string[] FlatPermissions =>
            FeaturePermissions.SelectMany(feature => feature.Permissions).ToArray();

        [Description(Keys.PermissionFeatures.Task)]
        public static class Task
        {
            public const string CreateTask = "ManagerPermissions.Task.CreateTask";//
            //public const string CreateUnassignedTask = "ManagerPermissions.Task.CreateUnassignedTask";
            public const string UpdateTask = "ManagerPermissions.Task.UpdateTask";
            public const string DeleteTask = "ManagerPermissions.Task.DeleteTask";
            public const string ChangeTaskStatus = "ManagerPermissions.Task.ChangeTaskStatus";
            public const string ReadUnassignedTask = "ManagerPermissions.Task.ReadUnassignedTask";
            public const string ExportTask = "ManagerPermissions.Task.ExportTask";
        }

        [Description(Keys.PermissionFeatures.Agent)]
        public static class Agent
        {
            public const string ReadAgent = "ManagerPermissions.Agent.ReadAgent";
            public const string CreateAgent = "ManagerPermissions.Agent.CreateAgent";
            public const string UpdateAgent = "ManagerPermissions.Agent.UpdateAgent";
            public const string DeleteAgent = "ManagerPermissions.Agent.DeleteAgent";
            public const string DeleteAllAgent = "ManagerPermissions.Agent.DeleteAllAgent";
            public const string UpdateAllAgent = "ManagerPermissions.Agent.UpdateAllAgent";
            //public const string ViewUnverifiedAgent = "ManagerPermissions.Agent.ViewUnverifiedAgent";
            public const string ChangeAgentPassword = "ManagerPermissions.Agent.ChangeAgentPassword";
            public const string ViewDriversLoginRequests = "ManagerPermissions.Agent.ViewDriversLoginRequests";
            public const string ExportAgent = "ManagerPermissions.Agent.ExportAgent";
            public const string ImportAgent = "ManagerPermissions.Agent.ImportAgent";
        }

        [Description(Keys.PermissionFeatures.Customer)]
        public static class Customer
        {
            public const string CreateCustomer = "ManagerPermissions.Customer.CreateCustomer";
            public const string DeleteCustomer = "ManagerPermissions.Customer.DeleteCustomer";
            public const string UpdateCustomer = "ManagerPermissions.Customer.UpdateCustomer";
            public const string ReadCustomer = "ManagerPermissions.Customer.ReadCustomer";
            public const string ExportCustomer = "ManagerPermissions.Customer.ExportCustomer";
            public const string ImportCustomer = "ManagerPermissions.Customer.ImportCustomer";
        }

        [Description(Keys.PermissionFeatures.Teams)]
        public static class Team
        {
            public const string CreateTeam = "ManagerPermissions.Team.CreateTeam";
            public const string DeleteTeam = "ManagerPermissions.Team.DeleteTeam";
            public const string UpdateTeam = "ManagerPermissions.Team.UpdateTeam";
            public const string ReadTeam = "ManagerPermissions.Team.ReadTeam";
            public const string DeleteAllTeam = "ManagerPermissions.Team.DeleteAllTeam";
            public const string UpdateAllTeam = "ManagerPermissions.Team.UpdateAllTeam";
            public const string ReadMyTeam = "ManagerPermissions.Team.ReadMyTeam";
        }

        [Description(Keys.PermissionFeatures.Settings)]
        public static class Settings
        {
            //public const string ReadAdvancePreference = "ManagerPermissions.Settings.ReadAdvancePreference";
            //public const string UpdateAdvancedPreference = "ManagerPermissions.Settings.UpdateAdvancedPreference";
            //public const string ReadAutoAllocation = "ManagerPermissions.Settings.ReadAutoAllocation";
            public const string UpdateAutoAllocation = "ManagerPermissions.Settings.UpdateAutoAllocation";
            public const string ReadNotification = "ManagerPermissions.Settings.ReadNotification";
            public const string UpdateNotification = "ManagerPermissions.Settings.UpdateNotification";
        }

        [Description(Keys.PermissionFeatures.Managers)]
        public static class Managers
        {
            public const string AddManager = "ManagerPermissions.Settings.AddManager";
            public const string ReadAllManagers = "ManagerPermissions.Settings.ReadAllManagers";
            public const string UpdateAllManager = "ManagerPermissions.Settings.UpdateAllManager";
            public const string ReadTeamManager = "ManagerPermissions.Settings.ReadTeamManager";
            public const string UpdateTeamManager = "ManagerPermissions.Settings.UpdateTeamManager";
            public const string ChangeManagerPassword = "ManagerPermissions.Settings.ChangeManagerPassword";
        }

        [Description(Keys.PermissionFeatures.Geofences)]
        public static class Geofences
        {
            public const string AddGeofence = "ManagerPermissions.Settings.AddGeofence";
            public const string ReadGeofence = "ManagerPermissions.Settings.ReadGeofence";
            public const string UpdateGeofence = "ManagerPermissions.Settings.UpdateGeofence";
            public const string DeleteGeofence = "ManagerPermissions.Settings.DeleteGeofence";
            public const string ExportGeofence = "ManagerPermissions.Settings.ExportGeoFence";
        }

        [Description(Keys.PermissionFeatures.Restaurants)]
        public static class Restaurants
        {
            public const string AddRestaurant = "ManagerPermissions.Settings.AddRestaurant";
            public const string ReadRestaurant = "ManagerPermissions.Settings.ReadRestaurant";
            public const string UpdateRestaurant = "ManagerPermissions.Settings.UpdateRestaurant";
            public const string DeleteRestaurant = "ManagerPermissions.Settings.DeleteRestaurant";
            public const string BlockRestaurant = "ManagerPermissions.Settings.BlockRestaurant";
            public const string UnBlockRestaurant = "ManagerPermissions.Settings.UnBlockRestaurant";
        }

        [Description(Keys.PermissionFeatures.Branches)]
        public static class Branches
        {
            public const string AddBranch = "ManagerPermissions.Settings.AddBranch";
            public const string ReadBranch = "ManagerPermissions.Settings.ReadBranch";
            public const string UpdateBranch = "ManagerPermissions.Settings.UpdateBranch";
            public const string DeleteBranch = "ManagerPermissions.Settings.DeleteBranch";
            public const string BlockBranch = "ManagerPermissions.Settings.BlockBranch";
            public const string UnBlockBranch = "ManagerPermissions.Settings.UnBlockBranch";
        }

        [Description(Keys.PermissionFeatures.DispatchingManager)]
        public static class DispatchingManager
        {
            public const string AddManagerDispatching = "ManagerPermissions.Settings.AddManagerDispatching";
            public const string ReadManagerDispatching = "ManagerPermissions.Settings.ReadManagerDispatching";
            public const string UpdateManagerDispatching = "ManagerPermissions.Settings.UpdateManagerDispatching";
            public const string DeleteManagerDispatching = "ManagerPermissions.Settings.DeleteManagerDispatching";
        }

        [Description(Keys.PermissionFeatures.Reports)]
        public static class Reports
        {
            public const string ViewReport = "ManagerPermissions.Reports.ViewReport";
            public const string ExportReport = "ManagerPermissions.Reports.ExportReport";
        }
    }
}
