﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DMS.Tests")]
namespace AMANAH.DMS.BLL.Authorization
{
    internal class PermissionsReflectionHelper
    {
        public static (string Name, string[] Permissions)[] GetFeaturePermissions(Type permissionsType)
        {

            var featurePermissions = permissionsType.GetNestedTypes()
                .Where(IsFeaturePermissions)
                .Select(type => (GetDescription(type), GetPermissions(type)))
                .ToArray();
            return featurePermissions;
        }

        private static string[] GetPermissions(Type featurePermissionsType)
        {
            var taskFlags = BindingFlags.Static | BindingFlags.Public;
            var permissions = featurePermissionsType.GetFields(taskFlags)
                .Where(fieldInfo => fieldInfo.IsLiteral && fieldInfo.FieldType == typeof(string))
                .Select(fieldInfo => (string)fieldInfo.GetValue(fieldInfo))
                .ToArray();
            return permissions;
        }

        private static string GetDescription(Type featurePermissionsType)
        {
            if (!featurePermissionsType.IsDefined(typeof(DescriptionAttribute)))
            {
                return featurePermissionsType.Name;
            }
            var descriptionAttribute = featurePermissionsType.GetCustomAttribute<DescriptionAttribute>();
            return descriptionAttribute.Description;
        }

        private static bool IsFeaturePermissions(Type type)
        {
            return type.IsAbstract && type.IsSealed;
        }
    }
}
