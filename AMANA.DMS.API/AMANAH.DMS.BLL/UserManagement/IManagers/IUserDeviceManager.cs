﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.Models.Entities;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IUserDeviceManager
    {
        Task AddIfNotExistAsync(UserDevice userDevice);
        bool SetUserDeviceLoggedIn(UserDevice userDevice);
        List<UserDevice> GetByUserId(string userId);
        bool DeleteDevice(string token);
    }
}
