﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.Models.Entities;
using System.Collections.Generic;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IRolePrivilgeManager : IRepositry<RolePrivilge>
    {
    }
}
