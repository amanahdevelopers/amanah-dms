﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> GetRoleAsyncByName(string roleName, RoleType type = (RoleType)1);
        Task<ApplicationRole> GetRoleAsync(ApplicationRole role);
        Task<string> AddRoleAsync(ApplicationRole applicationRole);
        Task AddRolesAsync(List<ApplicationRole> applicationRoles);
        Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole);
        Task<string> AddRoleAsync(string roleName, RoleType type = (RoleType)1);
        Task<bool> IsRoleExistAsync(string roleName);
        Task<bool> IsRoleExistAsync(string roleName, RoleType type = (RoleType)1);
        Task<List<Privilge>> GetPrivilgesByRoleName(string roleName, RoleType type = (RoleType)1);
        List<ApplicationRole> GetAllRoles(RoleType type = (RoleType)1);
        Task<bool> DeleteRoleAsync(ApplicationRole applicationRole);
        Task<bool> UpdateRoleAsync(ApplicationRole applicationRole);
        Task<bool> AddClaimAsync(ApplicationRole role, Claim claim);
        Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName);
        Task<IList<Claim>> GetClaimsAsync(ApplicationRole role);
        List<string> GetAllRolesByUserId(string userId);
    }
}