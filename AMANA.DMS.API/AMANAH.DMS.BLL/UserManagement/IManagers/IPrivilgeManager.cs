﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.Models.Entities;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IPrivilgeManager : IRepositry<Privilge>
    {
    }
}
