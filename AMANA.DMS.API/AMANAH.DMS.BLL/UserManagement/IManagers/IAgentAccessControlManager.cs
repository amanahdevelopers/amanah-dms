﻿using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IAgentAccessControlManager
    {
        Task<bool> Create(AgentAccessControlViewModel input);
        Task<bool> Update(AgentAccessControlViewModel input);
        Task<bool> Delete(string roleName);
        Task<AgentAccessControlViewModel> Get(string roleName);
        Task<List<AgentAccessControlViewModel>> GetAll();
        Task<List<FeaturePermissionViewModel>> GetAllPermissions();
    }
}
