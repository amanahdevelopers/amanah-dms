﻿using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IManagerAccessControlManager
    {
        Task<bool> Create(ManagerAccessControlViewModel input);
        Task<bool> Update(ManagerAccessControlViewModel input);
        Task<bool> Delete(string roleName);
        Task<ManagerAccessControlViewModel> Get(string roleName);
        Task<List<ManagerAccessControlViewModel>> GetAll();
        Task<List<FeaturePermissionViewModel>> GetAllPermissions();

    }
}
