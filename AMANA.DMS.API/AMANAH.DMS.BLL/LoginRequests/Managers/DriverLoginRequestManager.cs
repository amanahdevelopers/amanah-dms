﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class DriverLoginRequestManager :
        BaseManager<DriverLoginRequestViewModel, DriverLoginRequest>, IDriverLoginRequestManager
    {
        public DriverLoginRequestManager(
            ApplicationDbContext context,
            IRepositry<DriverLoginRequest> repository,
            IMapper mapper)
            : base(context, repository, mapper)
        {
        }

        public async Task<bool> Create(CreateDriverLoginRequestViewModel ent)
        {
            var driver = context.Driver.Single(t => t.Id == ent.DriverId);
            var loginRequest = new DriverLoginRequest(driver.Tenant_Id);
            mapper.Map(ent, loginRequest);
            //var mappedEntity = mapper.Map<DriverLoginRequest>(ent);
            context.DriverLoginRequests.Add(loginRequest);
            var res = await context.SaveChangesAsync();

            return res > 0;
        }

        public async Task<DriverLoginRequestViewModel> Get(int id)
        {
            var driverLoginRequest = await context.DriverLoginRequests
                .Include(t => t.Driver.Team)
                .Include(t => t.Driver.User)
                .Include(t => t.Driver.AgentType)
                .FirstOrDefaultAsync(t => t.Id == id);
            return mapper.Map<DriverLoginRequestViewModel>(driverLoginRequest);
        }
        public async Task<DriverLoginRequestViewModel> GetByPendingDriverId(int driverId)
        {
            var driverLoginRequest = await context.DriverLoginRequests
                .FirstOrDefaultAsync(t => t.DriverId == driverId && t.Status == (int)DriverLoginRequestStatus.Pending);
            return mapper.Map<DriverLoginRequestViewModel>(driverLoginRequest);
        }

        public override async Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>()
        {
            var driverLoginRequests = await context.DriverLoginRequests
                .Include(t => t.Driver.Team)
                .Include(t => t.Driver.User)
                .Include(t => t.Driver.AgentType)
                .ToListAsync();
            return mapper.Map<List<TProjectedModel>>(driverLoginRequests);
        }

        public Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> driverIds)
        {
            return repository.GetAll()
                .Include(t => t.Driver.Team)
                .Include(t => t.Driver.User)
                .Include(t => t.Driver.AgentType)
                .Where(x => driverIds.Contains(x.DriverId))
                .ProjectTo<TProjectedModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<bool> CancelDriverRequest(int driverId)
        {
            try
            {
                var loginReuests = await (context as ApplicationDbContext).DriverLoginRequests
                    .Where(t => t.DriverId == driverId && t.Status == (int)DriverLoginRequestStatus.Pending)
                    .ToListAsync();

                foreach (var r in loginReuests)
                {
                    r.Status = (int)DriverLoginRequestStatus.Canceled;
                    (context as ApplicationDbContext).Update(r);
                }

                await (context as ApplicationDbContext).SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
#warning cleanup - this exception is eaten and not logged, and false result has no meaning, base exception is so generic.
                return false;
            }
        }



        public Task<PagedResult<DriverLoginRequestViewModel>> GetAllByPaginationAsync(
            DriverLoginRequestSearchViewModel pagingparametermodel, List<int> driverIds = null)
        {
            var query = repository.GetAll();
            if (!string.IsNullOrWhiteSpace(pagingparametermodel.SearchBy))
            {
                query = query.Where(loginRequest =>
                    loginRequest.Driver.User.FirstName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                    loginRequest.Driver.User.LastName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                    loginRequest.Driver.User.UserName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                    loginRequest.Driver.User.Email.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()));
            }
            if (pagingparametermodel.StatusIDs != null && pagingparametermodel.StatusIDs.Any())
            {
                query = query.Where(loginRequest => pagingparametermodel.StatusIDs.Contains(loginRequest.Status));
            }
            if (pagingparametermodel.FilterTime.HasValue)
            {
                query = query.Where(loginRequest =>
                    loginRequest.CreatedDate >= pagingparametermodel.FilterTime.Value.ToUniversalTime());
            }
            if (driverIds != null)
            {
                query = query.Where(loginRequest => driverIds.Contains(loginRequest.DriverId));
            }
            var pagedResult = query.ToPagedResultAsync<DriverLoginRequest, DriverLoginRequestViewModel>(
                    pagingparametermodel,
                    mapper.ConfigurationProvider);

            return pagedResult;

        }
    }
}
