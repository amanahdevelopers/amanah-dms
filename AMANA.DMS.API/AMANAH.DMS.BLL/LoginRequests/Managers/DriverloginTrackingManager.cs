﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class DriverloginTrackingManager :
        BaseManager<DriverloginTrackingViewModel, DriverloginTracking>,
        IDriverloginTrackingManager
    {
        public DriverloginTrackingManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<DriverloginTracking> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<DriverloginTrackingViewModel> GetLoggedIn(int driverId)
        {
            return await context.DriverloginTracking
                .Where(x => x.DriverId == driverId)
                .OrderByDescending(x => x.LoginDate)
                .ProjectTo<DriverloginTrackingViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public Task<PagedResult<DriverloginTrackingViewModel>> GetByPaginationAsync(PaginatedDriverLoginTrackingViewModel pagingparametermodel)
        {
#warning cleanup-this predicate should has a helpful name or we can remove it and replace with wehre condition...
            var query = context.DriverloginTracking.AsQueryable();
            Expression<Func<DriverloginTracking, bool>> predicate = e => true;
            if (pagingparametermodel.LogoutDate.HasValue)
            {
                query = query.Where(e => pagingparametermodel.LogoutDate.Value.Date == e.LogoutDate.Value.Date);
            }

            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy))
            {
                query = query.Where(e =>
                    e.Driver != null &&
                    e.Driver.User != null &&
                    (
                        e.Driver.User.UserName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                        e.Driver.User.FirstName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                        e.Driver.User.LastName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower())
                    ));
            }
            if (pagingparametermodel.DriverId.HasValue)
            {
                query = query.Where(e => e.DriverId == pagingparametermodel.DriverId.Value);
            }

            var pagedResult = query.OrderByDescending(e => e.LoginDate)
                .ToPagedResultAsync<DriverloginTracking, DriverloginTrackingViewModel>(pagingparametermodel, mapper.ConfigurationProvider);

            return pagedResult;
        }

        public async override Task<DriverloginTrackingViewModel> AddAsync(DriverloginTrackingViewModel viewModel)
        {
            var driver = await context.Driver.FindAsync(viewModel.DriverId);
            var driverloginTracking = new DriverloginTracking(driver.Tenant_Id);
            mapper.Map(viewModel, driverloginTracking);
            await context.DriverloginTracking.AddAsync(driverloginTracking);
            await context.SaveChangesAsync();
            return mapper.Map<DriverloginTracking, DriverloginTrackingViewModel>(driverloginTracking);
        }

        public Task<List<ExportDriverloginTrackingViewModel>> ExportToExcel(PaginatedDriverLoginTrackingViewModel pagingparametermodel)
        {
#warning cleanup-this predicate to be removed and replaced with Where or named Expression method or field
            var ctx = (context as ApplicationDbContext);
            Expression<Func<DriverloginTracking, bool>> predicate = e => true;
            if (pagingparametermodel.LogoutDate.HasValue) predicate = PredicateBuilder.And(predicate, e => e.LogoutDate.HasValue && pagingparametermodel.LogoutDate.Value.Date == e.LogoutDate.Value.Date);
            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy)) predicate = PredicateBuilder.And(predicate, e => e.Driver != null && e.Driver.User != null && (
            e.Driver.User.UserName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
            e.Driver.User.FirstName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
            e.Driver.User.LastName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower())
            ));


            var query = ctx.DriverloginTracking
                .Include(x => x.Driver)
                .ThenInclude(x => x.User)
                .Where(predicate)
                .Select(t => new ExportDriverloginTrackingViewModel
                {
                    DriverId = t.DriverId,
                    LoginDate = t.LoginDate.HasValue ? t.LoginDate.Value.AddHours(3) : t.LoginDate,
                    LoginLatitude = t.LoginLatitude,
                    LoginLongitude = t.LoginLongitude,
                    LogoutDate = t.LogoutDate.HasValue ? t.LogoutDate.Value.AddHours(3) : t.LogoutDate,
                    LogoutLatitude = t.LogoutLatitude,
                    LogoutLongitude = t.LogoutLongitude,
                    LogoutActionBy = t.LogoutActionBy,
                    DriverName = t.Driver.User.UserName
                });

            return query.ToListAsync();
        }
    }
}
