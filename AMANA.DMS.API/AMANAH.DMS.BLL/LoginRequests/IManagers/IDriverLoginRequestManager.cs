﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IDriverLoginRequestManager : IBaseManager<DriverLoginRequestViewModel, DriverLoginRequest>
    {
        Task<bool> Create(CreateDriverLoginRequestViewModel ent);
        Task<DriverLoginRequestViewModel> Get(int id);
        Task<DriverLoginRequestViewModel> GetByPendingDriverId(int driverId);
        Task<bool> CancelDriverRequest(int driverId);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> driverIds);
        Task<PagedResult<DriverLoginRequestViewModel>> GetAllByPaginationAsync(DriverLoginRequestSearchViewModel pagingparametermodel, List<int> driverIds);
    }
}
