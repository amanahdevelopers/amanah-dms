﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IDriverloginTrackingManager : IBaseManager<DriverloginTrackingViewModel, DriverloginTracking>
    {
        Task<DriverloginTrackingViewModel> GetLoggedIn(int driverId);
        Task<PagedResult<DriverloginTrackingViewModel>> GetByPaginationAsync(PaginatedDriverLoginTrackingViewModel pagingparametermodel);
        Task<List<ExportDriverloginTrackingViewModel>> ExportToExcel(PaginatedDriverLoginTrackingViewModel paginatedDriverLoginTrackingViewModel);
    }
}
