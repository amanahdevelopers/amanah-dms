﻿using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class DriverloginTrackingViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public DateTime? LoginDate { get; set; }
        public double? LoginLatitude { get; set; }
        public double? LoginLongitude { get; set; }
        public DateTime? LogoutDate { get; set; }
        public double? LogoutLatitude { get; set; }
        public double? LogoutLongitude { get; set; }
        public string LogoutActionBy { get; set; }
        public string DriverName { get; set; }
    }
}
