﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class PaginatedDriverLoginTrackingViewModel : PaginatedItemsViewModel
    {
        public int? DriverId { get; set; }
        public DateTime? LogoutDate { get; set; }
    }
}
