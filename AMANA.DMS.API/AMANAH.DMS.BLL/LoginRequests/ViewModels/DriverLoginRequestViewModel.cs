﻿using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class DriverLoginRequestViewModel : BaseViewModel
    {
        public int Id { get; set; }

        /// <summary>
        /// DriverLoginRequestSatus enum type
        /// </summary>
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }
        public string ApprovedOrRejectBy { get; set; }
        public string DriverName { get; set; }
        public int? TeamId { set; get; }
        public string TeamName { get; set; }
        public int? AgentTypeId { set; get; }
        public string AgentTypeName { get; set; }
        public string Reason { get; set; }
        public bool IsExpired { get; set; }


    }
}
