﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
   public class CreateDriverLoginRequestViewModel
    {
        public int Id { get; set; }
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }
    }
}
