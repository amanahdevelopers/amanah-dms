﻿using AMANAH.DMS.BLL.Enums;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace AMANAH.DMS.ViewModels
{
    public class DriverLoginRequestSearchViewModel : PaginatedItemsViewModel
    {
        public List<int> StatusIDs { get; set; }
        public DateTime? FilterDate { get; set; }
        public DateTime? FilterTime { get; set; }
    }
}