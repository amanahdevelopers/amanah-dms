﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IMapRequestsLogManager : IBaseManager<AccountLogViewModel, MapRequestLog>
    {
    }
}
