﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace AMANAH.DMS.BLL.Managers
{
    public class AccountLogManager : BaseManager<AccountLogViewModel, AccountLogs>, IAccountLogManager
    {

        public AccountLogManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<AccountLogs> repository)
            : base(context, repository, mapper)
        {
        }




        public override Task<List<AccountLogViewModel>> GetAllAsync<AccountLogViewModel>()
        {

            return repository.GetAll()
                .ProjectTo<AccountLogViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<Utilities.Utilites.Paging.PagedResult<AccountLogViewModel>> GetByPagination(PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new Utilities.Utilites.Paging.PagedResult<AccountLogViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            var source = repository.GetAll()
                .Where(ApplyFilter(pagingparametermodel))
                .OrderByDescending(x => x.Id)
                .ProjectTo<AccountLogViewModel>(mapper.ConfigurationProvider);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
#warning cleanup-This pagination code is duplicated and should exsist in a single place
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = await source.CountAsync();//
            pagedResult.Result = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();

            return pagedResult;
        }


        public Task<bool> Create(AccountLogs accountLog)
        {
#warning cleanup- this is an async method calling sync and return value has no meaning
            repository.AddAsync(accountLog);
            return Task.FromResult(true);
        }



        #region Extra
        public async Task<bool> DeleteAsync(int id)
        {
            var accountLog = (context as ApplicationDbContext).AccountLogs.SingleOrDefault(t => t.Id == id);

            if (accountLog == null)
            {
                return false;
            }
            var data = await base.SoftDeleteAsync(mapper.Map<AccountLogViewModel>(accountLog));
            return data;
        }

        public Task<AccountLogViewModel> Get(int id)
        {
            return (context as ApplicationDbContext).AccountLogs
                .Where(x => x.Id == id)
                .ProjectTo<AccountLogViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public Task<List<AccountLogViewModel>> GetAccountLogsForExport(PaginatedItemsViewModel pagingparametermodel)
        {
            return repository.GetAll().Where(ApplyFilter(pagingparametermodel)).OrderByDescending(x => x.Id)
                .ProjectTo<AccountLogViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }


        private Expression<Func<AccountLogs, bool>> ApplyFilter(PaginatedItemsViewModel pagingparametermodel)
        {
            return accountLog => 
                (pagingparametermodel.EndDate.Value.Date == pagingparametermodel.StartDate.Value.Date
                    ||accountLog.CreatedDate.Date <= pagingparametermodel.EndDate.Value.Date) &&
                (pagingparametermodel.StartDate.Value.Date == pagingparametermodel.EndDate.Value.Date 
                    || accountLog.CreatedDate.Date >= pagingparametermodel.StartDate.Value.Date) &&
                (pagingparametermodel.SearchBy == "All" 
                    || accountLog.TableName == pagingparametermodel.SearchBy) &&
                (accountLog.CreatedBy_Id == pagingparametermodel.UserID 
                    || pagingparametermodel.UserID == null);
              
        }

        #endregion
    }
}
