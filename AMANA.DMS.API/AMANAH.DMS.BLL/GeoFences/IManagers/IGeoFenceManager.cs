﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IGeoFenceManager : IBaseManager<GeoFenceViewModel, GeoFence>
    {
        Task<GeoFenceViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> ids);
        Task<List<GeoFenceExportToExcelViewModel>> ExportExcel();
        Task<List<int>> GetManagerZonesAsync();
    }
}
