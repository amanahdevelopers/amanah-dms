﻿using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class CreateGeoFenceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<int> Drivers { get; set; }
        public virtual List<GeoFenceLocationViewModel> Locations { get; set; }
    }

}
