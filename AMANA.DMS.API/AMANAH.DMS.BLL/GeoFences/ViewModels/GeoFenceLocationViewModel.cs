﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
   public class GeoFenceLocationViewModel
    {
        public double? Longitude { get; set; }      
        public double? Latitude { get; set; }
        public int PointIndex { get; set; }
    }
}
