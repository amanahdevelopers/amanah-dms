﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface INotificationManager : IBaseManager<NotificationViewModel, Notification>
    {
        public Task<PagedResult<NotificationViewModel>> GetAllByPaginationAsyncForUser(Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel.PaginatedItemsViewModel pagingparametermodel);
        Task<NotificationViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> MarkAsReadsync(string  userId);
        Task<int> UnReadCountsync(string userId);

    }


}
