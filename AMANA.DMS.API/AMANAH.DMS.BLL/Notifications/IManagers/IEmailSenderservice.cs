﻿using AMANAH.DMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.BLL.IManagers
{
   public interface IEmailSenderservice
    {
        Task SendEmailAsync(string subject, string body, string toEmail);
        Task SendEmailAsync(string subject, string body, IList<string> toEmails);
    }
}
