﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    internal class NotificationManager : BaseManager<NotificationViewModel, Notification>, INotificationManager
    {
        public NotificationManager(
            ApplicationDbContext context,
            IRepositry<Notification> repository,
            IMapper mapper)
            : base(context, repository, mapper)
        {
        }

        public async Task<NotificationViewModel> Get(int id)
        {
            var notifications = await repository.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            return mapper.Map<Notification, NotificationViewModel>(notifications);
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            var notifications = await repository.GetAll().SingleOrDefaultAsync(x => x.Id == id);

            return mapper.Map<Notification, TQueryModel>(notifications);
        }
        public Task<PagedResult<NotificationViewModel>> GetAllByPaginationAsyncForUser(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = repository.GetAll()
                .Where(x => x.ToUserId == pagingparametermodel.UserID)
                .ToPagedResultAsync<Notification, NotificationViewModel>(pagingparametermodel, mapper.ConfigurationProvider);

            return pagedResult;
        }

        public async Task<bool> MarkAsReadsync(string userId)
        {
            try
            {
                var notifications = await context.Notifications
                    .Where(x => x.ToUserId == userId)
                    .ToListAsync();

                foreach (var noti in notifications)
                {
                    noti.IsSeen = true;
                    var r = context.Notifications.Update(noti);

                }
                await context.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }

        }

        public async Task<int> UnReadCountsync(string userId)
        {
            try
            {
                var UnReadCount = await context.Notifications
                    .Where(x => x.ToUserId == userId && x.IsSeen == false)
                    .CountAsync();

                return UnReadCount;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}


