﻿namespace AMANAH.DMS.BLL.Enums
{
    public enum NotificationTypeEnum
    {
        NewTask = 1,
        ReassignTask,
        UpdateDriverDutyToOn,
        UpdateDriverDutyToOff,
        ReadyToGo,
        Blocked
    }

    public enum NotificationTypeWeb
    {
        LoginRequest = 1,
        DriverLoggedin,
        CancelLoginRequest,
        LocationChanged,

        TaskAccept,
        DeclineAsync,
        TaskFailed,
        TaskSuccessful,
        TaskCancel,
        TaskStart,
        TaskDecline,

        AutoAllocationStarted,
        AutoAllocationSucessfully,
        AutoAllocationFailed,

        DriverSetReached,
        DriverSetDuty

    }
}
