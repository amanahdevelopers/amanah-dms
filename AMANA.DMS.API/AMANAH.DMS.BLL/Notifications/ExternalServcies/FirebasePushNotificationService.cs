﻿using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using AMANAH.DMS.Context;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ML.Mono.Helpers;

namespace AMANAH.DMS.BLL.ExternalServcies
{
    public class FirebasePushNotificationService : IPushNotificationService
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly string _applicationId;
        public FirebasePushNotificationService(
            ApplicationDbContext context,
            IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            _applicationId = _configuration.GetValue<string>("FcmSettings:ApiKey");
        }

        public async Task SendAsync(string toUserId, string notificationTitle, string notificationText, object notificationData)
        {
            var userDevices = await _context.UserDevice
                .Where(u => u.UserId == toUserId)
                .ToListAsync();
            foreach (var device in userDevices)
            {
                BackgroundJob.Enqueue(() => SendInBackgroundAsync(device.FCMDeviceId, notificationTitle, notificationText, notificationData, _applicationId));
            }
        }

        //public async Task SendToDriverAsync(int driverId, string notificationTitle, string notificationText, object notificationData)
        //{
        //    var userId = await _context.Driver
        //        .Where(driver => driver.Id == driverId)
        //        .Select(driver => driver.UserId)
        //        .SingleOrDefaultAsync();
        //    await SendAsync(userId, notificationTitle, notificationText, notificationData);
        //}

        public async Task SendInBackgroundAsync(string FCMDeviceId, string notificationTitle, string notificationText, object notificationData, string applicationId)
        {
            await FireBase.SendPushNotificationAsync(FCMDeviceId, notificationTitle, notificationText, notificationData, applicationId);

        }
    }
}
