﻿using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.ExternalServcies
{
    public interface IPushNotificationService
    {
        Task SendAsync(string toUserId, string notificationTitle, string notificationText, object notificationData);

        //Task SendToDriverAsync(int driverId, string notificationTitle, string notificationText, object notificationData);
    }
}
