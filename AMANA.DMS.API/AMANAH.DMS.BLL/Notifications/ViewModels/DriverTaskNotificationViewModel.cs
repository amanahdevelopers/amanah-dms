﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class DriverTaskNotificationViewModel
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public string Description { set; get; }
        public TasksViewModel Tasks { set; get; }
    }
}
