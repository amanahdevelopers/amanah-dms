﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
   public class CreateNotificationViewModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }
    }
}
