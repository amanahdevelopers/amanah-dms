﻿using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.ViewModel;
using System;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class NotificationViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public NotificationTypeWeb? NotificationType { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
        public bool IsSeen { get; set; }
        public DateTime Date { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }


    }
}
