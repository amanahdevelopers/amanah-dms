﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Utilites.ExcelToGenericList;

namespace AMANAH.DMS.API.SignalRHups
{
    [Authorize]
    public class NotificationHub : Hub
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IManagerManager _managerManager;

        public NotificationHub(ApplicationDbContext dbContext, IManagerManager managerManager)
        {
            _dbContext = dbContext;
            _managerManager = managerManager;
        }

        public async override Task OnConnectedAsync()
        {
            var user = Context.User;

            if (Context.User.IsInRole("Tenant") || Context.User.IsInRole("Admin") || Context.User.IsInRole("SuperAdmin"))
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name);
                await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value);

            }
            if (Context.User.Claims.Where(x => x.Type == "UserType").FirstOrDefault().Value.ToInt32() == (int)RoleType.Manager)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name);
                //Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value);

            }
            else
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Identity.Name);
                await Groups.AddToGroupAsync(Context.ConnectionId, Context.User.Claims.Where(x => x.Type == "Tenant_Id").FirstOrDefault().Value);

            }
            await base.OnConnectedAsync();
        }

        public async Task UpdateCurrentLocationAsync(string driverId, double longitude, double latitude)
        {
            var driver = _dbContext.Driver.Where(x => x.UserId == driverId).FirstOrDefault();
            if (driver != null)
            {
                var OldLocation = _dbContext.DriverCurrentLocation.Where(x => x.DriverId == driver.Id).FirstOrDefault();
                if (OldLocation == null)
                {
                    _dbContext.DriverCurrentLocation.Add(
                        new DriverCurrentLocation
                        {
                            DriverId = driver.Id,
                            Longitude = longitude,
                            Latitude = latitude,
                        });
                }
                else
                {
                    OldLocation.Longitude = longitude;
                    OldLocation.Latitude = latitude;
                    _dbContext.DriverCurrentLocation.Update(OldLocation);
                }

                await _dbContext.SaveChangesAsync();

                await Clients.Group(driver.Tenant_Id).SendAsync("LocationChanged", driverId, longitude, latitude);

                var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
                foreach (var driverManager in DriverManagers)
                {
                    await Clients.Group(driverManager.Username).SendAsync("LocationChanged", driverId, longitude, latitude);
                }
            }
        }
    }
}
