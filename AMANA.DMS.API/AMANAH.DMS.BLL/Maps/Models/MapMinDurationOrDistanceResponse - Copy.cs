﻿namespace MapsUtilities.Models
{
    public class MapMinDurationOrDistanceResponse : MapResponseBase
    {
        public int MinDurationOrDistance { get; set; }
    }
}
