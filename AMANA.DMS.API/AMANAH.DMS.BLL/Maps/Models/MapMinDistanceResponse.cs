﻿namespace MapsUtilities.Models
{
    public class MapMinDistanceResponse : MapResponseBase
    {
        public int Distance { get; set; }
    }
}
