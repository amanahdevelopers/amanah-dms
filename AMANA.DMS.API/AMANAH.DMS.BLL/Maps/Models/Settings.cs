﻿namespace MapsUtilities.Models
{
    public enum MapPriority
    {
        Default,
        Google,
        Bing
    }

    public class MapRequestSettings
    {
        public MapPriority Priority { get; set; }
        public string GoogleApiKey { get; set; }
        public string BingApiKey { get; set; }

        private bool? quotaFallback;
        public bool QuotaFallback
        {
            get => Priority != MapPriority.Default && quotaFallback.HasValue ? quotaFallback.Value : true;
            set => quotaFallback = value;
        }

        private bool? noRouteFallback;
        public bool NoRouteFallback
        {
            get => Priority != MapPriority.Default && noRouteFallback.HasValue ? noRouteFallback.Value : true;
            set => noRouteFallback = value;
        }
    }
}
