﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapsUtilities.Models
{
    public class MapPoint
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
