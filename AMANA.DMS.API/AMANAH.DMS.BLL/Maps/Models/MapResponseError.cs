﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MapsUtilities.Models
{
    public enum MapResponseError
    {
        QuotaLimit = 1,
        RouteNotAvailable,
        Exception
    }
}
