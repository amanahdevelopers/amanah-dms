﻿namespace MapsUtilities.Models
{
    public class MapResponseBase
    {
        public bool Success { get; set; }
        public MapResponseError Error { get; set; }
        public string ErrorMessage { get; set; }
    }
}
