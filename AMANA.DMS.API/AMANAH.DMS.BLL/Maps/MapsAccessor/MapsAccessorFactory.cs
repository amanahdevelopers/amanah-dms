﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.DATA.Entities;
using MapsUtilities.MapsAccessor;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace MapsUtilities.MapsAccessor
{
    public enum MapType
    {
        Google,
        Bing,
        OpenStreet
    }

    public class MapsAccessorFactory
    {
        public static IMapsAccessor GetMapsAccessor(MapType type)
        {
            return new OpenStreetMapsAccessor();
        }
        public static IMapsAccessor GetMapsAccessor(MapType type, MapRequestSettings settings, IRepositry<MapRequestLog> repository = null, DbContext context = null)
        {
            switch (type)
            {
                case MapType.Google:
                    return new GoogleMapsAccessor(settings.GoogleApiKey, repository, context);
                case MapType.Bing:
                    return new BingMapsAccessor(settings.BingApiKey);
                case MapType.OpenStreet:
                    return new OpenStreetMapsAccessor();
                default:
                    throw new NotImplementedException($"Map type {Enum.GetName(typeof(MapType), type)} is not implemented");
            }
        }
    }
}
