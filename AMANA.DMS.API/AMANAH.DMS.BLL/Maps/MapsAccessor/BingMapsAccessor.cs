﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Enums;
using BingMapsRESTToolkit;
using MapsUtilities.Models;

namespace MapsUtilities.MapsAccessor
{
    public class BingMapsAccessor : IMapsAccessor
    {
        private string _apiKey;

        public BingMapsAccessor(string apiKey)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentException("Api key cannot be empty");

            _apiKey = apiKey;
        }

        public async Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistance(MapPoint from, MapPoint to)
        {
            var resp = new MapMinDurationOrDistanceResponse();
            try
            {


                var req = new RouteRequest()
                {
                    RouteOptions = new RouteOptions()
                    {
                        Avoid = new List<AvoidType>()
                    {
                        AvoidType.MinimizeTolls
                    },
                        TravelMode = TravelModeType.Driving,
                        DistanceUnits = DistanceUnitType.Kilometers,
                        RouteAttributes = new List<RouteAttributeType>()
                    {
                        RouteAttributeType.All
                    }
                        ,
                        Optimize = RouteOptimizationType.TimeWithTraffic
                    },
                    Waypoints = new List<SimpleWaypoint>()
                {
                    new SimpleWaypoint
                    {
                        Coordinate = new Coordinate{ Latitude = from.Latitude, Longitude = from.Longitude }
                    },
                    new SimpleWaypoint
                    {
                        Coordinate = new Coordinate{ Latitude = to.Latitude, Longitude = to.Longitude }
                    }
                },
                    BingMapsKey = _apiKey
                };

                var res = await req.Execute();
                if (res.ResourceSets == null ||
                    !res.ResourceSets.Any() ||
                    res.ResourceSets.First().Resources == null ||
                    !res.ResourceSets.First().Resources.Any())
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                    return resp;
                }
                var resource = res.ResourceSets.FirstOrDefault()?.Resources.FirstOrDefault();
                var route = resource as Route;
                if (route == null)
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                    return resp;
                }

                resp.Success = true;
                resp.MinDurationOrDistance = Convert.ToInt32(Math.Round(route.TravelDurationTraffic));

                return resp;
            }
            catch (Exception)
            {
                resp.Error = MapResponseError.Exception;
                return resp;
            }
        }


        public async Task<MapMinDistanceResponse> GetDistance(MapPoint from, MapPoint to)
        {


            var resp = new MapMinDistanceResponse();

            try
            {


                var req = new RouteRequest()
                {
                    RouteOptions = new RouteOptions()
                    {
                        Avoid = new List<AvoidType>()
                    {
                        AvoidType.MinimizeTolls
                    },
                        TravelMode = TravelModeType.Driving,
                        DistanceUnits = DistanceUnitType.Kilometers,
                        RouteAttributes = new List<RouteAttributeType>()
                    {
                        RouteAttributeType.All
                    }
                        ,
                        Optimize = RouteOptimizationType.Distance
                    },
                    Waypoints = new List<SimpleWaypoint>()
                {
                    new SimpleWaypoint
                    {
                        Coordinate = new Coordinate{ Latitude = from.Latitude, Longitude = from.Longitude }
                    },
                    new SimpleWaypoint
                    {
                        Coordinate = new Coordinate{ Latitude = to.Latitude, Longitude = to.Longitude }
                    }
                },
                    BingMapsKey = _apiKey
                };

                var res = await req.Execute();
                if (res.ResourceSets == null ||
                    !res.ResourceSets.Any() ||
                    res.ResourceSets.First().Resources == null ||
                    !res.ResourceSets.First().Resources.Any())
                {

                }
                var resource = res.ResourceSets.FirstOrDefault()?.Resources.FirstOrDefault();
                var route = resource as Route;
                if (route == null)
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                    return resp;
                }

                resp.Success = true;
                resp.Distance = Convert.ToInt32(Math.Round(route.TravelDistance));

                return resp;

            }
            catch (Exception)
            {
                resp.Error = MapResponseError.Exception;
                return resp;
            }
        }


        public async Task<MapMinDurationOrDistanceResponse> GetMinDuration(MapPoint from, MapPoint to, int DrivingMode)
        {
            var resp = new MapMinDurationOrDistanceResponse();
            try
            {


                var req = new RouteRequest()
                {
                    RouteOptions = new RouteOptions()
                    {
                        Avoid = new List<AvoidType>()
                    {
                        AvoidType.MinimizeTolls
                    },
                        TravelMode = TravelModeType.Driving,
                        DistanceUnits = DistanceUnitType.Kilometers,
                        RouteAttributes = new List<RouteAttributeType>()
                    {
                        RouteAttributeType.All
                    }
                        ,
                        Optimize = RouteOptimizationType.TimeWithTraffic
                    },
                    Waypoints = new List<SimpleWaypoint>()
                {
                    new SimpleWaypoint
                    {
                        Coordinate = new Coordinate{ Latitude = from.Latitude, Longitude = from.Longitude }
                    },
                    new SimpleWaypoint
                    {
                        Coordinate = new Coordinate{ Latitude = to.Latitude, Longitude = to.Longitude }
                    }
                },
                    BingMapsKey = _apiKey
                };


                if (DrivingMode == (int)TransportTypeEnum.Walk)
                {
                    req.RouteOptions.TravelMode = TravelModeType.Walking;
                }
                else if (DrivingMode == (int)TransportTypeEnum.Car || DrivingMode == (int)TransportTypeEnum.Scooter)
                {
                    req.RouteOptions.TravelMode = TravelModeType.Driving;
                }
                else if (DrivingMode == (int)TransportTypeEnum.Truck)
                {
                    req.RouteOptions.TravelMode = TravelModeType.Truck;
                }
                else if (DrivingMode == (int)TransportTypeEnum.Cycle)
                {
                    req.RouteOptions.TravelMode = TravelModeType.Walking;
                }


                

                var res = await req.Execute();
                if (res.ResourceSets == null ||
                    !res.ResourceSets.Any() ||
                    res.ResourceSets.First().Resources == null ||
                    !res.ResourceSets.First().Resources.Any())
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                    return resp;
                }
                var resource = res.ResourceSets.FirstOrDefault()?.Resources.FirstOrDefault();
                var route = resource as Route;
                if (route == null)
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                    return resp;
                }

                resp.Success = true;
                resp.MinDurationOrDistance = Convert.ToInt32(Math.Round(route.TravelDurationTraffic));

                return resp;
            }
            catch (Exception)
            {
                resp.Error = MapResponseError.Exception;
                return resp;
            }
        }

    }
}
