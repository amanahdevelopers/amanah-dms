﻿using GeoCoordinatePortable;
using MapsUtilities.Models;
using System;
using System.Linq;

namespace MapsUtilities
{
    public static class MapsHelper
    {
        /// <summary>
        /// Gets distance between 2 points in meters
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns>Distnace in meters</returns>
        public static double GetDistanceBetweenPoints(MapPoint point1, MapPoint point2)
        {
            return GetDistanceBetweenPoints(point1.Latitude, point1.Longitude, point2.Latitude, point2.Longitude);
        }
        /// <summary>
        /// Gets distance between 2 points in meters
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="long1"></param>
        /// <param name="lat2"></param>
        /// <param name="long2"></param>
        /// <returns>Distnace in meters</returns>
        public static double GetDistanceBetweenPoints(double lat1, double long1, double lat2, double long2)
        {
            GeoCoordinate pin1 = new GeoCoordinate(lat1, long1);
            GeoCoordinate pin2 = new GeoCoordinate(lat2, long2);

            return pin1.GetDistanceTo(pin2);
        }

        /// <summary>
        /// Determines if the given point is inside the polygon
        /// </summary>
        /// <param name="polygon">the vertices of polygon</param>
        /// <param name="testPoint">the given point</param>
        /// <returns>true if the point is inside the polygon; otherwise, false</returns>
        public static bool IsPointInPolygon(MapPoint[] polygon, MapPoint point)
        {
            bool isInside = false;
            for (int i = 0, j = polygon.Length - 1; i < polygon.Length; j = i++)
            {
                if (((polygon[i].Latitude > point.Latitude) != (polygon[j].Latitude > point.Latitude)) &&
                (point.Longitude < (polygon[j].Longitude - polygon[i].Longitude) * (point.Latitude - polygon[i].Latitude) / (polygon[j].Latitude - polygon[i].Latitude) + polygon[i].Longitude))
                {
                    isInside = !isInside;
                }
            }
            return isInside;
        }
    }
}
