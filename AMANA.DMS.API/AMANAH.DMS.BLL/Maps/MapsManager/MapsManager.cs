﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.DATA.Entities;
using MapsUtilities.MapsAccessor;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace MapsUtilities.MapsManager
{
    public class MapsManager : IMapsManager
    {
        private MapRequestSettings _settings;
        private MapPoint _from;
        private MapPoint _to;

        protected readonly IRepositry<MapRequestLog> _repository;
        protected readonly DbContext _context;

        public MapsManager(IRepositry<MapRequestLog> repository, DbContext context)
        {
            _repository = repository;
            _context = context;
        }
        public async Task<MapMinDistanceResponse> GetDistance(MapPoint from, MapPoint to, MapRequestSettings settings)
        {
            _settings = settings;
            _from = from;
            _to = to;

            switch (settings.Priority)
            {
                case MapPriority.Default:
                    return await GetDistanceInternal(MapType.Google, MapType.Google, MapType.Google); 
                case MapPriority.Google:
                    return await GetDistanceInternal(MapType.Google, MapType.Google, MapType.Google);
                case MapPriority.Bing:
                    return await GetDistanceInternal(MapType.Google, MapType.Google, MapType.Google);
                default:
                    throw new NotImplementedException($"Settings priority {Enum.GetName(typeof(MapPriority), settings.Priority)} is not implemented");
            }
        }
        public async Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistance(MapPoint from, MapPoint to, MapRequestSettings settings)
        {
            _settings = settings;
            _from = from;
            _to = to;

            switch (settings.Priority)
            {
                case MapPriority.Default:
                    return await GetMinDurationOrDistanceInternal(MapType.Google, MapType.Google);
                case MapPriority.Google:
                    return await GetMinDurationOrDistanceInternal(MapType.Google, MapType.Google);
                case MapPriority.Bing:
                    return await GetMinDurationOrDistanceInternal(MapType.Google, MapType.Google);
                default:
                    return await GetMinDurationOrDistanceInternal(MapType.Google, MapType.Google);
            }
        }


        public async Task<MapMinDurationOrDistanceResponse> GetMinDuration(MapPoint from, MapPoint to, int drivingMode, MapRequestSettings settings)
        {
            _settings = settings;
            _from = from;
            _to = to;

            switch (settings.Priority)
            {
                case MapPriority.Default:
                    return await GetMinDurationInternal(drivingMode, MapType.Google, MapType.Google, MapType.Google);
                case MapPriority.Google:
                    return await GetMinDurationInternal(drivingMode, MapType.Google, MapType.Google, MapType.Google);
                case MapPriority.Bing:
                    return await GetMinDurationInternal(drivingMode, MapType.Google, MapType.Google, MapType.Google);
                default:
                    throw new NotImplementedException($"Settings priority {Enum.GetName(typeof(MapPriority), settings.Priority)} is not implemented");
            }
        }

        private async Task<MapMinDistanceResponse> GetDistanceInternal(params MapType[] types)
        {
               var resp = new MapMinDistanceResponse();
            foreach (var type in types)
            {
                resp = await MapsAccessorFactory.GetMapsAccessor(type, _settings, _repository, _context)
                                .GetDistance(_from, _to);
                if (resp.Success)
                    return resp;
                if ((resp.Error == MapResponseError.QuotaLimit && !_settings.QuotaFallback) ||
                    (resp.Error == MapResponseError.RouteNotAvailable && !_settings.NoRouteFallback))
                    return resp;
            }
            return resp;
        }

        private async Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistanceInternal(params MapType[] types)
        {
            var resp = new MapMinDurationOrDistanceResponse();

                 foreach (var type in types)
                {
                    resp = await MapsAccessorFactory.GetMapsAccessor(type, _settings, _repository, _context)
                                    .GetMinDurationOrDistance(_from, _to);
                    if (resp.Success)
                        return resp;
                    if ((resp.Error == MapResponseError.QuotaLimit && !_settings.QuotaFallback) ||
                        (resp.Error == MapResponseError.RouteNotAvailable && !_settings.NoRouteFallback))
                        return resp;
                }
                return resp;
          
            }

        private async Task<MapMinDurationOrDistanceResponse> GetMinDurationInternal(int drivingMode, params MapType[] types)
        {
            var resp = new MapMinDurationOrDistanceResponse();
            foreach (var type in types)
            {
                resp = await MapsAccessorFactory.GetMapsAccessor(type, _settings, _repository, _context)
                                .GetMinDuration(_from, _to, drivingMode);
                if (resp.Success)
                    return resp;
                if ((resp.Error == MapResponseError.QuotaLimit && !_settings.QuotaFallback) ||
                    (resp.Error == MapResponseError.RouteNotAvailable && !_settings.NoRouteFallback))
                    return resp;
            }
            return resp;
        }
    }
}
