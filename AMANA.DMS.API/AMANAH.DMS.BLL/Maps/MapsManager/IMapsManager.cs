﻿using MapsUtilities.Models;
using System.Threading.Tasks;

namespace MapsUtilities.MapsManager
{
    public interface IMapsManager
    {
        Task<MapMinDistanceResponse> GetDistance(MapPoint from, MapPoint to, MapRequestSettings settings);
        Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistance(MapPoint from, MapPoint to, MapRequestSettings settings);
        Task<MapMinDurationOrDistanceResponse> GetMinDuration(MapPoint from, MapPoint to, int drivingMode, MapRequestSettings settings);
    }
}
