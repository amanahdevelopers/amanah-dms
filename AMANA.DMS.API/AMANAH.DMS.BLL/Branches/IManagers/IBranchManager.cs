﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IBranchManager : IBaseManager<BranchViewModel, Branch>
    {
        Task<BranchViewModel> Get(int id);
        BranchViewModel GetbyId(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
        Task<bool> SetActivate(int id, bool isActive, string Reason);
        Task<List<BranchViewModel>> GetBranchesBy(int resturantId);
        Task<List<BranchViewModel>> GetBranchesBy(int resturantId, List<int> Ids);
        Task<List<BranchViewModel>> GetBranchesBy(int[] resturantIds, int[] geoFenceIds);
        Task<List<BranchViewModel>> GetBranchesBy(int[] resturantIds, int[] geoFenceIds, List<int> Ids);
        Task<PagedResult<BranchViewModel>> GetBranchesByRestaurantPagging(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<BranchViewModel>> GetBranchesByRestaurantPagging(PaginatedItemsViewModel pagingparametermodel, List<int> Ids);
        Task<List<BranchViewModel>> GetByNameAsync(string name);
        Task<List<BranchViewModel>> GetByNameAsync(string name, List<int> Ids);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> Ids);
        Task<PagedResult<BranchViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel, List<int> Ids);
        Task<List<int>> GetManagerBranchesAsync();
    }
}
