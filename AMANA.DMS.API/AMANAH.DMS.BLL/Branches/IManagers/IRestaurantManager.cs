﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IRestaurantManager : IBaseManager<RestaurantViewModel, Restaurant>
    {
        Task<RestaurantViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
        Task<bool> SetActivate(int id, bool isActive, string reason);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> Ids);
        Task<PagedResult<RestaurantViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel, List<int> Ids);
        Task<List<int>> GetManagerRestaurantsAsync();
    }
}
