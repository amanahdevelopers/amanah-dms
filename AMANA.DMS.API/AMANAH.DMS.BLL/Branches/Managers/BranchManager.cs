﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilites;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class BranchManager : BaseManager<BranchViewModel, Branch>, IBranchManager
    {
        private readonly ICurrentUser _currentUser;

        public BranchManager(
            ApplicationDbContext context,
            IRepositry<Branch> repository,
            IMapper _mapper,
            ICurrentUser currentUser)
            : base(context, repository, _mapper)
        {
            _currentUser = currentUser;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var branch = context.Branch.SingleOrDefault(t => t.Id == id);

            if (branch == null)
            {
                return false;
            }
            var data = await base.SoftDeleteAsync(mapper.Map<BranchViewModel>(branch));
            return data;

        }

        public async Task<BranchViewModel> Get(int id)
        {
            return await context.Branch
                .Where(x => x.Id == id)
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
        public BranchViewModel GetbyId(int id)
        {
            return context.Branch
                .Where(x => x.Id == id)
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefault();
        }
        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await context.Branch
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> SetActivate(int id, bool isActive, string Reason)
        {
            var branch = await context.Branch.Where(x => x.Id == id).SingleOrDefaultAsync();
            if (branch == null)
            {
                return false;
            }
            var restaurant = await context.Restaurant
                .Where(x => x.Id == branch.RestaurantId)
                .SingleOrDefaultAsync();
            if (restaurant == null || !restaurant.IsActive)
            {
                return false;
            }
            branch.IsActivePrevious = branch.IsActive;
            branch.IsActive = isActive;
            branch.Reason = Reason;

            context.Entry(branch);
            await context.SaveChangesAsync();
            return true;
        }

        public Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> Ids)
        {
            return repository.GetAll()
                .Include(x => x.Restaurant)
                .Where(x => Ids.Contains(x.Id) && x.Restaurant.IsActive == true && x.IsActive == true)
                .ProjectTo<TProjectedModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(bool IgnoreTenant)
        {
            return repository.GetAll()
                .Include(x => x.Restaurant)
                .Where(x => x.Restaurant.IsActive == true && x.IsActive == true)
                .ProjectTo<TProjectedModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public Task<PagedResult<BranchViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel, List<int> Ids)
        {
            return repository.GetAll()
                .Where(x => Ids.Contains(x.Id))
                .ToPagedResultAsync<Branch, BranchViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
        }

        public Task<List<BranchViewModel>> GetByNameAsync(string name)
        {
            return repository.GetAll()
                .Where(x =>
                    x.IsActive == true &&
                    x.IsDeleted == false &&
                    x.Restaurant.IsActive == true &&
                    x.Name.ToLower().Contains(name.ToLower()) && x.IsDeleted == false)
                .Take(10)
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<BranchViewModel>> GetByNameAsync(string name, List<int> Ids)
        {
            return repository.GetAll()
                .Where(x =>
                    x.IsActive == true &&
                    x.IsDeleted == false &&
                    x.Restaurant.IsActive == true &&
                    x.Name.ToLower().Contains(name.ToLower()) &&
                    Ids.Contains(x.Id))
                .Take(10)
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<BranchViewModel>> GetBranchesBy(int resturantId)
        {
            return repository.GetAll()
                .Include(x => x.Restaurant)
                .Where(x => x.RestaurantId == resturantId)
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<BranchViewModel>> GetBranchesBy(int resturantId, List<int> Ids)
        {
            return repository.GetAll()
                .Include(x => x.Restaurant)
                .Where(x => x.RestaurantId == resturantId && Ids.Contains(x.Id))
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<BranchViewModel>> GetBranchesBy(int[] resturantIds, int[] geoFenceIds, List<int> Ids)
        {
            return repository.GetAll()
                .Include(x => x.Restaurant)
                .Where(x =>
                    resturantIds.Contains(x.RestaurantId) &&
                    Ids.Contains(x.Id) && geoFenceIds.Contains(x.GeoFenceId))
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<BranchViewModel>> GetBranchesBy(int[] resturantIds, int[] geoFenceIds)
        {
            return repository.GetAll()
                .Include(x => x.Restaurant)
                .Where(x =>
                    resturantIds.Contains(x.RestaurantId) &&
                    x.IsActive == true &&
                    geoFenceIds.Contains(x.GeoFenceId))
                .ProjectTo<BranchViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public override async Task<BranchViewModel> AddAsync(BranchViewModel viewModel)
        {
            var branch = await base.AddAsync(viewModel);
            var customer = new Customer()
            {
                Email = Helper.RemoveSpecialCharacters(viewModel.Name.Trim().ToLower()) + "_" + branch.Id + "@dhub.com",
                Address = branch.Address,
                BranchId = branch.Id,
                CountryId = branch.CountryId,
                Latitude = Convert.ToDouble(branch.Latitude),
                Longitude = Convert.ToDouble(branch.Longitude),
                Name = branch.Name,
                Phone = branch.Phone
            };
            context.Customers.Add(customer);
            await context.SaveChangesAsync();
            return branch;
        }

        public override async Task<List<BranchViewModel>> AddAsync(List<BranchViewModel> ViewModelLst)
        {
            var list = await base.AddAsync(ViewModelLst);
            foreach (var branch in list)
            {
                var customer = new Customer()
                {
                    Address = branch.Address,
                    BranchId = branch.Id,
                    CountryId = branch.CountryId,
                    Latitude = Convert.ToDouble(branch.Latitude),
                    Longitude = Convert.ToDouble(branch.Longitude),
                    Name = branch.Name,
                    Phone = branch.Phone,
                };
                context.Customers.Add(customer);
                await context.SaveChangesAsync();
            }
            return list;
        }

        public override async Task<bool> UpdateAsync(BranchViewModel viewModel)
        {
            var result = await base.UpdateAsync(viewModel);
            await UpdateCustomerAsync(viewModel);
            return result;
        }

        private async Task UpdateCustomerAsync(BranchViewModel viewModel)
        {
            var customer = await context.Customers
                .FirstOrDefaultAsync(x => x.BranchId == viewModel.Id);
            customer.Address = viewModel.Address;
            customer.BranchId = viewModel.Id;
            customer.CountryId = viewModel.CountryId;
            customer.Latitude = Convert.ToDouble(viewModel.Latitude);
            customer.Longitude = Convert.ToDouble(viewModel.Longitude);
            customer.Name = viewModel.Name;
            customer.Phone = viewModel.Phone;
            context.Customers.Update(customer);
            await context.SaveChangesAsync();
        }

        public Task<PagedResult<BranchViewModel>> GetBranchesByRestaurantPagging(PaginatedItemsViewModel pagingparametermodel)
        {
            return repository.GetAll()
                .Include(x => x.Customers)
                .Where(x => x.RestaurantId == pagingparametermodel.Id)
                .ToPagedResultAsync<Branch, BranchViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
        }

        public Task<PagedResult<BranchViewModel>> GetBranchesByRestaurantPagging(
            PaginatedItemsViewModel pagingparametermodel, List<int> Ids)
        {
            return repository.GetAll()
                .Where(x =>
                    x.RestaurantId == pagingparametermodel.Id &&
                    Ids.Contains(x.Id))
                .ToPagedResultAsync<Branch, BranchViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
        }

        public async Task<List<int>> GetManagerBranchesAsync()
        {
            if (string.IsNullOrEmpty(_currentUser.Id))
            {
                return null;
            }
            var ManagerDispatchinglist = await context.ManagerDispatching
                .Include(x => x.Manager)
                .Where(x => x.Manager.UserId == _currentUser.Id || x.CreatedBy_Id == _currentUser.UserName)
                .ToListAsync();
            var branches = ManagerDispatchinglist.Select(x => x.Branches).ToList();
            var zones = ManagerDispatchinglist.Select(x => x.Zones).ToList();
            var createdBranchesIds = await context.Branch
                .Where(x => x.CreatedBy_Id == _currentUser.UserName)
                .Select(x => x.Id)
                .ToListAsync();

            var branchesIds = new List<int>();
            if (branches != null && branches.Any())
            {
                foreach (var b in branches)
                {
                    if (b.Trim() == "All")
                    {
                        foreach (var zone in zones)
                        {
                            branchesIds = await context.Branch
                             .Where(branch => zone.Contains("All") || zone.Contains(branch.GeoFenceId.ToString()))
                             .Select(branch => branch.Id)
                             .ToListAsync();
                        }
                        break;
                    }
                    else
                    {
                        branchesIds.AddRange(b.Split(',').Select(s => int.Parse(s)));
                    }
                }
                createdBranchesIds.ForEach(x =>
                {
                    if (branchesIds.Where(y => y == x).Count() == 0)
                    {
                        branchesIds.Add(x);
                    }
                });
            }

            return branchesIds;
        }
    }
}
