﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
   public class CreateBranchViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public int RestaurantId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int ManagerId { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }
        public AddressViewModel Location {set; get;}
    }
}
