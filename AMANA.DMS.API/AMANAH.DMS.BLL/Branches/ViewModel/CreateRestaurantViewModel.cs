﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class CreateRestaurantViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
