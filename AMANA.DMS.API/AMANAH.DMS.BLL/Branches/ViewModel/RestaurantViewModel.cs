﻿using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
  public class RestaurantViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }

    }
}
