﻿namespace AMANAH.DMS.BLL.Enums
{
    public enum AutoAllocationTypeEnum  :byte
    {
        Manual=0,
        Fifo,
        Nearest,
        OneByOne
    }
}
