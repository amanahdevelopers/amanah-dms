﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.ViewModels;
using MapsUtilities;
using MapsUtilities.MapsManager;
using MapsUtilities.Models;
using Microsoft.Extensions.Configuration;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public abstract class TaskAutoAssignment
    {
        protected readonly ITasksManager _tasksManager;
        protected readonly ISettingsManager _settingsManager;
        protected readonly IMapsManager _mapsManager;
        protected readonly IConfiguration _configuration;
        protected readonly IDriverManager _driverManager;
        protected readonly MapRequestSettings _mapsSettings;

        public TaskAutoAssignment(
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            IMapsManager mapsManager,
            IConfiguration configuration,
            IDriverManager driverManager)
        {
            _tasksManager = tasksManager;
            _settingsManager = settingsManager;
            _mapsManager = mapsManager;
            _configuration = configuration;
            _driverManager = driverManager;

            _mapsSettings = new MapRequestSettings
            {
                Priority = MapPriority.Default,
                GoogleApiKey = _configuration["MapsSettings:GoogleKey"],
                BingApiKey = _configuration["MapsSettings:BingKey"]
            };
        }
        public abstract Task<CreateTaskResponse> AssignDriverToTask(MainTaskViewModel task);

        public async virtual Task<CreateTaskResponse> AssignTasksToDriver(int driverId)
        {
            return new CreateTaskResponse { StatusCode = HttpStatusCode.OK };
        }

        public async Task<List<DriverViewModel>> GetAvailableDrivers(MainTaskViewModel task)
        {
            var input = new GetAailableDriversInput
            {
                TeamIds = task.Settings?.TeamIds,
                Tags = task.Settings?.Tags,
                PickupGeoFenceIds = task.Settings?.PickupGeoFenceIds,
                DeliveryGeoFenceIds = task.Settings?.DeliveryGeoFenceIds
            };

            var availableDrivers = await _tasksManager.GetAvailableDrivers(input);
            return availableDrivers;
        }

        public async Task<List<DriverViewModel>> SortDriversByNearestAsync(MainTaskViewModel mainTask, List<DriverViewModel> drivers)
        {
            if (mainTask.Tasks == null || !mainTask.Tasks.Any())
            {
                throw new ArgumentException($"Main task {mainTask.Id} has no child tasks");
            }

            foreach (var driver in drivers)
            {
                driver.RouteDistanceToTask = await GetDurationOrDistanceAsync(mainTask.Tasks.First(), driver);
            }
            drivers = drivers.Where(x => x.RouteDistanceToTask.HasValue)
                .OrderBy(x => x.RouteDistanceToTask)
                .ToList();

            return drivers;
        }

        /// <summary>
        /// Gets distance in KM between between task location and driver location
        /// </summary>
        /// <param name="task"></param>
        /// <param name="driver"></param>
        /// <returns>Distnace in KM</returns>
        public async Task<double?> GetDistance(TasksBaseInfoViewModel task, DriverViewModel driver)
        {
            if (!task.Latitude.HasValue || !task.Longitude.HasValue)
                throw new ArgumentNullException("Latitude and longitude can't be null");

            var d = MapsHelper.GetDistanceBetweenPoints(task.Latitude.Value, task.Longitude.Value, driver.Latitude.Value, driver.Longitude.Value);

            //Convert to km
            return d / 1000;
        }

        /// <summary>
        /// Gets distance in KM between between task location and driver location
        /// </summary>
        /// <param name="mainTask"></param>
        /// <param name="driver"></param>
        /// <returns>Distnace in KM</returns>
        public async Task<double?> GetDistance(MainTaskViewModel mainTask, DriverViewModel driver)
        {
            return await GetDistance(mainTask.Tasks.First(), driver);
        }

        private async Task<double?> GetDurationOrDistanceAsync(TasksBaseInfoViewModel task, DriverViewModel driver)
        {
            if (!task.Latitude.HasValue ||
                !task.Longitude.HasValue ||
                !driver.Latitude.HasValue ||
                !driver.Longitude.HasValue)
            {
                throw new ArgumentNullException("Latitude and longitude can't be null");
            }
            var result = await _mapsManager.GetMinDurationOrDistance(
                 new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value },
                 new MapPoint { Latitude = driver.Latitude.Value, Longitude = driver.Longitude.Value },
                 _mapsSettings);

            if (result.Success)
            {
                return result.MinDurationOrDistance;
            }
            else
            {
                return null;
            }
        }
    }
}
