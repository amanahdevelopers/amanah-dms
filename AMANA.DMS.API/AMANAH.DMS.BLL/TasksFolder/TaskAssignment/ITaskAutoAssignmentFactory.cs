﻿using AMANAH.DMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public interface ITaskAutoAssignmentFactory
    {
        Task<TaskAutoAssignment> GetTaskAutoAssignment();
        Task AssignNextDriver(int mainTaskId);
    }
}
