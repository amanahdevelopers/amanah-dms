﻿using System;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using Microsoft.Extensions.DependencyInjection;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public enum TaskAssignmentType
    {
        Manual,
        NearestAvailable,
        OneByOne,
        FIFO
    }

    public class TaskAutoAssignmentFactory : ITaskAutoAssignmentFactory
    {
        protected readonly ISettingsManager _settingsManager;
        private readonly IServiceProvider _serviceProvider;

        public TaskAutoAssignmentFactory(ISettingsManager settingsManager, IServiceProvider serviceProvider)
        {
            _settingsManager = settingsManager;
            _serviceProvider = serviceProvider;
        }

        public async Task<TaskAutoAssignment> GetTaskAutoAssignment()
        {
            TaskAssignmentType type = TaskAssignmentType.NearestAvailable;

            var method = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.SelectedAutoAllocationMethodName);
            if (method?.Value == Settings.Resources.SettingsKeys.OneByOneMethod)
            {
                type = TaskAssignmentType.OneByOne;
            }
            else if (method?.Value == Settings.Resources.SettingsKeys.FirstInFirstOutMethod)
            {
                type = TaskAssignmentType.FIFO;
            }
            return type switch
            {
                TaskAssignmentType.NearestAvailable =>
                    ActivatorUtilities.CreateInstance<NearestAvailableTaskAssignment>(_serviceProvider),
                TaskAssignmentType.OneByOne => ActivatorUtilities.CreateInstance<OneByOneTaskAssignment>(_serviceProvider),
                TaskAssignmentType.FIFO => ActivatorUtilities.CreateInstance<FirstInFirstOutTaskAssignment>(_serviceProvider),
                _ => throw new NotImplementedException($"Type {type} is not implemented"),
            };
        }

        public async Task AssignNextDriver(int mainTaskId)
        {

        }
    }
}
