﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using Hangfire;
using MapsUtilities.MapsManager;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public class OneByOneTaskAssignment : TaskAutoAssignment
    {
        private static int RETRIES_DEFAULT_VALUE = 3;
        private static int INTERVAL_DEFAULT_VALUE = 60;
        private readonly ILocalizer _localizer;

        public OneByOneTaskAssignment(
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            IMapsManager mapsManager,
            IConfiguration configuration,
            ILocalizer localizer)
            : base(tasksManager, settingsManager, mapsManager, configuration, null)
        {
            _localizer = localizer;
        }

        public override async Task<CreateTaskResponse> AssignDriverToTask(MainTaskViewModel task)
        {
            int interval = INTERVAL_DEFAULT_VALUE;
            var retries = RETRIES_DEFAULT_VALUE;
            var expiresSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.OneByOneAllocationRequestExpiresInSEC);
            if (expiresSetting != null)
            {
                int.TryParse(expiresSetting.Value, out interval);
            }
            var retriesSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.OneByOneAllocationNumberOfRetries);
            if (retriesSetting != null)
            {
                int.TryParse(retriesSetting.Value, out retries);
            }

            var geoFences = await _tasksManager.GetGeofencesForTask(task);
            task.Settings.PickupGeoFenceIds = geoFences.PickupGeoFences.Select(g => g.Id).ToList();
            task.Settings.DeliveryGeoFenceIds = geoFences.DeliveryGeoFences.Select(g => g.Id).ToList();

            task.Settings.MaxRetriesCount = retries;
            task.Settings.RetriesCount = 0;
            task.Settings.RemainingDrivers = task.Settings.DriversCount;
            task.Settings.IntervalInSeconds = interval;
            await StartJob(task);

            return new CreateTaskResponse { MainTask = task, StatusCode = HttpStatusCode.Created };
        }

        public async Task StartJob(MainTaskViewModel mainTask)
        {
            int interval = mainTask.Settings.IntervalInSeconds;

            if (!mainTask.Tasks.First().DriverId.HasValue)//First time
            {
                mainTask.Settings.RetriesCount = 0;

                var availableDrivers = await GetAvailableDrivers(mainTask);
                availableDrivers = await SortDriversByNearestAsync(mainTask, availableDrivers);
                //availableDrivers = availableDrivers.OrderByDescending(x => x.RouteDistanceToTask).ToList();
                var driver = availableDrivers.FirstOrDefault();

                if (driver == null)  //if no availlable drivers 
                {
                    return;
                }

                mainTask.AssignmentType = (int)TaskAssignmentType.OneByOne;
                mainTask.ExpirationDate = DateTime.UtcNow.AddSeconds(interval);

                await _tasksManager.UpdateTaskDriverAsync(
                    mainTask.Id,
                    driver.Id,
                    status: TaskStatusEnum.Assigned,
                    assignmentType: TaskAssignmentType.OneByOne,
                    expirationDate: DateTime.UtcNow.AddSeconds(interval));

                mainTask.Tasks.ForEach(t => t.DriverId = driver.Id);

                await SendTaskNotificationToDriver(driver.Id, mainTask, interval);
                var taskDriverRequest = new TaskDriverRequestViewModel
                {
                    MainTaskId = mainTask.Id,
                    DriverId = driver.Id,
                    ResponseStatus = TaskDriverResponseStatusEnum.NoResponse,
                    RetriesCount = 0,
                    CreatedDate = DateTime.UtcNow
                };
                await _tasksManager.CreateTaskDriverRequest(taskDriverRequest);
            }
            else //old task
            {
                var driverId = mainTask.Tasks.First().DriverId.Value;
                var r = await _tasksManager.GetTaskDriverRequests(mainTask.Id, driverId);
                var request = r.FirstOrDefault();
                if (mainTask.Tasks.First().TaskStatusId == (int)Enums.TaskStatusEnum.Accepted ||
                    request.ResponseStatus == TaskDriverResponseStatusEnum.Accepted)
                {
                    //task.Settings.RemainingDrivers--;
                    //if (task.Settings.RemainingDrivers < 1) return;// No needed drivers remainging so exit;

                    ////create new task for the next driver
                    //task.Id = 0;
                    //await StartJob(task);
                    return;
                }

                if (request.RetriesCount >= mainTask.Settings.MaxRetriesCount)
                {
                    request.ResponseStatus = TaskDriverResponseStatusEnum.Expired;
                    await _tasksManager.UpdateTaskDriverRequest(request);
                }

                var prevRequests = await _tasksManager.GetTaskDriverRequests(mainTask.Id);
                var excludedDriverIds = prevRequests.Where(p =>
                p.ResponseStatus == TaskDriverResponseStatusEnum.Expired ||
                p.ResponseStatus == TaskDriverResponseStatusEnum.Declined ||
                p.RetriesCount == mainTask.Settings.RetriesCount)
                    .Select(p => p.DriverId.Value).ToList();

                var allAvailableDrivers = await GetAvailableDrivers(mainTask);

                allAvailableDrivers = await SortDriversByNearestAsync(mainTask, allAvailableDrivers);
                //allAvailableDrivers = allAvailableDrivers.OrderByDescending(x => x.RouteDistanceToTask).ToList();
                var availableDriverIds = allAvailableDrivers.Select(d => d.Id).Except(excludedDriverIds);
                //No more available drivers, start from the begining if task didn't reach max retries
                if (!availableDriverIds.Any())
                {
                    if (mainTask.Settings.RetriesCount < mainTask.Settings.MaxRetriesCount && allAvailableDrivers.Any())
                    {
                        mainTask.Settings.RetriesCount++;
                        await StartJob(mainTask);
                        return;
                    }
                    else //Task reached max retries or no available drivers to retry so end task
                    {
                        await EndTask(mainTask);
                        return;
                    }
                }

                //Next driver Id
                driverId = availableDriverIds.FirstOrDefault();
                var req = prevRequests.Where(p => p.DriverId == driverId).FirstOrDefault();
                if (req != null)//Driver has existing request
                {
                    req.RetriesCount++;
                    await _tasksManager.UpdateTaskDriverRequest(req);
                }
                else
                {
                    req = new TaskDriverRequestViewModel
                    {
                        MainTaskId = mainTask.Id,
                        DriverId = driverId,
                        RetriesCount = 0,
                        ResponseStatus = Enums.TaskDriverResponseStatusEnum.NoResponse
                    };
                    await _tasksManager.CreateTaskDriverRequest(req);
                }

                mainTask.Tasks.ForEach(t => t.DriverId = driverId);
                await _tasksManager.UpdateTaskDriverAsync(mainTask.Id, driverId, interval, status: Enums.TaskStatusEnum.Assigned);
                mainTask.Tasks.ForEach(t => t.DriverId = driverId);
                await SendTaskNotificationToDriver(driverId, mainTask, interval);
            }


            BackgroundJob.Schedule(() => StartJob(mainTask), TimeSpan.FromSeconds(interval));
        }
        /// <summary>
        /// No more drivers to assign
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        private async Task EndTask(MainTaskViewModel task)
        {
            await _tasksManager.UpdateTaskDriverAsync(task.Id, null);
            await _tasksManager.AutoAllocationFail(task.Id);


            return;
        }

        private async Task SendTaskNotificationToDriver(int driverId, MainTaskViewModel mainTask, int interval)
        {
            await _tasksManager.SendPushNotificationToDriver(
                driverId,
                _localizer[Keys.Notifications.TaskPendingForYourAcceptance],
                new
                {
                    mainTask,
                    expirationTime = interval,
                    notificationType = NotificationTypeEnum.NewTask
                });
            await _tasksManager.AutoAllocationSucssesfull(mainTask.Id);
        }
    }
}
