﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using Hangfire;
using MapsUtilities.MapsManager;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public class FirstInFirstOutTaskAssignment : TaskAutoAssignment
    {
        // TODO: this state that is not readonly is a sign of something wrong in this service
        private int clubbingTime;
        private readonly ILocalizer _localizer;

        public FirstInFirstOutTaskAssignment(
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            IMapsManager mapsManager,
            IConfiguration configuration,
            IDriverManager driverManager,
            ILocalizer localizer)
            : base(tasksManager, settingsManager, mapsManager, configuration, driverManager)
        {
            _localizer = localizer;
        }

        /// <summary>
        /// Gets the proper tasks for this driver. This is typically caled when the driver checks in
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public override async Task<CreateTaskResponse> AssignTasksToDriver(int driverId)
        {
            var driverVM = new BackgroundJobViewModel<int> { ViewModel = driverId };
            BackgroundJob.Enqueue(() => DetermineTasksForDriver(driverVM, false));
            await GetDriversWithExpireCluppingTime();
            return new CreateTaskResponse { StatusCode = HttpStatusCode.Created };
        }
        public override async Task<CreateTaskResponse> AssignDriverToTask(MainTaskViewModel task)
        {
            var taskVM = new BackgroundJobViewModel<MainTaskViewModel> { ViewModel = task };
            BackgroundJob.Enqueue(() => DetermineTasksForDrivers(taskVM));
            await GetDriversWithExpireCluppingTime();
            return new CreateTaskResponse { MainTask = task, StatusCode = HttpStatusCode.Created };
        }

        public async Task DetermineTasksForDriver(BackgroundJobViewModel<int> driverVM, bool clubbingTimeOver = false)
        {
            await SetSettingAsync();

            var driverId = driverVM.ViewModel;

            var getDriverInput = new GetDriverInput
            {
                Status = AgentStatusesEnum.Available,
                IsReached = true,
                DriverId = driverId,
                IncludeOrdersWeights = true,
                TaskStatuses = new List<Enums.TaskStatusEnum> { Enums.TaskStatusEnum.Assigned, Enums.TaskStatusEnum.Accepted },
                IncludeDeliveryGeoFences = true,
                IsApplyingAutoAllocation = true,
                AutoAllocationType = AutoAllocationTypeEnum.Fifo
            };
            var driver = await _driverManager.GetDriver(getDriverInput);
            if (driver == null || driver.RemainingOrdersWeights <= 0) return;

            var branchId = driver.BranchId;

            var mainTasksInput = new GetMainTasksInput
            {
                Status = Enums.TaskStatusEnum.Unassigned,
                PickupBranchId = branchId,
                DeliveryGeoFenceIds = (await _driverManager.GetDriverDeliveryGeoFencesAsync(driver.Id))
                    .Select(geofence => geofence.Id)
                    .ToList()
            };
            var mainTasks = await _tasksManager.GetMainTasks(mainTasksInput);
            if (!mainTasks.Any()) return;


            var driverOrderCapacityAfterAssign = driver.RemainingOrdersWeights;
            foreach (var mainTask in mainTasks)
            {
                if (driverOrderCapacityAfterAssign >= 0)
                {
                    await _tasksManager.UpdateTaskDriverAsync(mainTask.Id, driver.Id, status: Enums.TaskStatusEnum.Accepted, assignmentType: TaskAssignmentType.FIFO, reachedTime: driver.ReachedTime);
                    await _tasksManager.UpdateMainTaskStatus(mainTask.Id);
                    await _tasksManager.AutoAllocationSucssesfull(mainTask.Id);
                }
                driverOrderCapacityAfterAssign -= mainTask.OrderWeight;
                if (driverOrderCapacityAfterAssign == 0) break;
            }

            await SendTaskNotificationToDriverAsync(driverId);

            //Driver just reached and no enough tasks
            if (driverOrderCapacityAfterAssign > 0)
            {
                var clubbingTimeSpan = TimeSpan.FromSeconds(clubbingTime);
                await _driverManager.UpdateDriverClubbingTime(new UpdateDriverClubbingTimeViewModel
                {
                    DriverId = driver.Id,
                    IsInClubbingTime = true,
                    ClubbingTimeExpiration = DateTime.UtcNow.AddSeconds(clubbingTime)
                });
                BackgroundJob.Schedule(() => GetDriversWithExpireCluppingTime(), clubbingTimeSpan);
            }
            else
            {
                await _driverManager.UpdateDriverClubbingTime(new UpdateDriverClubbingTimeViewModel
                {
                    DriverId = driver.Id,
                    IsInClubbingTime = false,
                    ClubbingTimeExpiration = null
                });
                await SendReadyToGoNotificationToDriverAsync(driver.Id);
            }
        }
        public async Task DetermineTasksForDrivers(BackgroundJobViewModel<MainTaskViewModel> mainTaskVM)
        {
            await SetSettingAsync();
            var taskId = mainTaskVM.ViewModel.Id;

            var branchId = mainTaskVM.ViewModel?.Tasks?.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup)?.BranchId;
            if (branchId.HasValue) await DetermineTasksForDriversByBranchId(branchId.Value);
            else return;
        }

        private async Task DetermineTasksForDriversByBranchId(int branchId)
        {
            var getDriversInput = new GetDriversInput
            {
                Status = AgentStatusesEnum.Available,
                IsReached = true,
                PickupBranchId = branchId,
                IncludeOrdersWeights = true,
                TaskStatuses = new List<Enums.TaskStatusEnum> { Enums.TaskStatusEnum.Accepted, Enums.TaskStatusEnum.Assigned },
                IncludeDeliveryGeoFences = true,
                IsApplyingAutoAllocation = true,
                AutoAllocationType = AutoAllocationTypeEnum.Fifo
            };
            var drivers = await _driverManager.GetDrivers(getDriversInput);
            if (drivers == null || drivers.Count <= 0) return;

            drivers = drivers.Where(d => d.ReachedTime.HasValue).OrderBy(d => d.ReachedTime.Value).ToList();

            var mainTasksInput = new GetMainTasksInput
            {
                Status = Enums.TaskStatusEnum.Unassigned,
                PickupBranchId = branchId
            };
            var mainTasks = await _tasksManager.GetMainTasks(mainTasksInput);

            var mainTaskgroups = mainTasks.GroupBy(t => t.Tasks?.FirstOrDefault(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery && t.GeoFenceId.HasValue)?.GeoFenceId.Value)
                .OrderBy(g => g.FirstOrDefault()?.Id).ToList();

            foreach (var mainTasksGroup in mainTaskgroups)
            {
                var tasksGroupDeliveryGeoFenceId = mainTasksGroup.Key;
                if (tasksGroupDeliveryGeoFenceId == null) continue;

                var driver = drivers
                    .Where(d =>
                        (
                            d.AllDeliveryGeoFences ||
                            d.DriverDeliveryGeoFences != null &&
                            d.DriverDeliveryGeoFences.Any(g => g.GeoFenceId == tasksGroupDeliveryGeoFenceId)
                        ) &&
                        d.RemainingOrdersWeights > 0)
                    .OrderBy(e => e.ReachedTime).FirstOrDefault();
                if (driver == null) continue;

                // var orderWeights = mainTasksGroup.Sum(t => t.OrderWeight);
                //if (orderWeights > driver.RemainingOrdersWeightsNoWait) continue;

                var mainTasksToAssign = new List<MainTaskViewModel>();
                var unAssignedTasks = mainTasksGroup.Where(t => t.Tasks.All(tt => !tt.DriverId.HasValue || !tt.TaskStatusId.HasValue || tt.TaskStatusId.Value == (int)Enums.TaskStatusEnum.Unassigned));
                foreach (var unassignedMainTask in unAssignedTasks)
                {
                    if ((mainTasksToAssign.Sum(t => t.OrderWeight) + driver.OrdersWeights + unassignedMainTask.OrderWeight) > driver.MaxOrdersWeightsCapacity) continue;

                    unassignedMainTask?.Tasks?.ForEach(t => t.DriverId = driver.Id);
                    unassignedMainTask.IsNewlyAssigned = true;
                    mainTasksToAssign.Add(unassignedMainTask);
                    await _tasksManager.UpdateTaskDriverAsync(unassignedMainTask.Id, driver.Id, status: Enums.TaskStatusEnum.Accepted, assignmentType: TaskAssignmentType.FIFO, reachedTime: driver.ReachedTime);
                    await _tasksManager.UpdateMainTaskStatus(unassignedMainTask.Id);
                    await _tasksManager.AutoAllocationSucssesfull(unassignedMainTask.Id);
                    driver.AssignedTasksCount++;
                }

                if (driver.AssignedMainTasks == null) driver.AssignedMainTasks = new List<MainTaskViewModel>();
                driver.AssignedMainTasks.AddRange(mainTasksToAssign);
            }


            foreach (var driver in drivers?.Where(d => d.AssignedMainTasks != null && d.AssignedMainTasks.Any(t => t.IsNewlyAssigned)))
            {
                await SendTaskNotificationToDriverAsync(driver.Id);
                var driverOrderCapacityAfterAssign = driver.RemainingOrdersWeightsNoWait - driver.AssignedMainTasks.Count(t => t.IsNewlyAssigned);
                if (!driver.IsInClubbingTime && driverOrderCapacityAfterAssign > 0)
                {
                    var clubbingTimeSpan = TimeSpan.FromSeconds(clubbingTime);
                    await _driverManager.UpdateDriverClubbingTime(new UpdateDriverClubbingTimeViewModel
                    {
                        DriverId = driver.Id,
                        IsInClubbingTime = true,
                        ClubbingTimeExpiration = DateTime.UtcNow.AddSeconds(clubbingTime)
                    });
                    BackgroundJob.Schedule(() => GetDriversWithExpireCluppingTime(), clubbingTimeSpan);
                }
                else if (driverOrderCapacityAfterAssign <= 0 || (driver.ClubbingTimeExpiration.HasValue && driver.ClubbingTimeExpiration.Value <= DateTime.UtcNow))
                {
                    await _driverManager.UpdateDriverClubbingTime(new UpdateDriverClubbingTimeViewModel
                    {
                        DriverId = driver.Id,
                        IsInClubbingTime = false,
                        ClubbingTimeExpiration = null
                    });
                    await SendReadyToGoNotificationToDriverAsync(driver.Id);
                }

            }
        }

        public async Task GetDriversWithExpireCluppingTime()
        {
            var drivers = await _driverManager.GetDriversWithExpireCluppingTime();
            foreach (var driver in drivers)
            {
                driver.IsInClubbingTime = false;
                driver.ClubbingTimeExpiration = null;
                _driverManager.Update(driver);
                await SendReadyToGoNotificationToDriverAsync(driver.Id);
            }
        }
        public async Task GetDriversWithExpireCluppingTimeAsync(int driverId)
        {
            var drivers = await _driverManager.GetDriversWithExpireCluppingTime();
            var driver = drivers.FirstOrDefault(e => e.Id == driverId);
            if (driver != null)
            {
                driver.IsInClubbingTime = false;
                driver.ClubbingTimeExpiration = null;
                _driverManager.Update(driver);
                await SendReadyToGoNotificationToDriverAsync(driver.Id);
            }
        }

        private async Task SetSettingAsync()
        {
            clubbingTime = DefaultSettingValue.FirstInFirstOutClubbingTimeInSec;
            var expiresSetting = await _settingsManager.GetSettingByKey(
                Settings.Resources.SettingsKeys.FirstInFirstOutClubbingTimeInSec);
            if (expiresSetting != null)
            {
                int.TryParse(expiresSetting.Value, out clubbingTime);
            }
        }

        private async Task SendReadyToGoNotificationToDriverAsync(int driverId)
        {
            await _tasksManager.SendPushNotificationToDriver(
                driverId,
                _localizer[Keys.Notifications.YourAreReadyToStartTasks],
                new { notificationType = NotificationTypeEnum.ReadyToGo });
        }

        private async Task SendTaskNotificationToDriverAsync(int driverId)
        {
            await _tasksManager.SendPushNotificationToDriver(
                driverId,
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                new { notificationType = NotificationTypeEnum.NewTask });
        }
    }
}
