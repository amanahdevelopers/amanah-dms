﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using Hangfire;
using MapsUtilities.MapsManager;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public class FirstFilledFirstOutTaskAssignment : TaskAutoAssignment
    {
        private static int CLUBBING_TIME_DEFAULT_VALUE = 120;

        private readonly ILocalizer _localizer;

        public FirstFilledFirstOutTaskAssignment(
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            IMapsManager mapsManager,
            IConfiguration configuration,
            IDriverManager driverManager,
            ILocalizer localizer)
            : base(tasksManager, settingsManager, mapsManager, configuration, driverManager)
        {
            _localizer = localizer;
        }

        /// <summary>
        /// Gets the proper tasks for this driver. This is typically caled when the driver checks in
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public override async Task<CreateTaskResponse> AssignTasksToDriver(int driverId)
        {
            var driverVM = new BackgroundJobViewModel<int> { ViewModel = driverId };
            await DetermineTasksToDriver(driverVM, false);
            //BackgroundJob.Enqueue(() => DetermineTasksToDriver(driverVM, false));
            var response = new CreateTaskResponse { StatusCode = HttpStatusCode.Created };
            return response;
        }
        public async Task DetermineTasksToDriver(BackgroundJobViewModel<int> driverVM, bool clubbingTimeOver = false)
        {
            var driverId = driverVM.ViewModel;
            //Get driver with reached status along with tasks already assigned (tasks having this driverId), so we can figure more suitable tasks for this driver
            //This can return null in case the driver is not reached now, this is typically when there are tasks created and assigned to this driver and the driver is ready to go before the clubbing time is over, so when this method is called from the background job after the clubbing time is over it will find the driver status not reached and will not return the driver
            var driver = await _driverManager.GetReachedDriverWithTasks(driverId);
            if (driver == null) return;

            var remainingOrderWeights = driver.MaxOrdersWeightsCapacity - driver.OrdersWeights;
            var mainTasks = new List<MainTaskViewModel>();

            //The driver already has existing tasks, this is typically when this method is called from the background job after clubbing time
            if (driver.AssignedMainTasks.Any())
            {
                var mainTasksInput = new GetMainTasksInput
                {
                    //Get tasks having the same delivery geofence as the tasks (first task is enough) already assigned to the driver, as this geofence now has the highest priority to complete tasks from the same geofence with the same driver
                    DeliveryGeoFenceId = driver.MainTaskDeliveryGeoFenceId,
                    Status = Enums.TaskStatusEnum.Unassigned,
                    Take = remainingOrderWeights,
                };
                mainTasks = await _tasksManager.GetMainTasks(mainTasksInput);
                remainingOrderWeights += mainTasks.Sum(t => t.OrderWeight);
            }

            //This will happen in either 3 cases:
            // 1. No tasks retruned with the same existing geofence
            // 2. Tasks with the same existing geofence didn't complete the driver's capacity
            // 3. Driver doent't have already existing tasks in the first place
            if (remainingOrderWeights < driver.MaxOrdersWeightsCapacity)
            {
                //var geoFencesIds = (await _driverManager.GetDriverDeliveryGeoFencesAsync(driver.Id))
                //    .Select(geofence => geofence.Id)
                //    .ToList();
                var geoFencesIds = driver.DriverDeliveryGeoFences.Select(e => e.Id).ToList();
                var mainTasksInput = new GetMainTasksInput
                {
                    Status = Enums.TaskStatusEnum.Unassigned,
                    DeliveryGeoFenceIds = geoFencesIds
                };
                mainTasks = await _tasksManager.GetMainTasks(mainTasksInput);
                mainTasks = mainTasks.GroupBy(t => t.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.Value)
                    .OrderBy(g => g.First().Id)
                    .SelectMany(g => g)
                    .Take(remainingOrderWeights)
                    .ToList();
            }

            await ProcessAssignTasksToDriver(driver, mainTasks, clubbingTimeOver);
        }

        public override async Task<CreateTaskResponse> AssignDriverToTask(MainTaskViewModel task)
        {
            var taskVM = new BackgroundJobViewModel<MainTaskViewModel> { ViewModel = task };
            BackgroundJob.Enqueue(() => DetermineDriverForTask(taskVM));
#warning cleanup - statuscode has no place here.
            return new CreateTaskResponse { MainTask = task, StatusCode = HttpStatusCode.Created };
        }

        private async Task DetermineDriverForTask(BackgroundJobViewModel<MainTaskViewModel> mainTaskVM)
        {
            var mainTask = mainTaskVM.ViewModel;
            var input = new GetDriversInput
            {
                PickupBranchId = mainTask.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId,
                Status = AgentStatusesEnum.Available,
                IsReached = true
            };
            var drivers = await _driverManager.GetDrivers(input);

            await DetermineDriverForTask(mainTask, drivers);
        }

        private async Task DetermineDriverForTask(MainTaskViewModel mainTask, List<DriverWithTasksViewModel> drivers)
        {
            var taskDeliveryGeoFenceId = mainTask.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.Value;

            var driver = drivers
                .Where(d =>
                    d.MainTaskDeliveryGeoFenceId == taskDeliveryGeoFenceId &&
                    d.RemainingOrdersWeights <= mainTask.OrderWeight)
                .OrderBy(d => d.RemainingOrdersWeights)
                .OrderBy(d => d.DriverDeliveryGeoFences.Count)
                .FirstOrDefault();

            if (driver != null)
                await ProcessAssignTasksToDriver(driver, mainTask);
        }

        private async Task ProcessAssignTasksToDriver(DriverWithTasksViewModel driver, MainTaskViewModel mainTask)
        {
            await ProcessAssignTasksToDriver(driver, new List<MainTaskViewModel> { mainTask });
        }

        private async Task ProcessAssignTasksToDriver(DriverWithTasksViewModel driver, List<MainTaskViewModel> mainTasks, bool clubbingTimeOver = false)
        {
            foreach (var mainTask in mainTasks)
            {
                mainTask.IsNewlyAssigned = true;
                mainTask.Tasks.ForEach(t => t.DriverId = driver.Id);
            }

            //If the driver reached the min orders to go without waiting
            //Or if clubbing time is over
            if (driver.OrdersWeights >= driver.MinOrdersNoWait || clubbingTimeOver)
            {
                foreach (var mainTask in mainTasks)
                {
                    mainTask.Tasks.ForEach(t => t.TaskStatusId = (int)Enums.TaskStatusEnum.Accepted);
                }
                await _tasksManager.UpdateTasks(mainTasks);
                await SendTaskNotificationToDriver(driver.Id, mainTasks);
            }

            //Start clubbing time only if all tasks assigned to the driver are new, if there are old tasks this means clubbing time already started, if no tasks at all this driver will keep waiting without clubbing time counting until the first task is assigned
            else if (driver.AssignedMainTasks.Any(t => t.IsNewlyAssigned) && !driver.AssignedMainTasks.Any(t => !t.IsNewlyAssigned))
            {
                int interval = CLUBBING_TIME_DEFAULT_VALUE;
                var expiresSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.FirstInFirstOutClubbingTimeInSec);
                if (expiresSetting != null)
                {
                    int.TryParse(expiresSetting.Value, out interval);
                }

                var driverVM = new BackgroundJobViewModel<int> { ViewModel = driver.Id };
                BackgroundJob.Schedule(
                    () => DetermineTasksToDriver(driverVM, true),
                    TimeSpan.FromSeconds(interval));
            }
        }

        private async Task SendTaskNotificationToDriver(int driverId, List<MainTaskViewModel> mainTasks)
        {
            await _tasksManager.SendPushNotificationToDriver(
                driverId,
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                new
                {
                    mainTasks,
                    notificationType = NotificationTypeEnum.NewTask
                });
        }
    }
}
