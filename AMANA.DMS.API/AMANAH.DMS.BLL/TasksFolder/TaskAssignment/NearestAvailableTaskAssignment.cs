﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.ViewModels;
using Hangfire;
using MapsUtilities.MapsManager;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public class NearestAvailableTaskAssignment : TaskAutoAssignment
    {
        private readonly ILocalizer _localizer;

        public NearestAvailableTaskAssignment(
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            IMapsManager mapsManager,
            IConfiguration configuration,
            IDriverManager driverCommonManager,
            ILocalizer localizer)
            : base(tasksManager, settingsManager, mapsManager, configuration, driverCommonManager)
        {
            _localizer = localizer;
        }

        public override async Task<CreateTaskResponse> AssignDriverToTask(MainTaskViewModel task)
        {
            task.Settings.RetriesCount = 0;
            var taskVM = new BackgroundJobViewModel<MainTaskViewModel> { ViewModel = task };
            BackgroundJob.Enqueue(() => AssignDriver(taskVM));
            return new CreateTaskResponse { MainTask = task, StatusCode = HttpStatusCode.Created };
        }

        public async Task AssignDriver(BackgroundJobViewModel<MainTaskViewModel> mainTaskVM)
        {
            int autoAllocationNeasrestRetriesCount = DefaultSettingValue.NearestAvailableMethodNumberOfRetries;
            var autoAllocationNeasrestRetriesSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.NearestAvailableMethodNumberOfRetries);
            if (autoAllocationNeasrestRetriesSetting != null) int.TryParse(autoAllocationNeasrestRetriesSetting.Value, out autoAllocationNeasrestRetriesCount);

            mainTaskVM.ViewModel.Settings.RetriesCount++;
            var maintask = mainTaskVM.ViewModel;
            var IsDriverAssignedToTask = await DetermineTaskDriver(maintask);
            if (!IsDriverAssignedToTask)
            {
                if (maintask.Settings.RetriesCount < autoAllocationNeasrestRetriesCount)
                {
                    BackgroundJob.Schedule(() => AssignDriver(mainTaskVM), TimeSpan.FromSeconds(DefaultSettingValue.NearestAvailableMethodReScheduleRequestTimeInSec));
                }
                else
                {
                    await _tasksManager.AutoAllocationFail(maintask.Id);   //  BackgroundJob.Schedule(() => AssignDriver(mainTaskVM), TimeSpan.FromMinutes(1));
                }
            }
        }

        public async Task<bool> DetermineTaskDriver(MainTaskViewModel task)
        {
            if (task?.Settings?.DriversCount == 0)
            {
                return true;
            }
            var mainTask = await _tasksManager.GetMainTaskStatus(task.Id);
            if (mainTask == null || mainTask.IsDeleted) return true;
            if (mainTask.MainTaskStatusId.HasValue && mainTask.MainTaskStatusId.Value != 0)
            {
                return true;
            }
            var geoFences = await _tasksManager.GetGeofencesForTask(task);
            task.Settings.PickupGeoFenceIds = geoFences.PickupGeoFences.Select(g => g.Id).ToList();
            task.Settings.DeliveryGeoFenceIds = geoFences.DeliveryGeoFences.Select(g => g.Id).ToList();
            if (geoFences == null || geoFences.PickupGeoFences == null || geoFences.DeliveryGeoFences == null)
            {
                return false;
            }
            var availableDrivers = await GetAvailableDrivers(task);
            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return false;
            }
            var radiusSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.NearestAvailableMethodRadiusInKM);
            if (availableDrivers != null && radiusSetting != null && double.TryParse(radiusSetting.Value, out double radius))
            {
                var filteredDrivers = new List<DriverViewModel>();
                foreach (var driver in availableDrivers)
                {
                    var distance = await GetDistance(task, driver);
                    var hasCapacity = await IsDriverHasCapacityAsync(driver);
                    if (distance <= radius && hasCapacity)
                    {
                        filteredDrivers.Add(driver);
                    }
                }
                availableDrivers = filteredDrivers;
            }
            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return false;
            }
            availableDrivers = await SortDriversByNearestAsync(task, availableDrivers);
            availableDrivers = availableDrivers?.Take(task.Settings.DriversCount).ToList();
            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return false;
            }
            foreach (var driver in availableDrivers)
            {
                await _tasksManager.UpdateTaskDriverAsync(task.Id, driver.Id, status: Enums.TaskStatusEnum.Accepted, assignmentType: TaskAssignmentType.NearestAvailable, reachedTime: driver.ReachedTime);

                task.Tasks[0].DriverId = driver.Id;

                await _tasksManager.UpdateMainTaskStatus(task.Id);
                await _tasksManager.AutoAllocationSucssesfull(task.Id);
                await _tasksManager.AddDriverTaskNotification(
                    task.Tasks.FirstOrDefault(),
                    _localizer[Keys.Notifications.NewTasksAssignedToYou],
                    (int)NotificationTypeEnum.NewTask);
            }

            return true;
        }

        private async Task<bool> IsDriverHasCapacityAsync(DriverViewModel d)
        {
            int NearstOrderCapacity = 5;
            var getMainTaskInput = new GetMainTasksInput
            {
                DriverId = d.Id,
                Statuses = new List<Enums.TaskStatusEnum>
                {
                    Enums.TaskStatusEnum.Assigned,
                    Enums.TaskStatusEnum.Accepted
                }
            };
            var DriverOrderWight = await _tasksManager.GetTasksWeights(getMainTaskInput);
            var MaximumDriverCapacitySetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.NearestAvailableMethodOrderCapacity);
            if (MaximumDriverCapacitySetting != null)
            {
                int.TryParse(MaximumDriverCapacitySetting.Value, out NearstOrderCapacity);
            }
            if (DriverOrderWight < NearstOrderCapacity)
            {
                return true;
            }
            return false;
        }
    }
}
