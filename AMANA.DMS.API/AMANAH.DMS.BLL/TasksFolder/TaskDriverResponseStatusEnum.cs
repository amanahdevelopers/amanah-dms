﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.Enums
{
    public enum TaskDriverResponseStatusEnum
    {
        Accepted = 1,
        Declined,
        Expired,
        NoResponse,
        Canceled
    }
}
