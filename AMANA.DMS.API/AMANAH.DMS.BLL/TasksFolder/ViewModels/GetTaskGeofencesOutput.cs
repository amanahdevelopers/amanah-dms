﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class GetTaskGeofencesOutput
    {
        public GetTaskGeofencesOutput()
        {
            PickupGeoFences = new List<GeoFenceViewModel>();
            DeliveryGeoFences = new List<GeoFenceViewModel>();
        }
        public List<GeoFenceViewModel> PickupGeoFences { get; set; }
        public List<GeoFenceViewModel> DeliveryGeoFences { get; set; }
    }
}
