﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class TaskCalenderViewModel
    {
        public DateTime? TaskDate { get;  set; }
        public int CompletedCount { get;  set; }
        public int PendingCount { get; set; }
        public double? TotalKM { get; set; }
        public double? TotalTime { get; set; }
    }
}
