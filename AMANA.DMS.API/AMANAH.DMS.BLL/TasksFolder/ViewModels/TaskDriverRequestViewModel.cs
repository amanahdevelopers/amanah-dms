﻿using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class TaskDriverRequestViewModel : BaseViewModel
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public int? DriverId { set; get; }
        public TaskDriverResponseStatusEnum ResponseStatus { set; get; }
        public int RetriesCount { get; set; }
        public MainTaskViewModel MainTask { get; set; }
    }
}
