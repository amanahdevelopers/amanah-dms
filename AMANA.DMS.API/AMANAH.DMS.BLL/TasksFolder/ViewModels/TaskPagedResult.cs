﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class TaskPagedResult<T> : PagedResult<T>
    {
        public int TotalOrderCount { set; get; }
    }
}
