﻿namespace AMANAH.DMS.BLL.ViewModels
{
    public class TaskGallaryViewModel
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public string FileName { set; get; }
        public string FileURL { set; get; }
    }
}
