﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class PaginatedTasksViewModel : PaginatedItemsViewModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<int> TeamIds { get; set; }
        public List<int> TaskStatusIds { get; set; }
        public List<string> FilterColumn { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public List<int> BranchesIds { get; set; }
        public bool GetCustomerTasks { get; set; } = false;    
    }
}
