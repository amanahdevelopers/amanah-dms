﻿using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class TaskChangeStatusViewModel
    {
        public int Id { set; get; }
        public int StatusId { set; get; }
        public string reason { set; get; }  
        public bool IsChangeConnectedTasks { set; get; }
    
    }
}
