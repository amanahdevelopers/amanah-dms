﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class TaskStatusCountViewModel
    {
        public int TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public int Count { set; get; }
    }
}
