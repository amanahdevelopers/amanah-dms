﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class TravelSummeryViewModel
    {
        public int TasksCount { get; set; }
        public double TasksHours { get; set; }
        public double? TasksKMs { get; set; }
        public List<TaskRouteViewModel> TasksRoutes { get; set; }
    }
}
