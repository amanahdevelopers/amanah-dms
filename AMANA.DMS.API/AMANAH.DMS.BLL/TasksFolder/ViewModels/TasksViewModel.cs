﻿using System;
using System.Collections.Generic;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class TasksViewModel : TasksBaseInfoViewModel
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }
        public string DriverPhoneNumber { set; get; }
        public string DriverImageUrl { set; get; }
        public string TeamName { set; get; }
        public string Description { set; get; } = "";
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? GeoFenceId { get; set; }
        public bool? IsTaskReached { get; set; }
        public DateTime? ReachedTime { get; set; }
        public TimeViewModel TotalWaitingTime { get; set; }
        public TimeViewModel TotalEstimationTime { get; set; }

        public CustomerViewModel Customer { set; get; }


        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }
       
        public double? TotalDistance { get; set; }
        public double? TotalTaskTime { get; set; }

        public List<TaskGallaryViewModel> TaskGallaries { set; get; }
    }
}
