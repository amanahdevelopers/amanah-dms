﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Mobile
{
    public class TaskHistoryDetailsViewModel : TasksBaseInfoViewModel
    {
        public DateTime? TaskDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }

        public List<TaskGallaryViewModel> TaskGallaries { set; get; }
    }
}
