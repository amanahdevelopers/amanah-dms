﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class PaginatedTasksDriverViewModel : PaginatedItemsViewModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<int> TaskStatusIds { get; set; }
        public int DriverId { get; set; }
    }
}
