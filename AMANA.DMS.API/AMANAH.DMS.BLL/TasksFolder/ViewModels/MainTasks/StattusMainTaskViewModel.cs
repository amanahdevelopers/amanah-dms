﻿using System.Collections.Generic;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class StattusMainTaskViewModel : BaseMainTaskViewModel
    {
        public List<TasksViewModel> Tasks { set; get; }
        public int MainTaskStatus { get; set; }
        public string MainTaskStatusName { get; set; }
        public string TeamName { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }
        public bool IsEnableAutoAllocation { get; set; }
        public int AutoAllocationType { get; set; }
        public int AssignmentType { get; set; }


    }


}
