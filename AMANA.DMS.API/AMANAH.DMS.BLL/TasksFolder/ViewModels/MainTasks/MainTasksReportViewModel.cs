﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class MainTasksReportViewModel
    {
        public List<string> DriversNames { get; internal set; }
        public List<List<TaskHistoryViewModel>> Histories { get; internal set; }
        public List<string> PickupAddresses { get; internal set; }
        public List<DateTime?> PickupDates { get; internal set; }
        public List<string> DeliveryAddresses { get; internal set; }
        public List<DateTime?> DeliverDates { get; internal set; }
        public List<double> Rates { get; internal set; }
        public List<string> Notes { get; internal set; }
        public int Id { get; internal set; }
    }
}
