﻿namespace AMANAH.DMS.BLL.ViewModels
{
    public class MainTaskCountViewModel
    {
        public int DriverId { get; set; }
        public double TotalMainTasksCount { get; set; }
        public double TotalOrderWeightCapacity { get; set; }

    }
}
