﻿using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class BaseMainTaskViewModel
    {
        public int Id { set; get; }
        public int? MainTaskTypeId { set; get; }
        public string MainTaskTypeName { set; get; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }
        public string DriverImageUrl { set; get; }
        public int NoOfTasks { set; get; }
        public int NoOfCompletedTasks { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }

        public TimeViewModel TotalEstimationTime { get; set; }

    }

}
