﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class ReassignDriverMainTaskViewModel
    {
        public int MainTaskId { get; set; }
        public List<int> DriverIds { get; set; }
        public int TaskStatusId { get; set; }
    }
}
