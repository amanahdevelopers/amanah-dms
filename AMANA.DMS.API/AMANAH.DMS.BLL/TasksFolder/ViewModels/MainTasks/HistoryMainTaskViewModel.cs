﻿using AMANAH.DMS.BLL.ViewModels.Mobile;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class HistoryMainTaskViewModel : BaseMainTaskViewModel
    {
        public List<TaskHistoryDetailsViewModel> Tasks { set; get; }
    }
}
