﻿using System.Collections.Generic;
using AMANAH.DMS.BLL.Enums;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class GetMainTasksInput
    {
        public int? Take { get; set; }
        public int? DriverId { get; set; }
        public List<int> DriverIds { get; set; }
        public int? GeoFenceId { get; set; }
        public int? PickupGeoFenceId { get; set; }
        public int? DeliveryGeoFenceId { get; set; }
        public IList<int> DeliveryGeoFenceIds { get; set; }
        public int? PickupBranchId { get; set; }
        public TaskStatusEnum? Status { get; set; }
        public IList<TaskStatusEnum> Statuses { get; set; }
    }
}
