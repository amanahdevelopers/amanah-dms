﻿using System;
using System.Collections.Generic;
using AMANAH.DMS.BLL.ViewModels.Tasks;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class MainTaskViewModel
    {
        public int Id { set; get; }
        public int MainTaskTypeId { set; get; }
        public string MainTaskTypeName { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }
        public int AssignmentType { get; set; }
        public int? RemainingTimeInSeconds { set; get; }
        public DateTime? ExpirationDate { get; set; }
        public bool IsNewlyAssigned { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }
        //TODO: Temp. return 1 until setting it is implemented
        public int OrderWeight => 1;
        public int? MainTaskStatusId { set; get; }
        public string MainTaskStatusName { set; get; }
        public List<TasksViewModel> Tasks { set; get; }
        public TaskSettingsViewModel Settings { get; set; }
    }
}
