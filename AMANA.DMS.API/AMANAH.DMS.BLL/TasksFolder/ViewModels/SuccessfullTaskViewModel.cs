﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilites.UploadFile;

namespace AMANAH.DMS.BLL.ViewModels.Mobile
{
    public class SuccessfullTaskViewModel
    {
        public int TaskId {set; get;}
        public int MainTaskId {               set; get; }
        public string Notes { set; get; }
        public string Signature { set; get; }

        /// <summary>
        ///  Base64 String for files . 
        /// </summary>
        public List<string> Gallary { set; get; }
        public double? Longitude { set; get; }
        public double? Latitude { set; get; }
        public string Reason { set; get; }

    }
}
