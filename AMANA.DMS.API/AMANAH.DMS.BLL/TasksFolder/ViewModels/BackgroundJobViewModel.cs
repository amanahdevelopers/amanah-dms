﻿namespace AMANAH.DMS.BLL.ViewModels
{
    public class BackgroundJobViewModel<T>
    {
        public BackgroundJobViewModel()
        {
        }

        public T ViewModel { get; set; }
    }
}
