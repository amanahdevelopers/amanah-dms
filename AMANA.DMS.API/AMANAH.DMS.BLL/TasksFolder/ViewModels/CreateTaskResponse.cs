﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Tasks
{
    public class CreateTaskResponse
    {
        public MainTaskViewModel MainTask { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
