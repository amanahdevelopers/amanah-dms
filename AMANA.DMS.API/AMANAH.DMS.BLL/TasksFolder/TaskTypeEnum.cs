﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.Enums
{
    public enum TaskTypeEnum
    {
        Pickup = 1,
        Delivery = 2,
    }
}
