﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IMainTaskManager : IBaseManager<MainTaskViewModel, MainTask>
    {
        Task<MainTaskViewModel> Get(int id);
        Task AcceptAsync(int id, double? longitude = null, double? latitude = null);
        Task DeclineAsync(int id, double? longitude = null, double? latitude = null);       
        Task<TaskPagedResult<StattusMainTaskViewModel>> GetUnassignedAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task<TaskPagedResult<StattusMainTaskViewModel>> GetAssignedAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task<TaskPagedResult<StattusMainTaskViewModel>> GetCompletedAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        bool IsCompleted(int id);
        Task<PagedResult<MainTaskViewModel>> GetDriverTasks(PaginatedTasksDriverViewModel pagingparametermodel);
        Task<PagedResult<MainTaskViewModel>> GetDriverUncompletedTasks(PaginatedTasksDriverViewModel pagingparametermodel);
        Task<List<MainTaskViewModel>> GetDriverNewTasks(int DriverId);
        Task<MainTaskViewModel> AddMainTaskToDriver(MainTaskViewModel mainTask, int driverId);
        Task<bool> TaskHasGeofences(MainTaskViewModel mainTask);
        Task<CreateTaskResponse> AssignMainTaskAsync(AssignDriverMainTaskViewModel assignDriverMainTaskViewModel);
        Task ReassignMainTaskAsync(ReassignDriverMainTaskViewModel reassignDriverMainTaskViewModel);
        Task<PagedResult<HistoryMainTaskViewModel>> GetTaskHistoryDetailsAsync(PaginatedTasksDriverViewModel pagingparametermodel);
        Task<int> GetDriverNewTasksCountAsync(int DriverId);
        Task<PagedResult<StattusMainTaskViewModel>> GetMainTasksByAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task<MainTaskViewModel> TryAutoAssignmentAgain(int id);

    }
}
