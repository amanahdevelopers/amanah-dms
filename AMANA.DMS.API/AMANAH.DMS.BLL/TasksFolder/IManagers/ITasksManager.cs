﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Managers.TaskAssignment;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using MapsUtilities.Models;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ITasksManager : IBaseManager<TasksViewModel, Tasks>
    {
        Task<TasksViewModel> Get(int id);
        Task ChangeStatus(int id, int statusId, string reason);
        Task<bool> SuccessfulAsync(SuccessfullTaskViewModel VM);
        Task<bool> FailedWithSinatureAsync(SuccessfullTaskViewModel VM);
        Task CancelAsync(int id, string reason, double? longitude = null, double? latitude = null);
        Task StartAsync(int id, double? longitude = null, double? latitude = null);
        Task DeclineAsync(int id, double? longitude = null, double? latitude = null);
        Task<PagedResult<TaskHistoryDetailsViewModel>> GetDriverFinishedTasksAsync(PaginatedTasksDriverViewModel pagingparametermodel);
        Task<PagedResult<TasksViewModel>> GetByPaginationAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task ReasignDriverTasksAsync(ReasignDriverViewModel reasignDriverViewModel);
        Task<PagedResult<TaskHistoryViewModel>> GetDriverTimeLineAsync(PaginatedDriverTimeLineViewModel pagingparametermodel);
        Task<List<TaskCalenderViewModel>> GetTaskCalenderAsync(CalenderViewModel calenderViewModel);
        Task<PagedResult<TasksViewModel>> GetTasksReportAsync(PaginatedTasksReportViewModel pagingparametermodel);
        Task<List<ExportTasksWithoutProgressViewModel>> ExportTasksWithoutProgressToExcelAsync(PaginatedTasksReportViewModel pagingparametermodel);
        Task<List<ExportTasksWithProgressViewModel>> ExportTasksWithProgressToExcelAsync(PaginatedTasksReportViewModel pagingparametermodel);
        Task<List<ExportTaskViewModel>> ExportTaskToExcelAsync(int id);
        Task<TravelSummeryViewModel> TravelSummeryAsync(DateTime date, int driverId);
        bool SetReached(int taskId, bool isTaskReached);
        Task<List<TaskStatusCountViewModel>> GetTaskStatusCountAsync(DateTime taskDate);

        Task AddTaskHistory(CreateTaskHistoryViewModel createTaskHistoryViewModel);
        Task AddTaskHistory(List<CreateTaskHistoryViewModel> TaskHistoryViewModelLst);

        Task AddDriverTaskNotification(TasksViewModel TasksViewModel, string msg, int notificationType = 0);
        Task AddDriverTaskNotification(List<TasksViewModel> TasksViewModelLst, string msg, int notificationType = 0);

        TasksViewModel InitStatusTask(TasksViewModel task);
        Task<CustomerViewModel> AddCustomerIsNotExist(CustomerViewModel viewModel);
        Task<TasksViewModel> AddCustomerToTaskAsync(TasksViewModel task);
        Task<List<TasksViewModel>> AddCustomerAndStatusAsync(List<TasksViewModel> ViewModelLst);

        Task SetMainTaskCompleted(int mainTaskId);
        Task SoftDeleteTaskHistory(List<int> taskIds);
        Task SendPushNotificationToDriver(int driverId, string message, dynamic MessageBody);
        //Task<MainTaskViewModel> AddAcceptedAsync(MainTaskViewModel viewModel);
        Task UpdateTasks(List<MainTaskViewModel> mainTasks);
        //Task UpdateTaskDriverAsync(int mainTaskId, int? driverId, int? intervalInSeconds = null);
        Task UpdateTaskDriverAsync(int mainTaskId, int? driverId, int? expirationIntervalInSeconds = null, Enums.TaskStatusEnum? status = null, TaskAssignmentType? assignmentType = null, DateTime? expirationDate = null, DateTime? reachedTime = null);
        Task<MainTaskViewModel> AddAsync(MainTaskViewModel viewModel);
        public Task<List<DriverViewModel>> GetAvailableDrivers(IEnumerable<int> teamIds = null, IEnumerable<string> tags = null, IEnumerable<int> geoFenceIds = null);
        Task<List<DriverViewModel>> GetAvailableDrivers(GetAailableDriversInput input);

        Task CreateTaskDriverRequest(TaskDriverRequestViewModel input);
        Task UpdateTaskDriverRequest(TaskDriverRequestViewModel input);
        Task<List<TaskDriverRequestViewModel>> GetTaskDriverRequests(int mainTaskId, int? driverId = null);
        Task<bool> TaskHasGeofences(MainTaskViewModel mainTask);
        Task<GetTaskGeofencesOutput> GetGeofencesForTask(MainTaskViewModel mainTask);
        Task<Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>> GetGeofencesForTasks(IEnumerable<MainTaskViewModel> mainTasks);
        Task<CustomerViewModel> GetCustomerAsync(CustomerViewModel viewModel);
        double GetTaskTotalTime(Tasks task);
        Task<int> GetTasksWeights(GetMainTasksInput input);
        Task<List<MainTaskViewModel>> GetMainTasks(GetMainTasksInput input);
        Task<Branch> GetRelatedBranchAsync(List<Branch> branches, MapPoint point);
        Task MakeDriverAvailable(int driverId);
        Task SetTaskDistanceHours(Tasks dbTask);
        Task AddTaskRoute(TaskRouteViewModel taskRouteViewModel);
        Task UpdateMainTaskStatus(int mainTaskId);
        Task<bool> AutoAllocationFail(int mainTaskId);
        Task<bool> AutoAllocationSucssesfull(int mainTaskId);
        Task<MainTask> GetMainTaskStatus(int mainTaskId);
        Task<bool> CheckDriverBusy(int driverId);
        Task<bool> SetDeliveryBranch(MainTask mainTask);
        Task<TasksViewModel> UpdateTaskGeoFence(TasksViewModel Task);
        Task<List<TasksViewModel>> UpdateTaskGeoFence(List<TasksViewModel> Tasks);
    }
}