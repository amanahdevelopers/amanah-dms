﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IMainTaskStatusManager : IBaseManager<LkpViewModel, MainTaskStatus>
    {
    }
}
