﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Repoistry;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ITaskStatusManager : IBaseManager<TaskStatusViewModel, DATA.Entities.TaskStatus>
    {
        Task<TaskStatusViewModel> Get(int id);
    }
}
