﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Repoistry;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ITaskRouteManager : IBaseManager<TaskRouteViewModel, DATA.Entities.TaskRoute>
    {
    }
}
