﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.ExternalServcies;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.Managers.TaskAssignment;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hangfire;
using MapsUtilities;
using MapsUtilities.MapsManager;
using MapsUtilities.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Utilites;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;
using TaskStatusEnum = AMANAH.DMS.BLL.Enums.TaskStatusEnum;

namespace AMANAH.DMS.BLL.Managers
{
    public class TasksManager : BaseManager<TasksViewModel, Tasks>, ITasksManager
    {
        //private readonly IDriverManager _driverManager;
        private readonly IUploadFileManager _uploadFileManager;
        private readonly IConfiguration _configuration;
        private readonly ILocalizer _localizer;
        private readonly MapRequestSettings _mapsSettings;
        private readonly IMapsManager _mapsManager;
        private readonly ICurrentUser _currentUser;
        private readonly IPushNotificationService _pushNotificationService;
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly IGeoFenceManager _geoFenceManager;
        private readonly IRepositry<MainTask> _mainTaskrepository;
        private readonly ISettingsManager _settingsManager;
        private readonly ILogger<TasksManager> _logger;

        public TasksManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Tasks> repository,
            IUploadFileManager uploadFileManager,
            //IDriverManager driverManager,
            IConfiguration configuration,
            ILocalizer localizer,
            IMapsManager mapsManager,
            ICurrentUser currentUser,
            IPushNotificationService pushNotificationService,
            IHubContext<NotificationHub> notificationhubContext,
            IGeoFenceManager geoFenceManager,
            IRepositry<MainTask> mainTaskrepository,
            ISettingsManager settingsManager,
            ILogger<TasksManager> logger)
            : base(context, repository, mapper)
        {
            _uploadFileManager = uploadFileManager;
            _configuration = configuration;
            _localizer = localizer;
            //_driverManager = driverManager;
            _mapsManager = mapsManager;
            _currentUser = currentUser;
            _mapsSettings = new MapRequestSettings
            {
                Priority = MapPriority.Default,
                GoogleApiKey = _configuration["MapsSettings:GoogleKey"],
                BingApiKey = _configuration["MapsSettings:BingKey"]
            };
            _pushNotificationService = pushNotificationService;
            _notificationhubContext = notificationhubContext;
            _geoFenceManager = geoFenceManager;
            _mainTaskrepository = mainTaskrepository;
            _settingsManager = settingsManager;
            _logger = logger;
        }

        public async Task<TasksViewModel> Get(int id)
        {
            var task = await context.Tasks
                .Include(x => x.TaskType)
                .Include(x => x.TaskStatus)
                .Include(x => x.Customer)
                .Include(x => x.Driver)
                .Include(x => x.TaskHistories)
                .FirstOrDefaultAsync(x => x.Id == id);
            task.TaskHistories = task.TaskHistories.Where(d => d.IsDeleted == false).ToList();
            return mapper.Map<Tasks, TasksViewModel>(task);
        }

        public async override Task<TasksViewModel> AddAsync(TasksViewModel viewModel)
        {
            viewModel = await AddCustomerToTaskAsync(viewModel);
            viewModel = InitStatusTask(viewModel);
            var taskVM = await base.AddAsync(viewModel);
            var actionName = "CREATED";
            var history = new CreateTaskHistoryViewModel
            {
                TaskVM = taskVM,
                ActionName = actionName,
                ToStatusId = viewModel.TaskStatusId
            };
            await AddTaskHistory(history);
            if (viewModel.DriverId != null && viewModel.DriverId > 0)
            {
                await AddTaskHistory(new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = "ASSIGNED",
                    ToStatusId = (int)TaskStatusEnum.Assigned
                });
            }
            await AddTaskHistory(history);
            await AddDriverTaskNotification(
                taskVM,
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                (int)NotificationTypeEnum.NewTask);

            await UpdateMainTaskStatus(taskVM.MainTaskId);

            return taskVM;
        }

        public async override Task<List<TasksViewModel>> AddAsync(List<TasksViewModel> ViewModelLst)
        {
            ViewModelLst = await AddCustomerAndStatusAsync(ViewModelLst);
            var tasksListVM = await base.AddAsync(ViewModelLst);
            var historiesVM = new List<CreateTaskHistoryViewModel>();
            foreach (var taskVM in tasksListVM)
            {
                historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "CREATED" });
                if (taskVM.DriverId != null && taskVM.DriverId > 0)
                {
                    historiesVM.Add(new CreateTaskHistoryViewModel()
                    {
                        TaskVM = taskVM,
                        ActionName = "ASSIGNED",
                        ToStatusId = (int)TaskStatusEnum.Assigned
                    });
                }
            }
            await AddTaskHistory(historiesVM);
            await AddDriverTaskNotification(
                tasksListVM,
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                (int)NotificationTypeEnum.NewTask);
            return tasksListVM;
        }

        public async override Task<bool> UpdateAsync(TasksViewModel viewModel)
        {
            viewModel = await AddCustomerToTaskAsync(viewModel);
            viewModel = await UpdateTaskGeoFence(viewModel);
            var result = await base.UpdateAsync(viewModel);
            await UpdateMainTaskStatus(viewModel.MainTaskId);
            var actionName = "UPDATED";
            var history = new CreateTaskHistoryViewModel() { TaskVM = viewModel, ActionName = actionName };
            await AddTaskHistory(history);
            await AddDriverTaskNotification(
                viewModel,
                _localizer.Format(Keys.Notifications.TaskNoHasBeenUpdated, viewModel.Id));
            return result;
        }

        public async override Task<bool> SoftDeleteAsync(TasksViewModel viewModel)
        {
            await SoftDeleteTaskHistory(new List<int> { viewModel.Id });
            var result = await base.SoftDeleteAsync(viewModel);
            await UpdateMainTaskStatus(viewModel.MainTaskId);
            await AddDriverTaskNotification(
                viewModel,
                _localizer.Format(Keys.Notifications.TaskNoHasBeenDeletedFromYou, viewModel.Id));
            return result;
        }

        public async override Task<bool> SoftDeleteAsync(List<TasksViewModel> ViewModelLst)
        {
            await SoftDeleteTaskHistory(ViewModelLst.Select(t => t.Id).ToList());
            var result = await base.SoftDeleteAsync(ViewModelLst);
            await AddDriverTaskNotification(ViewModelLst, _localizer[Keys.General.HasBeenDeleted]);
            return result;
        }

        public async Task ChangeStatus(int id, int statusId, string reason)
        {
            var dbTask = await context.Tasks
                .Include(t => t.MainTask)
                .Include(t => t.TaskStatus)
                .Include(t => t.TaskHistories)
                .FirstOrDefaultAsync(t => t.Id == id);

            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            var oldStatusId = dbTask.TaskStatusId;
            dbTask.TaskStatusId = statusId;

            if (statusId == (int)TaskStatusEnum.Started)
            {
                dbTask = SetIsDelayedTask(dbTask);
                var driver = await context.Driver.FindAsync((int)dbTask.DriverId);
                driver.AgentStatusId = (int)AgentStatusesEnum.Busy;
            }
            if (statusId == (int)TaskStatusEnum.Successful)
            {
                dbTask.SuccessfulDate = DateTime.UtcNow;
                dbTask.TotalTime = GetTaskTotalTime(dbTask);
            }
            context.Tasks.Update(dbTask);
            await context.SaveChangesAsync();

            List<int> statusIds = new List<int>()
            {
                (int)TaskStatusEnum.Successful ,
                (int)TaskStatusEnum.Failed ,
                (int)TaskStatusEnum.Cancelled ,
                (int)TaskStatusEnum.Declined
            };
            if (statusIds.Any(e => e == dbTask.TaskStatusId) && dbTask.DriverId.HasValue)
            {
                var isDriverBusyByAnotherTask = context.Tasks.Any(e =>
                    e.DriverId.HasValue &&
                    e.DriverId == dbTask.DriverId &&
                    e.TaskStatusId == (int)TaskStatusEnum.Started);
                if (!isDriverBusyByAnotherTask)
                {
                    var isDriverOnlineFromAnyDevice = context.DriverloginTracking.Any(e => e.DriverId == dbTask.DriverId && !e.LogoutDate.HasValue);
                    var driverAgentStatusId = isDriverOnlineFromAnyDevice ? (int)AgentStatusesEnum.Available : (int)AgentStatusesEnum.Offline;
                    var driver = await context.Driver.FindAsync((int)dbTask.DriverId);
                    driver.AgentStatusId = driverAgentStatusId;
                    await context.SaveChangesAsync();
                }
                await SetMainTaskCompleted(dbTask.MainTaskId);
            }

            var taskVM = mapper.Map<Tasks, TasksViewModel>(dbTask);


            if (dbTask.DriverId.HasValue)
            {

                await MakeDriverAvailable(dbTask.DriverId ?? 0);
            }
            var toStatusVM = await context.TaskStatus.FindAsync(statusId);
            var actionName = toStatusVM?.Name?.ToUpper();
            var history = new CreateTaskHistoryViewModel
            {
                TaskVM = taskVM,
                ActionName = actionName,
                FromStatusId = oldStatusId,
                ToStatusId = statusId,
                Reason = reason
            };
            await AddTaskHistory(history);
            var notification = _localizer.Format(
                Keys.Notifications.TaskNoActionByUserName,
                dbTask.Id,
                actionName.ToLower(),
                _currentUser.UserName);
            await AddDriverTaskNotification(taskVM, notification);
            await UpdateMainTaskStatus(taskVM.MainTaskId);
        }

        private Tasks SetIsDelayedTask(Tasks task)
        {
            task.StartDate = DateTime.UtcNow;
            DateTime? taskDate = null;

            if (task.PickupDate.HasValue)
            {
                taskDate = task.PickupDate.Value;
            }
            else if (task.DeliveryDate.HasValue)
            {
                taskDate = task.DeliveryDate.Value;
            }
            if (taskDate != null && task.StartDate > taskDate)
            {
                task.DelayTime = task.StartDate.Value.Subtract(taskDate.Value).TotalMilliseconds;
                task.MainTask.IsDelayed = true;
                context.MainTask.Update(task.MainTask);
            }

            return task;
        }

        public async Task<bool> SuccessfulAsync(SuccessfullTaskViewModel VM)
        {
            var TaskSignaturesPath = _configuration.GetValue<string>("TaskSignaturesPath");
            var TaskGallaryPath = _configuration.GetValue<string>("TaskGallaryPath");
            var uploadSignatureResult = _uploadFileManager.AddFile(
                new UploadFile { FileContent = VM.Signature, FileName = "FakeName.jpg" },
                TaskSignaturesPath);

            var dbTask = await context.Tasks
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskHistories)
                .Where(x => x.Id == VM.TaskId)
                .FirstOrDefaultAsync();

            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            var oldStatusId = dbTask.TaskStatusId;
            dbTask.Notes = VM.Notes;
            if (uploadSignatureResult.IsSucceeded == true)
            {
                dbTask.SignatureURL = uploadSignatureResult.returnData;
            }
            else { }   //throw new Exception("Can't Upload Task Signature");

            //Add Task Gallay  
            VM?.Gallary?.ForEach(F =>
            {
                var GallaryUploadResult = _uploadFileManager.AddFile(
                    new UploadFile() { FileContent = F, FileName = "FakeName.jpg" },
                    TaskGallaryPath);
                if (GallaryUploadResult.IsSucceeded == true)
                {
                    TaskGallary NewGallaryImage = new TaskGallary();

                    NewGallaryImage.MainTaskId = dbTask.MainTaskId;
                    NewGallaryImage.TaskId = dbTask.Id;
                    NewGallaryImage.FileName = GallaryUploadResult.returnData;
                    NewGallaryImage.FileURL = TaskGallaryPath + "/" + GallaryUploadResult.returnData;
                    NewGallaryImage.DriverId = dbTask.DriverId;

                    context.Add(NewGallaryImage);
                }
                else
                {
                    //throw new Exception("Can't Upload Task Gallary");
                }

            });
            await context.SaveChangesAsync();

            if (uploadSignatureResult.IsSucceeded == true)
            {
                dbTask.SignatureURL = uploadSignatureResult.returnData;
            }
            else { }    //throw new Exception("Can't Upload Task Signature");

            dbTask.TaskStatusId = (int)TaskStatusEnum.Successful;
            dbTask.SuccessfulDate = DateTime.UtcNow;
            dbTask.TotalTime = GetTaskTotalTime(dbTask);
            context.Tasks.Update(dbTask);
            await context.SaveChangesAsync();

            var taskVM = mapper.Map<Tasks, TasksViewModel>(dbTask);
            var histories = new List<CreateTaskHistoryViewModel>();
            string actionName = TaskStatusEnum.Successful.ToString().ToUpper();


            if (!string.IsNullOrEmpty(dbTask.Notes))
            {
                actionName = "ADDED THIS NOTE ";
                var historyNote = new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Successful
                };
                histories.Add(historyNote);
                await AddTaskHistory(historyNote);
            }

            if (VM.Gallary != null && VM.Gallary?.Count > 0)
            {
                actionName = "ADDED THIS IMAGE ";
                var historyGallary = new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Successful
                };
                histories.Add(historyGallary);
                await AddTaskHistory(historyGallary);
            }

            if (uploadSignatureResult.IsSucceeded)
            {
                actionName = "ADDED A SIGNATURE ";
                var historySignature = new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Successful
                };
                histories.Add(historySignature);
                await AddTaskHistory(historySignature);
            }

            var history1 = new CreateTaskHistoryViewModel()
            {
                TaskVM = taskVM,
                ActionName = TaskStatusEnum.Successful.ToString().ToUpper(),
                FromStatusId = oldStatusId,
                ToStatusId = (int)TaskStatusEnum.Successful,
                Longitude = VM.Longitude,
                Latitude = VM.Latitude
            };
            await AddTaskHistory(history1);
            await SetMainTaskCompleted(dbTask.MainTaskId);
            await MakeDriverAvailable(dbTask.DriverId ?? 0);

            if (VM.Longitude != null && VM.Latitude != null)
            {
                var taskRoute = new TaskRouteViewModel()
                {
                    TaskId = dbTask.Id,
                    DriverId = dbTask.DriverId ?? 0,
                    Longitude = VM.Longitude,
                    Latitude = VM.Latitude,
                    TaskStatusId = (int)TaskStatusEnum.Failed,
                };
                await AddTaskRoute(taskRoute);
            }
            await SetTaskDistanceHours(dbTask);
            await UpdateMainTaskStatus(taskVM.MainTaskId);
            return true;
        }


        public async Task<bool> FailedWithSinatureAsync(SuccessfullTaskViewModel VM)
        {
            var TaskSignaturesPath = _configuration.GetValue<string>("TaskSignaturesPath");
            var TaskGallaryPath = _configuration.GetValue<string>("TaskGallaryPath");
            var uploadSignatureResult = _uploadFileManager.AddFile(
                new UploadFile() { FileContent = VM.Signature, FileName = "FakeName.jpg" },
                TaskSignaturesPath);

            var dbTask = await context.Tasks
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskHistories)
                .Where(x => x.Id == VM.TaskId)
                .FirstOrDefaultAsync();

            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            var oldStatusId = dbTask.TaskStatusId;
            dbTask.Notes = VM.Notes;
            if (uploadSignatureResult.IsSucceeded == true)
            {
                dbTask.SignatureURL = uploadSignatureResult.returnData;
            }

            //Add Task Gallay  
            VM?.Gallary?.ForEach(F =>
            {
                var GallaryUploadResult = _uploadFileManager.AddFile(new UploadFile() { FileContent = F, FileName = "FakeName.jpg" }, TaskGallaryPath);
                if (GallaryUploadResult.IsSucceeded == true)
                {
                    TaskGallary NewGallaryImage = new TaskGallary();

                    NewGallaryImage.MainTaskId = dbTask.MainTaskId;
                    NewGallaryImage.TaskId = dbTask.Id;
                    NewGallaryImage.FileName = GallaryUploadResult.returnData;
                    NewGallaryImage.FileURL = TaskGallaryPath + "/" + GallaryUploadResult.returnData;
                    NewGallaryImage.DriverId = dbTask.DriverId;

                    context.Add(NewGallaryImage);
                }
                else
                {
                    //throw new Exception("Can't Upload Task Gallary");
                }

            });
            await context.SaveChangesAsync();

            if (uploadSignatureResult.IsSucceeded == true)
            {
                dbTask.SignatureURL = uploadSignatureResult.returnData;
            }
            else { }
            //throw new Exception("Can't Upload Task Signature");

            dbTask.TaskStatusId = (int)TaskStatusEnum.Failed;
            dbTask.SuccessfulDate = DateTime.UtcNow;
            dbTask.TotalTime = GetTaskTotalTime(dbTask);
            context.Tasks.Update(dbTask);
            await context.SaveChangesAsync();

            var taskVM = mapper.Map<Tasks, TasksViewModel>(dbTask);
            var histories = new List<CreateTaskHistoryViewModel>();
            string actionName = TaskStatusEnum.Successful.ToString().ToUpper();


            if (!string.IsNullOrEmpty(dbTask.Notes))
            {
                actionName = "ADDED THIS NOTE ";
                var historyNote = new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Successful
                };
                histories.Add(historyNote);
                await AddTaskHistory(historyNote);

            }

            if (VM.Gallary != null && VM.Gallary.Count > 0)
            {
                actionName = "ADDED THIS IMAGE ";
                var historyGallary = new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Successful
                };
                histories.Add(historyGallary);
                await AddTaskHistory(historyGallary);

            }

            if (uploadSignatureResult.IsSucceeded)
            {
                actionName = "ADDED A SIGNATURE ";
                var historySignature = new CreateTaskHistoryViewModel
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Successful
                };
                histories.Add(historySignature);
                await AddTaskHistory(historySignature);

            }
            var history1 = new CreateTaskHistoryViewModel()
            {
                TaskVM = taskVM,
                ActionName = "MARKED AS FAILED",
                FromStatusId = oldStatusId,
                ToStatusId = (int)TaskStatusEnum.Failed,
                Longitude = VM.Longitude,
                Latitude = VM.Latitude,
                Reason = VM.Reason
            };
            await AddTaskHistory(history1);
            await SetMainTaskCompleted(dbTask.MainTaskId);
            await MakeDriverAvailable(dbTask.DriverId ?? 0);

            if (VM.Longitude != null && VM.Latitude != null)
            {
                var taskRoute = new TaskRouteViewModel()
                {
                    TaskId = dbTask.Id,
                    DriverId = dbTask.DriverId ?? 0,
                    Longitude = VM.Longitude,
                    Latitude = VM.Latitude,
                    TaskStatusId = (int)TaskStatusEnum.Failed,
                };
                await AddTaskRoute(taskRoute);
            }
            await SetTaskDistanceHours(dbTask);
            await UpdateMainTaskStatus(taskVM.MainTaskId);
            return true;
        }


        public async Task CancelAsync(int id, string reason, double? longitude = null, double? latitude = null)
        {
            var dbTask = await context.Tasks
                .Include(x => x.TaskStatus)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            var oldStatusId = dbTask.TaskStatusId;
            dbTask.TaskStatusId = (int)TaskStatusEnum.Cancelled;
            context.Tasks.Update(dbTask);
            await context.SaveChangesAsync();

            var taskVM = mapper.Map<Tasks, TasksViewModel>(dbTask);
            var actionName = "MARKED AS CANCELED";
            var history = new CreateTaskHistoryViewModel()
            {
                TaskVM = taskVM,
                ActionName = actionName,
                FromStatusId = oldStatusId,
                ToStatusId = (int)TaskStatusEnum.Cancelled,
                Longitude = longitude,
                Latitude = latitude
            };
            await AddTaskHistory(history);
            await SetMainTaskCompleted(dbTask.MainTaskId);
            await MakeDriverAvailable(dbTask.DriverId ?? 0);

            if (longitude != null && latitude != null)
            {
                var taskRoute = new TaskRouteViewModel()
                {
                    TaskId = dbTask.Id,
                    DriverId = dbTask.DriverId ?? 0,
                    Longitude = longitude,
                    Latitude = latitude,
                    TaskStatusId = (int)TaskStatusEnum.Cancelled,
                };
                await AddTaskRoute(taskRoute);
            }
            await SetTaskDistanceHours(dbTask);
            await UpdateMainTaskStatus(taskVM.MainTaskId);
        }

        public async Task StartAsync(int id, double? longitude = null, double? latitude = null)
        {
            var q = context.Tasks
                .Include(x => x.MainTask)
                .Include(x => x.TaskStatus)
                .Include(x => x.Driver.Team)
                .Include(x => x.Driver.Team.LocationAccuracy)
                .Where(x => x.Id == id);
            if (longitude.HasValue && latitude.HasValue)
            {
                q = q.Include(x => x.Driver);
            }
            else
            {
                q = q.Include(x => x.Driver.CurrentLocation);
            }

            var dbTask = await q.FirstOrDefaultAsync();

            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            if (dbTask.TaskTypeId == (int)TaskTypesEnum.Pickup && !dbTask.ReachedTime.HasValue)
            {
                throw new DomainException(Keys.Validation.CanNotStartTaskNotInLocation);
            }
            var oldStatusId = dbTask.TaskStatusId;
            dbTask.TaskStatusId = (int)TaskStatusEnum.Started;
            dbTask = SetIsDelayedTask(dbTask);
            context.Tasks.Update(dbTask);

            if (dbTask.DriverId != null)
            {
                var dbDriver = await context.Driver.Include(x => x.DriverPickUpGeoFences)
                  .Include(x => x.DriverDeliveryGeoFences).FirstOrDefaultAsync(x => x.Id == dbTask.DriverId);
                if (dbDriver != null)
                {
                    dbDriver.AgentStatusId = (int)AgentStatusesEnum.Busy;
                    dbDriver.ReachedTime = null;
                    dbDriver.IsInClubbingTime = false;
                    dbDriver.ClubbingTimeExpiration = null;
                    context.Driver.Update(dbDriver);
                }

                if ((!longitude.HasValue || !latitude.HasValue) && dbTask.Driver.CurrentLocation != null)
                {
                    var currentLocation = dbTask.Driver.CurrentLocation;
                    longitude = currentLocation.Longitude;
                    latitude = currentLocation.Latitude;
                }

                var tasks = await context.Tasks
                    .Where(e => e.MainTaskId == dbTask.MainTaskId)
                    .ToListAsync();
                var lat = latitude;
                var lng = longitude;
                float mainTaskTime = 0;
                foreach (var task in tasks)
                {
                    var duration = await _mapsManager.GetMinDuration(
                        new MapPoint { Latitude = latitude.Value, Longitude = longitude.Value },
                        new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value },
                        dbTask.Driver.TransportTypeId,
                        _mapsSettings);

                    task.EstimatedTime = duration.MinDurationOrDistance;
                    context.Tasks.Update(task);

                    mainTaskTime += duration.MinDurationOrDistance;

                    latitude = task.Latitude;
                    longitude = task.Longitude;
                }

                var mainTask = await context.MainTask.Where(e => e.Id == dbTask.MainTaskId).FirstOrDefaultAsync();
                mainTask.EstimatedTime = mainTaskTime;

                context.MainTask.Update(mainTask);
            }

            await context.SaveChangesAsync();

            var taskVM = mapper.Map<Tasks, TasksViewModel>(dbTask);
            var actionName = TaskStatusEnum.Started.ToString().ToUpper();
            var history = new CreateTaskHistoryViewModel()
            {
                TaskVM = taskVM,
                ActionName = actionName,
                FromStatusId = oldStatusId,
                ToStatusId = (int)TaskStatusEnum.Started,
                Longitude = longitude,
                Latitude = latitude
            };
            await AddTaskHistory(history);
            await SetTaskDistanceHours(dbTask);

            if (longitude != null && latitude != null)
            {
                var taskRoute = new TaskRouteViewModel()
                {
                    TaskId = dbTask.Id,
                    DriverId = dbTask.DriverId ?? 0,
                    Longitude = longitude,
                    Latitude = latitude,
                    TaskStatusId = (int)TaskStatusEnum.Started,
                };
                await AddTaskRoute(taskRoute);
            }

            await UpdateMainTaskStatus(taskVM.MainTaskId);
        }

        public async Task DeclineAsync(int id, double? longitude = null, double? latitude = null)
        {
            var dbTask = await context.Tasks
                .Include(x => x.TaskStatus)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            var oldStatusId = dbTask.TaskStatusId;
            dbTask.TaskStatusId = (int)TaskStatusEnum.Declined;
            context.Tasks.Update(dbTask);
            await context.SaveChangesAsync();
            var taskVM = mapper.Map<Tasks, TasksViewModel>(dbTask);
            var actionName = TaskStatusEnum.Declined.ToString().ToUpper();
            var history = new CreateTaskHistoryViewModel()
            {
                TaskVM = taskVM,
                ActionName = actionName,
                FromStatusId = oldStatusId,
                ToStatusId = (int)TaskStatusEnum.Declined,
                Longitude = longitude,
                Latitude = latitude
            };
            await AddTaskHistory(history);
            await SetMainTaskCompleted(dbTask.MainTaskId);

            if (longitude != null && latitude != null)
            {
                var taskRoute = new TaskRouteViewModel()
                {
                    TaskId = dbTask.Id,
                    DriverId = dbTask.DriverId ?? 0,
                    Longitude = longitude,
                    Latitude = latitude,
                    TaskStatusId = (int)TaskStatusEnum.Failed,
                };
                await AddTaskRoute(taskRoute);
            }
            await SetTaskDistanceHours(dbTask);
            await UpdateMainTaskStatus(taskVM.MainTaskId);
        }

        public Task<Utilities.Utilites.Paging.PagedResult<TaskHistoryDetailsViewModel>> GetDriverFinishedTasksAsync(
            PaginatedTasksDriverViewModel pagingparametermodel)
        {

            var statuslst = new List<int>
            {
                (int)TaskStatusEnum.Successful,
                (int)TaskStatusEnum.Failed,
                (int)TaskStatusEnum.Declined,
                (int)TaskStatusEnum.Cancelled,
            };

            var pagedResult = repository.GetAll()
                .Where(t =>
                    statuslst.Contains(t.TaskStatusId) &&
                    t.DriverId == pagingparametermodel.DriverId &&
                    (pagingparametermodel.Id == 0 || pagingparametermodel.Id == t.Id) &&
                    (
                        string.IsNullOrEmpty(pagingparametermodel.SearchBy)
                        || t.Customer.Name.Contains(pagingparametermodel.SearchBy)
                        || t.Customer.Address.Contains(pagingparametermodel.SearchBy)
                    ))
                .OrderByDescending(t => t.DeliveryDate)
                .ToPagedResultAsync<Tasks, TaskHistoryDetailsViewModel>(
                    pagingparametermodel,
                    mapper.ConfigurationProvider);
            return pagedResult;

        }

        public async Task<Utilities.Utilites.Paging.PagedResult<TasksViewModel>> GetByPaginationAsync(
            PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null)
        {
            var pagedResult = new Utilities.Utilites.Paging.PagedResult<TasksViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            int.TryParse(pagingparametermodel.SearchBy, out var id);

            if (pagingparametermodel.FilterColumn != null && pagingparametermodel.FilterColumn.Any())
            {
                pagingparametermodel.FilterColumn = pagingparametermodel.FilterColumn.Select(x => x.ToLower()).ToList();
            }
            Expression<Func<Tasks, bool>> searchExpression = e => true;
            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy))
            {
                searchExpression = e => e.Customer != null && e.Customer.Name.Contains(pagingparametermodel.SearchBy) || e.Address.Contains(pagingparametermodel.SearchBy);
            }
            var sortColumn = string.IsNullOrEmpty(pagingparametermodel.SortColumn) ? " CreatedDate " : pagingparametermodel.SortColumn;
            var sortOrder = string.IsNullOrEmpty(pagingparametermodel.SortOrder) ? " desc " : pagingparametermodel.SortOrder;
            switch (sortColumn.ToLower())
            {
                case "drivername":
                    sortColumn = "driver.user.firstname";
                    break;
                case "teamname":
                    sortColumn = "driver.team.name";
                    break;
                case "tasktypename":
                    sortColumn = "tasktype.name";
                    break;
                case "customername":
                    sortColumn = "customer.name";
                    break;
                case "taskstatusname":
                    sortColumn = "taskstatus.name";
                    break;
            }
            var orderby = sortColumn + " " + sortOrder;

            var source = repository.GetAll()
                .Include(x => x.TaskHistories)
                .Include(x => x.Customer)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskType)
                //.Include(x => x.Driver)
                .Include(x => x.Driver.Team)
                .Include(x => x.Driver.User)
                .Include(x => x.MainTask)
                .ThenInclude(x => x.MainTaskType)
                //.Where(searchExpression)
                .Where(x =>
                            (managerTeamIds == null || !managerTeamIds.Any() || managerTeamIds.Contains(x.Driver.TeamId))
                        && (pagingparametermodel.TaskStatusIds == null || !pagingparametermodel.TaskStatusIds.Any() || pagingparametermodel.TaskStatusIds.Contains(x.TaskStatusId))
                        && (!pagingparametermodel.FromDate.HasValue
                            || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            )
                        && (!pagingparametermodel.ToDate.HasValue
                            || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                            || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                            )
                        //&& (pagingparametermodel.TeamIds == null || !pagingparametermodel.TeamIds.Any() || pagingparametermodel.TeamIds.Contains(x.Driver.TeamId))
                        && (pagingparametermodel.BranchesIds == null
                            || (!pagingparametermodel.BranchesIds.Any() ||
                                pagingparametermodel.BranchesIds.Contains(x.BranchId ?? 0)
                                )
                            || (x.BranchId == null && pagingparametermodel.GetCustomerTasks)
                            )

                        && (string.IsNullOrEmpty(pagingparametermodel.SearchBy)
                            || pagingparametermodel.FilterColumn == null
                            || !pagingparametermodel.FilterColumn.Any()
                            || (pagingparametermodel.FilterColumn.Contains("id") && id > 0 && x.Id == id)
                            || (pagingparametermodel.FilterColumn.Contains("orderid") && x.OrderId.Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("drivername") && (x.Driver.User.FirstName + " " + x.Driver.User.LastName).Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("teamname") && x.Driver.Team.Name.Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("tasktypename") && x.TaskType.Name.Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("description") && x.Description.Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("address") && x.Address.Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("name") && x.Customer.Name.Contains(pagingparametermodel.SearchBy))
                            || (pagingparametermodel.FilterColumn.Contains("taskstatusname") && x.TaskStatus.Name.Contains(pagingparametermodel.SearchBy))
                            )
                    );
            source = System.Linq.Dynamic.Core.DynamicQueryableExtensions.OrderBy(source, orderby);
            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source != null ? source.Count() : 0;
            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            tasks.ForEach(x => x.TaskHistories = x.TaskHistories.Where(h => h.IsDeleted == false).ToList());
            pagedResult.Result = mapper.Map<List<Tasks>, List<TasksViewModel>>(tasks);
            return pagedResult;
        }

        public async Task ReasignDriverTasksAsync(ReasignDriverViewModel reasignDriverViewModel)
        {
            var tasksVM = await repository.GetAll()
                .Include(x => x.Customer)
                .Where(x => x.DriverId == reasignDriverViewModel.OldDriverId
                        //&& reasignDriverViewModel.TaskStatusIds.Contains(x.TaskStatusId)
                        && (reasignDriverViewModel.TaskIds == null ||
                            reasignDriverViewModel.TaskIds.Contains(x.Id)
                            )
                        )
#warning cleanup - Why project if we are going to update!?
                .ProjectTo<TasksViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            foreach (var taskVM in tasksVM)
            {
                taskVM.DriverId = reasignDriverViewModel.NewDriverId;
                await UpdateAsync(taskVM);
            }


        }

        public async Task<PagedResult<TaskHistoryViewModel>> GetDriverTimeLineAsync(PaginatedDriverTimeLineViewModel pagingparametermodel)
        {
            var driver = await context.Driver
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == pagingparametermodel.DriverId);

            if (driver == null || driver.User == null)
            {
                return new Utilities.Utilites.Paging.PagedResult<TaskHistoryViewModel>();
            }
            var pagedResult = await context.TaskHistory
                .Include(e => e.Task)
                .Where(t => t.CreatedBy_Id == driver.User.UserName)
                .OrderByDescending(x => x.CreatedDate)
                .ToPagedResultAsync<TaskHistory, TaskHistoryViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }


        public async Task TakeSnapshotOfTaskRouteAsync(int taskid, int driverid, int interval)
        {
            var dbTask = context.Tasks
                .Where(x => x.Id == taskid)
                .FirstOrDefault();
            if (dbTask == null ||
                dbTask.TaskStatusId == (int)TaskStatusEnum.Successful ||
                dbTask.TaskStatusId == (int)TaskStatusEnum.Failed)
            {
                return;
            }
            var CurrentDriverLocation = context.DriverCurrentLocation
                .Where(x => x.DriverId == driverid)
                .FirstOrDefault();
            if (CurrentDriverLocation == null)
            {
                return;
            }

            var dbTaskRoute = new TaskRoute()
            {
                TaskId = taskid,
                DriverId = driverid,
                TaskStatusId = dbTask.TaskStatusId,
                Latitude = CurrentDriverLocation.Latitude,
                Longitude = CurrentDriverLocation.Longitude,
            };
            context.TaskRoute.Add(dbTaskRoute);
            await context.SaveChangesAsync();

            try
            {
                if (interval > 0)
                {
#warning Cleanup - Add job runners as part of our domain, and move try-catch, scheduling to infrastructure, how to test this code without firing hangfire?!

                    BackgroundJob.Schedule(() => TakeSnapshotOfTaskRouteAsync(taskid, (int)driverid, interval), TimeSpan.FromMilliseconds(interval));
                }
            }
            catch (Exception) { }

        }

        public override async Task<List<TasksViewModel>> GetAllAsync<TasksViewModel>()
        {
            var tasks = await repository.GetAll()
                .Include(t => t.TaskHistories)
                .ToListAsync();

            tasks.ForEach(x => x.TaskHistories = x.TaskHistories.Where(d => d.IsDeleted == false).ToList());
            return mapper.Map<List<Tasks>, List<TasksViewModel>>(tasks);
        }

        public override async Task<Utilities.Utilites.Paging.PagedResult<TasksViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new Utilities.Utilites.Paging.PagedResult<TasksViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0)
                ? 1
                : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            var source = repository.GetAll()
                        .Include(x => x.TaskHistories);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            tasks.ForEach(x => x.TaskHistories = x.TaskHistories.Where(d => d.IsDeleted == false).ToList());
            pagedResult.Result = mapper.Map<List<Tasks>, List<TasksViewModel>>(tasks);

            return pagedResult;
        }

        public Task<List<TaskCalenderViewModel>> GetTaskCalenderAsync(CalenderViewModel calenderViewModel)
        {
#warning cleanup- to be async
            var completedStatus = new List<int>
            {
                  (int)TaskStatusEnum.Successful,
                  (int)TaskStatusEnum.Failed
            };

            var tasks = repository.GetAll()
                .Where(x => x.DriverId == calenderViewModel.Driver_id)
                .Select(x =>
                    new
                    {
                        TaskDate = x.PickupDate ?? x.DeliveryDate,
                        x.TaskStatusId,
                        x.TotalDistance,
                        x.TotalTaskTime
                    });
            //Fixing query to be translated according to https://stackoverflow.com/questions/58102821/translating-query-with-group-by-and-count-to-linq
            return tasks
                .Where(x => (!calenderViewModel.FromDate.HasValue || x.TaskDate.Value.Date >= calenderViewModel.FromDate.Value.Date)
                        && (!calenderViewModel.ToDate.HasValue || x.TaskDate.Value.Date <= calenderViewModel.ToDate.Value.Date)
                        )
                .GroupBy(x => x.TaskDate.Value.Date)
                .Select(group => new TaskCalenderViewModel
                {
                    TaskDate = group.Key,
                    CompletedCount = group.Sum(x => completedStatus.Contains(x.TaskStatusId) ? 1 : 0),
                    PendingCount = group.Sum(x => (!completedStatus.Contains(x.TaskStatusId) && x.TaskStatusId != (int)TaskStatusEnum.Unassigned) ? 1 : 0),
                    TotalKM = group.Sum(x => x.TotalDistance.HasValue ? x.TotalDistance : 0),
                    TotalTime = group.Sum(x => x.TotalTaskTime.HasValue ? x.TotalTaskTime : 0)
                })
                .OrderBy(x => x.TaskDate)
                .ToListAsync();
        }

        public Task<Utilities.Utilites.Paging.PagedResult<TasksViewModel>> GetTasksReportAsync(
            PaginatedTasksReportViewModel pagingparametermodel)
        {
            var pagedResult = context.Tasks
                .Include(x => x.Driver).ThenInclude(x => x.User)
                .Include(x => x.Customer)
                .Include(x => x.Driver)
                .ThenInclude(x => x.Team)//
                .Include(x => x.DriverRates)
                .Include(x => x.MainTask)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.ToStatus)
                .Include(x => x.Branch)
                .Include(x => x.TaskType)
                .Include(x => x.TaskGallaries)
                .Include(x => x.TaskStatus)
                .Where(x => (pagingparametermodel.StatusIds == null || !pagingparametermodel.StatusIds.Any() || pagingparametermodel.StatusIds.Contains(x.TaskStatusId))
                    && (pagingparametermodel.TaskTypeIds == null || !pagingparametermodel.TaskTypeIds.Any() || pagingparametermodel.TaskTypeIds.Contains(x.TaskTypeId))
                    && (pagingparametermodel.DriversIds == null || !pagingparametermodel.DriversIds.Any() || pagingparametermodel.DriversIds.Contains(x.DriverId ?? 0))
                    && (pagingparametermodel.ZonesIds == null || !pagingparametermodel.ZonesIds.Any() || pagingparametermodel.ZonesIds.Contains(x.GeoFenceId ?? 0))
                    && (pagingparametermodel.RestaurantIds == null || !pagingparametermodel.RestaurantIds.Any() || pagingparametermodel.RestaurantIds.Contains(x.Branch.RestaurantId))
                    && (pagingparametermodel.BranchIds == null
                        || !pagingparametermodel.BranchIds.Any()
                        || pagingparametermodel.BranchIds.Contains(x.BranchId ?? 0)
                        || (x.BranchId == null && pagingparametermodel.GetCustomerTasks)
                        )
                    && (string.IsNullOrEmpty(pagingparametermodel.OrderId) || x.OrderId.Contains(pagingparametermodel.OrderId))
                    && (string.IsNullOrEmpty(pagingparametermodel.Address) || x.Address.Contains(pagingparametermodel.Address))
                    && (!pagingparametermodel.FromDate.HasValue
                            || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                        )
                    && (!pagingparametermodel.ToDate.HasValue
                            || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                            || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                        ))
                .OrderByDescending(x => x.MainTaskId)
                .ToPagedResultAsync<Tasks, TasksViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;

        }

        public async Task<List<ExportTasksWithoutProgressViewModel>> ExportTasksWithoutProgressToExcelAsync(
            PaginatedTasksReportViewModel pagingparametermodel)
        {
            var tasks = await context.Tasks
                .Include(t => t.TaskType)
                .Include(t => t.Customer)
                .Include(t => t.TaskStatus)
                .Include(x => x.Driver)
                .ThenInclude(x => x.User)
                .Include(x => x.Driver)
                .ThenInclude(x => x.DriverRates)
                .Include(x => x.MainTask)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.ToStatus)
                .Include(x => x.Branch)
                .Where(x => (pagingparametermodel.StatusIds == null || !pagingparametermodel.StatusIds.Any() || pagingparametermodel.StatusIds.Contains(x.TaskStatusId))
                   && (pagingparametermodel.TaskTypeIds == null || !pagingparametermodel.TaskTypeIds.Any() || pagingparametermodel.TaskTypeIds.Contains(x.TaskTypeId))
                   && (pagingparametermodel.DriversIds == null || !pagingparametermodel.DriversIds.Any() || pagingparametermodel.DriversIds.Contains(x.DriverId ?? 0))
                   && (pagingparametermodel.ZonesIds == null || !pagingparametermodel.ZonesIds.Any() || pagingparametermodel.ZonesIds.Contains(x.GeoFenceId ?? 0))
                   && (pagingparametermodel.RestaurantIds == null || !pagingparametermodel.RestaurantIds.Any() || pagingparametermodel.RestaurantIds.Contains(x.Branch.RestaurantId))
                   && (pagingparametermodel.BranchIds == null || !pagingparametermodel.BranchIds.Any() || pagingparametermodel.BranchIds.Contains(x.BranchId ?? 0))
                   && (string.IsNullOrEmpty(pagingparametermodel.OrderId) || x.OrderId.Contains(pagingparametermodel.OrderId))
                   && (string.IsNullOrEmpty(pagingparametermodel.Address) || x.Address.Contains(pagingparametermodel.Address))
                   && (!pagingparametermodel.FromDate.HasValue
                         || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                         || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                      )
                   && (!pagingparametermodel.ToDate.HasValue
                         || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                         || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                      )
                )
                .OrderByDescending(x => x.MainTaskId)
                .ProjectTo<ExportTasksWithoutProgressViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            string baseUrl = _configuration.GetValue<string>("IdentityUrl");
            tasks.ForEach(x => x.Reference_Image = string.IsNullOrEmpty(x.Reference_Image) ? x.Reference_Image : baseUrl + "/TaskImages/" + x.Reference_Image);
            return tasks;
        }

        public async Task<List<ExportTasksWithProgressViewModel>> ExportTasksWithProgressToExcelAsync(PaginatedTasksReportViewModel pagingparametermodel)
        {
            var tasks = await context.Tasks
                .Include(t => t.TaskType)
                .Include(t => t.Customer)
                .Include(t => t.TaskStatus)
                .Include(x => x.Driver)
                .ThenInclude(x => x.User)
                .Include(x => x.Driver)
                //.ThenInclude(x => x.DriverRates)
                .Include(x => x.MainTask)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.ToStatus)
                .Include(x => x.Branch)
                .Include(t => t.TaskGallaries)
                .Where(x => (pagingparametermodel.StatusIds == null || !pagingparametermodel.StatusIds.Any() || pagingparametermodel.StatusIds.Contains(x.TaskStatusId))
                   && (pagingparametermodel.TaskTypeIds == null || !pagingparametermodel.TaskTypeIds.Any() || pagingparametermodel.TaskTypeIds.Contains(x.TaskTypeId))
                   && (pagingparametermodel.DriversIds == null || !pagingparametermodel.DriversIds.Any() || pagingparametermodel.DriversIds.Contains(x.DriverId ?? 0))
                   && (pagingparametermodel.ZonesIds == null || !pagingparametermodel.ZonesIds.Any() || pagingparametermodel.ZonesIds.Contains(x.GeoFenceId ?? 0))
                   && (pagingparametermodel.RestaurantIds == null || !pagingparametermodel.RestaurantIds.Any() || pagingparametermodel.RestaurantIds.Contains(x.Branch.RestaurantId))
                   && (pagingparametermodel.BranchIds == null || !pagingparametermodel.BranchIds.Any() || pagingparametermodel.BranchIds.Contains(x.BranchId ?? 0))
                   && (string.IsNullOrEmpty(pagingparametermodel.OrderId) || x.OrderId.Contains(pagingparametermodel.OrderId))
                   && (string.IsNullOrEmpty(pagingparametermodel.Address) || x.Address.Contains(pagingparametermodel.Address))
                   && (!pagingparametermodel.FromDate.HasValue
                         || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                         || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                      )
                   && (!pagingparametermodel.ToDate.HasValue
                         || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                         || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                      )
                )
                .OrderByDescending(x => x.MainTaskId)
                .ToListAsync();

            var result = new List<ExportTasksWithProgressViewModel>();
            foreach (var task in tasks)
            {
                if (task.TaskHistories == null || !task.TaskHistories.Any())
                {
                    result.Add(CreateTaskWithoutHistory(task));
                    continue;
                }

                result.AddRange(CreateTaskWithHistory(task));
            }

            return result;
        }

        private List<ExportTasksWithProgressViewModel> CreateTaskWithHistory(Tasks task)
        {
            var result = new List<ExportTasksWithProgressViewModel>();
            string baseUrl = _configuration.GetValue<string>("IdentityUrl");


            string taskGalleries = "";

            foreach (var history in task.TaskHistories)
            {
                string attachment = "";

                if (history.ActionName == "ADDED A SIGNATURE ")
                {
                    if (!string.IsNullOrEmpty(task.SignatureURL))
                    {
                        attachment += baseUrl + "/TaskSignatures/" + task.SignatureURL + Environment.NewLine;
                    }
                }

                if (history.ActionName == "ADDED THIS IMAGE ")
                {

                    foreach (var gallery in task.TaskGallaries)
                    {
                        attachment += baseUrl + "/TasksGallary/" + gallery.FileName + Environment.NewLine;
                    }

                }
                if (history.ActionName.Trim() == "ADDED THIS NOTE")
                {
                    attachment = task.Notes;
                }



                var item = new ExportTasksWithProgressViewModel()
                {
                    Order_Id = task.OrderId,

                    Order_Type = task.TaskType.Name,
                    Order_Status = task.TaskStatus.Name,
                    Name = task.Customer.Name,
                    Address = task.Address,
                    Order_DateTime = task.PickupDate ?? task.DeliveryDate,
                    Progress_Date = history.CreatedDate.AddHours(3),
                    Progress_Time = history.CreatedDate.AddHours(3).ToShortTimeString(),
                    Reason = history.ActionName,
                    Attachments = attachment + taskGalleries,
                    Order_Description = task.Description
                };

                if (!string.IsNullOrEmpty(task.Image))
                {
                    item.Reference_Image = baseUrl + "/TaskImages/" + task.Image + Environment.NewLine;
                }

                if (task.Driver != null)
                {
                    if (task.Driver.User != null)
                    {
                        item.Driver_Name = task.Driver.User.FirstName;
                    }

                    //if (task.Driver.DriverRates != null)
                    //{
                    //    item.Rating = null;
                    //    item.Comments = task.Driver.DriverRates.FirstOrDefault()?.Note;
                    //}

                }

                result.Add(item);
            }
            return result;
        }

        private ExportTasksWithProgressViewModel CreateTaskWithoutHistory(Tasks task)
        {
            var item = new ExportTasksWithProgressViewModel()
            {
                Order_Id = task.OrderId,
                Order_Type = task.TaskType.Name,
                Order_Status = task.TaskStatus.Name,
                Name = task.Customer.Name,
                Address = task.Address,
                Order_DateTime = task.PickupDate ?? task.DeliveryDate,
            };

            //item.Order_DateTime = item.Order_DateTime.HasValue ? item.Order_DateTime.Value.AddHours(3) : item.Order_DateTime.Value;
            if (task.Driver != null)
            {
                if (task.Driver.User != null)
                {
                    item.Driver_Name = task.Driver.User.FirstName;
                }

                //if (task.Driver.DriverRates != null)
                //{
                //    item.Rating = null;
                //    item.Comments = task.Driver.DriverRates.FirstOrDefault()?.Note;
                //}

            }

            return item;
        }

        public async Task<List<ExportTaskViewModel>> ExportTaskToExcelAsync(int id)
        {
            var task = await context.Tasks
                .Where(x => x.Id == id)
                .Include(t => t.TaskType)
                .Include(t => t.Customer)
                .Include(t => t.TaskStatus)
                .Include(t => t.Driver)
                .ThenInclude(t => t.DriverRates)
                .Include(t => t.Driver)
                .ThenInclude(t => t.User)
                .Include(t => t.TaskHistories)
                .FirstOrDefaultAsync();

            if (task == null)
                throw new NotFoundException(Keys.Validation.TaskNotFound);

            var result = new List<ExportTaskViewModel>();

            if (task.TaskHistories == null || !task.TaskHistories.Any())
                result.Add(ExportTaskWithoutHistory(task));
            else
                result.AddRange(ExportTaskWithHistory(task));

            return result;
        }

        private List<ExportTaskViewModel> ExportTaskWithHistory(Tasks task)
        {
            var result = new List<ExportTaskViewModel>();
            string baseUrl = _configuration.GetValue<string>("IdentityUrl");


            foreach (var history in task.TaskHistories)
            {
                string attachment = "";

                if (history.ActionName == "ADDED A SIGNATURE ")
                {
                    if (!string.IsNullOrEmpty(task.SignatureURL))
                    {
                        attachment += baseUrl + "/TaskSignatures/" + task.SignatureURL + Environment.NewLine;
                    }
                }

                if (history.ActionName == "ADDED THIS IMAGE ")
                {
                    string taskGalleries = "";
                    if (task.TaskGallaries != null)
                        foreach (var gallery in task.TaskGallaries)
                            taskGalleries += baseUrl + "/TasksGallary/" + gallery + Environment.NewLine;


                    attachment = taskGalleries;


                }
                if (history.ActionName.Trim() == "ADDED THIS NOTE")
                {

                    attachment = task.Notes;

                }




                var item = new ExportTaskViewModel()
                {
                    Order_Id = task.OrderId,
                    Order_Type = task.TaskType.Name,
                    Order_Status = task.TaskStatus.Name,
                    Name = task.Customer.Name,
                    Address = task.Address,
                    Order_DateTime = task.PickupDate ?? task.DeliveryDate,
                    Progress_Date = history.CreatedDate,
                    Progress_Time = history.CreatedDate.ToShortTimeString(),
                    Reason = history.ActionName,
                    Attachments = attachment,
                    Order_Description = task.Description
                };

                if (!string.IsNullOrEmpty(task.Image))
                {
                    item.Reference_Image = baseUrl + "/TaskImages/" + task.Image + Environment.NewLine;
                }

                if (task.Driver != null)
                {
                    if (task.Driver.User != null)
                    {
                        item.Driver_Name = task.Driver.User.FirstName;
                    }

                    //if (task.Driver.DriverRates != null)
                    //{
                    //    item.Rating = null;
                    //    item.Comments = task.Driver.DriverRates.FirstOrDefault()?.Note;
                    //}

                }



                result.Add(item);
            }
            return result;
        }

        private ExportTaskViewModel ExportTaskWithoutHistory(Tasks task)
        {
            var item = new ExportTaskViewModel()
            {
                Order_Id = task.OrderId,
                Order_Type = task.TaskType.Name,
                Order_Status = task.TaskStatus.Name,
                Name = task.Customer.Name,
                Address = task.Address,
                Order_DateTime = task.PickupDate ?? task.DeliveryDate,
                Order_Description = task.Description
            };
            string baseUrl = _configuration.GetValue<string>("IdentityUrl");

            if (!string.IsNullOrEmpty(task.Image))
            {
                item.Reference_Image = baseUrl + "/TaskImages/" + task.Image + Environment.NewLine;
            }

            if (task.Driver != null)
            {
                if (task.Driver.User != null)
                {
                    item.Driver_Name = task.Driver.User.FirstName;
                }
            }

            return item;
        }

        public async Task<TravelSummeryViewModel> TravelSummeryAsync(DateTime date, int driverId)
        {
            var completedStatus = new List<int>
            {
               (int)TaskStatusEnum.Successful,
               (int)TaskStatusEnum.Failed
            };

            var tasks = context.Tasks
                .Include(x => x.TaskHistories)
                .Include(x => x.TaskRoutes)
                .Where(x =>
                    x.IsDeleted == false
                    && x.DriverId == driverId
                    && completedStatus.Contains(x.TaskStatusId)
                    && ((x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date == date.Date) || (x.PickupDate.HasValue && x.PickupDate.Value.Date == date.Date)));

            List<TaskRoute> taskRoutes = new List<TaskRoute>();
            var tasksCount = await tasks.CountAsync();
            var tasksKMs = await tasks.SumAsync(x => x.TotalDistance);
            var tasksHours = await tasks.SumAsync(x => x.TotalTime);
            var tasklist = tasks.ToList();
            foreach (var task in tasklist)
            {
                ////tasksHours += GetTaskHours(task);

                if (task.TaskRoutes != null || task.TaskRoutes.Any())
                    taskRoutes.AddRange(task.TaskRoutes.ToList());
            }

            taskRoutes.OrderByDescending(x => x.CreatedDate);
            List<TaskRouteViewModel> taskRoutesVM = mapper.Map<List<TaskRoute>, List<TaskRouteViewModel>>(taskRoutes);

            tasksHours = tasksHours.HasValue ? tasksHours.Value : 0;
            tasksKMs = tasksKMs.HasValue ? tasksKMs.Value : 0;



            return new TravelSummeryViewModel()
            {
                TasksCount = tasksCount,
                TasksHours = Math.Round((double)tasksHours, 2),
                TasksKMs = Math.Round((double)tasksKMs / 1000),
                TasksRoutes = taskRoutesVM,
            };

        }

        public bool SetReached(int taskId, bool isTaskReached)
        {
            var task = repository.Get(e => e.Id == taskId);
            if (task != null && !task.IsDeleted)
            {
                task.IsTaskReached = isTaskReached;
                task.ReachedTime = DateTime.UtcNow;
                return repository.Update(task);
            }

            return false;
        }

        public async Task<List<TaskStatusCountViewModel>> GetTaskStatusCountAsync(DateTime taskDate)
        {
            var tasks = repository.GetAll()
                .Where(x => x.PickupDate.Value.Date == taskDate.Date || x.DeliveryDate.Value.Date == taskDate.Date);

            return new List<TaskStatusCountViewModel>
                {
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Accepted),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Assigned),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Cancelled),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Declined),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Failed),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Inprogress),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Started),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Successful),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Unassigned)
                };
        }

        private async Task<TaskStatusCountViewModel> GetTaskStatusCountAsync(IQueryable<Tasks> tasks, TaskStatusEnum taskStatus)
        {
            return new TaskStatusCountViewModel()
            {
                TaskStatusId = (int)taskStatus,
                TaskStatusName = taskStatus.ToString(),
                Count = await tasks.CountAsync(x => x.TaskStatusId == (int)taskStatus)
            };
        }


        private async Task CreateTaskHistory(CreateTaskHistoryViewModel createTaskHistoryViewModel)
        {
            var taskHistory = new TaskHistory
            {
                TaskId = createTaskHistoryViewModel.TaskVM.Id,
                MainTaskId = createTaskHistoryViewModel.TaskVM.MainTaskId,
                ActionName = createTaskHistoryViewModel.ActionName,
                FromStatusId = createTaskHistoryViewModel.FromStatusId,
                ToStatusId = createTaskHistoryViewModel.ToStatusId,
                Longitude = createTaskHistoryViewModel.Longitude,
                Latitude = createTaskHistoryViewModel.Latitude,
                Reason = createTaskHistoryViewModel.Reason
            };

            await context.TaskHistory.AddAsync(taskHistory);
        }
        public async Task AddTaskHistory(CreateTaskHistoryViewModel createTaskHistoryViewModel)
        {
            await CreateTaskHistory(createTaskHistoryViewModel);
            await context.SaveChangesAsync();
        }
        public async Task AddTaskHistory(List<CreateTaskHistoryViewModel> TaskHistoryViewModelLst)
        {
            //TODO: db-performance , just mark them all as addd save changes in a single trip
            foreach (var TaskHistoryVM in TaskHistoryViewModelLst)
            {
                await CreateTaskHistory(TaskHistoryVM);
            }
            await context.SaveChangesAsync();
        }

        private async Task CreateDriverTaskNotification(TasksViewModel taskVM, string msg)
        {

            try
            {
                if (taskVM.DriverId != null && taskVM.DriverId > 0)
                {
                    var driverTaskNotification = new DriverTaskNotification
                    {
                        TaskId = taskVM.Id,
                        DriverId = taskVM.DriverId ?? 0,
                        Description = "Task number " + taskVM.Id + " " + msg,
                    };
#warning depending on literals (stringly typed code) need to be strongly typed instead
                    if (msg.Contains("has been updated"))
                    {
                        driverTaskNotification.ActionStatus = "Updated";
                    }
                    else if (msg.Contains("has been deleted"))
                    {
                        driverTaskNotification.ActionStatus = "Deleted";
                    }
                    else if (msg.Contains("New task has been added for you"))
                    {
                        driverTaskNotification.ActionStatus = "Assigned";
                    }
                    else
                    {

                        var TaskStatus = context.TaskStatus.FirstOrDefault(x => x.Id == taskVM.TaskStatusId);
                        if (TaskStatus != null)
                        {
                            driverTaskNotification.ActionStatus = TaskStatus.Name;
                        }
                    }


                    await context.DriverTaskNotification.AddAsync(driverTaskNotification);
                    await context.SaveChangesAsync();
                }

            }
            catch (Exception)
            {

            }
        }
        public async Task AddDriverTaskNotification(TasksViewModel taskVM, string msg, int notificationType = 0)
        {
            try
            {
                await CreateDriverTaskNotification(taskVM, msg);
                await context.SaveChangesAsync();

                if (taskVM.DriverId.HasValue)
                {
                    await SendPushNotificationToDriver((int)taskVM.DriverId, msg, new { notificationType = NotificationTypeEnum.NewTask, task = taskVM });
                }
            }
            catch (Exception) { }

        }
        public async Task AddDriverTaskNotification(List<TasksViewModel> TasksViewModelLst, string msg, int notificationType = 0)
        {
            foreach (var taskVM in TasksViewModelLst)
            {
                if (taskVM.DriverId.HasValue)
                {
                    await CreateDriverTaskNotification(taskVM, msg);

                    await SendPushNotificationToDriver((int)taskVM.DriverId, msg, new { notificationType = notificationType });
                }
            }
            await context.SaveChangesAsync();




        }

        public TasksViewModel InitStatusTask(TasksViewModel task)
        {
            task.TaskStatusId = (task.DriverId == null || task.DriverId == 0) ? (int)TaskStatusEnum.Unassigned : (int)TaskStatusEnum.Accepted;
            return task;
        }
        public async Task<CustomerViewModel> AddCustomerIsNotExist(CustomerViewModel viewModel)
        {
            var customerVM = await GetCustomerAsync(viewModel);

            if (customerVM != null)
            {
                var updatedCustomer = await context.Customers
                     .Where(x => x.Id == customerVM.Id)
                     .FirstOrDefaultAsync();

                updatedCustomer.Address = viewModel.Address;
                updatedCustomer.Latitude = viewModel.Latitude;
                updatedCustomer.Longitude = viewModel.Longitude;
                updatedCustomer.Name = viewModel.Name;
                context.Customers.Update(updatedCustomer);
                await context.SaveChangesAsync();

                return customerVM;
            }
            var customer = mapper.Map<CustomerViewModel, Customer>(viewModel);
            var createdCutomer = await context.Customers.AddAsync(customer);
            await context.SaveChangesAsync();
            return mapper.Map<Customer, CustomerViewModel>(createdCutomer.Entity);
        }
        public async Task<Customer> AddCustomerBranchIsNotExist(int branchId)
        {
            Branch branch = await context.Branch
                .FirstOrDefaultAsync(x => x.Id == branchId);
            var customer = new Customer()
            {
                Name = branch.Name,
                Address = branch.Address,
                CountryId = branch.CountryId,
                Latitude = Convert.ToDouble(branch.Latitude),
                Longitude = Convert.ToDouble(branch.Longitude),
                Email = Helper.RemoveSpecialCharacters(branch.Name.Trim().ToLower()) + "_" + branch.Id + "@dhub.com",
                BranchId = branchId,
                Phone = branch.Phone
            };
            var createdCutomer = await context.Customers.AddAsync(customer);
            await context.SaveChangesAsync();
            return createdCutomer.Entity;
        }

        public async Task<TasksViewModel> AddCustomerToTaskAsync(TasksViewModel task)
        {
            int customerId = 0;


            if (task.BranchId != null && task.BranchId != 0)
            {

                if (task.Id <= 0)
                {
                    var branchcustomer = await context.Customers
                        .Where(x => x.BranchId == task.BranchId)
                        .FirstOrDefaultAsync();
                    if (branchcustomer == null)
                    {
                        branchcustomer = await AddCustomerBranchIsNotExist(task.BranchId.Value);
                    }
                    customerId = branchcustomer.Id;
                }
                else if (task.TaskTypeId == (int)TaskTypesEnum.Delivery)
                {
                    var customer = await AddCustomerIsNotExist(task.Customer);
                    if (customer != null)
                    {
                        customerId = customer.Id;
                    }

                }
                else
                {
                    customerId = task.CustomerId;
                }
            }
            else
            {
                var customer = await AddCustomerIsNotExist(task.Customer);
                if (customer != null)
                {
                    customerId = customer.Id;
                }
            }

            task.CustomerId = customerId;
            return task;
        }

        public async Task<List<TasksViewModel>> AddCustomerAndStatusAsync(List<TasksViewModel> ViewModelLst)
        {
            var newViewModelLst = new List<TasksViewModel>();
            var branchId = ViewModelLst.FirstOrDefault(x => x.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId;
            foreach (var task in ViewModelLst)
            {
                var newTask = InitStatusTask(task);
                newTask = await AddCustomerToTaskAsync(task);
                //newTask.TaskStatusId = (int)TaskStatus.Accepted;
                newViewModelLst.Add(newTask);
            }
            return newViewModelLst;
        }

        public async Task SetMainTaskCompleted(int mainTaskId)
        {
            var statusIds = await context.Tasks
                .Where(x => x.MainTaskId == mainTaskId)
                .Select(x => x.TaskStatusId)
                .Distinct()
                .ToListAsync();

            var completedStatusIds = new List<int>()
            {
                (int)TaskStatusEnum.Successful ,
                (int)TaskStatusEnum.Failed ,
                (int)TaskStatusEnum.Cancelled ,
                (int)TaskStatusEnum.Declined
            };

            var IsCompleted = statusIds.All(i => completedStatusIds.Contains(i));
            if (!IsCompleted)
            {
                return;
            }
            var dbMainTask = await context.MainTask.FindAsync(mainTaskId);
            dbMainTask.IsCompleted = true;
            await context.SaveChangesAsync();
        }
        public async Task SoftDeleteTaskHistory(List<int> taskIds)
        {
            var taskHistories = await context.TaskHistory
                .Where(x => taskIds.Contains(x.TaskId))
                .ToListAsync();
            context.TaskHistory.RemoveRange(taskHistories);
            await context.SaveChangesAsync();
        }


        public async Task SendPushNotificationToDriver(int driverId, string message, dynamic MessageBody)
        {
            var DriverObj = context.Driver.Where(x => x.Id == driverId).FirstOrDefault();
            if (DriverObj != null)
            {
                await _pushNotificationService.SendAsync(DriverObj.UserId, "D-Hub", message, MessageBody);
            }
        }

        //public async Task<MainTaskViewModel> AddAcceptedAsync(MainTaskViewModel viewModel)
        //{
        //    return await AddInternalAsync(viewModel, Enums.TaskStatus.Assigned);
        //}

        public async Task UpdateTasks(List<MainTaskViewModel> mainTasks)
        {
            var tasks = await context.MainTask.Include(t => t.Tasks).AsNoTracking()
                .Where(t => !t.IsDeleted && mainTasks.Select(t => t.Id).Contains(t.Id)).ToListAsync();

            tasks = mapper.Map<List<MainTaskViewModel>, List<MainTask>>(mainTasks);

            context.MainTask.UpdateRange(tasks);

            await context.SaveChangesAsync();
        }

        public async Task UpdateTaskDriverAsync(
            int mainTaskId,
            int? driverId,
            int? expirationIntervalInSeconds,
            TaskStatusEnum? status,
            TaskAssignmentType? assignmentType,
            DateTime? expirationDate,
            DateTime? reachedTime)
        {
            var q = context.MainTask.Include(t => t.Tasks)
                 .Where(t => !t.IsDeleted && t.Id == mainTaskId);

            var mainTask = await q.FirstOrDefaultAsync();
            if (mainTask == null)
            {
                throw new InvalidOperationException("Task is deletd or doesn't exist");
            }

            foreach (var task in mainTask.Tasks)
            {
                task.DriverId = driverId;
                if (!driverId.HasValue)
                {
                    task.TaskStatusId = (int)TaskStatusEnum.Unassigned;
                }
                else if (status.HasValue)
                {


                    task.TaskStatusId = (int)status;

                    var taskVM = mapper.Map<Tasks, TasksViewModel>(task);

                    var historiesVM = new List<CreateTaskHistoryViewModel>();

                    historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "ASSIGNED", ToStatusId = (int)TaskStatusEnum.Assigned });


                    await AddTaskHistory(historiesVM);


                }

                if (reachedTime.HasValue)
                {
                    task.ReachedTime = reachedTime;
                    task.IsTaskReached = true;
                }
            }

            mainTask.AssignmentType = (int?)assignmentType ?? mainTask.AssignmentType;
            mainTask.ExpirationDate = expirationDate ?? mainTask.ExpirationDate;

            if (mainTask.ExpirationDate.HasValue && expirationIntervalInSeconds.HasValue)
            {
                mainTask.ExpirationDate = DateTime.UtcNow.AddSeconds(expirationIntervalInSeconds.Value);
            }

            await context.SaveChangesAsync();
        }

        public async Task<MainTaskViewModel> AddAsync(MainTaskViewModel viewModel)
        {
            return await AddInternalAsync(viewModel);
        }







        public async Task<TasksViewModel> UpdateTaskGeoFence(TasksViewModel task)
        {

            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();
            if (geoFences.Any())
            {
                if (!(task.Latitude.HasValue && task.Longitude.HasValue))
                {
                    return task;
                }

                var taskPoint = new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value };
                foreach (var geoFence in geoFences)
                {
                    var polygon = geoFence.Locations.Select(l => new MapPoint { Latitude = l.Latitude ?? 0, Longitude = l.Longitude ?? 0 }).ToArray();

                    if (MapsHelper.IsPointInPolygon(polygon, taskPoint))
                    {
                        task.GeoFenceId = geoFence.Id;
                        break;
                    }
                }

            }

            return task;
        }
        public async Task<List<TasksViewModel>> UpdateTaskGeoFence(List<TasksViewModel> Tasks)
        {

            //Assign geofence Id to tasks
            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();
            if (geoFences.Any())
            {
                foreach (var task in Tasks)
                {
                    if (!(task.Latitude.HasValue && task.Longitude.HasValue))
                        continue;

                    var taskPoint = new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value };
                    foreach (var geoFence in geoFences)
                    {
                        var polygon = geoFence.Locations.Select(l => new MapPoint { Latitude = l.Latitude ?? 0, Longitude = l.Longitude ?? 0 }).ToArray();

                        if (MapsHelper.IsPointInPolygon(polygon, taskPoint))
                        {
                            task.GeoFenceId = geoFence.Id;
                            break;
                        }
                    }
                }
            }


            return Tasks;
        }



        private async Task<MainTaskViewModel> AddInternalAsync(MainTaskViewModel viewModel, Enums.TaskStatusEnum? status = null)
        {
            viewModel.Tasks = await UpdateTaskGeoFence(viewModel.Tasks);
            viewModel.Tasks = await AddCustomerAndStatusAsync(viewModel.Tasks);
            if (status.HasValue)
            {
                viewModel.Tasks.ForEach(t => t.TaskStatusId = (int)status.Value);
            }
            var entity = mapper.Map<MainTaskViewModel, MainTask>(viewModel);
            var mainTaskEntity =
            _mainTaskrepository.AddAsync(entity);
            var mainTaskVM = mapper.Map<MainTask, MainTaskViewModel>(mainTaskEntity);
            int? driverId = mainTaskVM.Tasks.FirstOrDefault()?.DriverId;
            var historiesVM = new List<CreateTaskHistoryViewModel>();
            foreach (var taskVM in mainTaskVM.Tasks)
            {
                historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "CREATED" });
                if (driverId != null && driverId > 0)
                {
                    historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "ASSIGNED", ToStatusId = (int)TaskStatusEnum.Assigned });
                }
            }
            await AddTaskHistory(historiesVM);

            await SetDeliveryBranch(mainTaskEntity);
            viewModel.Id = mainTaskVM.Id;
            return mainTaskVM;
        }

        public async Task<List<DriverViewModel>> GetAvailableDrivers(IEnumerable<int> teamIds = null, IEnumerable<string> tags = null, IEnumerable<int> geoFenceIds = null)
        {
            return await GetAvailableDrivers(new GetAailableDriversInput
            {
                TeamIds = teamIds,
                Tags = tags,
                GeoFenceIds = geoFenceIds
            });
        }

        public async Task<List<DriverViewModel>> GetAvailableDrivers(GetAailableDriversInput input)
        {
            var driversQuery = context.Driver.Include(x => x.Team)
                .Where(x => x.AgentStatusId == (int)AgentStatusesEnum.Available);

            if (input.TeamIds != null && input.TeamIds.Any())
            {
                driversQuery = driversQuery.Where(driver => input.TeamIds.Contains(driver.TeamId));
            }
            if (input.Tags != null && input.Tags.Any())
            {
                var tagsPredicate = input.Tags
                    .Select(tag =>
                    {
                        Expression<Func<Driver, bool>> result =
                            driver => driver.Tags.ToLower().Contains(tag.ToLower());
                        return result;
                    })
                    .Aggregate((total, next) => total.Or(next));
                driversQuery = driversQuery.Where(tagsPredicate);
            }
            if (input.PickupGeoFenceIds != null && input.PickupGeoFenceIds.Any())
            {
                driversQuery = driversQuery.Where(driver =>
                    driver.AllPickupGeoFences ||
                    driver.DriverPickUpGeoFences.Any(pickupGeofence =>
                        input.PickupGeoFenceIds.Contains(pickupGeofence.GeoFenceId)));
            }
            if (input.DeliveryGeoFenceIds != null)
            {
                driversQuery = driversQuery.Where(driver =>
                    driver.AllDeliveryGeoFences ||
                    driver.DriverDeliveryGeoFences.Any(deliveryGeofence =>
                        input.DeliveryGeoFenceIds.Contains(deliveryGeofence.GeoFenceId)));
            }

            var result = await driversQuery.AsNoTracking()
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            return result;
        }

        public async Task CreateTaskDriverRequest(TaskDriverRequestViewModel input)
        {
            var request = mapper.Map<TaskDriverRequests>(input);
            context.TaskDriverRequests.Add(request);
            await context.SaveChangesAsync();
        }

        public async Task UpdateTaskDriverRequest(TaskDriverRequestViewModel input)
        {
            var request = await context.TaskDriverRequests
                .FirstOrDefaultAsync(e => e.MainTaskId == input.MainTaskId && e.DriverId == input.DriverId);
            if (request == null)
            {
                return;
            }
            request.ResponseStatus = (int)input.ResponseStatus;
            request.RetriesCount = input.RetriesCount;
            request.DriverId = input.DriverId;
            await context.SaveChangesAsync();
        }



        public async Task<List<TaskDriverRequestViewModel>> GetTaskDriverRequests(int mainTaskId, int? driverId = null)
        {
            var query = context.TaskDriverRequests
                .Include(x => x.MainTask)
                .Where(x => x.MainTaskId == mainTaskId);
            if (driverId.HasValue)
            {
                query = query.Where(x => x.DriverId == driverId.Value);
            }
            var requests = await query.AsNoTracking().ToListAsync();
            var driverRequests = mapper.Map<List<TaskDriverRequests>, List<TaskDriverRequestViewModel>>(requests);

            return driverRequests;
        }


        public async Task<bool> TaskHasGeofences(MainTaskViewModel mainTask)
        {
            mainTask.Settings.RestrictGeofences = true;
            var geoFences = await GetGeofencesForTask(mainTask);
            return geoFences.PickupGeoFences.Any();
        }

        public async Task<GetTaskGeofencesOutput> GetGeofencesForTask(MainTaskViewModel mainTask)
        {
            Dictionary<MainTaskViewModel, GetTaskGeofencesOutput> geofencesByTask;
            if (mainTask.Settings.RestrictGeofences)
            {
                geofencesByTask = await GetGeofencesForTasks(new List<MainTaskViewModel> { mainTask });
            }
            else
            {
                geofencesByTask = await GetGeofences(new List<MainTaskViewModel> { mainTask });
            }
            return geofencesByTask?.FirstOrDefault().Value;
        }
        public async Task<Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>> GetGeofencesForTasks(IEnumerable<MainTaskViewModel> mainTasks)

        {
            var geofencesByTask = new Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>();
            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();

            foreach (var mainTask in mainTasks)
            {
                var output = new GetTaskGeofencesOutput();
                var pickupTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
                var deliveryTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);
                if (!pickupTask.Latitude.HasValue || !pickupTask.Longitude.HasValue ||
                    !deliveryTask.Latitude.HasValue || !deliveryTask.Longitude.HasValue)
                    throw new ArgumentNullException("Latitude and longitude can't be null");

                var pickupPoint = new MapPoint
                {
                    Latitude = pickupTask.Latitude.Value,
                    Longitude = pickupTask.Longitude.Value
                };
                var deliveryPoint = new MapPoint
                {
                    Latitude = deliveryTask.Latitude.Value,
                    Longitude = deliveryTask.Longitude.Value
                };
                foreach (var geoFence in geoFences)
                {
                    var polygon = geoFence.Locations
                        .Select(l =>
                            new MapPoint
                            {
                                Latitude = l.Latitude ?? 0,
                                Longitude = l.Longitude ?? 0
                            })
                        .ToArray();

                    if (MapsHelper.IsPointInPolygon(polygon, pickupPoint))
                    {
                        output.PickupGeoFences.Add(geoFence);
                    }
                    if (MapsHelper.IsPointInPolygon(polygon, deliveryPoint))
                    {
                        output.DeliveryGeoFences.Add(geoFence);
                    }
                }

                geofencesByTask.Add(mainTask, output);
            }

            return geofencesByTask;
        }
        public async Task<Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>> GetGeofences(IEnumerable<MainTaskViewModel> mainTasks)
        {
            var geofencesByTask = new Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>();
            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();

            foreach (var mainTask in mainTasks)
            {
                var output = new GetTaskGeofencesOutput();
                var pickupTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
                var deliveryTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);
                foreach (var geoFence in geoFences)
                {
                    var polygon = geoFence.Locations
                        .Select(geoFenceLocationVioewModel =>
                            new MapPoint
                            {
                                Latitude = geoFenceLocationVioewModel.Latitude ?? 0,
                                Longitude = geoFenceLocationVioewModel.Longitude ?? 0
                            })
                        .ToArray();
                    output.PickupGeoFences.Add(geoFence);
                    output.DeliveryGeoFences.Add(geoFence);
                }

                geofencesByTask.Add(mainTask, output);
            }

            return geofencesByTask;
        }

        public async Task<CustomerViewModel> GetCustomerAsync(CustomerViewModel viewModel)
        {
            var customer = new CustomerViewModel();
            try
            {
                customer = await context.Customers
                   .Where(x => x.Phone == viewModel.Phone)
                   .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                   .FirstOrDefaultAsync();
            }
            catch
            {
#warning cleanup - why this should throw exception? and if it is thrown null show false state, whole unit of work should fail fast and return feedback
                return null;
            }

            return customer;
        }

        public double GetTaskTotalTime(Tasks task)
        {
            if (task.TaskHistories == null || !task.TaskHistories.Any())
            {
                return 0;
            }
            var taskHistory = task.TaskHistories
                .FirstOrDefault(x => x.TaskId == task.Id && x.ToStatusId == (int)TaskStatusEnum.Started);
            if (taskHistory != null)
            {
                return DateTime.UtcNow.Subtract(taskHistory.CreatedDate).TotalSeconds;
            }
            return 0;
        }

        public Task<int> GetTasksWeights(GetMainTasksInput input)
        {
            var q = context.Tasks.AsQueryable();
#warning cleanup - a switch pattern may be useful
#warning cleanup - replace q, c with meaningful literals

            if (input.DriverId.HasValue)
            {
                q = q.Where(e => e.DriverId == input.DriverId);
            }
            else if (input.DriverIds != null && input.DriverIds.Any())
            {
                q = q.Where(e => e.DriverId.HasValue && input.DriverIds.Contains(e.DriverId.Value));
            }
            if (input.Status.HasValue)
            {
                q = q.Where(e => e.TaskStatusId == (int)input.Status);
            }
            else if (input.Statuses != null && input.Statuses.Any())
            {
                q = q.Where(e => input.Statuses.Contains((TaskStatusEnum)e.TaskStatusId));
            }

            if (input.PickupBranchId.HasValue)
            {
                q = q.Where(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup && e.BranchId == input.PickupBranchId);
            }
            if (input.GeoFenceId.HasValue)
            {
                q = q.Where(e =>
                    (e.GeoFenceId.HasValue && e.GeoFenceId == input.GeoFenceId) ||
                    e.Branch.GeoFenceId == input.GeoFenceId);
            }
            if (input.DeliveryGeoFenceId.HasValue)
            {
                q = q.Where(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery && e.GeoFenceId == input.GeoFenceId);
            }
            else if (input.DeliveryGeoFenceIds != null)
            {
                q = q.Where(e =>
                    e.TaskTypeId == (int)TaskTypesEnum.Delivery &&
                    e.GeoFenceId.HasValue &&
                    input.DeliveryGeoFenceIds.Contains(e.GeoFenceId.Value));
            }
            if (input.PickupGeoFenceId.HasValue)
            {
                q = q.Where(e =>
                    e.TaskTypeId == (int)TaskTypesEnum.Pickup &&
                    (e.GeoFenceId == input.GeoFenceId || e.BranchId == input.GeoFenceId));
            }
            return q.Select(e => e.MainTaskId).Distinct().CountAsync();
        }

        public async Task<List<MainTaskViewModel>> GetMainTasks(GetMainTasksInput input)
        {
            var q = GetMainTaskQurey(input);

            var tasks = await q.OrderBy(e => e.Id).ToListAsync();
            var tasksVM = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(tasks);

            return tasksVM;
        }

        public async Task<MainTaskCountViewModel> GetMainTaskCount(GetMainTasksInput input)
        {
            var q = GetMainTaskQurey(input);
            var mainTaskViewModel = new MainTaskCountViewModel();
            var tasks = await q.CountAsync();

            return mainTaskViewModel;
        }

        public IQueryable<MainTask> GetMainTaskQurey(GetMainTasksInput input)
        {
#warning cleanup - seems duplicate
            var q = context.MainTask
               .Include(t => t.Tasks)
               .AsQueryable();

            if (input.DriverId.HasValue)
            {
                q = q.Where(e => e.Tasks.First().DriverId == input.DriverId);
            }
            else if (input.DriverIds != null && input.DriverIds.Any())
            {
                q = q.Where(e => e.Tasks.First().DriverId.HasValue && input.DriverIds.Any(d => d == e.Tasks.First().DriverId.Value));
            }
            if (input.Status.HasValue)
            {
                q = q.Where(e => e.Tasks.First().TaskStatusId == (int)input.Status);
            }
            else if (input.Statuses != null && input.Statuses.Any())
            {
                q = q.Where(e => input.Statuses.Contains((TaskStatusEnum)e.Tasks.First().TaskStatusId));
            }
            if (input.PickupBranchId.HasValue)
            {
                q = q.Where(e =>
                    e.Tasks.Any(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup) &&
                    e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId == input.PickupBranchId);
            }
            if (input.GeoFenceId.HasValue)
            {
                q = q.Where(e =>
                    (e.Tasks.First().GeoFenceId.HasValue && e.Tasks.First().GeoFenceId == input.GeoFenceId) ||
                    e.Tasks.First().Branch.GeoFenceId == input.GeoFenceId);
            }
            if (input.DeliveryGeoFenceId.HasValue)
            {
                q = q.Where(e => e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId == input.GeoFenceId);
            }
            else if (input.DeliveryGeoFenceIds != null)
            {
                q = q.Where(e =>
                    e.Tasks.Any(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery) &&
                    e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.HasValue &&
                    input.DeliveryGeoFenceIds.Any(d => d == e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.Value));
            }
            if (input.PickupGeoFenceId.HasValue)
            {
                q = q.Where(e =>
                    (
                        e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).GeoFenceId.HasValue &&
                        e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).GeoFenceId == input.GeoFenceId
                    ) ||
                    e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).Branch.GeoFenceId == input.GeoFenceId);
            }
            if (input.Take.HasValue)
            {
                q = q.Take(input.Take.Value);
            }
            return q;
        }

        public async Task<bool> CheckDriverBusy(int driverId)
        {
            var isDriverBusy = await context.Tasks
                .AnyAsync(x =>
                    x.DriverId == driverId &&
                    x.TaskStatusId == (int)TaskStatusEnum.Started);
            return isDriverBusy;
        }
        public async Task MakeDriverAvailable(int driverId)
        {
            var IsDriverBusy = await CheckDriverBusy(driverId);
            //if (!IsDriverBusy)
            //{
            var driver = await context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .FirstOrDefaultAsync(x => x.Id == driverId);
            if (driver != null)
            {
                if (IsDriverBusy)
                {
                    driver.AgentStatusId = (int)AgentStatusesEnum.Busy;
                }
                else
                {
                    driver.AgentStatusId = (int)AgentStatusesEnum.Available;
                }
                context.Update(driver);
                await context.SaveChangesAsync();
            }

            //}

        }

        private async Task CreateTaskRoute(TaskRouteViewModel taskRouteViewModel)
        {
            var taskRoute = new TaskRoute
            {
                TaskId = taskRouteViewModel.TaskId,
                DriverId = taskRouteViewModel.DriverId,
                Longitude = taskRouteViewModel.Longitude,
                Latitude = taskRouteViewModel.Latitude,
                TaskStatusId = taskRouteViewModel.TaskStatusId,
            };

            await context.TaskRoute.AddAsync(taskRoute);
        }

        public async Task AddTaskRoute(TaskRouteViewModel taskRouteViewModel)
        {
            await CreateTaskRoute(taskRouteViewModel);
            await context.SaveChangesAsync();
        }

        public async Task<Branch> GetRelatedBranchAsync(List<Branch> branches, MapPoint point)
        {
            _logger.LogTrace(
                $"{nameof(GetRelatedBranchAsync)} started for {{branches}}, {{point}}",
                string.Join(",", branches.Select(branch => branch.Id)),
                point);
            double reachedLimitDistanceInM = DefaultSettingValue.ReachedDistanceRestrictionInMeter;
            var reachedLimitDistanceInMSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.ReachedDistanceRestriction);

            if (reachedLimitDistanceInMSetting != null && !string.IsNullOrEmpty(reachedLimitDistanceInMSetting.Value))
            {
                double.TryParse(reachedLimitDistanceInMSetting.Value, out reachedLimitDistanceInM);
            }
            _logger.LogTrace("{ReachedDistanceRestriction} is configured", reachedLimitDistanceInM);

            var list = new List<(Branch, double)>();
            foreach (var branch in branches)
            {
                var distance = MapsHelper.GetDistanceBetweenPoints(branch.Latitude.Value, branch.Longitude.Value, point.Latitude, point.Longitude);
                list.Add((branch, distance));
            }

            var nearestBranch = list.OrderBy(x => x.Item2).FirstOrDefault();
            if (nearestBranch.Item2 <= reachedLimitDistanceInM)
            {
                _logger.LogInformation("{NearestBranch} is close enough.", new { nearestBranch.Item1.Id, Distance = nearestBranch.Item2 });
                return nearestBranch.Item1;
            }
            _logger.LogInformation("{NearestBranch} is not close enough.", new { nearestBranch.Item1.Id, Distance = nearestBranch.Item2 });

            return null;
        }
        public async Task SetTaskDistanceHours(Tasks dbTask)
        {
            var taskHistories = context.TaskHistory
                .Where(x => x.TaskId == dbTask.Id)
                .OrderByDescending(x => x.CreatedDate);

            var taskDates = await taskHistories
                .Select(x => x.CreatedDate)
                .ToArrayAsync();

            var taskPoints = await context.TaskRoute
                .Where(x => x.TaskId == dbTask.Id)
                .OrderByDescending(x => x.CreatedDate)
                .Where(x =>
                    (x.Latitude != null || x.Latitude == 0) &&
                    (x.Longitude != null || x.Longitude == 0))
                .ProjectTo<MapPoint>(mapper.ConfigurationProvider)
                .ToArrayAsync();

            if (taskPoints != null)
            {
                var distance = await GetTotalTaskKMAsync(taskPoints);
                dbTask.TotalDistance = double.IsNaN(distance) ? 0 : distance;
            }

            dbTask.TotalTaskTime = taskDates != null ? GetTotalTaskDate(taskDates) : 0;
            await context.SaveChangesAsync();

        }
        private double GetTotalTaskDate(DateTime[] dates)
        {
            double total = 0.0;
            for (var i = 0; i < dates.Length; i++)
            {
                if (i == 0) continue;
                total += dates[i - 1].Subtract(dates[i]).TotalMilliseconds;
            }
            return total;
        }

        private Task<double> GetTotalTaskKMAsync(MapPoint[] points)
        {
            double total = 0.0;
            for (var i = 0; i < points.Length; i++)
            {
                if (i == 0) continue;
                //var distanceresponse = await this._mapsManager.GetDistance(points[i - 1], points[i], this._mapsSettings);
                var distanceresponse = MapsHelper.GetDistanceBetweenPoints(points[i - 1], points[i]);
                total += (distanceresponse / 1000);
            }
            return Task.FromResult(total);
        }

        public Task<MainTask> GetMainTaskStatus(int mainTaskId)
        {
            return context.MainTask.FirstOrDefaultAsync(e => e.Id == mainTaskId);
        }

        public async Task UpdateMainTaskStatus(int mainTaskId)
        {
            var dbMainTask = await context.MainTask
                .Include(x => x.Tasks)
                .FirstOrDefaultAsync(x => x.Id == mainTaskId);

            var IsAssigned = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Assigned);
            if (IsAssigned)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Assigned;
                dbMainTask.IsCompleted = false;
            }
            var IsInprogress = dbMainTask.Tasks.Any(x => x.TaskStatusId == (int)TaskStatusEnum.Started || x.TaskStatusId == (int)TaskStatusEnum.Accepted);
            if (IsInprogress)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Inprogress;
                dbMainTask.IsCompleted = false;
            }
            var IsAccepted = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Accepted);
            if (IsAccepted)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Accepted;
                dbMainTask.IsCompleted = false;
            }
            var IsSuccessful = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Successful);
            if (IsSuccessful)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Successful;
                dbMainTask.IsCompleted = true;
            }
            var IsCanceled = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Cancelled);
            if (IsCanceled)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Canceled;
                dbMainTask.IsCompleted = true;
            }
            var IsDeclined = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Declined);
            if (IsDeclined)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Declined;
                dbMainTask.IsCompleted = true;
            }
            var IsFailed = dbMainTask.Tasks.Any(x => x.TaskStatusId == (int)TaskStatusEnum.Failed);
            if (IsFailed)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Failed;
            }

            await context.SaveChangesAsync();
        }

        public async Task<bool> AutoAllocationFail(int mainTaskId)
        {
            var tenantId = _currentUser.TenantId;
            var dbMainTask = await context.MainTask.FirstOrDefaultAsync(x => x.Id == mainTaskId);
            dbMainTask.IsFailToAutoAssignDriver = true;
            context.Update(dbMainTask);
            await context.SaveChangesAsync();
            await _notificationhubContext.Clients.Group(tenantId).SendAsync("AutoAllocationFailed", mainTaskId);
            var title = _localizer[Keys.Notifications.TaskAutoAllocationFailed];
            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationFailedDetailsTaskId, mainTaskId);
            await context.Notifications.AddAsync(new Notification
            {
                Title = title,
                Body = body,
                FromUserId = tenantId,
                ToUserId = tenantId,
                IsSeen = false
            });
            if (tenantId != _currentUser.Id)
            {
                await _notificationhubContext.Clients.Group(_currentUser.UserName).SendAsync("AutoAllocationFailed", mainTaskId);
                await context.Notifications.AddAsync(new Notification
                {
                    Title = title,
                    Body = body,
                    FromUserId = tenantId,
                    ToUserId = _currentUser.Id,
                    //NotificationType = NotificationTypeWeb.AutoAllocationFailed
                    IsSeen = false
                });
            }
            await context.SaveChangesAsync();

            return true;
        }


        public async Task<bool> SetDeliveryBranch(MainTask mainTask)
        {
            var tasks = await context.Tasks.Where(x => x.MainTaskId == mainTask.Id).ToListAsync();

            var branchId = tasks.FirstOrDefault(x => x.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId;
            if (branchId != null && branchId != 0)
            {
                foreach (var task in tasks)
                {
                    task.BranchId = branchId;
                    context.Tasks.Update(task);
                }
                await context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> AutoAllocationSucssesfull(int mainTaskId)
        {
            var dbMainTask = await context.MainTask
                .FirstOrDefaultAsync(x => x.Id == mainTaskId);
            var CreatedUserName = _currentUser.UserName;
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("AutoAllocationSucessfully", mainTaskId);
            var title = _localizer[Keys.Notifications.TaskAutoAllocationSucceded];
            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationSuccededDetailsTaskId, mainTaskId);
            await context.Notifications.AddAsync(new Notification
            {
                Title = title,
                Body = body,
                FromUserId = CreatedUserName,
                ToUserId = _currentUser.TenantId,
                IsSeen = false
            });
            if (_currentUser.TenantId != _currentUser.Id)
            {
                await _notificationhubContext.Clients.Group(CreatedUserName).SendAsync("AutoAllocationSucessfully", mainTaskId);
                await context.Notifications.AddAsync(new Notification
                {
                    Title = title,
                    Body = body,
                    FromUserId = CreatedUserName,
                    ToUserId = _currentUser.Id,
                    IsSeen = false
                });
            }
            await context.SaveChangesAsync();

            return true;
        }
    }
}
