﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;

namespace AMANAH.DMS.BLL.Managers
{
    public class TaskRouteManager : BaseManager<TaskRouteViewModel, TaskRoute>, ITaskRouteManager
    {
        public TaskRouteManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<TaskRoute> repository)
            : base(context, repository, mapper)
        {
        }
    }
}
