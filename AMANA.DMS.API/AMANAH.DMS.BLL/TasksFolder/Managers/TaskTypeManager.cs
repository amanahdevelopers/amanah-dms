﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;

namespace AMANAH.DMS.BLL.Managers
{
    public class TaskTypeManager : BaseManager<LkpViewModel, TaskType>, ITaskTypeManager
    {
        public TaskTypeManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<TaskType> repository)
            : base(context, repository, mapper)
        {
        }
    }
}
