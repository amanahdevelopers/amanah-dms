﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class MainTaskStatusManager : BaseManager<LkpViewModel, MainTaskStatus>, IMainTaskStatusManager
    {
        public MainTaskStatusManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<MainTaskStatus> repository)
            : base(context, repository, mapper)
        {
        }
  }
}
