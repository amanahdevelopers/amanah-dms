﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class TaskStatusManager : BaseManager<TaskStatusViewModel, DATA.Entities.TaskStatus>, ITaskStatusManager
    {
        public TaskStatusManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<DATA.Entities.TaskStatus> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<TaskStatusViewModel> Get(int id)
        {
            return await (context as ApplicationDbContext).TaskStatus
                .Where(x => x.Id == id)
                .ProjectTo<TaskStatusViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
    }
}
