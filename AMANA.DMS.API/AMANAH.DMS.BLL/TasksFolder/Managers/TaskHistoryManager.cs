﻿using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;

namespace AMANAH.DMS.BLL.Managers
{
    public class TaskHistoryManager : BaseManager<TaskHistoryViewModel, TaskHistory>, ITaskHistoryManager
    {
        public TaskHistoryManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<TaskHistory> repository)
            : base(context, repository, mapper)
        {
        }
    }
}
