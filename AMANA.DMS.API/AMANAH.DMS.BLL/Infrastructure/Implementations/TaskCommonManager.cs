﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Threading.Tasks;
//using AMANAH.DMS.API.SignalRHups;
//using AMANAH.DMS.BASE.Authentication;
//using AMANAH.DMS.BASE.Domain.Repositories;
//using AMANAH.DMS.BLL.Constants;
//using AMANAH.DMS.BLL.Enums;
//using AMANAH.DMS.BLL.ExternalServcies;
//using AMANAH.DMS.BLL.IManagers;
//using AMANAH.DMS.BLL.Managers.TaskAssignment;
//using AMANAH.DMS.BLL.ViewModels;
//using AMANAH.DMS.BLL.ViewModels.Tasks;
//using AMANAH.DMS.Context;
//using AMANAH.DMS.DATA.Entities;
//using AMANAH.DMS.Repoistry;
//using AMANAH.DMS.ViewModels;
//using AutoMapper;
//using AutoMapper.QueryableExtensions;
//using MapsUtilities;
//using MapsUtilities.MapsManager;
//using MapsUtilities.Models;
//using Microsoft.AspNetCore.SignalR;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Utilites;
//using Utilities.Utilites.Localization;
//using TaskStatusEnum = AMANAH.DMS.BLL.Enums.TaskStatusEnum;

//namespace AMANAH.DMS.BLL.Managers
//{
//    public class TaskCommonManager : ITaskCommonManager
//    {
//        private readonly ApplicationDbContext context;
//        private readonly IMapper mapper;
//        private readonly IConfiguration _configuration;
//        private readonly IRepositry<MainTask> _repository;
//        private readonly IGeoFenceManager _geoFenceManager;
//        private readonly ILocalizer _localizer;
//        private readonly ILogger<TaskCommonManager> _logger;
//        private readonly ICurrentUser _currentUser;
//        private readonly IHubContext<NotificationHub> _notificationhubContext;
//        private readonly ICommonManager commonManager;
//        private readonly INotificationManager _notificationManager;
//        private readonly IMapsManager _mapsManager;
//        private readonly IPushNotificationService _pushNotificationService;
//        private readonly MapRequestSettings _mapsSettings;
//        private readonly ISettingsManager _settingsManager;


//        public TaskCommonManager(
//            INotificationManager notificationManager,
//            IHubContext<NotificationHub> notificationhubContext,
//            ICommonManager _commonManager,
//            ISettingsManager settingsManager,
//            ApplicationDbContext context,
//            IMapper mapper,
//            IMapsManager mapsManager,
//            IConfiguration configuration,
//            IRepositry<MainTask> repository,
//            IGeoFenceManager geoFenceManager,
//            ILocalizer localizer,
//            IPushNotificationService pushNotificationService,
//            ILogger<TaskCommonManager> logger,
//            ICurrentUser currentUser)
//        {

//            this.context = context;
//            this.mapper = mapper;
//            _configuration = configuration;
//            _repository = repository;
//            _geoFenceManager = geoFenceManager;
//            _localizer = localizer;
//            _logger = logger;
//            _currentUser = currentUser;
//            _notificationhubContext = notificationhubContext;
//            commonManager = _commonManager;
//            _notificationManager = notificationManager;
//            _mapsManager = mapsManager;
//            _settingsManager = settingsManager;
//            _mapsSettings = new MapRequestSettings
//            {
//                Priority = MapPriority.Default,
//                GoogleApiKey = _configuration["MapsSettings:GoogleKey"],
//                BingApiKey = _configuration["MapsSettings:BingKey"]
//            };

//            _pushNotificationService = pushNotificationService;

//        }

//        private async Task CreateTaskHistory(CreateTaskHistoryViewModel createTaskHistoryViewModel)
//        {
//            var taskHistory = new TaskHistory
//            {
//                TaskId = createTaskHistoryViewModel.TaskVM.Id,
//                MainTaskId = createTaskHistoryViewModel.TaskVM.MainTaskId,
//                ActionName = createTaskHistoryViewModel.ActionName,
//                FromStatusId = createTaskHistoryViewModel.FromStatusId,
//                ToStatusId = createTaskHistoryViewModel.ToStatusId,
//                Longitude = createTaskHistoryViewModel.Longitude,
//                Latitude = createTaskHistoryViewModel.Latitude,
//                Reason = createTaskHistoryViewModel.Reason
//            };

//            await context.TaskHistory.AddAsync(taskHistory);
//        }
//        public async Task AddTaskHistory(CreateTaskHistoryViewModel createTaskHistoryViewModel)
//        {
//            await CreateTaskHistory(createTaskHistoryViewModel);
//            await context.SaveChangesAsync();
//        }
//        public async Task AddTaskHistory(List<CreateTaskHistoryViewModel> TaskHistoryViewModelLst)
//        {
//            //TODO: db-performance , just mark them all as addd save changes in a single trip
//            foreach (var TaskHistoryVM in TaskHistoryViewModelLst)
//            {
//                await CreateTaskHistory(TaskHistoryVM);
//            }
//            await context.SaveChangesAsync();
//        }

//        private async Task CreateDriverTaskNotification(TasksViewModel taskVM, string msg)
//        {

//            try
//            {
//                if (taskVM.DriverId != null && taskVM.DriverId > 0)
//                {
//                    var driverTaskNotification = new DriverTaskNotification
//                    {
//                        TaskId = taskVM.Id,
//                        DriverId = taskVM.DriverId ?? 0,
//                        Description = "Task number " + taskVM.Id + " " + msg,
//                    };
//#warning depending on literals (stringly typed code) need to be strongly typed instead
//                    if (msg.Contains("has been updated"))
//                    {
//                        driverTaskNotification.ActionStatus = "Updated";
//                    }
//                    else if (msg.Contains("has been deleted"))
//                    {
//                        driverTaskNotification.ActionStatus = "Deleted";
//                    }
//                    else if (msg.Contains("New task has been added for you"))
//                    {
//                        driverTaskNotification.ActionStatus = "Assigned";
//                    }
//                    else
//                    {

//                        var TaskStatus = context.TaskStatus.FirstOrDefault(x => x.Id == taskVM.TaskStatusId);
//                        if (TaskStatus != null)
//                        {
//                            driverTaskNotification.ActionStatus = TaskStatus.Name;
//                        }
//                    }


//                    await context.DriverTaskNotification.AddAsync(driverTaskNotification);
//                    await context.SaveChangesAsync();
//                }

//            }
//            catch (Exception)
//            {

//            }
//        }
//        public async Task AddDriverTaskNotification(TasksViewModel taskVM, string msg, int notificationType = 0)
//        {
//            try
//            {
//                await CreateDriverTaskNotification(taskVM, msg);
//                await context.SaveChangesAsync();

//                if (taskVM.DriverId.HasValue)
//                {
//                    await SendPushNotificationToDriver((int)taskVM.DriverId, msg, new { notificationType = NotificationTypeEnum.NewTask, task = taskVM });
//                }
//            }
//            catch (Exception) { }

//        }
//        public async Task AddDriverTaskNotification(List<TasksViewModel> TasksViewModelLst, string msg, int notificationType = 0)
//        {
//            foreach (var taskVM in TasksViewModelLst)
//            {
//                if (taskVM.DriverId.HasValue)
//                {
//                    await CreateDriverTaskNotification(taskVM, msg);

//                    await SendPushNotificationToDriver((int)taskVM.DriverId, msg, new { notificationType = notificationType });
//                }
//            }
//            await context.SaveChangesAsync();




//        }

//        public TasksViewModel InitStatusTask(TasksViewModel task)
//        {
//            task.TaskStatusId = (task.DriverId == null || task.DriverId == 0) ? (int)Enums.TaskStatusEnum.Unassigned : (int)Enums.TaskStatusEnum.Accepted;
//            return task;
//        }
//        public async Task<CustomerViewModel> AddCustomerIsNotExist(CustomerViewModel viewModel)
//        {
//            var customerVM = await GetCustomerAsync(viewModel);

//            if (customerVM != null)
//            {
//                var updatedCustomer = await context.Customers
//                     .Where(x => x.Id == customerVM.Id)
//                     .FirstOrDefaultAsync();

//                updatedCustomer.Address = viewModel.Address;
//                updatedCustomer.Latitude = viewModel.Latitude;
//                updatedCustomer.Longitude = viewModel.Longitude;
//                updatedCustomer.Name = viewModel.Name;
//                context.Customers.Update(updatedCustomer);
//                await context.SaveChangesAsync();

//                return customerVM;
//            }
//            var customer = mapper.Map<CustomerViewModel, Customer>(viewModel);
//            var createdCutomer = await context.Customers.AddAsync(customer);
//            await context.SaveChangesAsync();
//            return mapper.Map<Customer, CustomerViewModel>(createdCutomer.Entity);
//        }
//        public async Task<Customer> AddCustomerBranchIsNotExist(int branchId)
//        {
//            Branch branch = await context.Branch
//                .FirstOrDefaultAsync(x => x.Id == branchId);
//            var customer = new Customer()
//            {
//                Name = branch.Name,
//                Address = branch.Address,
//                CountryId = branch.CountryId,
//                Latitude = Convert.ToDouble(branch.Latitude),
//                Longitude = Convert.ToDouble(branch.Longitude),
//                Email = Helper.RemoveSpecialCharacters(branch.Name.Trim().ToLower()) + "_" + branch.Id + "@dhub.com",
//                BranchId = branchId,
//                Phone = branch.Phone
//            };
//            var createdCutomer = await context.Customers.AddAsync(customer);
//            await context.SaveChangesAsync();
//            return createdCutomer.Entity;
//        }

//        public async Task<TasksViewModel> AddCustomerToTaskAsync(TasksViewModel task)
//        {
//            int customerId = 0;


//            if (task.BranchId != null && task.BranchId != 0)
//            {

//                if (task.Id <= 0)
//                {
//                    var branchcustomer = await context.Customers
//                        .Where(x => x.BranchId == task.BranchId)
//                        .FirstOrDefaultAsync();
//                    if (branchcustomer == null)
//                    {
//                        branchcustomer = await AddCustomerBranchIsNotExist(task.BranchId.Value);
//                    }
//                    customerId = branchcustomer.Id;
//                }
//                else if (task.TaskTypeId == (int)TaskTypesEnum.Delivery)
//                {
//                    var customer = await AddCustomerIsNotExist(task.Customer);
//                    if (customer != null)
//                    {
//                        customerId = customer.Id;
//                    }

//                }
//                else
//                {
//                    customerId = task.CustomerId;
//                }
//            }
//            else
//            {
//                var customer = await AddCustomerIsNotExist(task.Customer);
//                if (customer != null)
//                {
//                    customerId = customer.Id;
//                }
//            }

//            task.CustomerId = customerId;
//            return task;
//        }

//        public async Task<List<TasksViewModel>> AddCustomerAndStatusAsync(List<TasksViewModel> ViewModelLst)
//        {
//            var newViewModelLst = new List<TasksViewModel>();
//            var branchId = ViewModelLst.FirstOrDefault(x => x.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId;
//            foreach (var task in ViewModelLst)
//            {
//                var newTask = InitStatusTask(task);
//                newTask = await AddCustomerToTaskAsync(task);
//                //newTask.TaskStatusId = (int)TaskStatus.Accepted;
//                newViewModelLst.Add(newTask);
//            }
//            return newViewModelLst;
//        }

//        public async Task SetMainTaskCompleted(int mainTaskId)
//        {
//            var statusIds = await context.Tasks
//                .Where(x => x.MainTaskId == mainTaskId)
//                .Select(x => x.TaskStatusId)
//                .Distinct()
//                .ToListAsync();

//            var completedStatusIds = new List<int>()
//            {
//                (int)TaskStatusEnum.Successful ,
//                (int)TaskStatusEnum.Failed ,
//                (int)TaskStatusEnum.Cancelled ,
//                (int)TaskStatusEnum.Declined
//            };

//            var IsCompleted = statusIds.All(i => completedStatusIds.Contains(i));
//            if (!IsCompleted)
//            {
//                return;
//            }
//            var dbMainTask = await context.MainTask.FindAsync(mainTaskId);
//            dbMainTask.IsCompleted = true;
//            await context.SaveChangesAsync();
//        }
//        public async Task SoftDeleteTaskHistory(List<int> taskIds)
//        {
//            var taskHistories = await context.TaskHistory
//                .Where(x => taskIds.Contains(x.TaskId))
//                .ToListAsync();
//            context.TaskHistory.RemoveRange(taskHistories);
//            await context.SaveChangesAsync();
//        }


//        public async Task SendPushNotificationToDriver(int driverId, string message, dynamic MessageBody)
//        {
//            var DriverObj = context.Driver.Where(x => x.Id == driverId).FirstOrDefault();
//            if (DriverObj != null)
//            {
//                await _pushNotificationService.SendAsync(DriverObj.UserId, "D-Hub", message, MessageBody);
//            }
//        }

//        //public async Task<MainTaskViewModel> AddAcceptedAsync(MainTaskViewModel viewModel)
//        //{
//        //    return await AddInternalAsync(viewModel, Enums.TaskStatus.Assigned);
//        //}

//        public async Task UpdateTasks(List<MainTaskViewModel> mainTasks)
//        {
//            var tasks = await context.MainTask.Include(t => t.Tasks).AsNoTracking()
//                .Where(t => !t.IsDeleted && mainTasks.Select(t => t.Id).Contains(t.Id)).ToListAsync();

//            tasks = mapper.Map<List<MainTaskViewModel>, List<MainTask>>(mainTasks);

//            context.MainTask.UpdateRange(tasks);

//            await context.SaveChangesAsync();
//        }

//        public async Task UpdateTaskDriverAsync(int mainTaskId, int? driverId, int? expirationIntervalInSeconds = null, Enums.TaskStatusEnum? status = null, TaskAssignmentType? assignmentType = null, DateTime? expirationDate = null, DateTime? reachedTime = null)
//        {
//            var q = context.MainTask.Include(t => t.Tasks)
//                 .Where(t => !t.IsDeleted && t.Id == mainTaskId);

//            var mainTask = await q.FirstOrDefaultAsync();
//            if (mainTask == null)
//            {
//                throw new InvalidOperationException("Task is deletd or doesn't exist");
//            }

//            foreach (var task in mainTask.Tasks)
//            {
//                task.DriverId = driverId;
//                if (!driverId.HasValue)
//                {
//                    task.TaskStatusId = (int)Enums.TaskStatusEnum.Unassigned;
//                }
//                else if (status.HasValue)
//                {


//                    task.TaskStatusId = (int)status;

//                    var taskVM = mapper.Map<Tasks, TasksViewModel>(task);

//                    var historiesVM = new List<CreateTaskHistoryViewModel>();

//                    historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "ASSIGNED", ToStatusId = (int)TaskStatusEnum.Assigned });


//                    await AddTaskHistory(historiesVM);


//                }

//                if (reachedTime.HasValue)
//                {
//                    task.ReachedTime = reachedTime;
//                    task.IsTaskReached = true;
//                }
//            }

//            mainTask.AssignmentType = (int?)assignmentType ?? mainTask.AssignmentType;
//            mainTask.ExpirationDate = expirationDate ?? mainTask.ExpirationDate;

//            if (mainTask.ExpirationDate.HasValue && expirationIntervalInSeconds.HasValue)
//            {
//                mainTask.ExpirationDate = DateTime.UtcNow.AddSeconds(expirationIntervalInSeconds.Value);
//            }

//            await context.SaveChangesAsync();
//        }

//        public async Task<MainTaskViewModel> AddAsync(MainTaskViewModel viewModel)
//        {
//            return await AddInternalAsync(viewModel);
//        }







//        public async Task<TasksViewModel> UpdateTaskGeoFence(TasksViewModel task)
//        {

//            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();
//            if (geoFences.Any())
//            {
//                if (!(task.Latitude.HasValue && task.Longitude.HasValue))
//                {
//                    return task;
//                }

//                var taskPoint = new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value };
//                foreach (var geoFence in geoFences)
//                {
//                    var polygon = geoFence.Locations.Select(l => new MapPoint { Latitude = l.Latitude ?? 0, Longitude = l.Longitude ?? 0 }).ToArray();

//                    if (MapsHelper.IsPointInPolygon(polygon, taskPoint))
//                    {
//                        task.GeoFenceId = geoFence.Id;
//                        break;
//                    }
//                }

//            }

//            return task;
//        }
//        public async Task<List<TasksViewModel>> UpdateTaskGeoFence(List<TasksViewModel> Tasks)
//        {

//            //Assign geofence Id to tasks
//            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();
//            if (geoFences.Any())
//            {
//                foreach (var task in Tasks)
//                {
//                    if (!(task.Latitude.HasValue && task.Longitude.HasValue))
//                        continue;

//                    var taskPoint = new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value };
//                    foreach (var geoFence in geoFences)
//                    {
//                        var polygon = geoFence.Locations.Select(l => new MapPoint { Latitude = l.Latitude ?? 0, Longitude = l.Longitude ?? 0 }).ToArray();

//                        if (MapsHelper.IsPointInPolygon(polygon, taskPoint))
//                        {
//                            task.GeoFenceId = geoFence.Id;
//                            break;
//                        }
//                    }
//                }
//            }


//            return Tasks;
//        }



//        private async Task<MainTaskViewModel> AddInternalAsync(MainTaskViewModel viewModel, Enums.TaskStatusEnum? status = null)
//        {
//            viewModel.Tasks = await UpdateTaskGeoFence(viewModel.Tasks);
//            viewModel.Tasks = await AddCustomerAndStatusAsync(viewModel.Tasks);
//            if (status.HasValue)
//            {
//                viewModel.Tasks.ForEach(t => t.TaskStatusId = (int)status.Value);
//            }
//            var entity = mapper.Map<MainTaskViewModel, MainTask>(viewModel);
//            var mainTaskEntity =
//            _repository.AddAsync(entity);
//            var mainTaskVM = mapper.Map<MainTask, MainTaskViewModel>(mainTaskEntity);
//            int? driverId = mainTaskVM.Tasks.FirstOrDefault()?.DriverId;
//            var historiesVM = new List<CreateTaskHistoryViewModel>();
//            foreach (var taskVM in mainTaskVM.Tasks)
//            {
//                historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "CREATED" });
//                if (driverId != null && driverId > 0)
//                {
//                    historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "ASSIGNED", ToStatusId = (int)Enums.TaskStatusEnum.Assigned });
//                }
//            }
//            await AddTaskHistory(historiesVM);

//            await SetDeliveryBranch(mainTaskEntity);
//            viewModel.Id = mainTaskVM.Id;
//            return mainTaskVM;
//        }

//        public async Task<List<DriverViewModel>> GetAvailableDrivers(IEnumerable<int> teamIds = null, IEnumerable<string> tags = null, IEnumerable<int> geoFenceIds = null)
//        {
//            return await GetAvailableDrivers(new GetAailableDriversInput
//            {
//                TeamIds = teamIds,
//                Tags = tags,
//                GeoFenceIds = geoFenceIds
//            });
//        }

//        public async Task<List<DriverViewModel>> GetAvailableDrivers(GetAailableDriversInput input)
//        {
//            var driversQuery = context.Driver.Include(x => x.Team)
//                .Where(x => x.AgentStatusId == (int)AgentStatusesEnum.Available);

//            if (input.TeamIds != null && input.TeamIds.Any())
//            {
//                driversQuery = driversQuery.Where(driver => input.TeamIds.Contains(driver.TeamId));
//            }
//            if (input.Tags != null && input.Tags.Any())
//            {
//                var tagsPredicate = input.Tags
//                    .Select(tag =>
//                    {
//                        Expression<Func<Driver, bool>> result =
//                            driver => driver.Tags.ToLower().Contains(tag.ToLower());
//                        return result;
//                    })
//                    .Aggregate((total, next) => total.Or(next));
//                driversQuery = driversQuery.Where(tagsPredicate);
//            }
//            if (input.PickupGeoFenceIds != null && input.PickupGeoFenceIds.Any())
//            {
//                driversQuery = driversQuery.Where(driver =>
//                    driver.AllPickupGeoFences ||
//                    driver.DriverPickUpGeoFences.Any(pickupGeofence =>
//                        input.PickupGeoFenceIds.Contains(pickupGeofence.GeoFenceId)));
//            }
//            if (input.DeliveryGeoFenceIds != null)
//            {
//                driversQuery = driversQuery.Where(driver =>
//                    driver.AllDeliveryGeoFences ||
//                    driver.DriverDeliveryGeoFences.Any(deliveryGeofence =>
//                        input.DeliveryGeoFenceIds.Contains(deliveryGeofence.GeoFenceId)));
//            }

//            var result = await driversQuery.AsNoTracking()
//                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
//                .ToListAsync();

//            return result;
//        }

//        public async Task CreateTaskDriverRequest(TaskDriverRequestViewModel input)
//        {
//            var request = mapper.Map<TaskDriverRequests>(input);
//            context.TaskDriverRequests.Add(request);
//            await context.SaveChangesAsync();
//        }

//        public async Task UpdateTaskDriverRequest(TaskDriverRequestViewModel input)
//        {
//            var request = await context.TaskDriverRequests
//                .FirstOrDefaultAsync(e => e.MainTaskId == input.MainTaskId && e.DriverId == input.DriverId);
//            if (request == null)
//            {
//                return;
//            }
//            request.ResponseStatus = (int)input.ResponseStatus;
//            request.RetriesCount = input.RetriesCount;
//            request.DriverId = input.DriverId;
//            await context.SaveChangesAsync();
//        }



//        public async Task<List<TaskDriverRequestViewModel>> GetTaskDriverRequests(int mainTaskId, int? driverId = null)
//        {
//            var query = context.TaskDriverRequests
//                .Include(x => x.MainTask)
//                .Where(x => x.MainTaskId == mainTaskId);
//            if (driverId.HasValue)
//            {
//                query = query.Where(x => x.DriverId == driverId.Value);
//            }
//            var requests = await query.AsNoTracking().ToListAsync();
//            var driverRequests = mapper.Map<List<TaskDriverRequests>, List<TaskDriverRequestViewModel>>(requests);

//            return driverRequests;
//        }


//        public async Task<bool> TaskHasGeofences(MainTaskViewModel mainTask)
//        {
//            mainTask.Settings.RestrictGeofences = true;
//            var geoFences = await GetGeofencesForTask(mainTask);
//            return geoFences.PickupGeoFences.Any();
//        }

//        public async Task<GetTaskGeofencesOutput> GetGeofencesForTask(MainTaskViewModel mainTask)
//        {
//            Dictionary<MainTaskViewModel, GetTaskGeofencesOutput> geofencesByTask;
//            if (mainTask.Settings.RestrictGeofences)
//            {
//                geofencesByTask = await GetGeofencesForTasks(new List<MainTaskViewModel> { mainTask });
//            }
//            else
//            {
//                geofencesByTask = await GetGeofences(new List<MainTaskViewModel> { mainTask });
//            }
//            return geofencesByTask?.FirstOrDefault().Value;
//        }
//        public async Task<Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>> GetGeofencesForTasks(IEnumerable<MainTaskViewModel> mainTasks)

//        {
//            var geofencesByTask = new Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>();
//            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();

//            foreach (var mainTask in mainTasks)
//            {
//                var output = new GetTaskGeofencesOutput();
//                var pickupTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
//                var deliveryTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);
//                if (!pickupTask.Latitude.HasValue || !pickupTask.Longitude.HasValue ||
//                    !deliveryTask.Latitude.HasValue || !deliveryTask.Longitude.HasValue)
//                    throw new ArgumentNullException("Latitude and longitude can't be null");

//                var pickupPoint = new MapPoint
//                {
//                    Latitude = pickupTask.Latitude.Value,
//                    Longitude = pickupTask.Longitude.Value
//                };
//                var deliveryPoint = new MapPoint
//                {
//                    Latitude = deliveryTask.Latitude.Value,
//                    Longitude = deliveryTask.Longitude.Value
//                };
//                foreach (var geoFence in geoFences)
//                {
//                    var polygon = geoFence.Locations
//                        .Select(l =>
//                            new MapPoint
//                            {
//                                Latitude = l.Latitude ?? 0,
//                                Longitude = l.Longitude ?? 0
//                            })
//                        .ToArray();

//                    if (MapsHelper.IsPointInPolygon(polygon, pickupPoint))
//                    {
//                        output.PickupGeoFences.Add(geoFence);
//                    }
//                    if (MapsHelper.IsPointInPolygon(polygon, deliveryPoint))
//                    {
//                        output.DeliveryGeoFences.Add(geoFence);
//                    }
//                }

//                geofencesByTask.Add(mainTask, output);
//            }

//            return geofencesByTask;
//        }
//        public async Task<Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>> GetGeofences(IEnumerable<MainTaskViewModel> mainTasks)
//        {
//            var geofencesByTask = new Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>();
//            var geoFences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();

//            foreach (var mainTask in mainTasks)
//            {
//                var output = new GetTaskGeofencesOutput();
//                var pickupTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
//                var deliveryTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);
//                foreach (var geoFence in geoFences)
//                {
//                    var polygon = geoFence.Locations
//                        .Select(geoFenceLocationVioewModel =>
//                            new MapPoint
//                            {
//                                Latitude = geoFenceLocationVioewModel.Latitude ?? 0,
//                                Longitude = geoFenceLocationVioewModel.Longitude ?? 0
//                            })
//                        .ToArray();
//                    output.PickupGeoFences.Add(geoFence);
//                    output.DeliveryGeoFences.Add(geoFence);
//                }

//                geofencesByTask.Add(mainTask, output);
//            }

//            return geofencesByTask;
//        }

//        public async Task<CustomerViewModel> GetCustomerAsync(CustomerViewModel viewModel)
//        {
//            var customer = new CustomerViewModel();
//            try
//            {
//                customer = await context.Customers
//                   .Where(x => x.Phone == viewModel.Phone)
//                   .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
//                   .FirstOrDefaultAsync();
//            }
//            catch
//            {
//#warning cleanup - why this should throw exception? and if it is thrown null show false state, whole unit of work should fail fast and return feedback
//                return null;
//            }

//            return customer;
//        }

//        public double GetTaskTotalTime(Tasks task)
//        {
//            if (task.TaskHistories == null || !task.TaskHistories.Any())
//            {
//                return 0;
//            }
//            var taskHistory = task.TaskHistories
//                .FirstOrDefault(x => x.TaskId == task.Id && x.ToStatusId == (int)TaskStatusEnum.Started);
//            if (taskHistory != null)
//            {
//                return DateTime.UtcNow.Subtract(taskHistory.CreatedDate).TotalSeconds;
//            }
//            return 0;
//        }

//        public Task<int> GetTasksWeights(GetMainTasksInput input)
//        {
//            var q = context.Tasks.AsQueryable();
//#warning cleanup - a switch pattern may be useful
//#warning cleanup - replace q, c with meaningful literals

//            if (input.DriverId.HasValue)
//            {
//                q = q.Where(e => e.DriverId == input.DriverId);
//            }
//            else if (input.DriverIds != null && input.DriverIds.Any())
//            {
//                q = q.Where(e => e.DriverId.HasValue && input.DriverIds.Contains(e.DriverId.Value));
//            }
//            if (input.Status.HasValue)
//            {
//                q = q.Where(e => e.TaskStatusId == (int)input.Status);
//            }
//            else if (input.Statuses != null && input.Statuses.Any())
//            {
//                q = q.Where(e => input.Statuses.Contains((TaskStatusEnum)e.TaskStatusId));
//            }

//            if (input.PickupBranchId.HasValue)
//            {
//                q = q.Where(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup && e.BranchId == input.PickupBranchId);
//            }
//            if (input.GeoFenceId.HasValue)
//            {
//                q = q.Where(e =>
//                    (e.GeoFenceId.HasValue && e.GeoFenceId == input.GeoFenceId) ||
//                    e.Branch.GeoFenceId == input.GeoFenceId);
//            }
//            if (input.DeliveryGeoFenceId.HasValue)
//            {
//                q = q.Where(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery && e.GeoFenceId == input.GeoFenceId);
//            }
//            else if (input.DeliveryGeoFenceIds != null)
//            {
//                q = q.Where(e =>
//                    e.TaskTypeId == (int)TaskTypesEnum.Delivery &&
//                    e.GeoFenceId.HasValue &&
//                    input.DeliveryGeoFenceIds.Contains(e.GeoFenceId.Value));
//            }
//            if (input.PickupGeoFenceId.HasValue)
//            {
//                q = q.Where(e =>
//                    e.TaskTypeId == (int)TaskTypesEnum.Pickup &&
//                    (e.GeoFenceId == input.GeoFenceId || e.BranchId == input.GeoFenceId));
//            }
//            return q.Select(e => e.MainTaskId).Distinct().CountAsync();
//        }

//        public async Task<List<MainTaskViewModel>> GetMainTasks(GetMainTasksInput input)
//        {
//            var q = GetMainTaskQurey(input);

//            var tasks = await q.OrderBy(e => e.Id).ToListAsync();
//            var tasksVM = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(tasks);

//            return tasksVM;
//        }

//        public async Task<MainTaskCountViewModel> GetMainTaskCount(GetMainTasksInput input)
//        {
//            var q = GetMainTaskQurey(input);
//            var mainTaskViewModel = new MainTaskCountViewModel();
//            var tasks = await q.CountAsync();

//            return mainTaskViewModel;
//        }

//        public IQueryable<MainTask> GetMainTaskQurey(GetMainTasksInput input)
//        {
//#warning cleanup - seems duplicate
//            var q = context.MainTask
//               .Include(t => t.Tasks)
//               .AsQueryable();

//            if (input.DriverId.HasValue)
//            {
//                q = q.Where(e => e.Tasks.First().DriverId == input.DriverId);
//            }
//            else if (input.DriverIds != null && input.DriverIds.Any())
//            {
//                q = q.Where(e => e.Tasks.First().DriverId.HasValue && input.DriverIds.Any(d => d == e.Tasks.First().DriverId.Value));
//            }
//            if (input.Status.HasValue)
//            {
//                q = q.Where(e => e.Tasks.First().TaskStatusId == (int)input.Status);
//            }
//            else if (input.Statuses != null && input.Statuses.Any())
//            {
//                q = q.Where(e => input.Statuses.Contains((TaskStatusEnum)e.Tasks.First().TaskStatusId));
//            }
//            if (input.PickupBranchId.HasValue)
//            {
//                q = q.Where(e =>
//                    e.Tasks.Any(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup) &&
//                    e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId == input.PickupBranchId);
//            }
//            if (input.GeoFenceId.HasValue)
//            {
//                q = q.Where(e =>
//                    (e.Tasks.First().GeoFenceId.HasValue && e.Tasks.First().GeoFenceId == input.GeoFenceId) ||
//                    e.Tasks.First().Branch.GeoFenceId == input.GeoFenceId);
//            }
//            if (input.DeliveryGeoFenceId.HasValue)
//            {
//                q = q.Where(e => e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId == input.GeoFenceId);
//            }
//            else if (input.DeliveryGeoFenceIds != null)
//            {
//                q = q.Where(e =>
//                    e.Tasks.Any(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery) &&
//                    e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.HasValue &&
//                    input.DeliveryGeoFenceIds.Any(d => d == e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.Value));
//            }
//            if (input.PickupGeoFenceId.HasValue)
//            {
//                q = q.Where(e =>
//                    (
//                        e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).GeoFenceId.HasValue &&
//                        e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).GeoFenceId == input.GeoFenceId
//                    ) ||
//                    e.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).Branch.GeoFenceId == input.GeoFenceId);
//            }
//            if (input.Take.HasValue)
//            {
//                q = q.Take(input.Take.Value);
//            }
//            return q;
//        }

//        public async Task<bool> CheckDriverBusy(int driverId)
//        {
//            var isDriverBusy = await context.Tasks
//                .AnyAsync(x =>
//                    x.DriverId == driverId &&
//                    x.TaskStatusId == (int)TaskStatusEnum.Started);
//            return isDriverBusy;
//        }
//        public async Task MakeDriverAvailable(int driverId)
//        {
//            var IsDriverBusy = await CheckDriverBusy(driverId);
//            //if (!IsDriverBusy)
//            //{
//            var driver = await context.Driver
//                .Include(x => x.DriverPickUpGeoFences)
//                .Include(x => x.DriverDeliveryGeoFences)
//                .FirstOrDefaultAsync(x => x.Id == driverId);
//            if (driver != null)
//            {
//                if (IsDriverBusy)
//                {
//                    driver.AgentStatusId = (int)AgentStatusesEnum.Busy;
//                }
//                else
//                {
//                    driver.AgentStatusId = (int)AgentStatusesEnum.Available;
//                }
//                context.Update(driver);
//                await context.SaveChangesAsync();
//            }

//            //}

//        }

//        private async Task CreateTaskRoute(TaskRouteViewModel taskRouteViewModel)
//        {
//            var taskRoute = new TaskRoute
//            {
//                TaskId = taskRouteViewModel.TaskId,
//                DriverId = taskRouteViewModel.DriverId,
//                Longitude = taskRouteViewModel.Longitude,
//                Latitude = taskRouteViewModel.Latitude,
//                TaskStatusId = taskRouteViewModel.TaskStatusId,
//            };

//            await context.TaskRoute.AddAsync(taskRoute);
//        }

//        public async Task AddTaskRoute(TaskRouteViewModel taskRouteViewModel)
//        {
//            await CreateTaskRoute(taskRouteViewModel);
//            await context.SaveChangesAsync();
//        }

//        public async Task<Branch> GetRelatedBranchAsync(List<Branch> branches, MapPoint point)
//        {
//            _logger.LogTrace(
//                $"{nameof(GetRelatedBranchAsync)} started for {{branches}}, {{point}}",
//                string.Join(",", branches.Select(branch => branch.Id)),
//                point);
//            double reachedLimitDistanceInM = DefaultSettingValue.ReachedDistanceRestrictionInMeter;
//            var reachedLimitDistanceInMSetting = await _settingsManager.GetSettingByKey(Settings.Resources.SettingsKeys.ReachedDistanceRestriction);

//            if (reachedLimitDistanceInMSetting != null && !string.IsNullOrEmpty(reachedLimitDistanceInMSetting.Value))
//            {
//                double.TryParse(reachedLimitDistanceInMSetting.Value, out reachedLimitDistanceInM);
//            }
//            _logger.LogTrace("{ReachedDistanceRestriction} is configured", reachedLimitDistanceInM);

//            var list = new List<(Branch, double)>();
//            foreach (var branch in branches)
//            {
//                var distance = MapsHelper.GetDistanceBetweenPoints(branch.Latitude.Value, branch.Longitude.Value, point.Latitude, point.Longitude);
//                list.Add((branch, distance));
//            }

//            var nearestBranch = list.OrderBy(x => x.Item2).FirstOrDefault();
//            if (nearestBranch.Item2 <= reachedLimitDistanceInM)
//            {
//                _logger.LogInformation("{NearestBranch} is close enough.", new { nearestBranch.Item1.Id, Distance = nearestBranch.Item2 });
//                return nearestBranch.Item1;
//            }
//            _logger.LogInformation("{NearestBranch} is not close enough.", new { nearestBranch.Item1.Id, Distance = nearestBranch.Item2 });

//            return null;
//        }
//        public async Task SetTaskDistanceHours(Tasks dbTask)
//        {
//            var taskHistories = context.TaskHistory
//                .Where(x => x.TaskId == dbTask.Id)
//                .OrderByDescending(x => x.CreatedDate);

//            var taskDates = await taskHistories
//                .Select(x => x.CreatedDate)
//                .ToArrayAsync();

//            var taskPoints = await context.TaskRoute
//                .Where(x => x.TaskId == dbTask.Id)
//                .OrderByDescending(x => x.CreatedDate)
//                .Where(x =>
//                    (x.Latitude != null || x.Latitude == 0) &&
//                    (x.Longitude != null || x.Longitude == 0))
//                .ProjectTo<MapPoint>(mapper.ConfigurationProvider)
//                .ToArrayAsync();

//            if (taskPoints != null)
//            {
//                var distance = await GetTotalTaskKMAsync(taskPoints);
//                dbTask.TotalDistance = double.IsNaN(distance) ? 0 : distance;
//            }

//            dbTask.TotalTaskTime = taskDates != null ? GetTotalTaskDate(taskDates) : 0;
//            await context.SaveChangesAsync();

//        }
//        private double GetTotalTaskDate(DateTime[] dates)
//        {
//            double total = 0.0;
//            for (var i = 0; i < dates.Length; i++)
//            {
//                if (i == 0) continue;
//                total += dates[i - 1].Subtract(dates[i]).TotalMilliseconds;
//            }
//            return total;
//        }

//        private Task<double> GetTotalTaskKMAsync(MapPoint[] points)
//        {
//            double total = 0.0;
//            for (var i = 0; i < points.Length; i++)
//            {
//                if (i == 0) continue;
//                //var distanceresponse = await this._mapsManager.GetDistance(points[i - 1], points[i], this._mapsSettings);
//                var distanceresponse = MapsHelper.GetDistanceBetweenPoints(points[i - 1], points[i]);
//                total += (distanceresponse / 1000);
//            }
//            return Task.FromResult(total);
//        }

//        public Task<MainTask> GetMainTaskStatus(int mainTaskId)
//        {
//            return context.MainTask.FirstOrDefaultAsync(e => e.Id == mainTaskId);
//        }

//        public async Task UpdateMainTaskStatus(int mainTaskId)
//        {
//            var dbMainTask = await context.MainTask
//                .Include(x => x.Tasks)
//                .FirstOrDefaultAsync(x => x.Id == mainTaskId);

//            var IsAssigned = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Assigned);
//            if (IsAssigned)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Assigned;
//                dbMainTask.IsCompleted = false;
//            }
//            var IsInprogress = dbMainTask.Tasks.Any(x => x.TaskStatusId == (int)TaskStatusEnum.Started || x.TaskStatusId == (int)TaskStatusEnum.Accepted);
//            if (IsInprogress)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Inprogress;
//                dbMainTask.IsCompleted = false;
//            }
//            var IsAccepted = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Accepted);
//            if (IsAccepted)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Accepted;
//                dbMainTask.IsCompleted = false;
//            }
//            var IsSuccessful = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Successful);
//            if (IsSuccessful)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Successful;
//                dbMainTask.IsCompleted = true;
//            }
//            var IsCanceled = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Cancelled);
//            if (IsCanceled)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Canceled;
//                dbMainTask.IsCompleted = true;
//            }
//            var IsDeclined = dbMainTask.Tasks.All(x => x.TaskStatusId == (int)TaskStatusEnum.Declined);
//            if (IsDeclined)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Declined;
//                dbMainTask.IsCompleted = true;
//            }
//            var IsFailed = dbMainTask.Tasks.Any(x => x.TaskStatusId == (int)TaskStatusEnum.Failed);
//            if (IsFailed)
//            {
//                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Failed;
//            }

//            await context.SaveChangesAsync();
//        }

//        public async Task<bool> AutoAllocationFail(int mainTaskId)
//        {
//            var tenantId = _currentUser.TenantId;
//            var dbMainTask = await context.MainTask.FirstOrDefaultAsync(x => x.Id == mainTaskId);
//            dbMainTask.IsFailToAutoAssignDriver = true;
//            context.Update(dbMainTask);
//            await context.SaveChangesAsync();
//            await _notificationhubContext.Clients.Group(tenantId).SendAsync("AutoAllocationFailed", mainTaskId);
//            var title = _localizer[Keys.Notifications.TaskAutoAllocationFailed];
//            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationFailedDetailsTaskId, mainTaskId);
//            await context.Notifications.AddAsync(new Notification
//            {
//                Title = title,
//                Body = body,
//                FromUserId = tenantId,
//                ToUserId = tenantId,
//                IsSeen = false
//            });
//            if (tenantId != _currentUser.Id)
//            {
//                await _notificationhubContext.Clients.Group(_currentUser.UserName).SendAsync("AutoAllocationFailed", mainTaskId);
//                await context.Notifications.AddAsync(new Notification
//                {
//                    Title = title,
//                    Body = body,
//                    FromUserId = tenantId,
//                    ToUserId = _currentUser.Id,
//                    //NotificationType = NotificationTypeWeb.AutoAllocationFailed
//                    IsSeen = false
//                });
//            }
//            await context.SaveChangesAsync();

//            return true;
//        }


//        public async Task<bool> SetDeliveryBranch(MainTask mainTask)
//        {
//            var tasks = await context.Tasks.Where(x => x.MainTaskId == mainTask.Id).ToListAsync();

//            var branchId = tasks.FirstOrDefault(x => x.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId;
//            if (branchId != null && branchId != 0)
//            {
//                foreach (var task in tasks)
//                {
//                    task.BranchId = branchId;
//                    context.Tasks.Update(task);
//                }
//                await context.SaveChangesAsync();
//                return true;
//            }
//            return false;
//        }

//        public async Task<bool> AutoAllocationSucssesfull(int mainTaskId)
//        {
//            var dbMainTask = await context.MainTask
//                .FirstOrDefaultAsync(x => x.Id == mainTaskId);
//            var CreatedUserName = _currentUser.UserName;
//            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("AutoAllocationSucessfully", mainTaskId);
//            var title = _localizer[Keys.Notifications.TaskAutoAllocationSucceded];
//            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationSuccededDetailsTaskId, mainTaskId);
//            await context.Notifications.AddAsync(new Notification
//            {
//                Title = title,
//                Body = body,
//                FromUserId = CreatedUserName,
//                ToUserId = _currentUser.TenantId,
//                IsSeen = false
//            });
//            if (_currentUser.TenantId != _currentUser.Id)
//            {
//                await _notificationhubContext.Clients.Group(CreatedUserName).SendAsync("AutoAllocationSucessfully", mainTaskId);
//                await context.Notifications.AddAsync(new Notification
//                {
//                    Title = title,
//                    Body = body,
//                    FromUserId = CreatedUserName,
//                    ToUserId = _currentUser.Id,
//                    IsSeen = false
//                });
//            }
//            await context.SaveChangesAsync();

//            return true;
//        }
//    }
//}
