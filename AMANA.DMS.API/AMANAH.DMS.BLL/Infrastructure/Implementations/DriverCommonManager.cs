﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using AMANAH.DMS.BASE.Authentication;
//using AMANAH.DMS.BASE.Domain.Repositories;
//using AMANAH.DMS.BLL.Constants;
//using AMANAH.DMS.BLL.Enums;
//using AMANAH.DMS.BLL.IManagers;
//using AMANAH.DMS.BLL.ViewModels;
//using AMANAH.DMS.BLL.ViewModels.Driver;
//using AMANAH.DMS.BLL.ViewModels.Tasks;
//using AMANAH.DMS.Context;
//using AMANAH.DMS.DATA.Entities;
//using AMANAH.DMS.Repoistry;
//using AMANAH.DMS.ViewModels;
//using AutoMapper;
//using AutoMapper.QueryableExtensions;
//using MapsUtilities.Models;
//using Microsoft.EntityFrameworkCore;

//namespace AMANAH.DMS.BLL.Managers
//{
//    public class DriverCommonManager : BaseManager<DriverViewModel, Driver>, IDriverCommonManager
//    {
//        private readonly ITaskCommonManager _taskCommonManager;
//        private readonly ISettingsManager _settingsManager;
//        private readonly IRepositry<GeoFence> _geofenceRepository;

//        public DriverCommonManager(
//            ApplicationDbContext context,
//            IMapper mapper,
//            IRepositry<Driver> repository,
//            ITaskCommonManager taskCommonManager,
//            ISettingsManager settingsManager,
//            IRepositry<GeoFence> geofenceRepository)
//            : base(context, repository, mapper)
//        {
//            _taskCommonManager = taskCommonManager;
//            _settingsManager = settingsManager;
//            _geofenceRepository = geofenceRepository;
//        }

//        public async Task UpdateDriverClubbingTime(UpdateDriverClubbingTimeViewModel viewModel)
//        {
//            var driver = await (context as ApplicationDbContext).Driver
//                .Where(x => x.Id == viewModel.DriverId)
//                .FirstOrDefaultAsync();
//            driver.IsInClubbingTime = viewModel.IsInClubbingTime;
//            driver.ClubbingTimeExpiration = viewModel.ClubbingTimeExpiration;
//            (context as ApplicationDbContext).Driver.Update(driver);
//            await (context as ApplicationDbContext).SaveChangesAsync();
//        }  
//        public async Task<List<GeoFence>> GetDriverDeliveryGeoFencesAsync(int driverId)
//        {
//            var dbContext = context as ApplicationDbContext;

//            var driver = await dbContext.Driver.FindAsync(driverId);
//            if (driver.AllDeliveryGeoFences)
//            {
//                return await _geofenceRepository.GetAll().ToListAsync();
//            }
//            return await dbContext.DriverDeliveryGeoFence
//                .Include(x => x.GeoFence)
//                .ThenInclude(x => x.GeoFenceLocations)
//                .Where(x => x.DriverId == driverId && x.GeoFence != null && !x.GeoFence.IsDeleted)
//                .Select(x => x.GeoFence)
//                .ToListAsync();
//        }
//        private IQueryable<Driver> GetReachedDriversWithTasksQueryable()
//        {
//            #warning cleanup - public methods should not expose iquerable
//            return (context as ApplicationDbContext).Driver
//                .Include(d => d.AssignedTasks)
//                    .ThenInclude(t => t.MainTask)
//                .Where(d =>
//                    d.ReachedTime != null &&
//                    d.AssignedTasks.All(t =>
//                        t.TaskStatusId == (int)Enums.TaskStatusEnum.Unassigned ||
//                        t.TaskStatusId == (int)Enums.TaskStatusEnum.Assigned));
//        }
//        public async Task<DriverWithTasksViewModel> GetReachedDriverWithTasks(int driverId)
//        {
//            var driver = await GetReachedDriversWithTasksQueryable()
//                .Where(d => d.Id == driverId)
//                .FirstOrDefaultAsync();
//            if (driver == null)
//            {
//                return null;
//            }
//            var driverVM = mapper.Map<Driver, DriverWithTasksViewModel>(driver);
//            driverVM.AssignedMainTasks = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(driver.AssignedTasks.Select(t => t.MainTask).Distinct().ToList());
//            return driverVM;
//        }
//        private IQueryable<Driver> GetDriversQueryable(GetDriverBaseInput input)
//        {
//            var query = (context as ApplicationDbContext).Driver
//                .Include(d => d.CurrentLocation)
//                .Include(d => d.DriverDeliveryGeoFences)
//                .Include(d => d.DriverPickUpGeoFences)
//                .AsQueryable();
//            if (input.IsReached.HasValue)
//            {
//                if (input.IsReached.Value)
//                {
//                    query = query.Where(d => d.ReachedTime.HasValue);
//                }
//                else
//                {
//                    query = query.Where(d => !d.ReachedTime.HasValue);
//                }
//            }

//            if (input.Status.HasValue)
//            {
//                query = query.Where(d => d.AgentStatusId == (int)input.Status);
//            }
//            if (input.Statuses != null && input.Statuses.Any())
//            {
//                query = query.Where(d => input.Statuses.Contains((AgentStatusesEnum)d.AgentStatusId));
//            }


//            return query;
//        }
//        public async Task<DriverWithTasksViewModel> GetDriver(GetDriverInput input)
//        {
//            var q = GetDriversQueryable(input);

//            q = q.Where(d => d.Id == input.DriverId);

//            var driver = q.FirstOrDefault();

//            var driverVM = mapper.Map<Driver, DriverWithTasksViewModel>(driver);
//            if (driverVM == null) return null;
//            int driverOrderCapacity = DefaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
//            if (input.IsApplyingAutoAllocation)
//            {
//                if (input.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
//                {
//                    var driverOrderCapacitySetting = await _settingsManager.GetSettingByKey(
//                        Settings.Resources.SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
//                    if (driverOrderCapacitySetting != null)
//                    {
//                        int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
//                    }
//                }
//            }

//            if (input.IncludeAssignedTasks)
//            {
//                var getMainTasksInput = new GetMainTasksInput
//                {
//                    Status = input.TaskStatus,
//                    Statuses = input.TaskStatuses,
//                    DriverId = driverVM?.Id
//                };
//                var tasksVM = await _taskCommonManager.GetMainTasks(getMainTasksInput);
//                driverVM.AssignedMainTasks = tasksVM.Where(t => t.Tasks.First().DriverId == driverVM.Id).ToList();
//            }
//            else if (input.IncludeOrdersWeights)
//            {
//                var getMainTasksInput = new GetMainTasksInput
//                {
//                    Status = input.TaskStatus,
//                    Statuses = input.TaskStatuses,
//                    DriverId = driverVM.Id
//                };
//                driverVM.MaxOrdersWeightsCapacity = driverOrderCapacity;
//                driverVM.MinOrdersNoWait = driverOrderCapacity;
//                driverVM.OrdersWeights = await _taskCommonManager.GetTasksWeights(getMainTasksInput);
//                driverVM.AssignedTasksCount = driverVM.OrdersWeights;
//            }

//            return driverVM;
//        }
//        public async Task<List<DriverWithTasksViewModel>> GetDrivers(GetDriversInput input)
//        {
//            var query = GetDriversQueryable(input);

//            if (input.PickupBranchId.HasValue)
//            {
//                query = query.Where(d => d.CurrentLocation.BranchId == input.PickupBranchId);
//            }
//            else if (input.SamePickupBranchAsDriverId.HasValue)
//            {
//                var pickupBranchId = await (context as ApplicationDbContext).Driver
//                    .Where(d => d.Id == input.SamePickupBranchAsDriverId)
//                    .Select(d => d.CurrentLocation.BranchId)
//                    .FirstOrDefaultAsync();
//                if (pickupBranchId.HasValue)
//                {
//                    query = query.Where(d => d.CurrentLocation.BranchId == pickupBranchId);
//                }
//            }
//            if (input.DeliveryGeoFenceId.HasValue)
//            {
//                query = query.Where(d => d.AllDeliveryGeoFences || d.DriverDeliveryGeoFences.Any(g => g.Id == input.DeliveryGeoFenceId));
//            }
//            if (input.DeliveryGeoFencesIds != null && input.DeliveryGeoFencesIds.Any())
//            {
//                query = query.Where(d => d.AllDeliveryGeoFences || d.DriverDeliveryGeoFences.Any(g => input.DeliveryGeoFencesIds.Contains(g.Id)));
//            }

//            var drivers = await query.ToListAsync();
//            var driversVM = mapper.Map<List<Driver>, List<DriverWithTasksViewModel>>(drivers);

//            int driverOrderCapacity = DefaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
//            if (input.IsApplyingAutoAllocation)
//            {
//                if (input.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
//                {
//                    var driverOrderCapacitySetting = await _settingsManager.GetSettingByKey(
//                        Settings.Resources.SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
//                    if (driverOrderCapacitySetting != null) int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
//                }
//            }


//            if (driversVM != null && driversVM.Count > 0)
//                if (input.IncludeAssignedTasks)
//                {
//                    var getMainTasksInput = new GetMainTasksInput
//                    {
//                        PickupBranchId = input.PickupBranchId,
//                        Status = input.TaskStatus,
//                        Statuses = input.TaskStatuses,
//                        DriverIds = drivers.Select(d => d.Id).ToList()
//                    };
//                    var tasksVM = await _taskCommonManager.GetMainTasks(getMainTasksInput);
//                    driversVM?.ForEach(d => d.AssignedMainTasks = tasksVM.Where(t => t.Tasks.First().DriverId == d.Id).ToList());
//                }
//                else if (input.IncludeOrdersWeights)
//                {
//                    var getMainTasksInput = new GetMainTasksInput
//                    {
//                        Status = input.TaskStatus,
//                        Statuses = input.TaskStatuses,
//                    };
//                    foreach (var driverVM in driversVM)
//                    {
//                        getMainTasksInput.DriverId = driverVM.Id;
//                        driverVM.MaxOrdersWeightsCapacity = driverOrderCapacity;
//                        driverVM.MinOrdersNoWait = driverOrderCapacity;
//                        driverVM.OrdersWeights = await _taskCommonManager.GetTasksWeights(getMainTasksInput);
//                        driverVM.AssignedTasksCount = driverVM.OrdersWeights;
//                    }
//                }

//            return driversVM;
//        }
//        public async Task<List<Driver>> GetDriversWithExpireCluppingTime()
//        {
//            return await (context as ApplicationDbContext).Driver.Where(e => e.IsInClubbingTime && e.ClubbingTimeExpiration.HasValue && e.ClubbingTimeExpiration.Value <= DateTime.UtcNow).ToListAsync();
//        }
//        public void Update(Driver driver)
//        {
//            repository.Update(driver);
//        }
//    }
//}
