﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Helpers;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.Repoistry
{
    public class BaseManager<TViewModel, TEntity> : IBaseManager<TViewModel, TEntity>
    {

        protected readonly IRepositry<TEntity> repository;
        protected readonly ApplicationDbContext context;
        protected readonly IMapper mapper;

        public BaseManager(ApplicationDbContext _context, IRepositry<TEntity> _repository, IMapper _mapper)
        {
            mapper = _mapper;
            context = _context;
            repository = _repository;
        }


        public virtual Task<TViewModel> AddAsync(TViewModel viewModel)
        {
            var entity = mapper.Map<TViewModel, TEntity>(viewModel);
            var resultentity = repository.AddAsync(entity);
            var result = mapper.Map<TEntity, TViewModel>(resultentity);
            return Task.FromResult(result);
        }

        public virtual Task<List<TViewModel>> AddAsync(List<TViewModel> ViewModelLst)
        {
            var entities = mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelLst);
            var resultentities = repository.Add(entities);
            var result = mapper.Map<List<TEntity>, List<TViewModel>>(resultentities);
            return Task.FromResult(result);
        }

        public virtual Task<bool> DeleteAsync(TViewModel viewModel)
        {
            var entity = mapper.Map<TViewModel, TEntity>(viewModel);
            bool result = repository.Delete(entity);
            return Task.FromResult(result);
        }

        public virtual Task<bool> DeleteAsync(List<TViewModel> ViewModelLst)
        {
            var entity = mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelLst);
            var result = repository.Delete(entity);
            return Task.FromResult(result);
        }

        public virtual Task<bool> DeleteByIdAsync(params object[] id)
        {
            var result = repository.DeleteById(id);
            return Task.FromResult(result);
        }



        public virtual Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>()
        {
            return repository.GetAll().ProjectTo<TProjectedModel>(mapper.ConfigurationProvider).ToListAsync();
        }



        public virtual Task<bool> SoftDeleteAsync(TViewModel viewModel)
        {
            var entity = mapper.Map<TViewModel, TEntity>(viewModel);
            bool result = repository.SoftDelete(entity);
            return Task.FromResult(result);
        }

        public virtual Task<bool> SoftDeleteAsync(List<TViewModel> ViewModelLst)
        {
            var entities = mapper.Map<List<TViewModel>, List<TEntity>>(ViewModelLst);
            bool result = repository.SoftDelete(entities);
            return Task.FromResult(result);
        }

        public virtual Task<bool> UpdateAsync(TViewModel viewModel)
        {
            TEntity entity = mapper.Map<TViewModel, TEntity>(viewModel);
            bool result = repository.Update(entity);
            return Task.FromResult(result);
        }

        public virtual Task<PagedResult<TViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = repository.GetAll()
                .ToPagedResultAsync<TEntity, TViewModel>(
                    pagingparametermodel,
                    mapper.ConfigurationProvider);
            return pagedResult;
        }


        public virtual Task<bool> UpdateAsync<TCommand>(TCommand viewModel)
        {
            var entity = mapper.Map<TCommand, TEntity>(viewModel);
            var result = repository.Update(entity);
            return Task.FromResult(result);
        }


        //public async Task<PagedResult<TViewModel>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel)
        //{
        //    return await Task.Run(() =>
        //    {
        //        var pagedResult = new PagedResult<TViewModel>();

        //        pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
        //        pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

        //        var source = listQuery.ProjectTo<TViewModel>();

        //        // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
        //        int CurrentPage = pagingparametermodel.PageNumber;

        //        // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
        //        int PageSize = pagingparametermodel.PageSize;
        //        pagedResult.TotalCount = source.Count();//
        //        pagedResult.Result = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

        //        return pagedResult;

        //    }
        //  );

        //}

        //public async Task<PagedResult<TQueryModel>> GetAllByPaginationAsync<TQueryModel>(
        //    IQueryable<TEntity> listQuery, PaginatedItemsViewModel pagingparametermodel)
        //{
        //    return await Task<PagedResult<TQueryModel>>.Run(() =>
        //    {
        //        var pagedResult = new PagedResult<TQueryModel>();

        //        pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
        //        pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

        //        var source = listQuery.ProjectTo<TQueryModel>();

        //        // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
        //        int CurrentPage = pagingparametermodel.PageNumber;

        //        // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
        //        int PageSize = pagingparametermodel.PageSize;
        //        pagedResult.TotalCount = source.Count();
        //        pagedResult.Result = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

        //        return pagedResult;

        //    }
        //  );

        //}

        public Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(bool IgnoreTenant)
        {
            return repository.GetAll(IgnoreTenant)
                .ProjectTo<TProjectedModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<PagedResult<TViewModel>> GetAllByPaginationAsync(
            bool IgnoreTenant, PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = repository.GetAll(IgnoreTenant)
                .ToPagedResultAsync<TEntity, TViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }
    }


}
