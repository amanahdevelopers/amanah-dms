﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.BaseResult
{
   public class EntityResult<TResult>
    {
        public EntityResult()
        {
            Errors = new List<IdentityError>();
            Succeeded = true;
        }

        public ICollection<IdentityError> Errors { get; set; }

        private bool succeeded;
        public bool Succeeded
        {
            get => succeeded && !Errors.Any();
            set => succeeded = value;
        }
        public TResult result { get; set; }

        public string Error { get; set; }
    }
}
