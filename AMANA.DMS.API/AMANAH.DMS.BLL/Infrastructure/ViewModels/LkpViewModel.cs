﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class LkpViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }

    public class TaskStatusViewModel : LkpViewModel
    {
        public string Color { set; get; }
    }
}
