﻿using System;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class TimeViewModel
    {
        public TimeSpan Time { get; set; }
        public int  Day { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }
    }
}
