﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class ManagerDispatchingManager :
        BaseManager<ManagerDispatchingViewModel, ManagerDispatching>,
        IManagerDispatchingManager
    {
        public ManagerDispatchingManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<ManagerDispatching> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<ManagerDispatchingViewModel> Get(int id)
        {
            var manager = await (context as ApplicationDbContext).ManagerDispatching
                .Include(x => x.Manager)
                .Include(x => x.Manager.User)
                .Where(x => x.Id == id)
                .ProjectTo<ManagerDispatchingViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            await GetManagerDispatchingMembersAsync(manager);
            return manager;
        }

        public async Task<List<ManagerDispatchingViewModel>> GetAllAsync()
        {
            var list = await base.GetAllAsync<ManagerDispatchingViewModel>();
            foreach (var x in list)
            {
                await GetManagerDispatchingMembersAsync(x);
            }
            return list;
        }

        public async override Task<PagedResult<ManagerDispatchingViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var list = await base.GetAllByPaginationAsync(pagingparametermodel);
            foreach (var x in list.Result)
            {
                await GetManagerDispatchingMembersAsync(x);
            }
            return list;
        }

        private async Task GetManagerDispatchingMembersAsync(ManagerDispatchingViewModel manger)
        {
            if (!string.IsNullOrEmpty(manger.Zones))
            {
                if (manger.Zones.ToLower() == "all")
                {
                    manger.LkpZones = await (context as ApplicationDbContext).GeoFence
                        .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                        .ToListAsync();
                }
                else
                {
                    var zones = manger.Zones.Split(',').ToList();
                    if (zones != null && zones.Any())
                    {
                        var zonesIds = new List<int>();
                        foreach (var zone in zones)
                        {
                            int.TryParse(zone, out int zoneId);
                            if (zoneId > 0) zonesIds.Add(zoneId);
                        }
                        manger.LkpZones = await (context as ApplicationDbContext).GeoFence
                            .Where(x => zonesIds.Any() && zonesIds.Contains(x.Id))
                            .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                            .ToListAsync();
                    }
                }
            }

            if (!string.IsNullOrEmpty(manger.Restaurants))
            {
                if (manger.Restaurants.ToLower() == "all")
                {
                    manger.LkpRestaurants = await (context as ApplicationDbContext).Restaurant
                          .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                          .ToListAsync();
                }
                else
                {
                    var restaurants = manger.Restaurants.Split(',').ToList();
                    if (restaurants != null && restaurants.Any())
                    {
                        var restaurantsIds = new List<int>();
                        foreach (var restaurant in restaurants)
                        {
                            int.TryParse(restaurant, out int restaurantId);
                            if (restaurantId > 0) restaurantsIds.Add(restaurantId);
                        }
                        manger.LkpRestaurants = await (context as ApplicationDbContext).Restaurant
                            .Where(x => restaurantsIds.Any() && restaurantsIds.Contains(x.Id))
                            .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                            .ToListAsync();
                    }
                }
            }

            if (!string.IsNullOrEmpty(manger.Branches))
            {
                if (manger.Branches.ToLower() == "all")
                {
                    manger.LkpBranches = await (context as ApplicationDbContext).Branch
                        .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                        .ToListAsync();
                }
                else
                {
                    var branches = manger.Branches.Split(',').ToList();
                    if (branches != null && branches.Any())
                    {
                        var branchesIds = new List<int>();
                        foreach (var branch in branches)
                        {
                            int.TryParse(branch, out int branchId);
                            if (branchId > 0) branchesIds.Add(branchId);
                        }
                        manger.LkpBranches = await (context as ApplicationDbContext).Branch
                            .Where(x => branchesIds.Any() && branchesIds.Contains(x.Id))
                            .ProjectTo<LkpViewModel>(mapper.ConfigurationProvider)
                            .ToListAsync();
                    }
                }
            }
        }
    }
}
