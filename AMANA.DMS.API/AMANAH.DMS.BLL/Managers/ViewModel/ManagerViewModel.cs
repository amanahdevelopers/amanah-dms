﻿using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMANAH.DMS.ViewModels
{
    public class BaseManagerViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CountryId { set; get; }
        public List<string> RoleNames { get; set; }
        public List<TeamManagerViewModel> TeamManagers { set; get; }

    }

    public class ManagerViewModel : BaseManagerViewModel
    {
        public int Id { set; get; }
        public string UserId { get; set; }
        public CountryViewModel Country { get; set; }
    }

    public class CreateManagerViewModel : BaseManagerViewModel
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
