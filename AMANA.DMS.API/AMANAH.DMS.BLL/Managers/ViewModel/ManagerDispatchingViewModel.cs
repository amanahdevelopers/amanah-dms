﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class ManagerDispatchingViewModel
    {
        public int Id { get; set; }
        public string DesignationName { get; set; }
        public string Zones { get; set; }
        public List<LkpViewModel> LkpZones { get; set; }
        public string Restaurants { get; set; }
        public List<LkpViewModel> LkpRestaurants { get; set; }
        public string Branches { get; set; }
        public List<LkpViewModel> LkpBranches { get; set; }

        public int ManagerId { get; set; }
        public string ManagerName { get; set; }

    }
}
