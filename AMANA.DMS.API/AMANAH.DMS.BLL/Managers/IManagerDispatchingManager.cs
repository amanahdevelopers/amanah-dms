﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IManagerDispatchingManager :IBaseManager<ManagerDispatchingViewModel, ManagerDispatching>
    {
        Task<ManagerDispatchingViewModel> Get(int id);
        Task<List<ManagerDispatchingViewModel>> GetAllAsync();
    }
}
