﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{

    public interface IManagerManager : IBaseManager<ManagerViewModel, Manager>
    {
        Task<ManagerViewModel> Get(int id);
        Task<PagedResult<ManagerViewModel>> GetAllMyByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<ManagerViewModel>> GetPaginationByAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<ManagerViewModel>> GetMyPaginationByAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<ManagerViewModel>> GetDriverManagersAsync(int driverId);
    }
}
