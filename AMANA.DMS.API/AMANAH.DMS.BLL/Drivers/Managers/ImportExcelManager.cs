﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Account;
using AMANAH.DMS.Entities;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers
{
    public class ImportExcelManager : IImportExcelManager
    {
        private readonly IMapper _mapper;
        private readonly IApplicationUserManager _appUserManager;
        private readonly IUploadFileManager _uploadFileManager;
        private readonly IDriverManager _driverManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;

        public ImportExcelManager(
            IMapper mapper,
            IApplicationUserManager appUserManager,
            IUploadFileManager uploadFileManager,
            IDriverManager driverManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _mapper = mapper;
            _appUserManager = appUserManager;
            _uploadFileManager = uploadFileManager;
            _driverManager = driverManager;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        public ImportDriverViewModel GetDriver(IList<string> rowData, IList<string> columnNames)
        {
            return new ImportDriverViewModel()
            {
                File = rowData[columnNames.IndexFor("File")].ToString(),
                UserName = rowData[columnNames.IndexFor("UserName")].ToString(),
                Password = rowData[columnNames.IndexFor("Password")].ToString(),
                Email = rowData[columnNames.IndexFor("Email")].ToString(),
                PhoneNumber = rowData[columnNames.IndexFor("PhoneNumber")].ToString(),
                FirstName = rowData[columnNames.IndexFor("FirstName")].ToString(),
                LastName = rowData[columnNames.IndexFor("LastName")].ToString(),
                Tags = rowData[columnNames.IndexFor("Tags")].ToString(),
                TransportDescription = rowData[columnNames.IndexFor("TransportDescription")].ToString(),
                LicensePlate = rowData[columnNames.IndexFor("LicensePlate")].ToString(),
                Color = rowData[columnNames.IndexFor("Color")].ToString(),
                RoleName = rowData[columnNames.IndexFor("RoleName")].ToString(),
                AgentTypeId = rowData[columnNames.IndexFor("AgentTypeId")].ToInt32Nullable(),
                TeamId = rowData[columnNames.IndexFor("TeamId")].ToInt32(),
                AgentStatusId = rowData[columnNames.IndexFor("AgentStatusId")].ToInt32Nullable(),
                TransportTypeId = rowData[columnNames.IndexFor("TransportTypeId")].ToInt32Nullable(),
                CountryId = rowData[columnNames.IndexFor("CountryId")].ToInt32Nullable(),
            };
        }

        public async Task<ProcessResult<List<ImportDriverViewModel>>> AddFromExcelSheetAsync(string path, UploadFile Uploadfile)
        {
            ProcessResult<List<ImportDriverViewModel>> result = new ProcessResult<List<ImportDriverViewModel>>();
            try
            {
                if (string.IsNullOrEmpty(path))
                {
                    result.IsSucceeded = false;
                    result.Exception = new Exception("There is no file uploaded");
                    return result;
                }

                UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
                ProcessResult<string> uploadedFile = uploadExcelFileManager.AddFile(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(uploadedFile.returnData, path);
                    var dataList = ExcelReader.GetDataToList(excelPath, GetDriver);
                    result.returnData = await AddDriverAsync((List<ImportDriverViewModel>)dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }

        public async Task<List<ImportDriverViewModel>> AddDriverAsync(List<ImportDriverViewModel> createDriversVM)
        {
            var importDriverViewModels = new List<ImportDriverViewModel>();
            foreach (var driver in createDriversVM)
            {
                var result = await AddDriverAsync(driver);
                importDriverViewModels.Add(result);
            }
            return importDriverViewModels;
        }

        public async Task<ImportDriverViewModel> AddDriverAsync(ImportDriverViewModel createDriverVM)
        {
            createDriverVM = DriverValidator(createDriverVM);
            if (!string.IsNullOrEmpty(createDriverVM.Error))
            {
                return createDriverVM;
            }
            var existUser = await _appUserManager.GetByEmailAsync(createDriverVM.Email);
            if (existUser != null)
            {
                createDriverVM.Error = _localizer[Keys.Validation.EmailAlreadyExists];
                return createDriverVM;
            }

            var result = await AddDriverUserAsync(createDriverVM);
            if (result == null || result.GetType() == typeof(string) || !result.Succeeded)
            {
                createDriverVM.Error = result.Error;
                return createDriverVM;
            }

            var driverToCreate = _mapper.Map<DriverViewModel>(createDriverVM);
            driverToCreate.UserId = result.User.Id;
            createDriverVM = AddDriverFile(createDriverVM, out string ImageUrl);
            if (!string.IsNullOrEmpty(createDriverVM.Error))
                return createDriverVM;
            driverToCreate.ImageUrl = ImageUrl;

            var created_driver = await _driverManager.AddAsync(driverToCreate);
            await _driverManager.ChangeDriverStatusByUserId(created_driver.UserId, (int)AgentStatusesEnum.Offline);
            return createDriverVM;
        }

        private async Task<UserManagerResult> AddDriverUserAsync(ImportDriverViewModel createDriverVM)
        {
            var userManagerToCreate = _mapper.Map<ApplicationUserViewModel>(createDriverVM);
            var userToCreate = _mapper.Map<ApplicationUser>(userManagerToCreate);
            userToCreate.RoleNames.Add(createDriverVM.RoleName);
            var result = await _appUserManager.AddUserAsync(userToCreate, userManagerToCreate.Password);
            return result;
        }

        private ImportDriverViewModel DriverValidator(ImportDriverViewModel createDriverVM)
        {
            const int MinCharacters = 1;
            const int MaxCharacters = 100;
            if (string.IsNullOrEmpty(createDriverVM.FirstName) || createDriverVM.FirstName.Length > MaxCharacters)
            {
                createDriverVM.Error = _localizer.Format(
                    Keys.Validation.NameMustBeBetweenMinAndMaxCharacters,
                    MinCharacters,
                    MaxCharacters);
                return createDriverVM;
            }

            if (string.IsNullOrEmpty(createDriverVM.PhoneNumber))
            {
                createDriverVM.Error = _localizer[Keys.Validation.PhoneNumberIsRequired];
                return createDriverVM;
            }

            if (createDriverVM.TeamId == 0)
            {
                createDriverVM.Error = _localizer[Keys.Validation.TeamIdIsRequired];
                return createDriverVM;
            }

            if (string.IsNullOrEmpty(createDriverVM.UserName))
            {
                createDriverVM.Error = _localizer[Keys.Validation.UserNameIsRequired];
                return createDriverVM;
            }

            if (createDriverVM.TransportTypeId == 0)
            {
                createDriverVM.Error = _localizer[Keys.Validation.TransportIdIsRequired];
                return createDriverVM;
            }

            return createDriverVM;
        }

        private ImportDriverViewModel AddDriverFile(ImportDriverViewModel createDriverVM, out string ImageUrl)
        {
            ImageUrl = "";
            if (!string.IsNullOrEmpty(createDriverVM.File))
            {
                var path = "DriverImages";
                UploadFile uploadFile = new UploadFile()
                {
                    FileContent = createDriverVM.File,
                    FileName = "Image.jpg"
                };

                var processResult = _uploadFileManager.AddFile(uploadFile, path);
                if (!processResult.IsSucceeded)
                    createDriverVM.Error = processResult.Exception.Message;

                ImageUrl = processResult.returnData;
            }
            else
            {
                ImageUrl = null;
            }

            return createDriverVM;
        }
    }
}
