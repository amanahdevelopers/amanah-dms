﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.Managers.TaskAssignment;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class DriverManager : BaseManager<DriverViewModel, Driver>, IDriverManager
    {
        private readonly ITaskAutoAssignmentFactory _taskAutoAssignmentFactory;
        private readonly ICurrentUser _currentUser;
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly IRepositry<GeoFence> _geofenceRepository;
        private readonly ITasksManager _tasksManager;
        private readonly ISettingsManager _settingsManager;

        public DriverManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Driver> repository,
            IApplicationRoleManager iApplicationRoleManager,
            ITaskAutoAssignmentFactory taskAutoAssignmentFactory,
            ICurrentUser currentUser,
            IRepositry<GeoFence> geofenceRepository,
            ISettingsManager settingsManager, ITasksManager tasksManager)
            : base(context, repository, mapper)
        {
            _applicationRoleManager = iApplicationRoleManager;
            _taskAutoAssignmentFactory = taskAutoAssignmentFactory;
            _currentUser = currentUser;
            _geofenceRepository = geofenceRepository;
            _settingsManager = settingsManager;
            _tasksManager = tasksManager;
        }

        public async Task<DriverViewModel> Get(int id)
        {
            return await context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Where(x => x.Id == id)
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

        }


        public Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Include(x => x.User)
                .Include(x => x.Team)
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }



        public Task<DriverViewModel> GetByUserID(string UserId)
        {
            return context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Include(x => x.Team).ThenInclude(x => x.LocationAccuracy)
                .Where(x => x.UserId == UserId)
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }


        public async Task<bool> ChangeDriverStatusByUserId(string userId, int statusId)
        {
            try
            {
                var driverVM = await GetByUserID(userId);
                driverVM.AgentStatusId = statusId;
                if (statusId != (int)AgentStatusesEnum.Available)
                {
                    driverVM.ReachedTime = null;
                    driverVM.ClubbingTimeExpiration = null;
                    driverVM.IsInClubbingTime = false;
                    driverVM.MaxOrdersWeightsCapacity = 1;
                    driverVM.MinOrdersNoWait = 1;
                }
                await UpdateAsync(driverVM);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<string>> GetTagsAsync()
        {
            var tags = await repository.GetAll()
                .Select(t => t.Tags)
                .ToListAsync();
            var splitTags = tags.SelectMany(tag => tag.Split(','))
                .Distinct()
                .ToList();
            return splitTags;
        }

        public async Task ChangeDriversTypeAsync(ChangeDriversTypeViewModel changeDriverTypeViewModel)
        {
            if (!changeDriverTypeViewModel.DriverIds.Any())
            {
                return;
            }
            var drivers = await context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Where(x => changeDriverTypeViewModel.DriverIds.Contains(x.Id))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            foreach (var driver in drivers)
            {
                driver.AgentTypeId = changeDriverTypeViewModel.AgentTypeId;
                await UpdateAsync(driver);
            }
        }


        public async Task BlockDriversAsync(BlockDriversViewModel blockDriverViewModel)
        {
            if (!blockDriverViewModel.DriverIds.Any())
            {
                return;
            }
#warning cleanup - reuse duplicated code
            var drivers = await (context as ApplicationDbContext).Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Where(x => blockDriverViewModel.DriverIds.Contains(x.Id))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
            foreach (var driver in drivers)
            {
                driver.AgentStatusId = (int)Enums.AgentStatusesEnum.Blocked;
                driver.Reason = blockDriverViewModel.Reason;
                await UpdateAsync(driver);
            }
        }



        public async Task UnBlockDriversAsync(BlockDriversViewModel blockDriverViewModel)
        {
            if (!blockDriverViewModel.DriverIds.Any())
            {
                return;
            }
#warning cleanup - reuse duplicate code includes, wheres, ...
#warning critical - cleanup - Why we map then update mapped!!!!!
            var drivers = await (context as ApplicationDbContext).Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Where(x => blockDriverViewModel.DriverIds.Contains(x.Id))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
            foreach (var driver in drivers)
            {
                driver.AgentStatusId = (int)Enums.AgentStatusesEnum.Offline;
                driver.Reason = blockDriverViewModel.Reason;
                await UpdateAsync(driver);
            }
        }

        public Task<List<DriverViewModel>> GetAllAsync(List<int> managerTeamIds)
        {
            return repository.GetAll()
                .Where(x => managerTeamIds.Contains(x.TeamId))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel driverSearchViewModel)
        {
            return await InternalGetAllDriverByPaginationAsync(driverSearchViewModel);
        }

        public async Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel driverSearchViewModel, List<int> managerTeamIds)
        {
            return await InternalGetAllDriverByPaginationAsync(driverSearchViewModel, managerTeamIds);
        }


        public override async Task<bool> DeleteAsync(DriverViewModel viewModel)
        {
            var driver = await Get(viewModel.Id);
            viewModel.AgentStatusId = driver.AgentStatusId;
            await RemoveDriverGeoFenceAsync(viewModel.Id);
            return await base.DeleteAsync(viewModel);
        }


        public override async Task<bool> DeleteAsync(List<DriverViewModel> driverViewModels)
        {
            foreach (var driverViewModel in driverViewModels)
            {
                await RemoveDriverGeoFenceAsync(driverViewModel.Id);
            }
            return await base.DeleteAsync(driverViewModels);
        }

        public async Task<DriverCurrentLocation> GetDriverCurrentLocationAsync(int driverId)
        {
            return await context.DriverCurrentLocation
                    .Where(x => x.DriverId == driverId)
                    .OrderByDescending(x => x.CreatedDate)
                    .FirstOrDefaultAsync();
        }


        private async Task RemoveDriverGeoFenceAsync(int id)
        {
            var pickupGeoFences = await context.DriverPickUpGeoFence
                .Where(x => x.DriverId == id)
                .ToListAsync();
            var deliveryGeoFences = await context.DriverDeliveryGeoFence
                .Where(x => x.DriverId == id)
                .ToListAsync();

            context.DriverPickUpGeoFence.RemoveRange(pickupGeoFences);
            context.DriverDeliveryGeoFence.RemoveRange(deliveryGeoFences);

            await context.SaveChangesAsync();
        }

        public async Task<List<GeoFence>> GetDriverPickUpGeoFencesAsync(int driverId)
        {
            var dbContext = context;

            var driver = await dbContext.Driver.FindAsync(driverId);
            if (driver.AllPickupGeoFences)
            {
                return await _geofenceRepository.GetAll().ToListAsync();
            }
            return await dbContext.DriverPickUpGeoFence
                .Include(x => x.GeoFence)
                .ThenInclude(x => x.GeoFenceLocations)
                .Where(x => x.DriverId == driverId && x.GeoFence != null && !x.GeoFence.IsDeleted)
                .Select(x => x.GeoFence)
                .ToListAsync();
        }

        public GeoFence GetDriverCurrentGeoFence(List<GeoFence> driverGeoFences, DriverCurrentLocation driverCurrentLocation)
        {
            var driverCurrentPoint = new MapPoint
            {
                Latitude = driverCurrentLocation.Latitude.Value,
                Longitude = driverCurrentLocation.Longitude.Value
            };
            foreach (var geoFence in driverGeoFences)
            {
                var zone = context.GeoFenceLocation
                    .Where(x => x.GeoFenceId == geoFence.Id && x.GeoFence != null && !x.GeoFence.IsDeleted)
                    .Select(x => new MapPoint { Latitude = x.Latitude.Value, Longitude = x.Longitude.Value })
                    .Select(x => new MapPoint { Latitude = x.Latitude, Longitude = x.Longitude })
                    .ToArray();

                if (MapsUtilities.MapsHelper.IsPointInPolygon(zone, driverCurrentPoint))
                {
                    return geoFence;
                }
            }
            return null;
        }

        public async Task<List<Branch>> GetDriverCurrentGeoFenceBranchesAsync(GeoFence driverCurrentGeoFence)
        {
            return await context.Branch
                .Where(x => x.GeoFenceId == driverCurrentGeoFence.Id & !x.IsDeleted)
                .ToListAsync();
        }


        public async Task CheckInAsync(int branchid)
        {
            //var driverId =_currentUser.DriverId;
            var driver = await GetCurrentDriver();

            driver.AgentStatusId = (int)AgentStatusesEnum.Available;
            driver.ReachedTime = DateTime.UtcNow;
            driver.IsInClubbingTime = false;
            driver.ClubbingTimeExpiration = null;
            var drivercurrentlocation = await context.DriverCurrentLocation
                .FirstOrDefaultAsync(x => x.DriverId == driver.Id);
            drivercurrentlocation.BranchId = branchid;
            context.Update(drivercurrentlocation);
            await context.SaveChangesAsync();
            await UpdateAsync(driver);

            var t = await _taskAutoAssignmentFactory.GetTaskAutoAssignment();
            await t.AssignTasksToDriver(driver.Id);
        }

        public async Task<DriverViewModel> GetCurrentDriver()
        {
            var driver = await context.Driver
                .FirstOrDefaultAsync(driver => driver.Id == _currentUser.DriverId);
            var driverVM = mapper.Map<Driver, DriverViewModel>(driver);
            return driverVM;
        }

        public Task<List<DriverForAssignViewModel>> GetAssignDriversAsync()
        {
            return repository.GetAll()
                .Where(x => x.AgentStatusId != (int)Enums.AgentStatusesEnum.Blocked)
                .ProjectTo<DriverForAssignViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<DriverTransportionTypeViewModel> GetDriverTransportionTypeAsync(string userId)
        {
            return await context.Driver
                .Include(x => x.TransportType)
                .Where(x => x.UserId == userId)
                .ProjectTo<DriverTransportionTypeViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }



        public override async Task<PagedResult<DriverViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var drivers = await base.GetAllByPaginationAsync(pagingparametermodel);
            drivers.Result.ForEach(x => x.RoleNames = _applicationRoleManager.GetAllRolesByUserId(x.UserId));
            return drivers;
        }


        public Task<List<DriverExportToExcelViewModel>> ExportExcel()
        {
            var ctx = context;

            var query = ctx.Driver
                .Include(t => t.Team)
                .Include(t => t.AgentStatus)
                .Include(t => t.User)
                .ThenInclude(t => t.UserDvices)
                .Select(t => new DriverExportToExcelViewModel
                {
                    Username = t.User.UserName,
                    FullName = t.User.FirstName + " " + t.User.LastName,
                    Email = t.User.Email,
                    PhoneNumber = t.User.PhoneNumber,
                    DeviceType = t.DeviceType,
                    Version = t.Version,
                    TeamName = t.Team.Name,
                    Tags = t.Tags,
                    AgentStatusName = t.AgentStatus.Name
                });

            return query.ToListAsync();
        }


        public async Task UpdateDriverGeoFenceAsync(DriverViewModel viewModel)
        {
            await RemoveDriverGeoFenceAsync(viewModel.Id);
            viewModel.DriverDeliveryGeoFences.ForEach(x => x.DriverId = viewModel.Id);
            var entityDriverDeliveryGeoFence = mapper.Map<List<DriverDeliveryGeoFenceViewModel>, List<DriverDeliveryGeoFence>>(viewModel.DriverDeliveryGeoFences);
            await context.DriverDeliveryGeoFence.AddRangeAsync(entityDriverDeliveryGeoFence);

            viewModel.DriverPickUpGeoFences.ForEach(x => x.DriverId = viewModel.Id);
            var entityDriverPickUpGeoFence = mapper.Map<List<DriverPickUpGeoFenceViewModel>, List<DriverPickUpGeoFence>>(viewModel.DriverPickUpGeoFences);
            await context.DriverPickUpGeoFence.AddRangeAsync(entityDriverPickUpGeoFence);

            await context.SaveChangesAsync();
        }

        private async Task<PagedResult<DriverViewModel>> InternalGetAllDriverByPaginationAsync(
            DriverSearchViewModel driverSearchViewModel, IEnumerable<int> managerTeamIds = null)
        {
            var query = repository.GetAll();
            if (!string.IsNullOrWhiteSpace(driverSearchViewModel.SearchBy))
            {
                query = query.Where(driver =>
                    driver.User.UserName.Contains(driverSearchViewModel.SearchBy) ||
                    driver.User.FirstName.Contains(driverSearchViewModel.SearchBy) ||
                    driver.User.MiddleName.Contains(driverSearchViewModel.SearchBy) ||
                    driver.User.LastName.Contains(driverSearchViewModel.SearchBy) ||
                    driver.User.Email.Contains(driverSearchViewModel.SearchBy) ||
                    driver.User.PhoneNumber.Contains(driverSearchViewModel.SearchBy) ||
                    driver.DeviceType.Contains(driverSearchViewModel.SearchBy) ||
                    driver.Version.Contains(driverSearchViewModel.SearchBy) ||
                    driver.Team.Name.Contains(driverSearchViewModel.SearchBy) ||
                    driver.Team.Tags.Contains(driverSearchViewModel.SearchBy) ||
                    driver.AgentStatus.Name.Contains(driverSearchViewModel.SearchBy));
            }
            if (driverSearchViewModel.statusType != default)
            {
                query = query.Where(driver => driver.AgentStatusId == (int)driverSearchViewModel.statusType);
            }
            if (driverSearchViewModel.driverType != default)
            {
                query = query.Where(driver => driver.AgentTypeId == driverSearchViewModel.driverType);
            }
            if (managerTeamIds != null)
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }
            var pagedResult = await query.ToPagedResultAsync<Driver, DriverViewModel>(driverSearchViewModel, mapper.ConfigurationProvider);
            pagedResult.Result?.ForEach(x => x.RoleNames = _applicationRoleManager.GetAllRolesByUserId(x.UserId));
            return pagedResult;
        }

        public async Task UpdateDriverClubbingTime(UpdateDriverClubbingTimeViewModel viewModel)
        {
            var driver = await context.Driver
                .Where(x => x.Id == viewModel.DriverId)
                .FirstOrDefaultAsync();
            driver.IsInClubbingTime = viewModel.IsInClubbingTime;
            driver.ClubbingTimeExpiration = viewModel.ClubbingTimeExpiration;
            context.Driver.Update(driver);
            await context.SaveChangesAsync();
        }
        public async Task<List<GeoFence>> GetDriverDeliveryGeoFencesAsync(int driverId)
        {
            var dbContext = context;

            var driver = await dbContext.Driver.FindAsync(driverId);
            if (driver.AllDeliveryGeoFences)
            {
                return await _geofenceRepository.GetAll().ToListAsync();
            }
            return await dbContext.DriverDeliveryGeoFence
                .Include(x => x.GeoFence)
                .ThenInclude(x => x.GeoFenceLocations)
                .Where(x => x.DriverId == driverId && x.GeoFence != null && !x.GeoFence.IsDeleted)
                .Select(x => x.GeoFence)
                .ToListAsync();
        }
        private IQueryable<Driver> GetReachedDriversWithTasksQueryable()
        {
#warning cleanup - public methods should not expose iquerable
            return (context as ApplicationDbContext).Driver
                .Include(d => d.AssignedTasks)
                    .ThenInclude(t => t.MainTask)
                .Where(d =>
                    d.ReachedTime != null &&
                    d.AssignedTasks.All(t =>
                        t.TaskStatusId == (int)Enums.TaskStatusEnum.Unassigned ||
                        t.TaskStatusId == (int)Enums.TaskStatusEnum.Assigned));
        }
        public async Task<DriverWithTasksViewModel> GetReachedDriverWithTasks(int driverId)
        {
            var driver = await GetReachedDriversWithTasksQueryable()
                .Where(d => d.Id == driverId)
                .FirstOrDefaultAsync();
            if (driver == null)
            {
                return null;
            }
            var driverVM = mapper.Map<Driver, DriverWithTasksViewModel>(driver);
            driverVM.AssignedMainTasks = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(driver.AssignedTasks.Select(t => t.MainTask).Distinct().ToList());
            return driverVM;
        }
        private IQueryable<Driver> GetDriversQueryable(GetDriverBaseInput input)
        {
            var query = context.Driver
                .Include(d => d.CurrentLocation)
                .Include(d => d.DriverDeliveryGeoFences)
                .Include(d => d.DriverPickUpGeoFences)
                .AsQueryable();
            if (input.IsReached.HasValue)
            {
                if (input.IsReached.Value)
                {
                    query = query.Where(d => d.ReachedTime.HasValue);
                }
                else
                {
                    query = query.Where(d => !d.ReachedTime.HasValue);
                }
            }

            if (input.Status.HasValue)
            {
                query = query.Where(d => d.AgentStatusId == (int)input.Status);
            }
            if (input.Statuses != null && input.Statuses.Any())
            {
                query = query.Where(d => input.Statuses.Contains((AgentStatusesEnum)d.AgentStatusId));
            }


            return query;
        }
        public async Task<DriverWithTasksViewModel> GetDriver(GetDriverInput input)
        {
            var q = GetDriversQueryable(input);

            q = q.Where(d => d.Id == input.DriverId);

            var driver = q.FirstOrDefault();

            var driverVM = mapper.Map<Driver, DriverWithTasksViewModel>(driver);
            if (driverVM == null) return null;
            int driverOrderCapacity = DefaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            if (input.IsApplyingAutoAllocation)
            {
                if (input.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
                {
                    var driverOrderCapacitySetting = await _settingsManager.GetSettingByKey(
                        Settings.Resources.SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
                    if (driverOrderCapacitySetting != null)
                    {
                        int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
                    }
                }
            }

            if (input.IncludeAssignedTasks)
            {
                var getMainTasksInput = new GetMainTasksInput
                {
                    Status = input.TaskStatus,
                    Statuses = input.TaskStatuses,
                    DriverId = driverVM?.Id
                };
                var tasksVM = await _tasksManager.GetMainTasks(getMainTasksInput);
                driverVM.AssignedMainTasks = tasksVM.Where(t => t.Tasks.First().DriverId == driverVM.Id).ToList();
            }
            else if (input.IncludeOrdersWeights)
            {
                var getMainTasksInput = new GetMainTasksInput
                {
                    Status = input.TaskStatus,
                    Statuses = input.TaskStatuses,
                    DriverId = driverVM.Id
                };
                driverVM.MaxOrdersWeightsCapacity = driverOrderCapacity;
                driverVM.MinOrdersNoWait = driverOrderCapacity;
                driverVM.OrdersWeights = await _tasksManager.GetTasksWeights(getMainTasksInput);
                driverVM.AssignedTasksCount = driverVM.OrdersWeights;
            }

            return driverVM;
        }
        public async Task<List<DriverWithTasksViewModel>> GetDrivers(GetDriversInput input)
        {
            var query = GetDriversQueryable(input);

            if (input.PickupBranchId.HasValue)
            {
                query = query.Where(d => d.CurrentLocation.BranchId == input.PickupBranchId);
            }
            else if (input.SamePickupBranchAsDriverId.HasValue)
            {
                var pickupBranchId = await context.Driver
                    .Where(d => d.Id == input.SamePickupBranchAsDriverId)
                    .Select(d => d.CurrentLocation.BranchId)
                    .FirstOrDefaultAsync();
                if (pickupBranchId.HasValue)
                {
                    query = query.Where(d => d.CurrentLocation.BranchId == pickupBranchId);
                }
            }
            if (input.DeliveryGeoFenceId.HasValue)
            {
                query = query.Where(d => d.AllDeliveryGeoFences || d.DriverDeliveryGeoFences.Any(g => g.Id == input.DeliveryGeoFenceId));
            }
            if (input.DeliveryGeoFencesIds != null && input.DeliveryGeoFencesIds.Any())
            {
                query = query.Where(d => d.AllDeliveryGeoFences || d.DriverDeliveryGeoFences.Any(g => input.DeliveryGeoFencesIds.Contains(g.Id)));
            }

            var drivers = await query.ToListAsync();
            var driversVM = mapper.Map<List<Driver>, List<DriverWithTasksViewModel>>(drivers);

            int driverOrderCapacity = DefaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            if (input.IsApplyingAutoAllocation)
            {
                if (input.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
                {
                    var driverOrderCapacitySetting = await _settingsManager.GetSettingByKey(
                        Settings.Resources.SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
                    if (driverOrderCapacitySetting != null) int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
                }
            }


            if (driversVM != null && driversVM.Count > 0)
                if (input.IncludeAssignedTasks)
                {
                    var getMainTasksInput = new GetMainTasksInput
                    {
                        PickupBranchId = input.PickupBranchId,
                        Status = input.TaskStatus,
                        Statuses = input.TaskStatuses,
                        DriverIds = drivers.Select(d => d.Id).ToList()
                    };
                    var tasksVM = await _tasksManager.GetMainTasks(getMainTasksInput);
                    driversVM?.ForEach(d => d.AssignedMainTasks = tasksVM.Where(t => t.Tasks.First().DriverId == d.Id).ToList());
                }
                else if (input.IncludeOrdersWeights)
                {
                    var getMainTasksInput = new GetMainTasksInput
                    {
                        Status = input.TaskStatus,
                        Statuses = input.TaskStatuses,
                    };
                    foreach (var driverVM in driversVM)
                    {
                        getMainTasksInput.DriverId = driverVM.Id;
                        driverVM.MaxOrdersWeightsCapacity = driverOrderCapacity;
                        driverVM.MinOrdersNoWait = driverOrderCapacity;
                        driverVM.OrdersWeights = await _tasksManager.GetTasksWeights(getMainTasksInput);
                        driverVM.AssignedTasksCount = driverVM.OrdersWeights;
                    }
                }

            return driversVM;
        }

        public async Task<List<Driver>> GetDriversWithExpireCluppingTime()
        {
            return await context.Driver
                .Where(e => e.IsInClubbingTime && e.ClubbingTimeExpiration.HasValue && e.ClubbingTimeExpiration.Value <= DateTime.UtcNow)
                .ToListAsync();
        }

        public void Update(Driver driver)
        {
            repository.Update(driver);
        }

        public async Task<List<int>> GetDriverIdsByGeofenceIdAsync(int geofenceId)
        {
            var driverPickUpGeoFences = await context.DriverPickUpGeoFence
                .Where(e => e.GeoFenceId == geofenceId)
                .Select(e => e.DriverId)
                .ToArrayAsync();
            var driverDeliveryUpGeoFences = await context.DriverDeliveryGeoFence
                .Where(e => e.GeoFenceId == geofenceId)
                .Select(e => e.DriverId)
                .ToArrayAsync();
            var result = driverPickUpGeoFences.Concat(driverDeliveryUpGeoFences)
                .Distinct()
                .ToList();
            return result;
        }


        public async Task<List<int>> GetManagerDriversAsync(IEnumerable<int> teamIds)
        {

            if (teamIds == null)
            {
                return null;
            }
            return await context.Driver
                .Where(x => teamIds.Contains(x.TeamId))
                .Select(x => x.Id)
                .ToListAsync();
        }

    }
}
