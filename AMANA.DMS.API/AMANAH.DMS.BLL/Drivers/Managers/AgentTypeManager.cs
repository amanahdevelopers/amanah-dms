﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class AgentTypeManager : BaseManager<AgentTypeViewModel, AgentType>, IAgentTypeManager
    {
        public AgentTypeManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<AgentType> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<AgentTypeViewModel> Get(int id)
        {
            return await (context as ApplicationDbContext).AgentType
                .Where(x => x.Id == id)
                .ProjectTo<AgentTypeViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await (context as ApplicationDbContext).AgentType
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
    }
}
