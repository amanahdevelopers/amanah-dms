﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class DriverTaskNotificationManager :
        BaseManager<DriverTaskNotificationViewModel, DriverTaskNotification>,
        IDriverTaskNotificationManager
    {

        public DriverTaskNotificationManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<DriverTaskNotification> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task MarkAsRead(int driverId)
        {
            var dbDriverTaskNotifications = await (context as ApplicationDbContext).DriverTaskNotification
                .Where(x => x.DriverId == driverId)
                .ToListAsync();

            foreach (var notification in dbDriverTaskNotifications)
            {
                notification.IsRead = true;
            }

            await context.SaveChangesAsync();
        }

        public int GetUnReadCount(int driverId)
        {
            return (context as ApplicationDbContext).DriverTaskNotification
                .Where(x => x.DriverId == driverId && x.IsRead != true)
                .Count();
        }

        public Task<PagedResult<TaskNotificationViewModel>> GetByPaginationAsync(
            PaginatedTasksDriverViewModel pagingparametermodel)
        {
            return (context as ApplicationDbContext).DriverTaskNotification
                .Include(x => x.Task.TaskType)
                .Include(x => x.Task.TaskStatus)
                .Include(x => x.Task.Customer)
                .Where(x => x.DriverId == pagingparametermodel.DriverId)
                .OrderByDescending(x => x.CreatedDate)
                .AsNoTracking()
                .ToPagedResultAsync<DriverTaskNotification, TaskNotificationViewModel>(
                pagingparametermodel,
                mapper.ConfigurationProvider);
        }

        public async Task Clear(int driverId)
        {
            // TODO: (performance) this need reconsideration why should I load all notifications of user in memory just to mark as deleted, what if user has hundreds of notifications!
            // we may add EntityFramework.Extended for bulk operations + reuse code to put our aspects like soft-delete, auditing, ...
            // maybe better place to handle this is the DbContext itself
            var dbDriverTaskNotifications = await (context as ApplicationDbContext).DriverTaskNotification
                .Where(x => x.DriverId == driverId)
                .ToListAsync();
            context.RemoveRange(dbDriverTaskNotifications);
            await context.SaveChangesAsync();
        }
    }
}
