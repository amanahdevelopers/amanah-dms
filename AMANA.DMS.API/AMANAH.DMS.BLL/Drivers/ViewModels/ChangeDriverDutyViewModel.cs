﻿namespace AMANAH.DMS.ViewModels
{
    public class ChangeDriverDutyViewModel
    {
        /// <summary>
        /// This is Driver User Id not DriverId
        /// </summary>
        public string DriverId { set; get; }
        public bool IsAvailable { set; get; }
    }
}
