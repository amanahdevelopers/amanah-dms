﻿using System;
using System.Collections.Generic;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.ViewModel;

namespace AMANAH.DMS.ViewModels
{
    public class DriverViewModel : BaseViewModel
    {
        public int Id { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string ImageUrl { get; set; }
        public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public List<string> RoleNames { get; set; }
        public int TeamId { set; get; }
        public string TeamName { get; set; }
        public int? AgentTypeId { set; get; }
        public string AgentTypeName { get; set; }
        public DateTime AgentStatusUpdateDate { get; set; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
        public int? CountryId { set; get; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        public int? AgentStatusId { set; get; }
        public string Reason { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public int MaxOrdersWeightsCapacity { get; set; } = DefaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
        public int MinOrdersNoWait { get; set; } = DefaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
        public int AssignedTasksCount { get; set; }
        public DateTime? TokenExpirationDate { get; set; }



        public double? DriverAvgRate { get; set; }
        public string AgentStatusName
        {
            get; set;
            //  public ApplicationUser User { get; set; }
        }
        public DateTime? ReachedTime { get; set; }
        public bool IsReached => ReachedTime.HasValue;
        public double? RouteDistanceToTask { set; get; }
        public string LocationAccuracyName { set; get; }
        public int? LocationAccuracyDuration { set; get; }
        public int? BranchId { get; set; }
        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
        public DateTime? TokenTimeExpiration { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }
        public List<DriverPickUpGeoFenceViewModel> DriverPickUpGeoFences { get; set; }
        public List<DriverDeliveryGeoFenceViewModel> DriverDeliveryGeoFences { get; set; }
    }
}
