﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class DriverTransportionTypeViewModel
    {
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
    }
}
