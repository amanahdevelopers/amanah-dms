﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.ViewModels
{
    public class AgentTypeViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
