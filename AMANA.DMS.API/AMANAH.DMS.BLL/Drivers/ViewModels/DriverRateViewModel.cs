﻿using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.DATA.Entities;

namespace AMANAH.DMS.BLL.ViewModels
{
   public class DriverRateViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int DriverId { get; set; }
        public int TasksId { get; set; }
        public double Rate { get; set; }
        public string Note { get; set; }

        
    }
}
