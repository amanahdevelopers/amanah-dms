﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class BlockDriversViewModel
    {
        public List<int> DriverIds { get; set; }
        public string Reason { get; set; }
    }
}
