﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class ChangeDriversTypeViewModel
    {
        public List<int> DriverIds { get; set; }
        public int AgentTypeId { set; get; }
    }
}
