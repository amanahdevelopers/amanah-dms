﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class DriverTeamViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string ImageUrl { get; set; }
    }
}
