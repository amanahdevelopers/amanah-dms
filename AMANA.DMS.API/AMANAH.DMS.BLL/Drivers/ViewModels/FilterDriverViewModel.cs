﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Mobile
{
    public class FilterDriverViewModel
    {
        public List<int> TeamIds { set; get; }
        public List<string> Tags { set; get; }
        public List<int> GeoFenceIds  { set; get; }

    }
}
