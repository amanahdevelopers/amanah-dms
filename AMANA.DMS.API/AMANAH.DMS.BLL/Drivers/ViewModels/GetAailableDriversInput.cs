﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class GetAailableDriversInput
    {
        public IEnumerable<int> TeamIds { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IEnumerable<int> GeoFenceIds { get; set; }
        public IEnumerable<int> PickupGeoFenceIds { get; set; }
        public IEnumerable<int> DeliveryGeoFenceIds { get; set; }
    }
}
