﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Driver
{
    public class EditDriverProfileViewModel
    {
        public string phone { get; set; }
        public string CurrentPassword  { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

    }
}
