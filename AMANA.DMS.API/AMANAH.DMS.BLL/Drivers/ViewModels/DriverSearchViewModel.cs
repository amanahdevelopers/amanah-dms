﻿using AMANAH.DMS.BLL.Enums;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace AMANAH.DMS.ViewModels
{
    public class DriverSearchViewModel: PaginatedItemsViewModel
    {
        public AgentStatusesEnum statusType { set; get; }
        public int driverType { set; get; }       
    }
}