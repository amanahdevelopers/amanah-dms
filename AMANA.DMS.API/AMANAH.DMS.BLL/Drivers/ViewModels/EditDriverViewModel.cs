﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.ViewModels
{
   public class EditDriverViewModel
    {
        public int Id { set; get; }
        public string File { get; set; }
        public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public int? TransportTypeId { set; get; }
        public string Color { get; set; }
        public string Role { get; set; }
        public int TeamId { set; get; }
        public int? AgentTypeId { set; get; }
        public int? CountryId { set; get; }
        public string UserId { get; set; }
        // Edit in user 
        public string Email { get; set; }
        public string userName { get; set; }
        public string RoleName { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }

        public List<DriverPickUpGeoFenceViewModel> DriverPickUpGeoFences { get; set; }
        public List<DriverDeliveryGeoFenceViewModel> DriverDeliveryGeoFences { get; set; }

    }
}
