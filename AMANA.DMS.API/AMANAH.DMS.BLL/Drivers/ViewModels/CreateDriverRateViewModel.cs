﻿using AMANAH.DMS.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class CreateDriverRateViewModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int DriverId { get; set; }
        public int TasksId { get; set; }
        public double Rate { get; set; }
        public string Note { get; set; }

    }
}
