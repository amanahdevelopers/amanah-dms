﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class CalenderViewModel
    {
        public int  Driver_id { set; get; }
        public DateTime? FromDate { get;  set; }
        public DateTime? ToDate { get;  set; }
    }
}
