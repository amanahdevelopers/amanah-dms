﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ML.Mono.Helpers;

namespace AMANAH.DMS.BLL.BackgroundServcies
{
    public class DriverMonitorService : IDriverMonitorService
    {
        private readonly ApplicationDbContext _context;
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly IManagerManager _managerManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;



        public DriverMonitorService(
            ApplicationDbContext context,
            IHubContext<NotificationHub> notificationhubContext,
            IManagerManager managerManager,
            IConfiguration configuration,
            IMapper mapper)
        {
            _context = context;
            _notificationhubContext = notificationhubContext;
            _managerManager = managerManager;
            _configuration = configuration;
            _mapper = mapper;
        }


        public async Task LogoutDriversWithExpirdTokens()
        {
            var drivers = await _context.Driver
                .Include(d => d.AgentStatus)
                .Include(d => d.User)
                .Include(d => d.CurrentLocation)
                .Where(d =>
                    d.TokenTimeExpiration.HasValue &&
                    (d.AgentStatusId == (int)AgentStatusesEnum.Available || d.AgentStatusId == (int)AgentStatusesEnum.Busy) &&
                    d.TokenTimeExpiration.Value.Date == DateTime.UtcNow.Date &&
                    d.TokenTimeExpiration.Value.Hour == DateTime.UtcNow.Hour)
                .ToListAsync();
            if (!drivers.Any())
            {
                return;
            }
            var apiKey = Convert.ToString(_configuration.GetSection("FcmSettings").GetValue(typeof(String), "ApiKey"));

            foreach (var driver in drivers)
            {
                ///
                driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
                driver.TokenTimeExpiration = null;
                _context.Driver.Update(driver);

                var userdevices = await _context.UserDevice.Where(x => x.UserId == driver.UserId).ToListAsync();
                foreach (var userDevice in userdevices)
                {
                    ///Force logout mobile app
                    await FireBase.SendPushNotificationAsync(
                   userDevice.FCMDeviceId,
                   "D-Hub",
                   "Your Session Expired You Need To Login Again",
                   new { IsLoggedOut = true },
                   apiKey);

                    _context.Remove(userDevice);
                }




                var DriverVM = _mapper.Map<DriverViewModel>(driver);

                DriverVM.AgentStatusId = (int)AgentStatusesEnum.Offline;
                DriverVM.AgentStatusName = AgentStatusesEnum.Offline.ToString();

                ///Send Notifications To web front end 
                await _notificationhubContext.Clients.Group(driver.Tenant_Id).SendAsync("DriverLoggedOut", DriverVM);
                var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
                foreach (var driverManager in DriverManagers)
                {
                    await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("DriverLoggedOut", DriverVM);
                }
            }
            await _context.SaveChangesAsync();

        }
    }
}
