﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ITransportTypeManager : IBaseManager<TransportTypeViewModel, TransportType>
    {
    }
}