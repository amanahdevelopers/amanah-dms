﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IDriverRateManager : IBaseManager<DriverRateViewModel, DriverRate>
    {
        Task<DriverRateViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
    }
}
