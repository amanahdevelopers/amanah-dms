﻿using AMANAH.DMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IImportExcelManager
    { 
        Task<ProcessResult<List<ImportDriverViewModel>>> AddFromExcelSheetAsync(string path, UploadFile Uploadfile);
    }
}
