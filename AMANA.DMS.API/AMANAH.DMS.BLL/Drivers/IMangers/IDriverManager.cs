﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{

    public interface IDriverManager : IBaseManager<DriverViewModel, Driver>
    {
        Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel pagingparametermodel);
        Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel pagingparametermodel, List<int> managerTeamIds);
        Task CheckInAsync(int branchid);
        Task<DriverViewModel> GetCurrentDriver();
        Task<List<DriverForAssignViewModel>> GetAssignDriversAsync();
        Task<DriverTransportionTypeViewModel> GetDriverTransportionTypeAsync(string userId);
        Task<List<DriverExportToExcelViewModel>> ExportExcel();
        Task<DriverViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<DriverViewModel> GetByUserID(string UserId);
        Task<bool> ChangeDriverStatusByUserId(string userId, int statusId);
        Task<List<string>> GetTagsAsync();
        Task ChangeDriversTypeAsync(ChangeDriversTypeViewModel changeDriverTypeViewModel);
        Task BlockDriversAsync(BlockDriversViewModel blockDriverViewModel);
        Task UnBlockDriversAsync(BlockDriversViewModel blockDriverViewModel);
        Task UpdateDriverGeoFenceAsync(DriverViewModel viewModel);
        Task<List<DriverViewModel>> GetAllAsync(List<int> managerTeamIds);
        Task<DriverCurrentLocation> GetDriverCurrentLocationAsync(int driverId);
        Task<List<GeoFence>> GetDriverPickUpGeoFencesAsync(int driverId);
        GeoFence GetDriverCurrentGeoFence(List<GeoFence> driverGeoFences, DriverCurrentLocation driverCurrentLocation);
        Task<List<Branch>> GetDriverCurrentGeoFenceBranchesAsync(GeoFence driverCurrentGeoFence);

        Task<List<GeoFence>> GetDriverDeliveryGeoFencesAsync(int driverId);
        Task<DriverWithTasksViewModel> GetReachedDriverWithTasks(int driverId);
        Task<DriverWithTasksViewModel> GetDriver(GetDriverInput input);
        Task<List<DriverWithTasksViewModel>> GetDrivers(GetDriversInput input);
        Task<List<Driver>> GetDriversWithExpireCluppingTime();
        Task UpdateDriverClubbingTime(UpdateDriverClubbingTimeViewModel viewModel);
        void Update(Driver driver);

        Task<List<int>> GetDriverIdsByGeofenceIdAsync(int geofenceId);

        Task<List<int>> GetManagerDriversAsync(IEnumerable<int> teamIds);
    }
}
