﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IDriverTaskNotificationManager : IBaseManager<DriverTaskNotificationViewModel, DriverTaskNotification>
    {
        Task MarkAsRead(int driverId);
        int GetUnReadCount(int driverId);
        Task<PagedResult<TaskNotificationViewModel>> GetByPaginationAsync(PaginatedTasksDriverViewModel pagingparametermodel);
        Task Clear(int driverId);
    }
}
