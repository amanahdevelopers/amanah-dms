﻿using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IAgentTypeManager : IBaseManager<AgentTypeViewModel, AgentType>
    {
        Task<AgentTypeViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);  
    }
}
