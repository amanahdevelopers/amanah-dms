﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.DATA.Helpers;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.BLL.Managers
{
    public class CustomersManager : BaseManager<CustomerViewModel, Customer>, ICustomersManager
    {


        public CustomersManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Customer> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<CustomerViewModel> Get(int id)
        {
            return await (context as ApplicationDbContext).Customers
                .Where(x => x.Id == id)
                .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public Task<List<CustomerViewModel>> GetByNameAsync(string name)
        {
            return (context as ApplicationDbContext).Customers
                .Where(x =>
                    x.BranchId == null &&
                    x.Name.ToLower().Contains(name.ToLower()))
                .Take(10)
                .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }


        public async Task<List<CustomerViewModel>> GetByPhoneAsync(string phone)
        {
            return await (context as ApplicationDbContext).Customers
                .Where(x =>
                    x.BranchId == null &&
                    x.Phone.ToLower().Contains(phone.ToLower()))
                .Take(10)
                .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }



        public async Task<bool> IsEmailExistAsync(string email)
        {
            return await (context as ApplicationDbContext).Customers.AnyAsync(x => x.Email == email);
        }

        public async Task<bool> IsPhoneExistAsync(string phone)
        {
            return await (context as ApplicationDbContext).Customers.AnyAsync(x => x.Phone == phone);
        }

        public override Task<List<CustomerViewModel>> GetAllAsync<CustomerViewModel>()
        {
            return repository.GetAll()
                .Where(x => x.BranchId == null)
                .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<PagedResult<CustomerViewModel>> GetAllCustomersByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var source = repository.GetAll().Where(x => x.BranchId == null);
            if (pagingparametermodel.SearchBy != null)
            {
                source = source.Where(x =>
                    x.Name.Contains(pagingparametermodel.SearchBy) ||
                    x.Email.Contains(pagingparametermodel.SearchBy) ||
                    x.Phone.Contains(pagingparametermodel.SearchBy) ||
                    x.Address.Contains(pagingparametermodel.SearchBy));
            }
            var pagedResult = source.ToPagedResultAsync<Customer, CustomerViewModel>(
                pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }

        public override Task<PagedResult<CustomerViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = repository
                .GetAll()
                .Where(x => x.BranchId == null)
                .ToPagedResultAsync<Customer, CustomerViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }

        public Task<List<CustomerExportToExcelViewModel>> ExportExcel()
        {
            var query = (context as ApplicationDbContext).Customers
                .Include(t => t.Country)
                .Where(x => x.BranchId == null)
                .Select(t => new CustomerExportToExcelViewModel
                {
                    Name = t.Name,
                    Email = t.Email,
                    Phone = t.Phone,
                    Address = t.Address,
                    Latitude = t.Latitude,
                    Longitude = t.Longitude,
                    Tags = t.Tags,
                    Country = t.Country.Name ?? null,

                });

            return query.ToListAsync();
        }

    }
}
