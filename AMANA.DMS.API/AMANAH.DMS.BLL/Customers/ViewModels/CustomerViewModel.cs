﻿using System.ComponentModel.DataAnnotations;

namespace AMANAH.DMS.BLL.ViewModels
{
    public class CustomerViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        [Required]
        public double? Latitude { set; get; }
        [Required]
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        public CountryViewModel Country { get; set; }
        public int? BranchId { set; get; }
        public AddressViewModel Location { set; get; }

    }


    public class ImportCustomerViewModel : CustomerViewModel
    {
        public string Error { get; set; }
    }
}
