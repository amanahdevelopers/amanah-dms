﻿using AMANAH.DMS.BLL.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IImportCustomerExcelManager
    {
        Task<ProcessResult<List<ImportCustomerViewModel>>> AddFromExcelSheetAsync(string path, UploadFile Uploadfile);
    }
}

