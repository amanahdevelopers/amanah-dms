﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;
namespace AMANAH.DMS.BLL.IManagers
{
    public interface ICustomersManager :IBaseManager<CustomerViewModel, Customer>
    {
        Task<CustomerViewModel> Get(int id);
        Task<List<CustomerViewModel>> GetByNameAsync(string name);
        Task<List<CustomerViewModel>> GetByPhoneAsync(string phone);
        Task<bool> IsEmailExistAsync(string email);
        Task<bool> IsPhoneExistAsync(string phone);        
        Task<PagedResult<CustomerViewModel>> GetAllCustomersByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<CustomerExportToExcelViewModel>> ExportExcel();

    }
}
