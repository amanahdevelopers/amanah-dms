﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class SettingsManager : BaseManager<SettingViewModel, Setting>, ISettingsManager
    {
        private readonly ICurrentUser _currentUser;

        public SettingsManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Setting> repository,
            ICurrentUser currentUser)
            : base(context, repository, mapper)
        {
            _currentUser = currentUser;
        }

        public async Task<List<SettingViewModel>> GetSettingByGroupId(string GroupId)
        {
            ///Return Setting by Key for specific tenant 
            await AddUserDefaultSettingsAsync(_currentUser.Id);
            return await (context as ApplicationDbContext).Settings
                .Where(x => x.GroupId == GroupId)
                .ProjectTo<SettingViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        /// <summary>
        /// Get Setting by key for current Tenant that come from token 
        /// </summary>
        /// <param name="settingkey">Setting Key </param>
        /// <returns>Setting Object </returns>
        public async Task<SettingViewModel> GetSettingByKey(string settingkey)
        {
            return await (context as ApplicationDbContext).Settings
                .Where(x => x.SettingKey.Trim().ToLower() == settingkey.Trim().ToLower())
                .ProjectTo<SettingViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<SettingViewModel> Get(int id)
        {
            return await (context as ApplicationDbContext).Settings
                .Where(x => x.Id == id)
                .ProjectTo<SettingViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }


        public async Task<bool> AddUserDefaultSettingsAsync(string userId)
        {
            var resourceSet = Settings.Resources.SettingsKeys.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            if (resourceSet == null)
            {
                return false;
            }
            var settings = await CreateSettingList(userId, resourceSet);
            if (settings == null || !settings.Any())
            {
                return false;
            }
            await (context as ApplicationDbContext).Settings.AddRangeAsync(settings);
            return (await (context as ApplicationDbContext).SaveChangesAsync()) > 0;

        }

        private async Task<List<Setting>> CreateSettingList(string userId, ResourceSet resourceSet)
        {
            var oldSettings = await (context as ApplicationDbContext).Settings
                .Where(e => e.Tenant_Id == userId && !e.IsDeleted)
                .ToListAsync();
            var settings = new List<Setting>();
            foreach (DictionaryEntry entry in resourceSet)
            {
                var isExist = oldSettings.Any(e => e.SettingKey.ToLower().Trim() == entry.Key.ToString().ToLower().Trim());
                if (isExist)
                {
                    continue;
                }
                settings.Add(CreateSetting(userId, entry.Key.ToString()));
            }
            return settings;
        }

        private Setting CreateSetting(string userId, string settingKey)
        {
            string dataType = "string";
            string value = "";
            string group = ((int)SettingGroupEnum.AutoAllocation).ToString();

            switch (settingKey)
            {
                case "IsEnableAutoAllocation":
                    value = DefaultSettingValue.IsEnableAutoAllocation.ToString();
                    dataType = "boolean";
                    group = ((int)SettingGroupEnum.AutoAllocation).ToString();
                    break;
                case "ReachedDistanceRestriction":
                    value = DefaultSettingValue.ReachedDistanceRestrictionInMeter.ToString();
                    group = ((int)SettingGroupEnum.General).ToString();
                    break;
                case "DriverLoginRequestExpiration":
                    value = DefaultSettingValue.DriverLoginRequestExpirationInHour.ToString();
                    group = ((int)SettingGroupEnum.General).ToString();
                    break;
            }

            return new Setting(tenantId: userId)
            {
                GroupId = group,
                SettingDataType = dataType,
                SettingKey = settingKey,
                Value = value,
            };
        }

        public async Task<int> GetAutoAllocationTypeidAsync()
        {
            var autoallocationSetting = await GetSettingByKey("SelectedAutoAllocationMethodName");
            var enableAutoallocationSetting = await GetSettingByKey("IsEnableAutoAllocation");
            int autoAllocationType = enableAutoallocationSetting == null
                ? (int)AutoAllocationTypeEnum.Manual
                : enableAutoallocationSetting.Value.ToLower() == "false"
                    ? (int)AutoAllocationTypeEnum.Manual
                    : autoallocationSetting == null
                        ? (int)AutoAllocationTypeEnum.Manual
                        : autoallocationSetting.Value == "FirstInFirstOutMethod"
                            ? (int)AutoAllocationTypeEnum.Fifo
                            : autoallocationSetting.Value == "NearestAvailableMethod"
                                ? (int)AutoAllocationTypeEnum.Nearest
                                : autoallocationSetting.Value == "OneByOneMethod"
                                    ? (int)AutoAllocationTypeEnum.OneByOne
                                    : (int)AutoAllocationTypeEnum.Manual;

            return autoAllocationType;
        }
    }
}
