﻿using AMANAH.DMS.BLL.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMANAH.DMS.ViewModels
{
    public class SettingViewModel
    {
        public int Id { set; get; }
        public string SettingKey { set; get; }
        public string Value { set; get; }
        public string SettingDataType { set; get; }
        public string GroupId { set; get; }
    }
    
}
