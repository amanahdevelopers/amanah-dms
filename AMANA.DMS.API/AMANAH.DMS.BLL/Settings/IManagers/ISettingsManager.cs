﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.ViewModels;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface ISettingsManager : IBaseManager<SettingViewModel, Setting>
    {
        Task<List<SettingViewModel>> GetSettingByGroupId(string key);
        Task<SettingViewModel> GetSettingByKey(string key);
        Task<SettingViewModel> Get(int id);
        Task<bool> AddUserDefaultSettingsAsync(string userId);
        Task<int> GetAutoAllocationTypeidAsync();
    }
}
