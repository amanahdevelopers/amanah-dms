﻿namespace AMANAH.DMS.BLL.Constants
{
    public class DefaultSettingValue
    {
        public static double ReachedDistanceRestrictionInMeter = 300;
        public static int DriverLoginRequestExpirationInHour = 24;

        public static bool IsEnableAutoAllocation = false;

        public static int FirstInFirstOutClubbingTimeInSec = 60;
        public static int FirstInFirstOutMethodDriverOrderCapacity =3;

        public static double NearestAvailableMethodRadiusInKM = 100;
        public static int NearestAvailableMethodNumberOfRetries = 10;
        public static int NearestAvailableMethodReScheduleRequestTimeInSec = 10;

    }
}
