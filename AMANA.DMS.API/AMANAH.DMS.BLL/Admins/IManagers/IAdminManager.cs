﻿using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Admin;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using System.Threading.Tasks;

namespace AMANAH.DMS.BLL.IManagers
{
    public interface IAdminManager : IBaseManager<AdminViewModel, Admin>
    {
        Task<AdminViewModel> GetAsync(string id);
        Task<bool> UpdateProfileAsync(AdminProfileViewModel adminProfileViewModel);
        Task<AdminLanguageViewModel> UpdateLanguageAsync(AdminLanguageViewModel adminLanguageViewModel,string userid);
        Task<bool> SetDeactivationReason(string UserId, string reason);
        Task<AdminDefaultBranchViewModel> UpdateDefaultBranchAsync(AdminDefaultBranchViewModel DefaultbranchViewModel, string userid);
    }
}
