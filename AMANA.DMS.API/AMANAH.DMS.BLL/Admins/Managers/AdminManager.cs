﻿using System.Threading.Tasks;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels.Admin;
using AMANAH.DMS.Context;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Repoistry;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AMANAH.DMS.BLL.Managers
{
    public class AdminManager : BaseManager<AdminViewModel, Admin>, IAdminManager
    {
        public AdminManager(ApplicationDbContext context, IRepositry<Admin> repository, IMapper mapper)
            : base(context, repository, mapper)
        {
        }

        public async Task<AdminViewModel> GetAsync(string id)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.FirstOrDefaultAsync(x => x.Id == id);
            return mapper.Map<Admin, AdminViewModel>(admin);
        }

        public async Task<AdminLanguageViewModel> UpdateLanguageAsync(AdminLanguageViewModel languageViewModel, string userid)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.FirstOrDefaultAsync(x => x.Id == userid);
            if (admin == null)
                return null;
            admin.DashBoardLanguage = languageViewModel.DashBoardLanguage;
            admin.TrackingPanelLanguage = languageViewModel.TrackingPanelLanguage;
            myContext.Admins.Update(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return languageViewModel;
            return null;
        }

        public async Task<AdminDefaultBranchViewModel> UpdateDefaultBranchAsync(AdminDefaultBranchViewModel DefaultbranchViewModel, string userid)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.FirstOrDefaultAsync(x => x.Id == userid);
            if (admin == null)
                return null;
            admin.BranchId = DefaultbranchViewModel.DefaultBranchId;
            myContext.Admins.Update(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return DefaultbranchViewModel;
            return null;
        }

        public async Task<bool> UpdateProfileAsync(AdminProfileViewModel adminProfileViewModel)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.FirstOrDefaultAsync(x => x.Id == adminProfileViewModel.Id);
            admin.CompanyAddress = adminProfileViewModel.CompanyAddress;
            admin.CompanyName = adminProfileViewModel.CompanyName;
            admin.DisplayImage = adminProfileViewModel.DisplayImage;
            myContext.Admins.Update(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return true;
            return false;
        }

        public async Task<bool> SetDeactivationReason(string UserId, string reason)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.FirstOrDefaultAsync(x => x.Id == UserId);
            admin.DeactivationReason = reason;
            myContext.Admins.Remove(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return true;
            return false;
        }

    }
}
