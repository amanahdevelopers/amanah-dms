﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Admin
{
    public class AccountToDeleteViewModel
    {
        public string  deactivationReason { get; set; }
        public string password { get; set; }
    }
}
