﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Admin
{
    public class AdminFullProfile
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public string Id { get; set; }
        public string Email { get; set; }
        public string phone { get; set; }
        // Country Code for Phone
        public int CountryId { set; get; }
        // Country Residency for user
        public int ResidentCountryId { set; get; }
        public string FirstName { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public int? BranchId { set; get; }

    }

    public class AdminViewModel
    {
        public string Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public string DeactivationReason { get; set; }
        public int ResidentCountryId { get; set; }
        public int? BranchId { set; get; }

        public virtual string CreatedBy_Id { get; set; }
        public virtual string UpdatedBy_Id { get; set; }
        public virtual string DeletedBy_Id { get; set; }
        public string Tenant_Id { set; get; }
        public virtual bool IsDeleted { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime UpdatedDate { get; set; }
        public virtual DateTime DeletedDate { get; set; }





    }
    public class AdminProfileViewModel
    {
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public int ResidentCountryId { set; get; }
        public string Id { get; set; }
  
    }

    public class UserInfoViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string phone { get; set; }
        public int CountryId { set; get; }
        public int ResidentCountryId { set; get; }
        public string FirstName { get; set; }
    }
}
