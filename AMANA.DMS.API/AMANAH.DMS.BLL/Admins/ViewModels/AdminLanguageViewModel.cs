﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AMANAH.DMS.BLL.ViewModels.Admin
{
    public class AdminLanguageViewModel
    {
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; } 
    }
}
