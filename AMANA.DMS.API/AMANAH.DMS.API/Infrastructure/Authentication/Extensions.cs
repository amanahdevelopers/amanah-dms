﻿using AMANAH.DMS.BASE.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace AMANAH.DMS.API.Infrastructure.Authentication
{
    public static class Extensions
    {
        public static IServiceCollection AddCurrentUser(this IServiceCollection services)
        {
            services.AddSingleton<CurrentUser>();
            services.AddSingleton<ICurrentUser>(ServiceProvider => ServiceProvider.GetRequiredService<CurrentUser>());
            services.AddSingleton<ICurrentUserDetailsStore>(ServiceProvider => ServiceProvider.GetRequiredService<CurrentUser>());
            return services;
        }
    }
}
