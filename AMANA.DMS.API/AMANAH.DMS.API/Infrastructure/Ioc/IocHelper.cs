﻿using System;
using System.Linq;
using AMANAH.DMS.BLL.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace AMANAH.DMS.API.Infrastructure.Ioc
{
    internal static class IocHelper
    {
        public static void RegisterConventinos(IServiceCollection services)
        {
            var assemblies = new[]
            {
                typeof(SettingsManager).Assembly,
                typeof(Startup).Assembly
            };
            var types = assemblies.SelectMany(assembly => assembly.GetTypes())
                .Where(IsConcrete)
                .ToArray();
            foreach (var implementation in types)
            {
                if (HasDefaultConvention(implementation, out var serviceType))
                {
                    services.AddScoped(serviceType, implementation);
                }
            }

        }
        private static bool HasDefaultConvention(Type implementationType, out Type serviceType)
        {
            serviceType = null;
            var hasSuffix = HasSuffix(implementationType);
            var hasInterfaceWithSuffix = implementationType.GetInterfaces()
                .Any(HasSuffix);
            if (hasSuffix && hasInterfaceWithSuffix)
            {
                serviceType = implementationType
                    .GetInterfaces()
                    .First(HasSuffix);
                return true;
            }
            return false;
        }

        private static bool HasSuffix(Type type)
        {
            var suffixes = new[]
            {
                "Manager",
                "Service",
                //"Repository",
                // "Job"
            };
            return suffixes.Any(suffix => type.Name.EndsWith(suffix));
        }

        public static bool IsConcrete(Type type)
        {
            return type.IsClass && !type.IsAbstract && !type.ContainsGenericParameters;
        }
    }
}