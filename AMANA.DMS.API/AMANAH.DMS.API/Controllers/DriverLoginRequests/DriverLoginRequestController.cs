﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.ExternalServcies;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.ViewModels;
using DocumentFormat.OpenXml.Office2010.ExcelAc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DriverLoginRequestController : Controller
    {
        private readonly IDriverLoginRequestManager _driverLoginRequestManager;
        private readonly IDriverManager _driverManager;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ITeamsManager _teamsManager;
        private readonly ILocalizer _localizer;
        private readonly IPushNotificationService _pushNotificationService;
        private readonly ICurrentUser _currentUser;
        private readonly ISettingsManager _settingManager;

        public DriverLoginRequestController(

            IUserDeviceManager userDeviceManager,
            IDriverLoginRequestManager driverLoginRequestManager,
            IDriverManager driverManager,
            IAccountLogManager accountLogManager,
            ITeamsManager teamsManager,
            ISettingsManager settingManager,
            ILocalizer localizer,
            IPushNotificationService pushNotificationService,
            ICurrentUser currentUser)
        {
            _driverLoginRequestManager = driverLoginRequestManager;
            _driverManager = driverManager;
            _userDeviceManager = userDeviceManager;
            _teamsManager = teamsManager;
            _accountLogManager = accountLogManager;
            _settingManager = settingManager;
            _localizer = localizer;
            _pushNotificationService = pushNotificationService;
            _currentUser = currentUser;
        }

        [Route("[action]")]
        [HttpGet]
        [Authorize(ManagerPermissions.Agent.ViewDriversLoginRequests)]
        public async Task<ActionResult> GetAll()
        {
            if (_currentUser.UserType == RoleType.Manager && _currentUser.HasPermission(ManagerPermissions.Team.ReadTeam))
            {
                var alldrivers = await _driverLoginRequestManager.GetAllAsync<DriverLoginRequestViewModel>();
                return Ok(alldrivers);
            }

            if (_currentUser.UserType == RoleType.Manager && _currentUser.HasPermission(ManagerPermissions.Team.ReadMyTeam))
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                var managerDriverIds = await _driverManager.GetManagerDriversAsync(managerTeamIds);
                if (managerDriverIds == null || !managerDriverIds.Any())
                {
                    return Ok();
                }
                var ManagerLoginDrivers = await _driverLoginRequestManager.GetAllAsync<DriverLoginRequestViewModel>(managerDriverIds);
                return Ok(ManagerLoginDrivers);
            }

            var data = await _driverLoginRequestManager.GetAllAsync<DriverLoginRequestViewModel>();
            return Ok(data);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        [Authorize(ManagerPermissions.Agent.ViewDriversLoginRequests)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] DriverLoginRequestSearchViewModel pagingparametermodel)
        {
            ///Get All Managers By Pagination 
            if (_currentUser.UserType == RoleType.Manager && _currentUser.HasPermission(ManagerPermissions.Team.ReadTeam))
            {
                var alldrivers = await _driverLoginRequestManager.GetAllByPaginationAsync(pagingparametermodel);
                alldrivers.Result=await CheckExpiredRequestsAsync(alldrivers.Result);
                return Ok(alldrivers);
            }

            if (_currentUser.UserType == RoleType.Manager && _currentUser.HasPermission(ManagerPermissions.Team.ReadMyTeam))
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                var managerDriverIds = await _driverManager.GetManagerDriversAsync(managerTeamIds);
                if (managerDriverIds == null || !managerDriverIds.Any())
                {
                    return Ok();
                }
                var ManagerLoginDrivers = await _driverLoginRequestManager.GetAllByPaginationAsync(pagingparametermodel, managerDriverIds);
                ManagerLoginDrivers.Result = await CheckExpiredRequestsAsync(ManagerLoginDrivers.Result);        
                return Ok(ManagerLoginDrivers);
            }

            var AllManagers = await _driverLoginRequestManager.GetAllByPaginationAsync(pagingparametermodel,null);
            AllManagers.Result = await CheckExpiredRequestsAsync(AllManagers.Result);
            return Ok(AllManagers);
        }

        
        private async Task<List<DriverLoginRequestViewModel>> CheckExpiredRequestsAsync(List<DriverLoginRequestViewModel> driverLoginRequests)
        {
            foreach (var driverLoginRequest in driverLoginRequests)
            {
                int expireHour = 24;
                var expireHourSetting = await _settingManager.GetSettingByKey("DriverLoginRequestExpiration");
                if (expireHourSetting != null)
                {
                    int.TryParse(expireHourSetting.Value, out expireHour);
                }
                driverLoginRequest.IsExpired = driverLoginRequest.CreatedDate.AddHours(expireHour) <= DateTime.UtcNow;
            }
            return driverLoginRequests;
        }



        [HttpGet("[action]")]
        public async Task<IActionResult> ChangeStatus(int id, DriverLoginRequestStatus status, string reason)
        {
            var driverLoginRequest = await _driverLoginRequestManager.Get(id);

            if (driverLoginRequest == null)
            {
                return Ok(false);
            }

            if (status == DriverLoginRequestStatus.Approved && (byte)driverLoginRequest.Status == (byte)DriverLoginRequestStatus.Approved)
            {
                int expireHour = 24;
                var expireHourSetting = await _settingManager.GetSettingByKey("DriverLoginRequestExpiration");
                if (expireHourSetting != null)
                {
                    int.TryParse(expireHourSetting.Value, out expireHour);
                }

                if (driverLoginRequest.UpdatedDate.AddHours(expireHour).Date > DateTime.UtcNow)
                {
                    return Ok(false);
                }
            }

            driverLoginRequest.Status = (byte)status;
            driverLoginRequest.ApprovedOrRejectBy = _currentUser.UserName;
            driverLoginRequest.UpdatedDate = DateTime.UtcNow;
            driverLoginRequest.Reason = reason;
            var result = await _driverLoginRequestManager.UpdateAsync(driverLoginRequest);

            var Driver = await _driverManager.Get(driverLoginRequest.DriverId);
            if (status == DriverLoginRequestStatus.Approved)
            {
                await _pushNotificationService.SendAsync(
                    Driver.UserId,
                    "D-Hub",
                    _localizer[Keys.Notifications.AdminApprovedLoginRequest],
                    new { isAdminApproved = true });
            }
            else if (status == DriverLoginRequestStatus.Rejected)
            {
                await _pushNotificationService.SendAsync(
                    Driver.UserId,
                    "D-Hub",
                   _localizer[Keys.Notifications.AdminRejectedLoginRequest],
                    new { isAdminApproved = false });
                var deviceIds = _userDeviceManager.GetByUserId(Driver.UserId);
                if (deviceIds != null)
                {
                    foreach (var device in deviceIds)
                    {
                        _userDeviceManager.DeleteDevice(device.FCMDeviceId);
                    }
                }
            }
            return Ok(result);
        }


        // GET: Driver/Delete/5
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var driverToDelete = await _driverLoginRequestManager.Get(id);

            var result = await _driverLoginRequestManager.SoftDeleteAsync(driverToDelete);
            // TODO: Add insert logic here

            if (result)
            {

                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "DriverLoginRequest",
                    ActivityType = "Delete",
                    Description = driverToDelete.DriverName + " login request " + " ( " + driverToDelete.Id + " ) has been deleted ",
                    Record_Id = driverToDelete.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);

                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteDriverLoginRequest]);
            }
        }
    }
}
