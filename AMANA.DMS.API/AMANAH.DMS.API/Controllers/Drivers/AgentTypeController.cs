﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AgentTypeController : Controller
    {
        private readonly ILocalizer _localizer;

        private IAgentTypeManager _agentTypeManager { set; get; }

        public AgentTypeController(
            IAgentTypeManager agentTypeManager,
            ILocalizer localizer)
        {
            _agentTypeManager = agentTypeManager;
            _localizer = localizer;
        }


        // GET: AgentType
        [Route("GetAll")]
        [HttpGet]
        //  [AllowAnonymous]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllAgentTypes 
            var AllAgentTypes = await _agentTypeManager.GetAllAsync<AgentTypeViewModel>(true);
            return Ok(AllAgentTypes);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Teams By Pagination 
            var AllTeams = await _agentTypeManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }


        // GET: AgentType/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var agentType = await _agentTypeManager.Get(id);
            return Ok(agentType);
        }


        // POST: Teams/AgentType
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] AgentTypeViewModel agentTypeVM)
        {
            var created_agentType = await _agentTypeManager.AddAsync(agentTypeVM);
            // TODO: Add insert logic here
            return Ok(created_agentType);
        }

        [HttpPut]
        public async Task<ActionResult> Update(AgentTypeViewModel agentTypeVM)
        {
            // TODO: Add update logic here
            var updated_agentType = await _agentTypeManager.UpdateAsync(agentTypeVM);
            return Ok(updated_agentType);
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var agentTypeToDelete = await _agentTypeManager.Get(id);
            var result = await _agentTypeManager.SoftDeleteAsync(agentTypeToDelete);
            // TODO: Add insert logic here
            if (result)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDelteAgentType]);
            }
        }
    }
}