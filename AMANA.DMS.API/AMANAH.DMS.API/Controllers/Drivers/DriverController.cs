﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.ExternalServcies;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Utilites.ExcelToGenericList;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DriverController : Controller
    {

        private readonly IAccountLogManager _accountLogManager;
        private readonly IMapper _mapper;
        private readonly IApplicationUserManager _appUserManager;
        private readonly IDriverManager _driverManager;
        private readonly IUploadFileManager _uploadFileManager;
        private readonly ITasksManager _tasksManager;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly IConfiguration _configuration;
        private readonly IImportExcelManager _importExcelManager;
        private readonly IDriverloginTrackingManager _driverloginTrackingManager;
        private readonly ITeamsManager _teamsManager;
        private readonly ILocalizer _localizer;
        private readonly IApplicationUserManager _userManager;
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly INotificationManager _notificationManager;
        private readonly IPushNotificationService _pushNotificationService;
        private readonly ICurrentUser _currentUser;
        private readonly IManagerManager _managerManager;

        public DriverController(
            IDriverManager driverManager,
            IMapper mapper,
            IApplicationUserManager appUserManager,
            IUploadFileManager uploadFileManager,
            ITasksManager taskCommonManager,
            IUserDeviceManager userDeviceManager,
            IConfiguration configuration,
            IImportExcelManager importExcelManager,
            IAccountLogManager accountLogManager,
            INotificationManager notificationManager,
            IApplicationUserManager userManager,
            IHubContext<NotificationHub> notificationhubContext,
            IDriverloginTrackingManager driverloginTrackingManager,
            ITeamsManager teamsManager,
            ILocalizer localizer,
            IPushNotificationService pushNotificationService,
            ICurrentUser currentUser,
            IManagerManager managerManager)
        {
            _driverManager = driverManager;
            _mapper = mapper;
            _appUserManager = appUserManager;
            _uploadFileManager = uploadFileManager;
            _tasksManager = taskCommonManager;
            _userDeviceManager = userDeviceManager;
            _configuration = configuration;
            _importExcelManager = importExcelManager;
            _accountLogManager = accountLogManager;
            _notificationhubContext = notificationhubContext;
            _userManager = userManager;
            _driverloginTrackingManager = driverloginTrackingManager;
            _teamsManager = teamsManager;
            _localizer = localizer;
            _notificationManager = notificationManager;
            _pushNotificationService = pushNotificationService;
            _currentUser = currentUser;
            _managerManager = managerManager;
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
                var ManagerDrivers = await _driverManager.GetAllAsync(managerTeamIds);
                return Ok(ManagerDrivers);
            }
            var AllDrivers = await _driverManager.GetAllAsync<DriverViewModel>();
            return Ok(AllDrivers);
        }


        [Route("GetAllDriversForAssignment")]
        [HttpGet]
        public async Task<ActionResult> GetAssignDriversAsync()
        {
            var Drivers = await _driverManager.GetAssignDriversAsync();
            return Ok(Drivers);
        }

        [Route("GetTags")]
        [HttpGet]
        public async Task<ActionResult> GetTagsAsync()
        {
            var Drivers = await _driverManager.GetTagsAsync();
            return Ok(Drivers);
        }


        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] DriverSearchViewModel pagingparametermodel)
        {
            ///Get All Teams By Pagination 
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
                var ManagerDrivers = await _driverManager.GetAllDriverByPaginationAsync(pagingparametermodel, managerTeamIds);
                return Ok(ManagerDrivers);
            }
            var AllDrivers = await _driverManager.GetAllDriverByPaginationAsync(pagingparametermodel);
            return Ok(AllDrivers);
        }


        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var driver = await _driverManager.Get(id);
            return Ok(driver);
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("Create")]
        [Authorize(ManagerPermissions.Agent.CreateAgent)]
        public async Task<ActionResult> CreateAsync([FromBody] CreateDriverViewModel createDriverVM)
        {
            var existUser = await _appUserManager.GetByEmailAsync(createDriverVM.Email);
            if (existUser != null)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }
            createDriverVM.AgentStatusId = (int)AgentStatusesEnum.Offline;
            var driverToCreate = _mapper.Map<DriverViewModel>(createDriverVM);
            // Add User 
            var userManagerToCreate = _mapper.Map<ApplicationUserViewModel>(createDriverVM);
            var userToCreate = _mapper.Map<ApplicationUser>(userManagerToCreate);
            if (!string.IsNullOrEmpty(createDriverVM.RoleName) || !string.IsNullOrWhiteSpace(createDriverVM.RoleName))
            {
                userToCreate.RoleNames.Add(createDriverVM.RoleName);
            }
            ApplicationUser created_user = new ApplicationUser();
            var result = await _appUserManager.AddUserAsync(userToCreate, userManagerToCreate.Password);
            if (result == null || result.GetType() == typeof(string))
            {       //error
                return BadRequest(result);
            }
            else if (result.Succeeded) //Success 
            {
                //created_user = result.;
                driverToCreate.UserId = result.User.Id;
                if (createDriverVM.File != null)//Image Base64
                {
                    var path = "DriverImages";
                    UploadFile uploadFile = new UploadFile()
                    {
                        FileContent = createDriverVM.File,
                        FileName = "Image.jpg"
                    };

                    var processResult = _uploadFileManager.AddFile(uploadFile, path);
                    if (processResult.IsSucceeded)
                    {
                        driverToCreate.ImageUrl = processResult.returnData;
                    }
                    else
                    {
                        return BadRequest(processResult.Exception);
                    }
                }
                var created_driver = await _driverManager.AddAsync(driverToCreate);
                await _driverManager.ChangeDriverStatusByUserId(created_driver.UserId, (int)AgentStatusesEnum.Offline);

                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Create",
                    Description = driverToCreate.FirstName + " " + driverToCreate.LastName + " ( " + (await _driverManager.GetAllAsync<DriverViewModel>()).Max(x => x.Id) + " ) has been created ",
                    Record_Id = driverToCreate.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);

                return Ok(created_driver);
            }
            else
            {
                return BadRequest(result.Error);
            }
        }


        //POST: Driver/Edit/5
        [HttpPut, DisableRequestSizeLimit]
        [Authorize(ManagerPermissions.Agent.UpdateAgent)]
        public async Task<ActionResult> Update([FromBody] EditDriverViewModel updateDriverVM)
        {
            var existUser = await _appUserManager.GetByEmailAsync(updateDriverVM.Email);
            if (existUser != null && existUser.Id != updateDriverVM.UserId)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }
            var oldDriver = await _driverManager.GetByUserID(updateDriverVM.UserId);
            //Update User
            var userManagerToUpdate = _mapper.Map<ApplicationUserViewModel>(updateDriverVM);
            var userToUpdate = _mapper.Map<ApplicationUser>(userManagerToUpdate);
            userToUpdate.RoleNames.Add(updateDriverVM.RoleName);
            var result = await _appUserManager.UpdateUserAsync(userToUpdate);
            if (result)
            {
                //Update Driver
                var driverToUpdate = _mapper.Map<DriverViewModel>(updateDriverVM);
                if (updateDriverVM.File != null)//Image Base64
                {
                    var path = "DriverImages";
                    UploadFile uploadFile = new UploadFile()
                    {
                        FileContent = updateDriverVM.File,
                        FileName = "Image.jpg"
                    };
                    var processResult = _uploadFileManager.AddFile(uploadFile, path);
                    if (processResult.IsSucceeded)
                    {
                        driverToUpdate.ImageUrl = processResult.returnData;
                    }
                    else
                    {
                        return BadRequest(processResult.Exception);
                    }
                }
                await _driverManager.UpdateDriverGeoFenceAsync(driverToUpdate);
                driverToUpdate.AgentStatusId = oldDriver.AgentStatusId;
                var updated_driver = await _driverManager.UpdateAsync(driverToUpdate);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Update",
                    Description = driverToUpdate.FirstName + " " + driverToUpdate.LastName + " ( " + driverToUpdate.Id + " ) has been updated ",
                    Record_Id = driverToUpdate.Id
                };
                var created_log = await _accountLogManager.Create(accountLog);
                return Ok(updated_driver);
            }
            else
            {
                return BadRequest(result);
            }
        }

        // GET: Driver/Delete/5
        [HttpPost("BullkDelete")]
        [Authorize(ManagerPermissions.Agent.DeleteAllAgent)]
        public async Task<ActionResult> BullkDeleteAsync(List<DriverViewModel> DeletedDrivers)
        {
            var result = await _driverManager.SoftDeleteAsync(DeletedDrivers);
            // TODO: Add insert logic here
            if (result)
            {
                foreach (var item in DeletedDrivers)
                {
                    AccountLogs accountLog = new AccountLogs()
                    {
                        TableName = "Driver",
                        ActivityType = "Delete",
                        Description = item.FirstName + " " + item.LastName + " ( " + item.Id + " ) has been deleted ",
                        Record_Id = item.Id

                    };
                    var created_log = await _accountLogManager.Create(accountLog);
                }
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteDrivers]);
            }
        }


        // GET: Driver/Delete/5
        [HttpDelete("Delete/{id}")]
        [Authorize(ManagerPermissions.Agent.DeleteAgent)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var driverToDelete = await _driverManager.Get(id);
            var User = await _appUserManager.GetByUserIdAsync(driverToDelete.UserId);
            var deletedUser = await _appUserManager.SoftDeleteAsync(User);
            var result = await _driverManager.SoftDeleteAsync(driverToDelete);
            // TODO: Add insert logic here
            if (result)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Delete",
                    Description = driverToDelete.FullName + " ( " + driverToDelete.Id + " ) has been deleted ",
                    Record_Id = driverToDelete.Id
                };
                var created_log = await _accountLogManager.Create(accountLog);
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteDrivers]);
            }
        }



        [Route("ChangeDriversType")]
        [HttpPost]
        public async Task<IActionResult> ChangeDriversTypeAsync([FromBody] ChangeDriversTypeViewModel model)
        {
            await _driverManager.ChangeDriversTypeAsync(model);

            foreach (var item in model.DriverIds)
            {
                var driverToDelete = await _driverManager.Get(item);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Change Driver Type ",
                    Description = driverToDelete.FullName + " ( " + driverToDelete.Id + " ) type has been changed ",
                    Record_Id = driverToDelete.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);
            }

            return Ok();
        }

        [Route("BlockDrivers")]
        [HttpPut]
        public async Task<IActionResult> BlockDriversAsync([FromBody] BlockDriversViewModel model)
        {
            await _driverManager.BlockDriversAsync(model);
            foreach (var item in model.DriverIds)
            {
                var driverToBlock = await _driverManager.Get(item);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Block",
                    Description = driverToBlock.FirstName + " " + driverToBlock.LastName + " ( " + driverToBlock.Id + " )  has been blocked " + " - Block Reason: " + model.Reason,
                    Record_Id = driverToBlock.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);

                if (driverToBlock.AgentStatusId == (int)AgentStatusesEnum.Blocked)
                {
                    var deviceIds = _userDeviceManager.GetByUserId(driverToBlock.UserId);
                    var apiKey = Convert.ToString(_configuration.GetSection("FcmSettings").GetValue(typeof(String), "ApiKey"));
                    foreach (var device in deviceIds)
                    {

                        await _pushNotificationService.SendAsync(driverToBlock.UserId,
                              "D-Hub",
                              _localizer[Keys.Notifications.YourAccountBlocked],
                              new
                              {
                                  notificationType = NotificationTypeEnum.Blocked,
                                  IsLoggedOut = true
                              });

                        _userDeviceManager.DeleteDevice(device.FCMDeviceId);
                    }
                    await UpdateDriverLoginTracking(driverToBlock.Id);
                }
            }

            return Ok();
        }


        [Route("UnBlockDrivers")]
        [HttpPut]
        public async Task<IActionResult> UnBlockDriversAsync([FromBody] BlockDriversViewModel model)
        {
            await _driverManager.UnBlockDriversAsync(model);

            foreach (var item in model.DriverIds)
            {
                var driver = await _driverManager.Get(item);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "UnBlock",
                    Description = driver.FirstName + " " + driver.LastName + " ( " + driver.Id + " )  has been unblocked " + " - Unblock Reason: " + model.Reason,
                    Record_Id = driver.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);
            }

            return Ok();
        }

        [Route("BlockDriver")]
        [HttpPut]
        public async Task<IActionResult> BlockDriverAsync(int id, string reason)
        {
            var driver = await _driverManager.Get(id);
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.NoDriverWithThisId]);
            }
            driver.AgentStatusId = (int)AgentStatusesEnum.Blocked;
            driver.Reason = reason;
            await _driverManager.UpdateAsync(driver);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Driver",
                ActivityType = "Block",
                Description = driver.FirstName + " " + driver.LastName + " ( " + driver.Id + " )  has been blocked " + " - Block Reason: " + reason,
                Record_Id = driver.Id

            };
            var created_log = await _accountLogManager.Create(accountLog);
            return Ok();
        }

        [Route("UnBlockDriver")]
        [HttpPut]
        public async Task<IActionResult> UnBlockDriverAsync(int id, string reason)
        {
            var driver = await _driverManager.Get(id);
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.NoDriverWithThisId]);
            }

            driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
            driver.Reason = reason;

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Driver",
                ActivityType = "UnBlock",
                Description = driver.FirstName + " " + driver.LastName + " ( " + driver.Id + " )  has been unblocked " + " - Unblocked Reason: " + reason,
                Record_Id = driver.Id

            };
            var created_log = await _accountLogManager.Create(accountLog);

            await _driverManager.UpdateAsync(driver);
            return Ok();
        }

        [Route("SetOnDuty")]
        [HttpPost]
        public async Task<IActionResult> SetOnDuty([FromBody] ChangeDriverDutyViewModel model)
        {
            int statusId = model.IsAvailable ? (int)AgentStatusesEnum.Available : (int)AgentStatusesEnum.Unavailable;
            var res = await _driverManager.ChangeDriverStatusByUserId(model.DriverId, statusId);

            var message = model.IsAvailable
                ? _localizer[Keys.Notifications.AdminPutYouOnDuty]
                : _localizer[Keys.Notifications.AdminPutYouOffDuty];
            await _pushNotificationService.SendAsync(
                model.DriverId,
                "D-Hub",
                message,
                new
                {
                    notificationType = model.IsAvailable
                        ? NotificationTypeEnum.UpdateDriverDutyToOn
                        : NotificationTypeEnum.UpdateDriverDutyToOff
                });

            return Ok(res);
        }



        // GET: Driver/Delete/5
        [HttpPost("FilterDriver")]
        public async Task<ActionResult> TestFilterAvailbleDrivers([FromBody] FilterDriverViewModel filterDriverViewModel)
        {
            var driverToDelete = await _tasksManager.GetAvailableDrivers(filterDriverViewModel.TeamIds, filterDriverViewModel.Tags, filterDriverViewModel.GeoFenceIds);
            // TODO: Add insert logic here

            if (driverToDelete != null)
            {
                return Ok(driverToDelete);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.NoAvailableDrivers]);
            }
        }


        [Route("ForceLogout/{driverId}")]
        [HttpGet]
        public async Task<IActionResult> ForceLogout(int driverId)
        {
            var driver = await _driverManager.Get(driverId);
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.DriverNotFound]);
            }


            await _pushNotificationService.SendAsync(
                      driver.UserId,
                      "D-Hub",
                      _localizer[Keys.Notifications.AdminEndedYourSession],
                      new { IsLoggedOut = true });

            var deviceIds = _userDeviceManager.GetByUserId(driver.UserId);
            foreach (var device in deviceIds)
            {
                _userDeviceManager.DeleteDevice(device.FCMDeviceId);
            }

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Driver",
                ActivityType = "ForceLogout",
                Description = driver.FirstName + " " + driver.LastName + " ( " + driver.Id + " )  has been ForceLogout ",
                Record_Id = driver.Id

            };
            var created_log = await _accountLogManager.Create(accountLog);
            await UpdateDriverLoginTracking(driver.Id);

            await _driverManager.ChangeDriverStatusByUserId(driver.UserId, (int)AgentStatusesEnum.Offline);

            var driveruser = await _userManager.GetByUserNameOrEmailAsync(driver.Email);
            driver = await _driverManager.Get(driverId);
            await _notificationhubContext.Clients.Group(driveruser.Tenant_Id).SendAsync("DriverLoggedOut", driver);
            var title = _localizer[Keys.Notifications.DriverHasBeenLoggedOut];
            var body = _localizer.Format(Keys.Notifications.DriverNameHasBeenLoggedOut, driver.FullName);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driver.UserId,
                ToUserId = driveruser.Tenant_Id,
                NotificationType = NotificationTypeWeb.DriverLoggedin
            });

            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driver.UserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.DriverLoggedin
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("DriverLoggedOut", driver);
            }

            return Ok(true);
        }

        private async Task UpdateDriverLoginTracking(int driverId)
        {
            var driverLoggedIn = await _driverloginTrackingManager.GetLoggedIn(driverId);
            if (driverLoggedIn != null)
            {
                driverLoggedIn.LogoutDate = DateTime.UtcNow;
                driverLoggedIn.LogoutActionBy = _currentUser.UserName;
                await _driverloginTrackingManager.UpdateAsync(driverLoggedIn);
            }
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("AddFromExcelSheet")]
        public async Task<IActionResult> AddFromExcelSheetAsync([FromBody] UploadFile file)
        {
            var uploadpath = "DriverSheet";
            var result = await _importExcelManager.AddFromExcelSheetAsync(uploadpath, file);
            var FaildRows = result.returnData.Where(x => x.Error != null || x.Error == "").ToList();
            if (FaildRows.Any())
            {
                string fileName = "Faild" + file.FileName + ".csv";
                var faildPath = "FaildDriverSheet";
                string filePath = Path.Combine(faildPath, fileName);
                var resultPath = FaildRows.ToCSV<ImportDriverViewModel>(filePath);
                byte[] fileBytes = System.IO.File.ReadAllBytes(resultPath);
                return File(fileBytes, "application/force-download", fileName);
            }

            return Ok();
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel()
        {
            var data = await _driverManager.ExportExcel();
            var path = "ImportTemplates";
            var fileName = "Drivers.xlsx";
            string filePath = Path.Combine(path, fileName);

            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpPost]
        [Route("GetDriverLoginTracking")]
        public async Task<ActionResult> GetDriverLoginTracking(PaginatedDriverLoginTrackingViewModel paginatedDriverLoginTrackingViewModel)
        {
            var result = await _driverloginTrackingManager.GetByPaginationAsync(paginatedDriverLoginTrackingViewModel);
            return Ok(result);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> ExportDriverLoginTrackingToExcel(PaginatedDriverLoginTrackingViewModel paginatedDriverLoginTrackingViewModel)
        {
            var data = await _driverloginTrackingManager.ExportToExcel(paginatedDriverLoginTrackingViewModel);
            var path = "ImportTemplates";
            var fileName = "Driverslogoutactions.xlsx";
            string filePath = Path.Combine(path, fileName);

            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }
    }
}