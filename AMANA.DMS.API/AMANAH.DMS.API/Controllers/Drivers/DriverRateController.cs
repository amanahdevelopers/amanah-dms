﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.VALIDATOR;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace AMANAH.DMS.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class DriverRateController : Controller
    {
        public readonly IMapper _mapper;
        public readonly IDriverRateManager _DriverRateManager;


        public DriverRateController(
            IMapper mapper,
            IDriverRateManager DriverRateManager)
        {
            _mapper = mapper;
            _DriverRateManager = DriverRateManager;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var data = await _DriverRateManager.GetAllAsync<DriverRateViewModel>();
            return Ok(data);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All DriverRatees By Pagination 
            var DriverRatees = await _DriverRateManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(DriverRatees);
        }



        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            var data = await _DriverRateManager.Get<DriverRateViewModel>(id);
            return Ok(data);
        }

    
        [Route("[action]/{id}")]
        [HttpGet]
        public async Task<ActionResult> Details([FromRoute] int id)
        {
            var driver = await _DriverRateManager.Get(id);
            return Ok(driver);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> Create([FromBody] CreateDriverRateViewModel input)
        {
            var mappedInput = _mapper.Map<DriverRateViewModel>(input);
            var validator = new DriverRateViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);


            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                return Ok(await _DriverRateManager.AddAsync(mappedInput));
            }
        }

        [HttpPut]
        [Route("[action]")]
        public async Task<ActionResult> Update([FromBody] CreateDriverRateViewModel input)
        {
            var mappedInput = _mapper.Map<DriverRateViewModel>(input);
            var validator = new DriverRateViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                return Ok(await _DriverRateManager.UpdateAsync(mappedInput));
            }
        }

        [HttpDelete]
        [Route("[action]/{id}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            return Ok(await _DriverRateManager.DeleteAsync(id));
        }
    }
}