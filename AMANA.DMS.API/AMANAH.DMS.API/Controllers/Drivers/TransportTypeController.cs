﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TransportTypeController : Controller
    {
        private readonly ITransportTypeManager _transportTypeManager;
        private readonly ILocalizer _localizer;

        public TransportTypeController(ITransportTypeManager transportTypeManager, ILocalizer localizer)
        {
            _transportTypeManager = transportTypeManager;
            _localizer = localizer;
        }

        // GET: TransportType
        [Route("GetAll")]
        [HttpGet]
        //  [AllowAnonymous]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllTransportTypes 
            var AllTransportTypes = await _transportTypeManager.GetAllAsync<TransportTypeViewModel>(true);
            return Ok(AllTransportTypes);
        }

        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Teams By Pagination 
            var AllTeams = await _transportTypeManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }
    }
}