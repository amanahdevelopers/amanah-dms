﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ManagerController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IMapper _mapper;
        private readonly IApplicationUserManager _appUserManager;
        private IManagerManager _managerManager;

        public ManagerController(
            IManagerManager ManagerManager,
            IMapper mapper,
            IApplicationUserManager appUserManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _managerManager = ManagerManager;
            _mapper = mapper;
            _appUserManager = appUserManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
            _currentUser = currentUser;
        }


        // GET: Manager
        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllManagers 
            var AllManagers = await _managerManager.GetAllAsync<ManagerViewModel>();
            return Ok(AllManagers);
        }


        [Route("GetAllByPagination")]
        [HttpPost]
        [Authorize(ManagerPermissions.Managers.ReadTeamManager)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        { ///Get All Teams By Pagination
            if (_currentUser.UserType == RoleType.Manager)
            {
                if (_currentUser.HasPermission(ManagerPermissions.Managers.ReadAllManagers))
                {
                    var AllManagerss = await _managerManager.GetAllByPaginationAsync(pagingparametermodel);
                    return Ok(AllManagerss);
                }
                var MyManages = await _managerManager.GetAllMyByPaginationAsync(pagingparametermodel);
                return Ok(MyManages);
            }


            var AllManagers = await _managerManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllManagers);
        }

        [Route("GetPaginationBy")]
        [HttpPost]
        [Authorize(ManagerPermissions.Managers.ReadTeamManager)]
        public async Task<ActionResult> GetPaginationByAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                if (_currentUser.HasPermission(ManagerPermissions.Managers.ReadAllManagers))
                {
                    var AllManagerss = await _managerManager.GetPaginationByAsync(pagingparametermodel);
                    return Ok(AllManagerss);
                }
                var MyManagers = await _managerManager.GetMyPaginationByAsync(pagingparametermodel);
                return Ok(MyManagers);
            }

            ///Get All Managers By Pagination 
            var AllManagers = await _managerManager.GetPaginationByAsync(pagingparametermodel);
            return Ok(AllManagers);
        }


        // GET: Manager/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var Manager = await _managerManager.Get(id);
            return Ok(Manager);
        }


        // POST: Manager/Create
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateManagerViewModel createManagerVM)
        {
            if (!createManagerVM.TeamManagers.Any())
            {
                return BadRequest(_localizer[Keys.Validation.CreateMangerTeamIsRequired]);
            }
            var existUser = await _appUserManager.GetByEmailAsync(createManagerVM.Email);
            if (existUser != null)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }
            var userManager = _mapper.Map<ApplicationUserViewModel>(createManagerVM);
            var user = _mapper.Map<ApplicationUser>(userManager);
            user.RoleNames.AddRange(createManagerVM.RoleNames);
            var result = await _appUserManager.AddUserAsync(user, userManager.Password);

            if (result == null || result.GetType() == typeof(string))
            {
                return BadRequest(result);
            }
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }
            var manager = _mapper.Map<ManagerViewModel>(createManagerVM);
            manager.UserId = result.User.Id;
            var createdManager = await _managerManager.AddAsync(manager);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Manager",
                ActivityType = "Create",
                Description = manager.FirstName + " " + manager.LastName + " ( " + (await _managerManager.GetAllAsync<ManagerViewModel>()).Max(x => x.Id) + " ) has been created ",
                Record_Id = manager.Id

            };
            await _accountLogManager.Create(accountLog);

            return Ok(createdManager);

        }


        //PUST: Manager/Update
        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] ManagerViewModel updateManagerVM)
        {

            var existUser = await _appUserManager.GetByEmailAsync(updateManagerVM.Email);
            if (existUser != null && existUser.Id != updateManagerVM.UserId)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }

            var userManager = _mapper.Map<ApplicationUserViewModel>(updateManagerVM);
            var user = _mapper.Map<ApplicationUser>(userManager);
            user.RoleNames.AddRange(updateManagerVM.RoleNames);
            var result = await _appUserManager.UpdateUserAsync(user);

            if (!result)
            {
                return BadRequest(result);
            }

            var manager = _mapper.Map<ManagerViewModel>(updateManagerVM);
            var dbManager = await _managerManager.UpdateAsync(manager);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Manager",
                ActivityType = "Update",
                Description = manager.FirstName + " " + manager.LastName + " ( " + manager.Id + " ) has been updated ",
                Record_Id = manager.Id

            };
            await _accountLogManager.Create(accountLog);

            return Ok(dbManager);


        }


        // GET: Manager/Delete/5
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var dbManager = await _managerManager.Get(id);
            var userIds = new List<string>
                {
                    dbManager.UserId
                };
            var dbUser = _appUserManager.GetByUserIds(userIds);
            var userResult = await _appUserManager.SoftDeleteAsync(dbUser.FirstOrDefault());
            if (!userResult)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteManager]);
            }
            var result = await _managerManager.SoftDeleteAsync(dbManager);
            if (result)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Manager",
                    ActivityType = "Delete",
                    Description = dbManager.FirstName + " " + dbManager.LastName + " ( " + dbManager.Id + " ) has been deleted ",
                    Record_Id = dbManager.Id

                };
                await _accountLogManager.Create(accountLog);

                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteManager]);
            }
        }

    }
}