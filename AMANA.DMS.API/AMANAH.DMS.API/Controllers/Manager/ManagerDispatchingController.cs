﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ManagerDispatchingController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;
        private readonly IManagerDispatchingManager _managerDispatchingManager;

        public ManagerDispatchingController(
            IManagerDispatchingManager managerDispatchingManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer)
        {
            _managerDispatchingManager = managerDispatchingManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
        }


        // GET: ManagerDispatching
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var AllManagerDispatchings = await _managerDispatchingManager.GetAllAsync();
            return Ok(AllManagerDispatchings);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var AllManagerDispatchings = await _managerDispatchingManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllManagerDispatchings);
        }


        // GET: ManagerDispatching/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var managerDispatching = await _managerDispatchingManager.Get(id);
            return Ok(managerDispatching);
        }


        // POST: Create/ManagerDispatching
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] ManagerDispatchingViewModel managerDispatchingVM)
        {
            var created_managerDispatching = await _managerDispatchingManager.AddAsync(managerDispatchingVM);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "ManagerDispatching",
                ActivityType = "Create",
                Description = " Manager Dispatching ( " + (await _managerDispatchingManager.GetAllAsync()).Max(x => x.Id) + " ) has been created ",
                Record_Id = created_managerDispatching.Id

            };
            await _accountLogManager.Create(accountLog);

            return Ok(created_managerDispatching);
        }


        //POST: ManagerDispatching/Edit/5
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] ManagerDispatchingViewModel managerDispatchingVM)
        {
            var updated_managerDispatching = await _managerDispatchingManager.UpdateAsync(managerDispatchingVM);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "ManagerDispatching",
                ActivityType = "Update",
                Description = " Manager Dispatching ( " + managerDispatchingVM.Id + " ) has been updated ",
                Record_Id = managerDispatchingVM.Id

            };
            await _accountLogManager.Create(accountLog);
            return Ok(updated_managerDispatching);
        }

        // GET: ManagerDispatching/Delete/5
        [HttpPost("BullkDelete")]
        public async Task<ActionResult> BullkDeleteAsync(List<ManagerDispatchingViewModel> DeletedManagerDispatchings)
        {
            var result = await _managerDispatchingManager.SoftDeleteAsync(DeletedManagerDispatchings);
            if (result)
            {
                foreach (var item in DeletedManagerDispatchings)
                {
                    AccountLogs accountLog = new AccountLogs()
                    {
                        TableName = "ManagerDispatching",
                        ActivityType = "Delete",
                        Description = " Manager Dispatching ( " + item.Id + " ) has been deleted ",
                        Record_Id = item.Id

                    };
                    await _accountLogManager.Create(accountLog);
                }
                return Ok(result);
            }

            return BadRequest(_localizer[Keys.Validation.CanNotDeleteDispatchingManagers]);
        }


        // GET: ManagerDispatching/Delete/5
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var managerDispatchingToDelete = await _managerDispatchingManager.Get(id);
            var result = await _managerDispatchingManager.SoftDeleteAsync(managerDispatchingToDelete);
            if (result)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "ManagerDispatching",
                    ActivityType = "Delete",
                    Description = " Manager Dispatching ( " + managerDispatchingToDelete.Id + " ) has been deleted ",
                    Record_Id = managerDispatchingToDelete.Id

                };
                await _accountLogManager.Create(accountLog);
                return Ok(result);
            }

            return BadRequest(_localizer[Keys.Validation.CanNotDeleteDispatchingManager]);
        }
    }
}