﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    public class CountryController : Controller
    {
        private readonly ILocalizer _localizer;

        private ICountryManager _countryManager { set; get; }

        public CountryController(ICountryManager countryManager, ILocalizer localizer)
        {
            _countryManager = countryManager;
            _localizer = localizer;
        }


        // GET: Country
        [Route("GetAll")]
        [HttpGet]
        //  [AllowAnonymous]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllCountrys 
            var AllCountrys = await _countryManager.GetAllAsync<CountryViewModel>(true);
            return Ok(AllCountrys);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Teams By Pagination 
            var AllTeams = await _countryManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }


        // GET: Country/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var country = await _countryManager.Get(id);
            return Ok(country);
        }


    }
}