﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AddressController : Controller
    {
        private IAddressService _addressService { set; get; }
        private IPACIService _paciService { set; get; }

        private readonly IAreaManager _areaManager;

        public AddressController(
            IAddressService addressService,
           IPACIService paciService,
            IAreaManager areaManager)
        {
            _addressService = addressService;
            _paciService = paciService;
            _areaManager = areaManager;
        }

        // GET: Area
        [Route("search-by-components")]
        [HttpGet]
        public async Task<ActionResult> SearchByComponentsAsync([FromQuery] AddressViewModel addressViewModel)
        {
            var paciaddress = await _addressService.SearchForAddressAsync(addressViewModel);
            if (paciaddress == null)
            {
                return NotFound();
            }
            return Ok(paciaddress);
        }


        [HttpGet]
        [Route("get-blocks-from-paci")]
        public async Task<IActionResult> GetBlocksAsync(int areaId)
        {
            var result = await _paciService.GetBlocksAsync(areaId);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("get-streets-from-paci")]
        public async Task<IActionResult> GetStreetsAsync(int areaId, string blockName)
        {
            var result = await _paciService.GetStreetsAsync(areaId, blockName);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        // GET: Area
        [Route("GetAllAreas")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Areas 
            var AllAreas = await _areaManager.GetAllAsync<AreaViewModel>(true);
            return Ok(AllAreas);
        }

    }
}