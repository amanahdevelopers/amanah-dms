﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class NotificationController : Controller
    {
        public readonly IMapper _mapper;
        public readonly INotificationManager _notificationManager;
        private readonly ICurrentUser _currentUser;

        public NotificationController(IMapper mapper, INotificationManager notificationManager, ICurrentUser currentUser)
        {
            _mapper = mapper;
            _notificationManager = notificationManager;
            _currentUser = currentUser;
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            pagingparametermodel.UserID = _currentUser.Id;
            var notificationlist = await _notificationManager.GetAllByPaginationAsyncForUser(pagingparametermodel);
            return Ok(notificationlist);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var data = await _notificationManager.GetAllAsync<NotificationViewModel>();
            return Ok(data);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            var data = await _notificationManager.Get<NotificationViewModel>(id);
            return Ok(data);
        }


        [Route("[action]/{id}")]
        [HttpGet]
        public async Task<ActionResult> Details([FromRoute] int id)
        {
            var driver = await _notificationManager.Get(id);
            return Ok(driver);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> MarkAsRead()
        {

            var driver = await _notificationManager.MarkAsReadsync(_currentUser.Id);
            return Ok(driver);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> UnReadCount()
        {
            var UnReadCount = await _notificationManager.UnReadCountsync(_currentUser.Id);
            return Ok(UnReadCount);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> MarkThemAsRead(List<NotificationViewModel> notificationlist)
        {
            foreach (var noti in notificationlist)
            {
                var notification = await _notificationManager.Get(noti.Id);
                notification.IsSeen = true;
                var updateresult = await _notificationManager.UpdateAsync(notification);
            }

            return Ok(true);
        }
    }
}