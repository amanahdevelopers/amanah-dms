﻿using System.IO;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.GenericListToExcel;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountLogsController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;

        public AccountLogsController(IAccountLogManager accountLogManager)
        {
            _accountLogManager = accountLogManager;

        }

        // GET: Country
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllCountrys 
            var AllLogs = await _accountLogManager.GetAllAsync<AccountLogViewModel>();
            return Ok(AllLogs);
        }


        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Customers By Pagination 
            var AllCustomers = await _accountLogManager.GetByPagination(pagingparametermodel);
            return Ok(AllCustomers);
        }





        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            var data = await _accountLogManager.GetAccountLogsForExport(pagingparametermodel);
            data.ForEach(x => x.CreatedDate = x.CreatedDate.ToUniversalTime().AddHours(3));
            var path = "ImportTemplates";
            var fileName = "AccountLogs.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

    }
}