﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilites.ExcelToGenericList;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class CustomersController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;
        private readonly ICustomersManager _customerManager;
        private readonly ITasksManager _tasksManager;
        private IImportCustomerExcelManager _importCustomerExcelManage;

        public CustomersController(
            ICustomersManager customerManager,
            ITasksManager tasksManager,
            IImportCustomerExcelManager importCustomerExcelManage,
            IAccountLogManager accountLogManager,
            ILocalizer localizer)
        {
            _customerManager = customerManager;
            _tasksManager = tasksManager;
            _importCustomerExcelManage = importCustomerExcelManage;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
        }

        // GET: Customers



        [Route("GetAll")]
        [HttpGet]
        [Authorize(ManagerPermissions.Customer.ReadCustomer)]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Customers 
            var AllCustomers = await _customerManager.GetAllAsync<CustomerViewModel>();
            return Ok(AllCustomers);
        }

        // GET: Customers by name
        [Route("GetByName/{name}")]
        [HttpGet]
        public async Task<ActionResult> GetByNameAsync(string name)
        {
            ///Get Customers by name
            var AllCustomers = await _customerManager.GetByNameAsync(name);
            return Ok(AllCustomers);
        }


        // GET: Customers by name
        [Route("GetByPhone/{phone}")]
        [HttpGet]
        public async Task<ActionResult> GetByPhoneAsync(string phone)
        {
            ///Get Customers by name
            var AllCustomers = await _customerManager.GetByPhoneAsync(phone);
            return Ok(AllCustomers);
        }



        [Route("GetAllByPagination")]
        [HttpPost]
        [Authorize(ManagerPermissions.Customer.ReadCustomer)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Customers By Pagination 
             //var AllCustomers = await _customerManager.GetAllByPaginationAsync(pagingparametermodel);
            var AllCustomers = await _customerManager.GetAllCustomersByPaginationAsync(pagingparametermodel);
            return Ok(AllCustomers);
        }

        // GET: Customers/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        [Authorize(ManagerPermissions.Customer.ReadCustomer)]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var Customer = await _customerManager.Get(id);
            return Ok(Customer);
        }

        // POST: Customers/Create


        [HttpPost]
        [Route("Create")]
        [Authorize(ManagerPermissions.Customer.CreateCustomer)]
        public async Task<ActionResult> CreateAsync([FromBody] CustomerViewModel CustomerVM)
        {
            //var IsEmailExist = await _customerManager.IsEmailExistAsync(CustomerVM.Email);
            //if (IsEmailExist)
            //{
            //    return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            //}
            var IsPhoneExist = await _customerManager.IsPhoneExistAsync(CustomerVM.Email);
            if (IsPhoneExist)
            {
                return BadRequest(_localizer[Keys.Validation.PhoneAlreadyExists]);
            }
            var existCustomer = await _tasksManager.GetCustomerAsync(CustomerVM);
            if (existCustomer != null)
            {
                return BadRequest(_localizer[Keys.Validation.CustomerAlreadyExists]);
            }
            var created_Customer = await _customerManager.AddAsync(CustomerVM);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Customers",
                ActivityType = "Create",
                Description = CustomerVM.Name + " ( " + (await _customerManager.GetAllAsync<CustomerViewModel>()).Max(a => a.Id) + " ) has been created ",
                Record_Id = CustomerVM.Id

            };
            var created_log = await _accountLogManager.Create(accountLog);
            return Ok(created_Customer);
        }

        // GET: Customers/Edit/5


        // POST: Customers/Edit/5
        [HttpPut]
        [Authorize(ManagerPermissions.Customer.UpdateCustomer)]
        public async Task<ActionResult> Update([FromBody] CustomerViewModel CustomerVM)
        {
            var existCustomer = await _tasksManager.GetCustomerAsync(CustomerVM);
            if (existCustomer != null && existCustomer.Id != CustomerVM.Id)
            {
                return BadRequest(_localizer[Keys.Validation.CustomerAlreadyExists]);
            }
            var updated_Customer = await _customerManager.UpdateAsync(CustomerVM);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Customers",
                ActivityType = "Update",
                Description = CustomerVM.Name + " ( " + CustomerVM.Id + " ) has been updated ",
                Record_Id = CustomerVM.Id

            };
            var created_log = await _accountLogManager.Create(accountLog);
            return Ok(updated_Customer);
        }

        // GET: Customers/Delete/5
        [HttpDelete("Delete/{id}")]
        [Authorize(ManagerPermissions.Customer.DeleteCustomer)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var CustomerToDelete = await _customerManager.Get(id);
            var result = await _customerManager.SoftDeleteAsync(CustomerToDelete);
            // TODO: Add insert logic here

            if (result)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Customers",
                    ActivityType = "Delete",
                    Description = CustomerToDelete.Name + " ( " + CustomerToDelete.Id + " ) has been deleted ",
                    Record_Id = CustomerToDelete.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteCustomer]);
            }
        }

        // GET: Customers/Delete


        [HttpPost("BulkDelete")]
        [Authorize(ManagerPermissions.Customer.DeleteCustomer)]
        public async Task<ActionResult> BulkDelete([FromBody] List<CustomerViewModel> CustomerVM)
        {
            var result = await _customerManager.SoftDeleteAsync(CustomerVM);
            if (result)
            {
                foreach (var item in CustomerVM)
                {
                    AccountLogs accountLog = new AccountLogs()
                    {
                        TableName = "Customers",
                        ActivityType = "Delete",
                        Description = item.Name + " ( " + item.Id + " ) has been deleted by ",
                        Record_Id = item.Id

                    };
                    var created_log = await _accountLogManager.Create(accountLog);
                }
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteCustomers]);
            }
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("AddFromExcelSheet")]
        public async Task<IActionResult> AddFromExcelSheetAsync([FromBody] UploadFile file)
        {
            var uploadpath = "CustomerSheet2";
            var result = await _importCustomerExcelManage.AddFromExcelSheetAsync(uploadpath, file);
            var FaildRows = result.returnData.Where(x => x.Error != null || x.Error == "").ToList();

            if (FaildRows.Any())
            {
                string fileName = "Faild" + file.FileName + ".csv";
                var faildPath = "FaildCustomerSheet";

                string filePath = System.IO.Path.Combine(faildPath, fileName);

                string resultPath = FaildRows.ToCSV<ImportCustomerViewModel>(filePath);
                byte[] fileBytes = System.IO.File.ReadAllBytes(resultPath);
                return File(fileBytes, "application/force-download", fileName);
            }

            return Ok();
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel()
        {
            var data = await _customerManager.ExportExcel();
            ////var fileData = data.CreateDataForCsvFile();
            ////byte[] fileDataBytes = Encoding.UTF8.GetBytes(fileData);
            var path = "ImportTemplates";
            var fileName = "customers.xlsx";
            string filePath = Path.Combine(path, fileName);

            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);


        }


    }
}
