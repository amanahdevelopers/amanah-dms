﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize]
    public class MainTaskController : BaseController
    {
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly INotificationManager _notificationManager;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IDriverManager _driverManager;
        private readonly IMainTaskManager _mainTaskManager;
        private readonly ISettingsManager _settingManager;
        private readonly IManagerManager _managerManager;

        public MainTaskController(
            IMainTaskManager MainTaskManager,
            IHubContext<NotificationHub> notificationhubContext,
            IDriverManager driverManager,
            INotificationManager notificationManager,
            IApplicationUserManager applicationUserManager,
            ApplicationDbContext context,
            ISettingsManager settingManager,
            IManagerManager managerManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _mainTaskManager = MainTaskManager;
            _notificationhubContext = notificationhubContext;
            _driverManager = driverManager;
            _notificationManager = notificationManager;
            _applicationUserManager = applicationUserManager;
            _context = context;
            _settingManager = settingManager;
            _managerManager = managerManager;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        [Route("GetMyTasks")]
        [HttpPost]
        public async Task<ActionResult> GetMyTasks([FromBody] PaginatedTasksDriverViewModel paginatedTasksDriverViewModel)
        {
            var Driver = await _driverManager.GetByUserID(_currentUser.Id);
            if (Driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            paginatedTasksDriverViewModel.DriverId = Driver.Id;
            var enableAutoallocationSetting = await _settingManager.GetSettingByKey("IsEnableAutoAllocation");
            var autoallocationSetting = await _settingManager.GetSettingByKey("SelectedAutoAllocationMethodName");
            int autoAllocationType = enableAutoallocationSetting == null
                ? (int)AutoAllocationTypeEnum.Manual
                : enableAutoallocationSetting.Value.ToLower() == "false"
                    ? (int)AutoAllocationTypeEnum.Manual
                    : autoallocationSetting == null
                        ? (int)AutoAllocationTypeEnum.Manual
                        : autoallocationSetting.Value == "FirstInFirstOutMethod"
                            ? (int)AutoAllocationTypeEnum.Fifo
                            : autoallocationSetting.Value == "NearestAvailableMethod"
                                ? (int)AutoAllocationTypeEnum.Nearest
                                : autoallocationSetting.Value == "OneByOneMethod"
                                    ? (int)AutoAllocationTypeEnum.OneByOne
                                    : (int)AutoAllocationTypeEnum.Manual;
            var result = await _mainTaskManager.GetDriverTasks(paginatedTasksDriverViewModel);
            MainTaskPagedResult<MainTaskViewModel> data = new MainTaskPagedResult<MainTaskViewModel>
            {
                DriverAgentStatusId = Driver.AgentStatusId ?? (int)AgentStatusesEnum.Unavailable,
                DriverIsSetReached = Driver.ReachedTime.HasValue,
                AutoAllocationType = autoAllocationType,
                Result = result != null ? result.Result : new List<MainTaskViewModel>(),
                TotalCount = result != null ? result.TotalCount : 0,
                IsInClubbingTime = Driver.IsInClubbingTime,
                ClubbingTimeExpiration = Driver.ClubbingTimeExpiration
            };
            return Ok(data);
        }

        [Route("GetNewTasks")]
        [HttpGet]
        public async Task<ActionResult> GetNewTasks()
        {
            var Driver = await _driverManager.GetByUserID(_currentUser.Id);
            if (Driver != null)
            {
                var result = await _mainTaskManager.GetDriverNewTasks(Driver.Id);
                result = result.OrderByDescending(x => x.RemainingTimeInSeconds).ToList();
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
        }

        [Route("Accept")]
        [HttpGet]
        public async Task<ActionResult> AcceptAsync(int id, double? longitude = null, double? latitude = null)
        {
            await _mainTaskManager.AcceptAsync(id, longitude, latitude);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("TaskAccept", id);
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var mainTask = _context.MainTask.SingleOrDefault(t => t.Id == id);
            var driver = await _driverManager.GetByUserID(driverUserId);
            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            var title = _localizer[Keys.Notifications.DriverAcceptedTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameAcceptedTaskNo, driver.FullName, mainTask.Id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.TaskAccept
            });

            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driverUserId,
                    ToUserId = mainTask.CreatedBy_Id,
                    NotificationType = NotificationTypeWeb.TaskAccept
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("TaskAccept", id);
            }

            return Ok();
        }

        [Route("Decline")]
        [HttpGet]
        public async Task<ActionResult> DeclineAsync(int id, double? longitude = null, double? latitude = null)
        {
            await _mainTaskManager.DeclineAsync(id, longitude, latitude);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("DeclineAsync", id);
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var mainTask = _context.MainTask.SingleOrDefault(t => t.Id == id);
            var driver = await _driverManager.Get(mainTask.Tasks.FirstOrDefault().DriverId.Value);
            var driverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            var title = _localizer[Keys.Notifications.DriverDeclinedTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameDeclinedTaskNo, driver.FullName, mainTask.Id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.DeclineAsync
            });
            foreach (var driverManager in driverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driverUserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.DeclineAsync
                });
                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("DeclineAsync", id);
            }
            return Ok();
        }

        [Route("GetTaskHistoryDetails")]
        [HttpPost]
        public async Task<ActionResult> GetTaskHistoryDetailsAsync([FromBody] PaginatedTasksDriverViewModel paginatedTasksDriverViewModel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            paginatedTasksDriverViewModel.DriverId = _currentUser.DriverId.Value;
            var result = await _mainTaskManager.GetTaskHistoryDetailsAsync(paginatedTasksDriverViewModel);
            return Ok(result);
        }
    }
}