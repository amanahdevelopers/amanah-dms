﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : BaseController
    {
        protected IApplicationUserManager _appUserManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        public readonly IMapper _mapper;

        public UserController(
            IMapper mapper,
            IApplicationUserManager userManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _mapper = mapper;
            _appUserManager = userManager;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        [Authorize]
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] EditUserProfileViewModel model)
        {
            if (await _appUserManager.IsUserNameExistAsync(model.UserName, _currentUser.Id))
            {
                return BadRequest(_localizer.Format(Keys.Validation.UserNameAlreadyExists, model.UserName));
            }
#warning code clean up this is done automatically no need to handle invalid model state
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.Values.Select(e => e.Errors).ToList());
            }
            var appUser = _mapper.Map<EditUserProfileViewModel, ApplicationUser>(model);
            await _appUserManager.UpdateAsync(appUser);
            if (!string.IsNullOrWhiteSpace(model.CurrentPassword) && !string.IsNullOrWhiteSpace(model.NewPassword))
            {
                await _appUserManager.ChangePasswordAsync(appUser, model.CurrentPassword, model.NewPassword);
            }
            return Ok();
        }

    }
}