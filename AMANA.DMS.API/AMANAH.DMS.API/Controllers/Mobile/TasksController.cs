﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Constants;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.Context;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [Authorize]
    public class TasksController : BaseController
    {
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly ITasksManager _tasksManager;
        private readonly IDriverManager _driverManager;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly ApplicationDbContext _context;
        private readonly ICurrentUser _currentUser;
        private readonly ILocalizer _localizer;
        private readonly INotificationManager _notificationManager;
        private readonly ISettingsManager _settingManager;
        private readonly IManagerManager _managerManager;

        public TasksController(
            ITasksManager TasksManager,
            IHubContext<NotificationHub> notificationhubContext,
            IDriverManager DriverManager,
            IApplicationUserManager applicationUserManager,
            ApplicationDbContext context,
            INotificationManager notificationManager,
            ISettingsManager settingManager,
            IManagerManager managerManager,
            ICurrentUser currentUser,
            ILocalizer localizer)
        {
            _tasksManager = TasksManager;
            _notificationhubContext = notificationhubContext;
            _driverManager = DriverManager;
            _applicationUserManager = applicationUserManager;
            _context = context;
            _currentUser = currentUser;
            _localizer = localizer;
            _notificationManager = notificationManager;
            _settingManager = settingManager;
            _managerManager = managerManager;
        }


        [Route("Successful")]
        [HttpPost]
        public async Task<ActionResult> SuccessfulAsync([FromBody] SuccessfullTaskViewModel Input)
        {
            await _tasksManager.SuccessfulAsync(Input);
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var mainTask = _context.MainTask.SingleOrDefault(t => t.Id == Input.MainTaskId);
            await _driverManager.ChangeDriverStatusByUserId(driverUserId, (int)AgentStatusesEnum.Available);
            var driver = await _driverManager.GetByUserID(driverUserId);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("TaskSuccessful", Input.TaskId, driver);
            var driverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            var title = _localizer[Keys.Notifications.DriverCompletedTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameAcceptedTaskNo, driver.FullName, mainTask.Id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.TaskSuccessful
            });
            if (driverManagers != null)
            {
                foreach (var driverManager in driverManagers)
                {
                    await _notificationManager.AddAsync(new NotificationViewModel
                    {
                        Title = title,
                        Body = body,
                        CreatedDate = DateTime.UtcNow,
                        FromUserId = driverUserId,
                        ToUserId = driverManager.UserId,
                        NotificationType = NotificationTypeWeb.TaskSuccessful
                    });
                    await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("TaskSuccessful", Input.TaskId, driver);
                }
            }
            return Ok();
        }

        [Route("FaildWithSignature")]
        [HttpPost]
        public async Task<ActionResult> FaildWithSignature([FromBody] SuccessfullTaskViewModel Input)
        {
            await _tasksManager.FailedWithSinatureAsync(Input);
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var mainTask = _context.MainTask.SingleOrDefault(t => t.Id == Input.MainTaskId);
            await _driverManager.ChangeDriverStatusByUserId(driverUserId, (int)AgentStatusesEnum.Available);

            var driver = await _driverManager.GetByUserID(driverUserId);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("TaskFailed", Input.TaskId, driver, Input.Reason);
            var title = _localizer[Keys.Notifications.DriverFailedTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameFailedTaskNo, driver.FullName, mainTask.Id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.TaskFailed
            });
            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driverUserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.TaskFailed
                });
                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("TaskFailed", Input.TaskId, driver, Input.Reason);
            }

            return Ok();
        }


        [Route("Cancel")]
        [HttpGet]
        public async Task<ActionResult> CancelAsync(int id, string reason, double? longitude, double? latitude)
        {
            await _tasksManager.CancelAsync(id, reason, longitude, latitude);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("TaskCancel", id, reason);
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var driver = await _driverManager.GetByUserID(driverUserId);
            var driverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            var title = _localizer[Keys.Notifications.DriverCanceledTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameCanceledTaskNo, driver.FullName, id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.TaskCancel
            });
            foreach (var driverManager in driverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driverUserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.TaskCancel
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("TaskCancel", id, reason);
            }

            return Ok();
        }

        [Route("Start")]
        [HttpGet]
        public async Task<IActionResult> StartAsync(int id, double? longitude, double? latitude)
        {
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var driver = await _driverManager.GetByUserID(driverUserId);
            if (driver.IsInClubbingTime)
            {
                return Fail("Sorry, you can't start this task, Orders will be assigned to you shortly, please wait.");
            }

            await _tasksManager.StartAsync(id, longitude, latitude);

            driver = await _driverManager.GetByUserID(driverUserId);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("TaskStart", id, driver);
            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            var title = _localizer[Keys.Notifications.DriverStartedTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameStartedTaskNo, driver.FullName, id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.TaskStart
            });
            if (DriverManagers != null)
            {
                foreach (var driverManager in DriverManagers)
                {
                    await _notificationManager.AddAsync(new NotificationViewModel
                    {
                        Title = title,
                        Body = body,
                        CreatedDate = DateTime.UtcNow,
                        FromUserId = driverUserId,
                        ToUserId = driverManager.UserId,
                        NotificationType = NotificationTypeWeb.TaskStart
                    });
                    await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("TaskStart", id, driver);
                }
            }

            return Success();
        }


        [Route("Decline")]
        [HttpGet]
        public async Task<ActionResult> DeclineAsync(int id, double? longitude, double? latitude)
        {
            await _tasksManager.DeclineAsync(id, longitude, latitude);
            await _notificationhubContext.Clients.Group(_currentUser.TenantId).SendAsync("TaskDecline", id);
            var driverUser = await _applicationUserManager.GetByUserNameOrEmailAsync(_currentUser.UserName);
            var driverUserId = driverUser.Id;
            var mainTask = _context.MainTask.SingleOrDefault(t => t.Id == id);
            var driver = await _driverManager.Get(mainTask.Tasks.FirstOrDefault().DriverId.Value);
            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            await _driverManager.ChangeDriverStatusByUserId(driverUserId, (int)AgentStatusesEnum.Available);
            var title = _localizer[Keys.Notifications.DriverDeclinedTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameDeclinedTaskNo, driver.FullName, mainTask.Id);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driverUserId,
                ToUserId = _currentUser.TenantId,
                NotificationType = NotificationTypeWeb.TaskDecline
            });

            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driverUserId,
                    ToUserId = driverManager.UserId
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("TaskDecline", id);
            }
            return Ok();
        }

        [Route("GetDriverFinishedTasks")]
        [HttpPost]
        public async Task<ActionResult> GetDriverFinishedTasksAsync(PaginatedTasksDriverViewModel paginatedTasksDriverViewModel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            paginatedTasksDriverViewModel.DriverId = _currentUser.DriverId.Value;
            var result = await _tasksManager.GetDriverFinishedTasksAsync(paginatedTasksDriverViewModel);
            return Ok(result);
        }



        [Route("GetTaskCalender")]
        [HttpPost]
        public async Task<ActionResult> GetTaskCalenderAsync([FromBody] CalenderViewModel calenderViewModel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            calenderViewModel.Driver_id = _currentUser.DriverId.Value;
            var tasks = await _tasksManager.GetTaskCalenderAsync(calenderViewModel);
            return Ok(tasks);
        }

        [Route("GetTravelSummery")]
        [HttpPost]
        public async Task<ActionResult> GetTravelSummeryAsync(DateTime date)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var result = await _tasksManager.TravelSummeryAsync(date, _currentUser.DriverId.Value);
            return Ok(result);
        }

        [Route("SetReached")]
        [HttpGet]
        public async Task<IActionResult> SetReachedAsync(int taskId, double longitude, double latitude)
        {
            if (!_currentUser.IsDriver())
            {
                return Fail(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }

            var task = await _tasksManager.Get(taskId);
            if (task == null)
            {
                return Fail(_localizer[Keys.Validation.TaskDeletedOrNotExisting]);
            }
            var distance = MapsUtilities.MapsHelper
                .GetDistanceBetweenPoints(task.Latitude.Value, task.Longitude.Value, latitude, longitude);
            double allowedDistanceInMeter = DefaultSettingValue.ReachedDistanceRestrictionInMeter;
            var allowedDistanceInMeterSetting = await _settingManager.GetSettingByKey("ReachedDistanceRestriction");
            if (allowedDistanceInMeterSetting != null)
            {
                double.TryParse(allowedDistanceInMeterSetting.Value, out allowedDistanceInMeter);
            }

            if (distance <= allowedDistanceInMeter)
            {
                var branchName = task.BranchName;
                //need to check if driver has started tasks to be busy or available 
                await _driverManager.ChangeDriverStatusByUserId(_currentUser.Id, (int)AgentStatusesEnum.Available);
                if (task.BranchId.HasValue)
                {
                    await _driverManager.CheckInAsync(task.BranchId.Value);
                }
                var result = _tasksManager.SetReached(taskId, true);
                return Success(_localizer.Format(Keys.Validation.YouReachedPickupLocation));
            }
            return Fail(_localizer.Format(Keys.Validation.YouDidNotReachPickupLocation));

        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetTaskStatusCountAsync(DateTime taskDate)
        {
            var taskStatus = await _tasksManager.GetTaskStatusCountAsync(taskDate);
            return Ok(new { IsSuccessful = true, data = taskStatus });
        }
    }
}