﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.BaseResult;
using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.ViewModels;
using MapsUtilities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize]
    public class DriverController : BaseController
    {
        private readonly IDriverManager _driverManager;
        private readonly ITasksManager _tasksManager;
        private readonly IManagerManager _managerManager;
        private readonly IUploadFileManager _uploadFileManager;
        private readonly IApplicationUserManager _appUserManager;
        private readonly ILocalizer _localizer;
        private readonly ILogger<DriverController> _logger;
        private readonly ICurrentUser _currentUser;
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly INotificationManager _notificationManager;

        public DriverController(
            IDriverManager driverManager,
            ITasksManager tasksManager,
            IManagerManager managerManager,
            INotificationManager notificationManager,
            IHubContext<NotificationHub> notificationhubContext,
            IUploadFileManager uploadFileManager,
            IApplicationUserManager appUserManager,
            ILocalizer localizer,
            ILogger<DriverController> logger,
            ICurrentUser currentUser)
        {
            _driverManager = driverManager;
            _tasksManager = tasksManager;
            _managerManager = managerManager;
            _uploadFileManager = uploadFileManager;
            _appUserManager = appUserManager;
            _localizer = localizer;
            _logger = logger;
            _currentUser = currentUser;
            _notificationhubContext = notificationhubContext;
            _notificationManager = notificationManager;
        }

        [Route("SetOnDuty")]
        [HttpPost]
        public async Task<IActionResult> SetOnDuty([FromBody] ChangeDriverDutyViewModel model)
        {
            int statusId = model.IsAvailable ? (int)AgentStatusesEnum.Available : (int)AgentStatusesEnum.Unavailable;
            var res = false;
            var driver = await _driverManager.GetByUserID(model.DriverId);
            var driveruser = await _appUserManager.GetByUserIdAsync(model.DriverId);

            if (model.IsAvailable)
            {
                await _tasksManager.MakeDriverAvailable(driver.Id);
                res = true;
            }
            else
            {
                statusId = (int)AgentStatusesEnum.Unavailable;
                res = await _driverManager.ChangeDriverStatusByUserId(model.DriverId, statusId);

            }
            await _notificationhubContext.Clients.Group(driveruser.Tenant_Id).SendAsync("SetDuty", driver.Id, statusId);
            var dutyValue = model.IsAvailable
                ? _localizer[Keys.General.On]
                : _localizer[Keys.General.Off];
            var title = _localizer[Keys.Notifications.DriverSetDuty];
            var body = _localizer.Format(Keys.Notifications.DriverNameSetDutyVaue, driver.FullName, dutyValue);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driver.UserId,
                ToUserId = driveruser.Tenant_Id,
                NotificationType = NotificationTypeWeb.DriverSetDuty
            });

            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            if (DriverManagers != null)
                foreach (var driverManager in DriverManagers)
                {
                    await _notificationManager.AddAsync(new NotificationViewModel
                    {
                        Title = title,
                        Body = body,
                        CreatedDate = DateTime.UtcNow,
                        FromUserId = driver.UserId,
                        ToUserId = driverManager.UserId,
                        NotificationType = NotificationTypeWeb.DriverSetDuty
                    });
                    await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("SetDuty", driver.Id);
                }

            return Ok(res);
        }


        [Route("UpdateDriverVichaleType")]
        [HttpPost]
        public async Task<IActionResult> UpdateVichaleType([FromBody] ChangeDriverTransportTypeViewModel model)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var driver = await _driverManager.GetByUserID(_currentUser.Id);
            driver.TransportTypeId = model.TransportTypeId;
            driver.TransportDescription = model.TransportDescription;
            driver.Color = model.Color;
            driver.LicensePlate = model.LicensePlate;

            var res = await _driverManager.UpdateAsync(driver);

            return Ok(res);
        }

        [Route("ChangeDriversType")]
        [HttpPut]
        public async Task<IActionResult> ChangeDriversTypeAsync([FromBody] ChangeDriversTypeViewModel model)
        {
            await _driverManager.ChangeDriversTypeAsync(model);
            return Ok();

        }

        [Route("CheckIn")]
        [HttpPut]
        public async Task<IActionResult> CheckInAsync(double Latitude, double Longitude)
        {
#warning need simplification
            _logger.LogTrace("Starting Checkin at {lat}, {long}", Latitude, Longitude);
            var driver = await _driverManager.GetCurrentDriver();
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.DriverNotFound]);
            }
            var isDriverBusy = await _tasksManager.CheckDriverBusy(driver.Id);
            if (isDriverBusy || driver.AgentStatusId != (int)AgentStatusesEnum.Available)
            {
                _logger.LogWarning("{Driver} is busy and tried to check-in", driver.Id);
                return Fail(_localizer[Keys.Validation.CanNotCheckInTasksnNotCompleted]);
            }

            var driverCurrentLocation = await _driverManager.GetDriverCurrentLocationAsync(driver.Id);
            if (driverCurrentLocation == null)
            {
                _logger.LogWarning("DriverCurrentLocation for {driver} not found", driver.Id);
                return Fail(_localizer[Keys.Validation.CanNotCheckInOutOfRangeOfAnyBranch]);
            }

            driverCurrentLocation.Latitude = Latitude;
            driverCurrentLocation.Longitude = Longitude;

            var driverGeoFences = await _driverManager.GetDriverPickUpGeoFencesAsync(driver.Id);
            if (driverGeoFences == null || !driverGeoFences.Any())
            {
                _logger.LogWarning("No PickupGeofences found for {driver}", driver.Id);
                return Fail(_localizer[Keys.Validation.CanNotCheckInNoGeofencesFound]);
            }
            var driverCurrentGeoFence = _driverManager.GetDriverCurrentGeoFence(driverGeoFences, driverCurrentLocation);
            if (driverCurrentGeoFence == null)
            {
                _logger.LogWarning("Could not resolve current geofence for {driver}", driver.Id);
                return Fail(_localizer[Keys.Validation.CanNotCheckInOutOfRangeOfAnyGeofence]);
            }
            var driverBranches = await _driverManager.GetDriverCurrentGeoFenceBranchesAsync(driverCurrentGeoFence);
            if (driverBranches == null || !driverBranches.Any())
            {
                _logger.LogWarning(
                    "Could not find branches in current {geofence} for {driver}", driverCurrentGeoFence.Id, driver.Id);
                return Fail(_localizer[Keys.Validation.CanNotCheckInOutOfRangeOfAnyBranch]);
            }

            var point = new MapPoint() { Latitude = Latitude, Longitude = Longitude };
            var driverCurrentBranch = await _tasksManager.GetRelatedBranchAsync(driverBranches, point);
            if (driverCurrentBranch == null)
            {
                _logger.LogWarning(
                    "Could not locate a close branch for {driver} at {lat},{long} ", driver.Id, Latitude, Longitude);
                return Fail(_localizer[Keys.Validation.CanNotCheckInOutOfRangeOfAnyBranch]);
            }
            await _driverManager.CheckInAsync(driverCurrentBranch.Id);
            _logger.LogInformation("{Driver} checked-in successfully at {Branch}", driver.Id, driverCurrentBranch.Id);

            return Success(_localizer.Format(Keys.Messages.DriverReachedBranchName, driverCurrentBranch.Name));
        }


        [Route("DriverTransportionType")]
        [HttpGet]
        public async Task<IActionResult> GetDriverTransportionTypeAsync()
        {
            var driver = await _driverManager.GetDriverTransportionTypeAsync(_currentUser.Id);
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            return Ok(driver);
        }

        [Route("DriverChangePhoto")]
        [HttpPost]
        [Authorize(AgentPermissions.Profile.UpdateProfilePicture)]
        public async Task<IActionResult> ChangePhoto([FromBody] DriverPhotoViewModel driverPhotoViewModel)
        {

            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var driver = await _driverManager.GetByUserID(_currentUser.Id);
            if (driverPhotoViewModel.photo != null)//Image Base64
            {
                var path = "DriverImages";
                UploadFile uploadFile = new UploadFile()
                {
                    FileContent = driverPhotoViewModel.photo,
                    FileName = "Image.jpg"
                };
                var processResult = _uploadFileManager.AddFile(uploadFile, path);
                if (processResult.IsSucceeded)
                {
                    driver.ImageUrl = processResult.returnData;
                }
                else
                {
                    return BadRequest(processResult.Exception);
                }
            }
            var res = await _driverManager.UpdateAsync(driver);

            return Ok(res);
        }

        [Route("DriverUpdateProfile")]
        [HttpPost]
        [Authorize(AgentPermissions.Profile.UpdateProfile)]
        public async Task<IActionResult> UpdateProfile([FromBody] EditDriverProfileViewModel driverProfileViewModel)
        {
            // Response to Return With Success or Fail
            var response = new EntityResult<string>();

            // Get User
            var oldUser = await _appUserManager.GetByUserIdAsync(_currentUser.Id);

            if (!string.IsNullOrWhiteSpace(driverProfileViewModel.CurrentPassword) ||
                !string.IsNullOrWhiteSpace(driverProfileViewModel.NewPassword) ||
                !string.IsNullOrWhiteSpace(driverProfileViewModel.ConfirmPassword))
            {

                //change Password 
                bool result = await _appUserManager.ChangePasswordAsync(
                    oldUser, driverProfileViewModel.CurrentPassword, driverProfileViewModel.NewPassword);

                if (!result)
                {
                    response.Error = _localizer[Keys.Validation.CouldNotChangeDriverPassword];
                    response.Succeeded = false;
                    return Ok(response);
                }
            }

            oldUser.PhoneNumber = driverProfileViewModel.phone;
            var res = await _appUserManager.UpdateAsync(oldUser);

            if (!res.Succeeded)
            {
                response.Error = res.Errors.FirstOrDefault()?.Description;
                response.Succeeded = false;
                return Ok(response);
            }

            return Ok(response);
        }
    }
}