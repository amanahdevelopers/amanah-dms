﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Account;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json.Linq;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class DriverAccountController : BaseController
    {
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly IDriverManager _driverManager;
        private readonly IApplicationUserManager _userManager;
        private readonly IMainTaskManager _mainTaskManager;
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly INotificationManager _notificationManager;
        private readonly IDriverLoginRequestManager _driverLoginRequestManager;
        private readonly IDriverloginTrackingManager _driverloginTrackingManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IManagerManager _managerManager;
        private readonly ITasksManager _tasksManager;
        private readonly ISettingsManager _settingManager;

        public DriverAccountController(
            IDriverLoginRequestManager driverLoginRequestManager,
            IApplicationUserManager userManager,
            IUserDeviceManager userDeviceManager,
            INotificationManager notificationManager,
            IHubContext<NotificationHub> notificationhubContext,
            ITasksManager tasksManager,
            IDriverManager driverManager,
            IMainTaskManager mainTaskManager,
            IDriverloginTrackingManager driverloginTrackingManager,
            ISettingsManager settingManager,
            ILocalizer localizer,
            ICurrentUser currentUser,
            IManagerManager managerManager)
        {
            _userManager = userManager;
            _driverManager = driverManager;
            _userDeviceManager = userDeviceManager;
            _mainTaskManager = mainTaskManager;
            _driverLoginRequestManager = driverLoginRequestManager;
            _notificationhubContext = notificationhubContext;
            _notificationManager = notificationManager;
            _driverloginTrackingManager = driverloginTrackingManager;
            _tasksManager = tasksManager;
            _settingManager = settingManager;
            _localizer = localizer;
            _currentUser = currentUser;
            _managerManager = managerManager;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] AgentLoginViewModel model)
        {
            var loginResult = await _userManager.DriverLoginAsync(model);
            if (loginResult.Succeeded == true)
            {
                var driver = await _driverManager.GetByUserID(loginResult.User.Id);
                if (driver.IsDeleted)
                {
                    loginResult.Succeeded = false;
                    loginResult.Errors.Add(
                        new IdentityError
                        {
                            Code = "UserDeleted",
                            Description = _localizer[Keys.Validation.DriverIsDeleted]
                        });
                    return Ok(loginResult);
                }

                loginResult.DriverID = driver.Id;

                var isDriverBusy = await _tasksManager.CheckDriverBusy(driver.Id);
                var driverAgentStatusId = isDriverBusy ? (int)AgentStatusesEnum.Busy : (int)AgentStatusesEnum.Available;
                await _driverManager.ChangeDriverStatusByUserId(loginResult.User.Id, driverAgentStatusId);
                driver.AgentStatusId = driverAgentStatusId;
                driver.DeviceType = model.DeviceType;
                driver.Version = model.Version;
                driver.TokenTimeExpiration = DateTime.UtcNow.AddDays(1);
                await _driverManager.UpdateAsync(driver);
                var driver_obj_New = await _driverManager.GetByUserID(loginResult.User.Id);
                string str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
                loginResult.ProfilePictureURL = $"{str}/DriverImages/{driver_obj_New.ImageUrl}";
                loginResult.TransportTypeId = driver_obj_New.TransportTypeId;
                loginResult.TransportTypeName = driver_obj_New.TransportTypeName;
                loginResult.AgentStatusId = driver_obj_New.AgentStatusId;
                loginResult.NewTasksCount = await _mainTaskManager.GetDriverNewTasksCountAsync(driver_obj_New.Id);
                loginResult.LocationAccuracyName = driver_obj_New.LocationAccuracyName;
                loginResult.LocationAccuracyDuration = driver_obj_New.LocationAccuracyDuration;
                var driveruser = await _userManager.GetByUserNameOrEmailAsync(model.Email);

                var driverloginTraking = new DriverloginTrackingViewModel()
                {
                    DriverId = loginResult.DriverID,
                    DriverName = loginResult.User?.FullName,
                    LoginDate = DateTime.UtcNow,
                    LoginLatitude = model.Latitude,
                    LoginLongitude = model.Longitude,
                    LogoutDate = null
                };
                await _driverloginTrackingManager.AddAsync(driverloginTraking);
                await _notificationhubContext.Clients.Group(driveruser.Tenant_Id).SendAsync("DriverLoggedin", driver_obj_New);
                var title = _localizer[Keys.Notifications.DriverHasLoggedIn];
                var body = _localizer.Format(Keys.Notifications.DriverNameHasLoggeIn, driver.FullName);
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driver.UserId,
                    ToUserId = driveruser.Tenant_Id,
                    NotificationType = NotificationTypeWeb.DriverLoggedin
                });

                var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
                foreach (var driverManager in DriverManagers)
                {
                    await _notificationManager.AddAsync(new NotificationViewModel
                    {
                        Title = title,
                        Body = body,
                        CreatedDate = DateTime.UtcNow,
                        FromUserId = driver.UserId,
                        ToUserId = driverManager.UserId,
                        NotificationType = NotificationTypeWeb.DriverLoggedin
                    });

                    await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("DriverLoggedin", driver_obj_New);
                }
            }
            return Ok(loginResult);
        }



        [Route("ForgotPassword/{userNameOrEmail}")]
        public async Task<IActionResult> ForgotPassword(string userNameOrEmail)
        {
            var res = await _userManager.ForgotPassword(userNameOrEmail);
            return Ok(res);
        }

        [HttpGet]
        [Route("Logout")]
        [Authorize]
        public async Task<IActionResult> Logout(double Latitude, double Longitude)
        {
            var driver = await _driverManager.GetCurrentDriver();
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.DriverNotFound]);
            }
            var deviceIds = _userDeviceManager.GetByUserId(_currentUser.Id);
            foreach (var device in deviceIds)
            {
                _userDeviceManager.DeleteDevice(device.FCMDeviceId);
            }
            await UpdateDriverLoginTracking(Latitude, Longitude, driver.Id);
            await _driverManager.ChangeDriverStatusByUserId(_currentUser.Id, (int)AgentStatusesEnum.Offline);
            var driveruser = await _userManager.GetByUserNameOrEmailAsync(driver.Email);
            driver = await _driverManager.GetByUserID(driveruser.Id);
            var title = _localizer[Keys.Notifications.DriverHasBeenLoggedOut];
            var body = _localizer.Format(Keys.Notifications.DriverNameHasBeenLoggedOut, driver.FullName);
            await _notificationhubContext.Clients.Group(driveruser.Tenant_Id).SendAsync("DriverLoggedOut", driver);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driver.UserId,
                ToUserId = driveruser.Tenant_Id,
                NotificationType = NotificationTypeWeb.DriverLoggedin
            });

            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driver.UserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.DriverLoggedin
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("DriverLoggedOut", driver);
            }

            return Ok(true);
        }

        [HttpGet]
        [Route("LogoutByUserId")]
        [AllowAnonymous]
        public async Task<IActionResult> LogoutByUserIdAsync(string userId, double Latitude, double Longitude)
        {
            var driver = await _driverManager.GetByUserID(userId);
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.DriverNotFound]);
            }
            var deviceIds = _userDeviceManager.GetByUserId(userId);
            foreach (var device in deviceIds)
            {
                _userDeviceManager.DeleteDevice(device.FCMDeviceId);
            }
            await _driverManager.ChangeDriverStatusByUserId(userId, (int)AgentStatusesEnum.Offline);
            await UpdateDriverLoginTracking(Latitude, Longitude, driver.Id);

            var driveruser = await _userManager.GetByUserNameOrEmailAsync(driver.Email);
            driver = await _driverManager.GetByUserID(driveruser.Id);
            await _notificationhubContext.Clients.Group(driveruser.Tenant_Id).SendAsync("DriverLoggedOut", driver);
            var title = _localizer[Keys.Notifications.DriverHasBeenLoggedOut];
            var body = _localizer.Format(Keys.Notifications.DriverNameHasBeenLoggedOut, driver.FullName);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driver.UserId,
                ToUserId = driveruser.Tenant_Id,
                NotificationType = NotificationTypeWeb.DriverLoggedin
            });

            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driver.UserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.DriverLoggedin
                });
                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("DriverLoggedOut", driver);
            }
            return Ok(new { IsSuccess = true });
        }

        private async Task UpdateDriverLoginTracking(double Latitude, double Longitude, int driverId)
        {
            var driverLoggedIn = await _driverloginTrackingManager.GetLoggedIn(driverId);
            if (driverLoggedIn != null)
            {
                driverLoggedIn.LogoutDate = DateTime.UtcNow;
                driverLoggedIn.LogoutLatitude = Latitude;
                driverLoggedIn.LogoutLongitude = Longitude;
                driverLoggedIn.LogoutActionBy = _currentUser.UserName;
                await _driverloginTrackingManager.UpdateAsync(driverLoggedIn);
            }
        }

        [Route("GetProfile/{userId}")]
        [HttpGet]
        public async Task<IActionResult> GetProfile(string userId)
        {
            var res = await _driverManager.GetByUserID(userId);
            var str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (!string.IsNullOrEmpty(res.ImageUrl))
            {
                res.ImageUrl = $"{str}/DriverImages/{res.ImageUrl}";
            }
            return Ok(res);
        }

        [AllowAnonymous]
        [Route("LoginRequest")]
        [HttpPost]
        public async Task<IActionResult> LoginRequest([FromBody] AgentLoginRequestViewModel model)
        {
            dynamic returnDto = new JObject();
            var user = await _userManager.GetByUserNameOrEmailAsync(model.Email);
            if (user == null)
            {
                returnDto.succeeded = false;
                returnDto.error = _localizer[Keys.Validation.DriverUserNameNotFound];
                return Ok(returnDto);
            }

            var driver = await _driverManager.GetByUserID(user.Id);
            if (driver.AgentStatusId == (int)AgentStatusesEnum.Blocked)
            {
                returnDto.succeeded = false;
                returnDto.error = _localizer[Keys.Validation.DriverAccountBlocked];
                return Ok(returnDto);
            }
            var countUserDevices = _userDeviceManager.GetByUserId(user.Id);
            //check if user has pending request
            var loginPendingReques = await _driverLoginRequestManager.GetByPendingDriverId(driver.Id);
            if (loginPendingReques != null)
            {

                int expireHour = 24;
                var expireHourSetting = await _settingManager.GetSettingByKey("DriverLoginRequestExpiration");
                if (expireHourSetting != null) int.TryParse(expireHourSetting.Value, out expireHour);
                if (loginPendingReques.CreatedDate.AddHours(expireHour) <= DateTime.UtcNow)
                {
                    //expired 
                    foreach (var userDevice in countUserDevices)
                    {
                        _userDeviceManager.DeleteDevice(userDevice.FCMDeviceId);
                    }
                }
                else
                {
                    returnDto.succeeded = false;
                    returnDto.error = _localizer[Keys.Validation.DriverLoginRequestIsPending];
                    return Ok(returnDto);
                }
            }

            //check if user has active devices 
            if (countUserDevices.Count() != 0)
            {
                returnDto.succeeded = false;
                if (countUserDevices.FirstOrDefault().IsLoggedIn == true)
                {
                    returnDto.succeeded = false;
                    returnDto.error = _localizer[Keys.Validation.DriverAlreadyLoggedIn];
                    return Ok(returnDto);
                }
                else
                {
                    int expireHour = 24;
                    var expireHourSetting = await _settingManager.GetSettingByKey("DriverLoginRequestExpiration");
                    if (expireHourSetting != null) int.TryParse(expireHourSetting.Value, out expireHour);
                    if (loginPendingReques != null)
                    {
                        if (loginPendingReques.CreatedDate.AddHours(expireHour) <= DateTime.UtcNow)
                        {
                            //expired 
                            foreach (var userDevice in countUserDevices)
                            {
                                _userDeviceManager.DeleteDevice(userDevice.FCMDeviceId);
                            }
                        }
                        else
                        {
                            returnDto.succeeded = false;
                            returnDto.error = _localizer[Keys.Validation.DriverLoginRequestIsAlreadyApproved];
                            return Ok(returnDto);
                        }
                    }
                }
            }

            //check if user has looked 
            await _userDeviceManager.AddIfNotExistAsync(
                new UserDevice
                {
                    FCMDeviceId = model.FCMDeviceId,
                    UserId = user.Id,
                    Version = model.Version,
                    DeviceType = model.DeviceType,
                    IsLoggedIn = false
                });

            var driverLoginRequest = new CreateDriverLoginRequestViewModel
            {
                DriverId = driver.Id,
                Status = (int)DriverLoginRequestStatus.Pending,
                Token = ""
            };

            var result = await _driverLoginRequestManager.Create(driverLoginRequest);

            await _notificationhubContext.Clients.Group(user.Tenant_Id).SendAsync("LoginRequest", driver.Username);
            var title = _localizer[Keys.Notifications.DriverSentLoginRequest];
            var body = _localizer.Format(Keys.Notifications.DriverNameSentLoginRequest, driver.FullName);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driver.UserId,
                ToUserId = user.Tenant_Id,
                NotificationType = NotificationTypeWeb.LoginRequest
            });


            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driver.UserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.LoginRequest
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("LoginRequest", driver.Username);
            }

            returnDto.succeeded = true;
            returnDto.error = "";
            return Ok(returnDto);
        }


        [AllowAnonymous]
        [Route("CancelLoginRequest")]
        [HttpPost]
        public async Task<IActionResult> CancelLoginRequest([FromBody] AgentLoginRequestViewModel model)
        {
            var user = await _userManager.GetByUserNameOrEmailAsync(model.Email);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.DriverUserNameNotFound]);
            }
            var driver = await _driverManager.GetByUserID(user.Id);
            var deviceIds = _userDeviceManager.GetByUserId(user.Id);
            foreach (var device in deviceIds)
            {
                _userDeviceManager.DeleteDevice(device.FCMDeviceId);
            }
            var result = await _driverLoginRequestManager.CancelDriverRequest(driver.Id);
            await _notificationhubContext.Clients.Group(user.Tenant_Id).SendAsync("CancelLoginRequest", driver.Username);
            var title = _localizer[Keys.Notifications.DriverLoginRequestCanceled];
            var body = _localizer.Format(Keys.Notifications.DriverNameLoginRequestCanceled, driver.FullName);
            await _notificationManager.AddAsync(new NotificationViewModel
            {
                Title = title,
                Body = body,
                CreatedDate = DateTime.UtcNow,
                FromUserId = driver.UserId,
                ToUserId = user.Tenant_Id,
                NotificationType = NotificationTypeWeb.CancelLoginRequest
            });
            var DriverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
            foreach (var driverManager in DriverManagers)
            {
                await _notificationManager.AddAsync(new NotificationViewModel
                {
                    Title = title,
                    Body = body,
                    CreatedDate = DateTime.UtcNow,
                    FromUserId = driver.UserId,
                    ToUserId = driverManager.UserId,
                    NotificationType = NotificationTypeWeb.CancelLoginRequest
                });

                await _notificationhubContext.Clients.Group(driverManager.Username).SendAsync("CancelLoginRequest", driver.Username);
            }

            return Ok(result);
        }
    }
}