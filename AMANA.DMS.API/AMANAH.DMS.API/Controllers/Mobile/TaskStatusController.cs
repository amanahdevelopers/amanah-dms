﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize]
    public class TaskStatusController : BaseController
    {
        private readonly ITaskStatusManager TaskStatusManager;
        public TaskStatusController(ITaskStatusManager TaskStatusManager)
        {
            this.TaskStatusManager = TaskStatusManager;
        }

        // GET: TaskStatus
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All TaskStatuss 
            var AllTaskStatuss = await TaskStatusManager.GetAllAsync<TaskStatusViewModel>(true);
            return Ok(AllTaskStatuss);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All TaskStatuss By Pagination 
            var AllTeams = await TaskStatusManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }

    }
}