﻿using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    public class DriverTaskNotificationController : BaseController
    {
        private readonly IDriverTaskNotificationManager _driverTaskNotificationManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;

        public DriverTaskNotificationController(
            IDriverTaskNotificationManager driverTaskNotificationManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _driverTaskNotificationManager = driverTaskNotificationManager;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        [Route("GetByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetByPaginationAsync([FromBody] PaginatedTasksDriverViewModel pagingparametermodel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            pagingparametermodel.DriverId = _currentUser.DriverId.Value;
            var AllDriverTaskNotification = await _driverTaskNotificationManager.GetByPaginationAsync(pagingparametermodel);
            return Ok(AllDriverTaskNotification);
        }

        [Route("MarkAsRead")]
        [HttpGet]
        public async Task<ActionResult> MarkAsRead()
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            await _driverTaskNotificationManager.MarkAsRead(_currentUser.DriverId.Value);
            return Ok(_localizer[Keys.Messages.AllNotificationsMarkedAsRead]);
        }

        [Route("GetUnReadCount")]
        [HttpGet]
        public async Task<ActionResult> GetUnReadCount()
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var count = _driverTaskNotificationManager.GetUnReadCount(_currentUser.DriverId.Value);
            return Ok(count);
        }

        [Route("Clear")]
        [HttpGet]
        public async Task<ActionResult> Clear()
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            await _driverTaskNotificationManager.Clear(_currentUser.DriverId.Value);

            return Ok(_localizer[Keys.Messages.AllNotificationsCleared]);
        }
    }
}