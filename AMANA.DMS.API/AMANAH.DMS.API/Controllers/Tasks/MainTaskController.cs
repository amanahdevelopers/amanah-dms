﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class MainTaskController : Controller
    {
        private readonly IMainTaskManager _mainTaskManager;
        private readonly IUploadFileManager _uploadFileManager;
        private readonly IBranchManager _branchManager;
        private readonly ITeamsManager _teamsManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ISettingsManager _settingManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly ITasksManager _tasksManager;

        public MainTaskController(
            IMainTaskManager MainTaskManager,
            IUploadFileManager uploadFileManager,
            IBranchManager _branchManager,
            ITeamsManager teamsManager,
            IAccountLogManager accountLogManager,
            ITasksManager TasksManager,
            ISettingsManager settingManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _mainTaskManager = MainTaskManager;
            _uploadFileManager = uploadFileManager;
            this._branchManager = _branchManager;
            _teamsManager = teamsManager;
            _accountLogManager = accountLogManager;
            _settingManager = settingManager;
            _localizer = localizer;
            _currentUser = currentUser;
            _tasksManager = TasksManager;
        }

        // GET: MainTask
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All MainTask 
            var AllMainTask = await _mainTaskManager.GetAllAsync<MainTaskViewModel>();
            return Ok(AllMainTask);
        }

        // GET: MainTask Pagination
        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All MainTask By Pagination 
            var AllMainTask = await _mainTaskManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllMainTask);
        }

        // GET: MainTask/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var mainTask = await _mainTaskManager.Get(id);
            return Ok(mainTask);
        }

        // POST: MainTask/Create
        [HttpPost]
        [Route("TaskHasGeofences")]
        public async Task<ActionResult> TaskHasGeofences([FromBody] MainTaskViewModel mainTask)
        {
            try
            {
                var hasGeofences = await _mainTaskManager.TaskHasGeofences(mainTask);
                return Ok(new { hasGeofences });
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }

        // POST: MainTask/Create
        [HttpPost, DisableRequestSizeLimit]
        [Route("Create")]
        [Authorize(ManagerPermissions.Task.CreateTask)]
        public async Task<ActionResult> CreateAsync([FromBody] AssignDriverMainTaskViewModel assignDriverMainTaskVM)
        {
            foreach (var task in assignDriverMainTaskVM.MainTask.Tasks)
            {
                if (!string.IsNullOrEmpty(task.Image))//Image Base64
                {
                    var path = "TasksImages";
                    UploadFile uploadFile = new UploadFile()
                    {
                        FileContent = task.Image,
                        FileName = "Image.jpg"
                    };

                    var processResult = _uploadFileManager.AddFile(uploadFile, path);
                    if (processResult.IsSucceeded)
                    {
                        task.Image = processResult.returnData;
                    }
                    else
                    {
                        return BadRequest(_localizer[Keys.Validation.CanNotUploadTaskImage]);
                    }
                }
            }
            var res = await _mainTaskManager.AssignMainTaskAsync(assignDriverMainTaskVM);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "MainTask",
                ActivityType = "Create",
                Description = "Main Task ( " + res.MainTask.Id + " )  has been created ",
                Record_Id = assignDriverMainTaskVM.MainTask.Id

            };
            await _accountLogManager.Create(accountLog);
            return StatusCode((int)res.StatusCode, res);
        }

        [Route("GetUnassigned")]
        [HttpPost]
        [Authorize(ManagerPermissions.Task.ReadUnassignedTask)]
        public async Task<ActionResult> GetUnassignedAsync([FromBody] PaginatedTasksViewModel pagingparametermodel)
        {
            var enableAutoallocationSetting = await _settingManager.GetSettingByKey("IsEnableAutoAllocation");
            int autoAllocationType = await _settingManager.GetAutoAllocationTypeidAsync();

            PagedResult<StattusMainTaskViewModel> unAssignedMainTasks = null;
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
                if (pagingparametermodel.BranchesIds == null || !pagingparametermodel.BranchesIds.Any())
                {
                    var managerBranchIds = await _branchManager.GetManagerBranchesAsync();
                    pagingparametermodel.BranchesIds = managerBranchIds;
                    pagingparametermodel.GetCustomerTasks = true;
                }
                unAssignedMainTasks = await _mainTaskManager.GetUnassignedAsync(pagingparametermodel);

            }
            else
            {
                unAssignedMainTasks = await _mainTaskManager.GetUnassignedAsync(pagingparametermodel);
            }
            if (unAssignedMainTasks != null)
                foreach (var MainTask in unAssignedMainTasks.Result)
                {
                    MainTask.IsEnableAutoAllocation = enableAutoallocationSetting.Value.ToLower() != "false";
                    MainTask.AutoAllocationType = autoAllocationType;
                    MainTask.Tasks.ForEach(T =>
                    {
                        T.TaskHistories = T.TaskHistories.OrderByDescending(x => x.CreatedDate).ToList();
                    });
                }

            return Ok(unAssignedMainTasks);
        }

        [Route("GetAssigned")]
        [HttpPost]
        public async Task<ActionResult> GetAssignedAsync([FromBody] PaginatedTasksViewModel pagingparametermodel)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                    return Ok();
                if (pagingparametermodel.BranchesIds == null || !pagingparametermodel.BranchesIds.Any())
                {
                    var managerBranchIds = await _branchManager.GetManagerBranchesAsync();
                    pagingparametermodel.BranchesIds = managerBranchIds;
                    pagingparametermodel.GetCustomerTasks = true;
                }
                var ManagerMainTasks = await _mainTaskManager.GetAssignedAsync(pagingparametermodel, managerTeamIds);
                return Ok(ManagerMainTasks);
            }
            var AllMainTasks = await _mainTaskManager.GetAssignedAsync(pagingparametermodel);

            foreach (var MainTask in AllMainTasks.Result)
            {
                MainTask.Tasks.ForEach(T => { T.TaskHistories = T.TaskHistories.OrderByDescending(x => x.CreatedDate).ToList(); });

            }

            return Ok(AllMainTasks);
        }

        [Route("GetCompleted")]
        [HttpPost]
        public async Task<ActionResult> GetCompletedAsync([FromBody] PaginatedTasksViewModel pagingparametermodel)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                    return Ok();

                if (pagingparametermodel.BranchesIds == null || !pagingparametermodel.BranchesIds.Any())
                {
                    var managerBranchIds = await _branchManager.GetManagerBranchesAsync();
                    pagingparametermodel.BranchesIds = managerBranchIds;
                    pagingparametermodel.GetCustomerTasks = true;
                }
                var ManagerMainTasks = await _mainTaskManager.GetCompletedAsync(pagingparametermodel, managerTeamIds);
                return Ok(ManagerMainTasks);
            }
            var AllMainTasks = await _mainTaskManager.GetCompletedAsync(pagingparametermodel);

            foreach (var MainTask in AllMainTasks.Result)
            {
                MainTask.Tasks.ForEach(T => { T.TaskHistories = T.TaskHistories.OrderByDescending(x => x.CreatedDate).ToList(); });

            }

            return Ok(AllMainTasks);
        }

        [Route("ReassignMainTask")]
        [HttpPost]
        public async Task<ActionResult> ReassignMainTaskAsync([FromBody] ReassignDriverMainTaskViewModel reassignDriverMainTaskVM)
        {
            await _mainTaskManager.ReassignMainTaskAsync(reassignDriverMainTaskVM);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "MainTask",
                ActivityType = "Reassign",
                Description = "Main Task ( " + reassignDriverMainTaskVM.MainTaskId + " )  has been reassigned ",
                Record_Id = reassignDriverMainTaskVM.MainTaskId

            };
            await _accountLogManager.Create(accountLog);

            return Ok();
        }

        [Route("GetDriverTasks")]
        [HttpPost]
        public async Task<ActionResult> GetDriverTasks([FromBody] PaginatedTasksDriverViewModel paginatedTasksDriverViewModel)
        {
            var result = await _mainTaskManager.GetDriverUncompletedTasks(paginatedTasksDriverViewModel);
            return Ok(result);
        }

        [Route("GetConnectedTasks")]
        [HttpPost]
        public async Task<ActionResult> GetConnectedTasksAsync([FromBody] PaginatedTasksViewModel pagingparametermodel)
        {
            List<int> managerTeamIds = null;
            if (_currentUser.UserType == RoleType.Manager)
            {
                managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }

                if (pagingparametermodel.BranchesIds == null || !pagingparametermodel.BranchesIds.Any())
                {
                    var managerBranchIds = await _branchManager.GetManagerBranchesAsync();
                    pagingparametermodel.BranchesIds = managerBranchIds;
                    pagingparametermodel.GetCustomerTasks = true;
                }
            }
            var AllMainTasks = await _mainTaskManager.GetMainTasksByAsync(pagingparametermodel, managerTeamIds);
            return Ok(AllMainTasks);
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var mainTaskVM = await _mainTaskManager.Get(id);
            if (mainTaskVM != null)
            {
                foreach (var Task in mainTaskVM.Tasks)
                {
                    await this._tasksManager.SoftDeleteAsync(Task);
                }
                var result = await _mainTaskManager.SoftDeleteAsync(mainTaskVM);
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteTask]);
            }
        }

        // GET: MainTask/Details/5
        [Route("TryAutoAssignmentAgain/{id}")]
        [HttpGet]
        public async Task<ActionResult> TryAutoAssignmentAgainAsync(int id)
        {
            await _mainTaskManager.TryAutoAssignmentAgain(id);
            return Ok();
        }


    }
}