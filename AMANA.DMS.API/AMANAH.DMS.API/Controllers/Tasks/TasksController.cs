﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.DATA.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TasksController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IMainTaskManager _mainTaskManager;
        private readonly ITasksManager _tasksManager;
        private readonly ITaskStatusManager _taskStatusManager;
        private readonly IUploadFileManager _uploadFileManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ITeamsManager _teamsManager;
        private readonly IBranchManager _branchManager;
        private readonly ICurrentUser _currentUser;
        private readonly ILocalizer _localizer;

        public TasksController(
            ITasksManager tasksManager,
            ITaskStatusManager taskStatusManager,
            IMapper mapper,
            IMainTaskManager mainTaskManager,
            IUploadFileManager uploadFileManager,
            ITeamsManager teamsManager,
            IBranchManager branchManager,
            IAccountLogManager accountLogManager,
            ICurrentUser currentUser,
            ILocalizer localizer)
        {
            _mainTaskManager = mainTaskManager;
            _tasksManager = tasksManager;
            _taskStatusManager = taskStatusManager;
            _mapper = mapper;
            _uploadFileManager = uploadFileManager;
            _teamsManager = teamsManager;
            _branchManager = branchManager;
            _accountLogManager = accountLogManager;
            _currentUser = currentUser;
            _localizer = localizer;
        }

        // GET: Tasks
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Tasks 
            var AllTasks = await _tasksManager.GetAllAsync<TasksViewModel>();
            return Ok(AllTasks);
        }

        // GET: Tasks Pagination
        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Tasks By Pagination 
            var AllTasks = await _tasksManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllTasks);
        }

        // GET: Tasks/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var task = await _tasksManager.Get(id);
            return Ok(task);
        }

        // POST: Tasks/Create
        [HttpPost, DisableRequestSizeLimit]
        [Route("Create")]
        [Authorize(ManagerPermissions.Task.CreateTask)]
        public async Task<ActionResult> CreateAsync([FromBody] TasksViewModel taskVM)
        {

            if (_mainTaskManager.IsCompleted(taskVM.MainTaskId))
            {
                return BadRequest(_localizer[Keys.Validation.CanNotAddTaskToCompletedMainTask]);
            }
            var tasksToCreate = _mapper.Map<TasksViewModel>(taskVM);
            if (!string.IsNullOrEmpty(taskVM.Image))//Image Base64
            {
                var path = "TasksImages";
                UploadFile uploadFile = new UploadFile()
                {
                    FileContent = taskVM.Image,
                    FileName = "Image.jpg"
                };

                var processResult = _uploadFileManager.AddFile(uploadFile, path);
                if (processResult.IsSucceeded)
                {
                    tasksToCreate.Image = processResult.returnData;
                }
                else
                {
                    return BadRequest(processResult.Exception);
                }
            }
            var created_task = await _tasksManager.AddAsync(tasksToCreate);


            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Tasks",
                ActivityType = "Create",
                Description = "Task ( " + (await _tasksManager.GetAllAsync<TasksViewModel>()).Max(x => x.Id) + " ) has been created ",
                Record_Id = created_task.Id

            };
            await _accountLogManager.Create(accountLog);

            return Ok(created_task);
        }

        // POST: Tasks/Edit/5
        [HttpPut, DisableRequestSizeLimit]
        [Route("Update")]
        [Authorize(ManagerPermissions.Task.UpdateTask)]
        public async Task<ActionResult> UpdateAsync([FromBody] TasksViewModel taskVM)
        {
            if (!string.IsNullOrEmpty(taskVM.Image))//Image Base64
            {
                var path = "TasksImages";
                UploadFile uploadFile = new UploadFile()
                {
                    FileContent = taskVM.Image,
                    FileName = "Image.jpg"
                };

                var processResult = _uploadFileManager.AddFile(uploadFile, path);
                if (processResult.IsSucceeded)
                {
                    taskVM.Image = processResult.returnData;
                }
                else
                {
                    return BadRequest(processResult.Exception);
                }
            }
            var updated_task = await _tasksManager.UpdateAsync(taskVM);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Tasks",
                ActivityType = "Update",
                Description = "Task ( " + taskVM.Id + " ) has been updated ",
                Record_Id = taskVM.Id

            };
            await _accountLogManager.Create(accountLog);
            return Ok(updated_task);
        }

        // GET: Tasks/Delete/5
        [HttpDelete("Delete/{id}")]
        [Authorize(ManagerPermissions.Task.DeleteTask)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var taskToDelete = await _tasksManager.Get(id);
            var result = await _tasksManager.SoftDeleteAsync(taskToDelete);
            if (taskToDelete.DriverId != null)
            {
                await _tasksManager.SendPushNotificationToDriver(
                    (int)taskToDelete.DriverId,
                    _localizer.Format(Keys.Notifications.TaskNoHasBeenDeletedFromYou, taskToDelete.Id),
                    new
                    {
                        notificationType = NotificationTypeEnum.ReassignTask
                    });
            }
            await _tasksManager.MakeDriverAvailable(taskToDelete.DriverId ?? 0);

            if (result)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Tasks",
                    ActivityType = "Delete",
                    Description = "Task ( " + taskToDelete.Id + " ) has been deleted ",
                    Record_Id = taskToDelete.Id
                };

                await _accountLogManager.Create(accountLog);
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteTask]);
            }
        }

        [HttpPut("ChangeStatus")]
        [Authorize(ManagerPermissions.Task.ChangeTaskStatus)]
        public async Task<ActionResult> ChangeStatus([FromBody] TaskChangeStatusViewModel StatusVM)
        {
            List<TasksViewModel> tasksToChange = new List<TasksViewModel>();
            var selectedtask = await _tasksManager.Get(StatusVM.Id);
            var OldStateID = (int)selectedtask.TaskStatusId;
            if (StatusVM.IsChangeConnectedTasks)
            {
                var mainTaskVM = await _mainTaskManager.Get(selectedtask.MainTaskId);
                tasksToChange = mainTaskVM.Tasks;
            }
            else
            {
                tasksToChange.Add(selectedtask);
            }
            foreach (var task in tasksToChange)
            {
                await _tasksManager.ChangeStatus(task.Id, StatusVM.StatusId, StatusVM.reason);
            }

            var oldstatus = await _taskStatusManager.Get(OldStateID);
            var Newstatus = await _taskStatusManager.Get(StatusVM.StatusId);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Tasks",
                ActivityType = "Change Status",
                Description = "Task ( " + StatusVM.Id + " ) status has been changed "
                            + "from ( " + oldstatus.Name + " ) To ( " + Newstatus.Name + " )",
                Record_Id = StatusVM.Id

            };
            await _accountLogManager.Create(accountLog);
            return Ok();
        }

        [Route("GetByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetByPaginationAsync([FromBody] PaginatedTasksViewModel pagingparametermodel)
        {
            ///Get All Tasks By Pagination 
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
                if (pagingparametermodel.BranchesIds == null || !pagingparametermodel.BranchesIds.Any())
                {
                    var managerBranchIds = await _branchManager.GetManagerBranchesAsync();
                    pagingparametermodel.BranchesIds = managerBranchIds;
                    pagingparametermodel.GetCustomerTasks = true;
                }
                var ManagerTasks = await _tasksManager.GetByPaginationAsync(pagingparametermodel, managerTeamIds);
                return Ok(ManagerTasks);
            }
            var AllTasks = await _tasksManager.GetByPaginationAsync(pagingparametermodel);
            return Ok(AllTasks);
        }

        [Route("ReasignDriverTasks")]
        [HttpPost]
        public async Task<ActionResult> ReasignDriverTasksAsync([FromBody] ReasignDriverViewModel reasignDriverViewModel)
        {
            if (reasignDriverViewModel.TaskStatusIds == null || !reasignDriverViewModel.TaskStatusIds.Any())
            {
                return BadRequest(_localizer[Keys.Validation.TaskStatusIsRequired]);
            }
            await _tasksManager.ReasignDriverTasksAsync(reasignDriverViewModel);
            foreach (var taskID in reasignDriverViewModel.TaskIds)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Tasks",
                    ActivityType = "Reasign Driver",
                    Description = "Task ( " + taskID + " ) has been reasign from driver ( "
                        + reasignDriverViewModel.OldDriverId + " ) To driver ( " + reasignDriverViewModel.NewDriverId + " )",
                    Record_Id = taskID
                };

                await _accountLogManager.Create(accountLog);
            }




            return Ok();
        }

        [Route("GetDriverTimeLine")]
        [HttpPost]
        public async Task<ActionResult> GetDriverTimeLineAsync(
            [FromBody] PaginatedDriverTimeLineViewModel paginatedDriverTimeLineViewModel)
        {
            var histories = await _tasksManager.GetDriverTimeLineAsync(paginatedDriverTimeLineViewModel);
            return Ok(histories);
        }

        [Route("GetTasksReport")]
        [HttpPost]
        public async Task<ActionResult> GetTasksReportAsync([FromBody] PaginatedTasksReportViewModel pagingparametermodel)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                if (pagingparametermodel.BranchIds == null || !pagingparametermodel.BranchIds.Any())
                {
                    var managerBranchIds = await _branchManager.GetManagerBranchesAsync();
                    pagingparametermodel.BranchIds = managerBranchIds;
                    pagingparametermodel.GetCustomerTasks = true;
                }
            }
            var AllTasks = await _tasksManager.GetTasksReportAsync(pagingparametermodel);
            return Ok(AllTasks);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> ExportWithoutProgressToExcel(
            [FromBody] PaginatedTasksReportViewModel pagingparametermodel)
        {
            var data = await _tasksManager.ExportTasksWithoutProgressToExcelAsync(pagingparametermodel);
            if (data == null || !data.Any())
            {
                return Ok();
            }

            var path = "ImportTemplates";
            var fileName = "TaskReport.xlsx";
            string filePath = Path.Combine(path, fileName);

            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult> ExportWithProgressToExcel([FromBody] PaginatedTasksReportViewModel pagingparametermodel)
        {
            var data = await _tasksManager.ExportTasksWithProgressToExcelAsync(pagingparametermodel);
            if (data == null || !data.Any())
            {
                return Ok();
            }

            var path = "ImportTemplates";
            var fileName = "TaskReportwithprogress.xlsx";
            string filePath = Path.Combine(path, fileName);

            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportTaskToExcel(int id)
        {
            var data = await _tasksManager.ExportTaskToExcelAsync(id);
            var path = "ImportTemplates";
            var fileName = "Tasks.xlsx";
            string filePath = Path.Combine(path, fileName);

            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);

        }


        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetTaskStatusCountAsync(DateTime taskDate)
        {
            var taskStatus = await _tasksManager.GetTaskStatusCountAsync(taskDate);
            return Ok(new { IsSuccessful = true, data = taskStatus });
        }
    }
}