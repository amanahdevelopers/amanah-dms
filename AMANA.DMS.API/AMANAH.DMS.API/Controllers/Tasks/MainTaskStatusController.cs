﻿using System;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MainTaskStatusController : Controller
    {
        private readonly IMainTaskStatusManager _mainTaskStatusManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;

        public MainTaskStatusController(
            IMainTaskStatusManager MainTaskStatusManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer)
        {
            _mainTaskStatusManager = MainTaskStatusManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
        }

        // GET: MainTaskStatus
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All MainTaskStatuss 
            var AllMainTaskStatuss = await _mainTaskStatusManager.GetAllAsync<LkpViewModel>(true);
            return Ok(AllMainTaskStatuss);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All MainTaskStatuss By Pagination 
            var AllTeams = await _mainTaskStatusManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }


      
    }
}