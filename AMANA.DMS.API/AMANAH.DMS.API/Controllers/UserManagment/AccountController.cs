﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        protected readonly IApplicationUserManager _userManager;
        protected readonly ISettingsManager _settingsManager;
        private readonly ILocalizer _localizer;

        public AccountController(IApplicationUserManager userManager, ISettingsManager settingsManager, ILocalizer localizer)
        {
            _userManager = userManager;
            _settingsManager = settingsManager;
            _localizer = localizer;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel model)
        {
            var existUser = await _userManager.GetByEmailAsync(model.Email);
            if (existUser != null)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }
            var res = await _userManager.AddTenantAsync(model);
            if (res.Succeeded == false)
            {
                if (res.Errors.Count > 0)
                {
                    return BadRequest(res.Errors.FirstOrDefault()?.Description);
                }
            }
            await _settingsManager.AddUserDefaultSettingsAsync(res.User.Id);

            return Ok(res);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            var res = await _userManager.LoginAsync(model);
            if (res.Errors.Any())
                return BadRequest(res.Errors.FirstOrDefault()?.Description);

            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest model)
        {
            var res = await _userManager.ForgotPassword(model.Email);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequest model)
        {
            var res = await _userManager.ResetPasswordAsync(model.Email, model.Token, model.NewPassword);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> CheckResetToken([FromBody] CheckResetTokenRequest model)
        {
            var res = await _userManager.CheckResetToken(model.Email, model.Token);

            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }
    }
}