﻿using System;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Authorize]
    [Route("api/AgentAccessControl")]

    public class AgentAccessControlController : Controller
    {
        private readonly IAgentAccessControlManager _agentAcceessControlManager;
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ILocalizer _localizer;
        private readonly IAccountLogManager _accountLogManager;

        public AgentAccessControlController(
            IAgentAccessControlManager agentAcceessControlManager,
            IAccountLogManager accountLogManager,
            IApplicationRoleManager applicationRoleManager,
            ILocalizer localizer)
        {
            _agentAcceessControlManager = agentAcceessControlManager;
            _applicationRoleManager = applicationRoleManager;
            _localizer = localizer;
            _accountLogManager = accountLogManager;
        }

        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(string roleName)
        {
            return Ok(await _agentAcceessControlManager.Get(roleName));
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _agentAcceessControlManager.GetAll());
        }

        [Route("GetAllPermissions")]
        [HttpGet]
        public async Task<IActionResult> GetAllPermissions()
        {
            return Ok(await _agentAcceessControlManager.GetAllPermissions());
        }

        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] AgentAccessControlViewModel model)
        {
            await _agentAcceessControlManager.Update(model);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "AgentAccessControl",
                ActivityType = "Update",
                Description = model.RoleName + " has been updated ",
                Record_Id = 0

            };
            await _accountLogManager.Create(accountLog);
            return Ok();
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AgentAccessControlViewModel model)
        {
            if (await _applicationRoleManager.IsRoleExistAsync(model.RoleName.Trim()))
            {
                return BadRequest(_localizer[Keys.Validation.RoleAlreadyExists]);
            }
            else
            {
                await _agentAcceessControlManager.Create(model);

                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "AgentAccessControl",
                    ActivityType = "Create",
                    Description = model.RoleName + " has been created ",
                    Record_Id = 0

                };
                await _accountLogManager.Create(accountLog);
                return Ok();
            }
        }


        [Route("Delete/{roleName}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] string roleName)
        {
            await _agentAcceessControlManager.Delete(roleName);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "AgentAccessControl",
                ActivityType = "Delete",
                Description = roleName + " has been deleted ",
                Record_Id = 0

            };
            await _accountLogManager.Create(accountLog);
            return Ok();
        }
    }
}