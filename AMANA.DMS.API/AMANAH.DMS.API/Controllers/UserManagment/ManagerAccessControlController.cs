﻿using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Route("api/ManagerAccessControl")]
    [Authorize]
    public class ManagerAccessControlController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly IManagerAccessControlManager _managerAccessControlManager;
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ILocalizer _localizer;

        public ManagerAccessControlController(
            IManagerAccessControlManager managerAccessControlManager,
            IAccountLogManager accountLogManager,
            IApplicationRoleManager applicationRoleManager,
            ILocalizer localizer)
        {
            _managerAccessControlManager = managerAccessControlManager;
            _applicationRoleManager = applicationRoleManager;
            _localizer = localizer;
            _accountLogManager = accountLogManager;
        }

        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(string roleName)
        {
            return Ok(await _managerAccessControlManager.Get(roleName));
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _managerAccessControlManager.GetAll());
        }

        [Route("GetAllPermissions")]
        [HttpGet]
        public async Task<IActionResult> GetAllPermissions()
        {
            return Ok(await _managerAccessControlManager.GetAllPermissions());
        }

        [Route("Update")]
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ManagerAccessControlViewModel model)
        {
            await _managerAccessControlManager.Update(model);

            var modelFromDB = await _managerAccessControlManager.Get(model.RoleName);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "ManagerAccessControl",
                ActivityType = "Update",
                Description = $"{model.RoleName} has been updated with Id {modelFromDB.Id}",
                //Record_Id = modelFromDB.Id

            };
            await _accountLogManager.Create(accountLog);
            return Ok();
        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ManagerAccessControlViewModel model)
        {
            if (await _applicationRoleManager.IsRoleExistAsync(model.RoleName.Trim()))
            {
                return BadRequest(_localizer[Keys.Validation.RoleAlreadyExists]);
            }
            else
            {
                await _managerAccessControlManager.Create(model);
                var modelFromDB = await _managerAccessControlManager.Get(model.RoleName);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "ManagerAccessControl",
                    ActivityType = "Create",
                    Description = $"{model.RoleName} has been created with Id {modelFromDB.Id}",
                    //Record_Id = modelFromDB.Id

                };
                await _accountLogManager.Create(accountLog);

                return Ok(true);
            }
        }


        [Route("Delete/{roleName}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] string roleName)
        {
            await _managerAccessControlManager.Delete(roleName);
            var modelFromDB = _managerAccessControlManager.Get(roleName);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "ManagerAccessControl",
                ActivityType = "Delete",
                Description = $"{roleName} has been deleted with Id {modelFromDB.Id}",
                //Record_Id = modelFromDB.Id

            };
            await _accountLogManager.Create(accountLog);
            return Ok();
        }
    }
}