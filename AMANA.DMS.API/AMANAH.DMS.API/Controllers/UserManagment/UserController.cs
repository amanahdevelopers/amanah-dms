﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModel;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Account;
using AMANAH.DMS.BLL.ViewModels.Admin;
using AMANAH.DMS.BLL.ViewModels.BaseResult;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Settings;
using AMANAH.DMS.ViewModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Utilites;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/User")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUserManager _appUserManager;
        private readonly AppSettings _appSettings;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly IAdminManager _adminManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;
        private readonly IBranchManager _branchManager;
        private readonly ICurrentUser _currentUser;

        public UserController(
            IMapper mapper,
            UserManager<ApplicationUser> userManager,
            IApplicationUserManager appuserManager,
            IOptions<AppSettings> appSettings,
            IUserDeviceManager userDeviceManager,
            IAdminManager adminManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer,
            IBranchManager branchManager,
            ICurrentUser currentUser)
        {
            _mapper = mapper;
            _userManager = userManager;
            _appUserManager = appuserManager;
            _appSettings = appSettings.Value;
            _userDeviceManager = userDeviceManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
            _adminManager = adminManager;
            _branchManager = branchManager;
            _currentUser = currentUser;
        }

        #region DefaultCrudOperation
        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationUserViewModel model)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            ApplicationUser entityResult = new ApplicationUser();
            entityResult = _mapper.Map<ApplicationUserViewModel, ApplicationUser>(result);
            entityResult = await _appUserManager.GetAsync(entityResult);
            result = _mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
            return Ok(result);
        }

        [Route("IsAllowToDeleteAdmin")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> IsAllowToDeleteAdmin()
        {
            if (await _appUserManager.IsAllowToDeleteAdmin())
            {
                return Ok(true);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteAdmin]);
            }
        }

        [Route("GetAll")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetAll()
        {
            var entityResult = await _appUserManager.GetAll();
            var result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetByUserIds")]
        [HttpPost]
        [Authorize]
        public List<ApplicationUserViewModel> GetByUserIds([FromBody] List<string> userIds)
        {
            var entityResult = _appUserManager.GetByUserIds(userIds);
            var result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

        [Route("GetTechnicansExceptUserIds")]
        [HttpPost]
        [Authorize]
        public async Task<List<ApplicationUserViewModel>> GetTechnicansExceptUserIds([FromBody] List<string> userIds)
        {
            var entityResult = await _appUserManager.GetByRole(_appSettings.TechnicanRole, userIds);
            var result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }
        [Route("GetAllExceptUserIds")]
        [HttpGet]
        [Authorize]
        public List<ApplicationUserViewModel> GetByRoleExcept([FromBody] List<string> userIds)
        {
            List<ApplicationUser> entityResult = _appUserManager.GetAllExcept(userIds);
            List<ApplicationUserViewModel> result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }
        [Route("GetTechnicans")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetTechnicans()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetByRole(_appSettings.TechnicanRole);
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatchers")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetDispatchers()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetByRole(_appSettings.DispatcherRole);
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatcherSupervisor")]
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetDispatcherSupervisor()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetByRole(_appSettings.SupervisorDispatcherRole);
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        [HttpGet]
        [Route("UserRoles/{username}")]
        [Authorize]
        public async Task<ApplicationUserViewModel> UserRoles([FromRoute] string username, string authHeader = null)
        {
            ApplicationUserViewModel result = null;
            if (Request != null && authHeader == null)
            {
                authHeader = Helper.GetValueFromRequestHeader(Request);
            }
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrWhiteSpace(username))
            {
                var user = await _appUserManager.GetByEmailAsync(username);
                if (user != null)
                {
                    user.RoleNames = (await _appUserManager.GetRolesAsync(username)).ToList();
                    result = _mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    return result;
                }

            }
            return result;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        [Authorize]

        public async Task<IActionResult> Update([FromBody] EditApplicationUserViewModel model)
        {
            if (await _appUserManager.IsUserNameExistAsync(model.UserName, model.Id))
            {
                return BadRequest(_localizer.Format(Keys.Validation.UserNameAlreadyExists, model.UserName));
            }
            bool result = false;
            if (ModelState.IsValid)
            {
                var entityResult = _mapper.Map<EditApplicationUserViewModel, ApplicationUser>(model);
                result = await _appUserManager.UpdateUserAsync(entityResult);
            }
            return Ok(result);
        }

        #region Update User Password

        //private async Task<bool> UpdateUserPassword(string userId, string newPassword, string modifiedByUserId)
        //{
        //    ApplicationUser applicationUser = await _appUserManager.GetByUserIdAsync(userId);

        //    if (applicationUser == null)
        //    {
        //        return false;
        //    }

        //    applicationUser.PasswordHash = _appUserManager.HashPassword(applicationUser, newPassword);
        //    applicationUser.UpdatedBy_Id = modifiedByUserId;
        //    applicationUser.UpdatedDate = DateTime.UtcNow;

        //    IdentityResult identityResult = await _appUserManager.UpdateAsync(applicationUser);

        //    return identityResult.Succeeded;
        //}

        #endregion Update User Password

        #region DeleteApi

        [Route("Delete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] string username)
        {
            var entity = await _appUserManager.GetByEmailAsync(username);

            entity.UserName = entity.UserName + "_" + "Deleted_" + DateTime.UtcNow.ToString("yy_MM_dd_mm_ss");
            var result = await _appUserManager.SoftDeleteAsync(entity);
            return Ok(result);
        }

        [Route("ToggleDelete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Lock([FromRoute] string username)
        {
            var entity = await _appUserManager.GetByEmailAsync(username);
            entity.IsLocked = !entity.IsLocked;
            var identity = await _appUserManager.UpdateAsync(entity);
            return Ok(identity.Succeeded);
        }
        #endregion
        #endregion

        [HttpGet]
        [Route("UsernameExists/{username}")]
        [Authorize]

        public async Task<IActionResult> UsernameExists([FromRoute] string username)
        {
            bool result = await _appUserManager.IsUserNameExistAsync(username);
            return Ok(result);
        }

        [HttpGet]
        [Route("EmailExists/{email}")]
        [Authorize]
        public async Task<IActionResult> EmailExists([FromRoute] string email)
        {
            bool result = await _appUserManager.IsEmailExistAsync(email);
            return Ok(result);
        }

        [HttpPost]
        [Route("AssignUserToRole")]
        [Authorize]
        public async Task<IActionResult> AssignUserToRole([FromBody] AssignRoleViewModel assignRoleViewModel)
        {

            bool result = await _appUserManager.AddUserToRoleAsync(assignRoleViewModel.UserName, assignRoleViewModel.RoleName);
            return Ok(result);
        }





        [HttpDelete, Route("DeleteDevice")]
        public IActionResult DeleteDevice([FromQuery] string token)
        {
            var result = _userDeviceManager.DeleteDevice(token);
            return Ok(result);
        }

        [HttpPost]
        [Route("ChangePassword")]
        [Authorize]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordViewModel changePasswordViewModel)
        {
            var UserName = _currentUser.UserName;
            string UserId;
            if (changePasswordViewModel.userId == null)
            {
                UserId = _currentUser.Id;
            }
            else
            {
                UserId = changePasswordViewModel.userId;
            }
            if (string.IsNullOrEmpty(UserId))
            {
                return Unauthorized();
            }
            if (!string.IsNullOrEmpty(changePasswordViewModel.currentPassword))
            {
                var loginResult = await _appUserManager.LoginAsync(
                    new LoginViewModel
                    {
                        Email = UserName,
                        Password = changePasswordViewModel.currentPassword
                    });
                if (!loginResult.Succeeded)
                {
                    return BadRequest(_localizer[Keys.Validation.InCorrectPassword]);
                }
            }
            var user = await _appUserManager.GetByUserIdAsync(UserId);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
            bool result = await _appUserManager.ChangePasswordAsync(user, changePasswordViewModel.newPassword);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Users",
                ActivityType = "Change Password",
                Description = "User  ( " + user.FirstName + " " + user.LastName + " ) password has been changed ",
                Record_Id = 0

            };
            await _accountLogManager.Create(accountLog);

            var res = new EntityResult<string>
            {
                result = _localizer[Keys.Messages.PasswordChangedSuccessfully]
            };
            return Ok(res);
        }

        [HttpPost]
        [Route("UpdateLanguage")]
        public async Task<IActionResult> UpdateLanguage([FromBody] AdminLanguageViewModel adminLanguageViewModel)
        {
            var user = await _appUserManager.GetByUserIdAsync(_currentUser.Id);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
            var result = await _adminManager.UpdateLanguageAsync(adminLanguageViewModel, user.Id);

            if (result == null)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotUpdateLanguage]);
            }
            var res = new EntityResult<string>
            {
                result = _localizer[Keys.Messages.LanguageUpdateSuccessfully]
            };
            return Ok(res);

        }
        [HttpPost]
        [Route("UpdateProfile")]
        public async Task<IActionResult> UpdateProfileInfo([FromBody] AdminFullProfile adminFullProfile)
        {

            var viewAdmin = _mapper.Map<AdminFullProfile, AdminProfileViewModel>(adminFullProfile);
            var viewUser = _mapper.Map<AdminFullProfile, UserInfoViewModel>(adminFullProfile);
            ApplicationUser existUser = await _appUserManager.GetByEmailAsync(adminFullProfile.Email);

            if (existUser != null && existUser.Id != viewAdmin.Id)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }
            var admin = await _adminManager.GetAsync(viewAdmin.Id);
            var updateadminresult = false; 
            if (admin != null)
            {
                // Update Admin
                admin.CompanyAddress = viewAdmin.CompanyAddress;
                admin.CompanyName = viewAdmin.CompanyName;
                admin.DisplayImage = viewAdmin.DisplayImage;
                admin.ResidentCountryId = viewAdmin.ResidentCountryId;
                updateadminresult = await _adminManager.UpdateAsync(admin);
            }
            else
            {
                var newAdmin = new AdminViewModel
                {
                    Id = viewUser.Id
                };
                await _adminManager.AddAsync(newAdmin);
            }
            ApplicationUser User = await _appUserManager.GetByUserIdAsync(viewUser.Id);
            // Update user
            User.PhoneNumber = viewUser.phone;
            User.FirstName = viewUser.FirstName;
            User.CountryId = viewUser.CountryId;
            User.Email = viewUser.Email;
            var identity = await _appUserManager.UpdateAsync(User);
            if (identity.Succeeded)
            {
                var res = new EntityResult<AdminFullProfile>
                {
                    result = adminFullProfile
                };
                return Ok(res);
            }
            return BadRequest(_localizer[Keys.Validation.CanNotUpdateProfileData]);

        }
        [HttpPost]
        [Route("DeleteAccount")]
        public async Task<IActionResult> DeleteAccount([FromBody] AccountToDeleteViewModel accountToDeleteViewModel)
        {
            var user = await _appUserManager.GetByUserIdAsync(_currentUser.Id);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
            // Check Password 
            var passResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, accountToDeleteViewModel.password);
            if (passResult != PasswordVerificationResult.Success)
            {
                return BadRequest(_localizer[Keys.Validation.InCorrectPassword]);
            }
            // Update User
            var result = await _appUserManager.SoftDeleteAsync(user);
            if (!result)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteAccount]);
            }
            var done = await _adminManager.SetDeactivationReason(user.Id, accountToDeleteViewModel.deactivationReason);
            if (!done)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotSetReason]);
            }
            var res = new EntityResult<string>
            {
                result = _localizer[Keys.Messages.AccountDeletedSuccessfully]
            };
            return Ok(res);
        }

        [HttpGet]
        [Route("GetUserAdminInfo")]
        public async Task<IActionResult> GetUserAdminInfo()
        {
            var user = await _appUserManager.GetByUserIdAsync(_currentUser.Id);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
            AdminFullProfile adminProfile = null;
            var admin = await _adminManager.GetAsync(user.Id);
            if (admin == null)
            {
                adminProfile = new AdminFullProfile()
                {
                    FirstName = user.FirstName,
                    phone = user.PhoneNumber,
                    CountryId = user.CountryId,
                    Id = user.Id,
                    DisplayImage = (int)DisplayIamge.DriverImage,
                    Email = user.Email,
                };

                var newAdmin = new AdminViewModel();
                newAdmin.Id = user.Id;
                var res = await _adminManager.AddAsync(newAdmin);
                if (res == null)
                {
                    return BadRequest(_localizer[Keys.Validation.CanNotCreateAdminProfile]);
                }
            }
            else
            {
                adminProfile = new AdminFullProfile()
                {
                    FirstName = user.FirstName,
                    phone = user.PhoneNumber,
                    CountryId = user.CountryId,
                    ResidentCountryId = admin.ResidentCountryId,
                    Id = user.Id,
                    CompanyAddress = admin.CompanyAddress,
                    CompanyName = admin.CompanyName,
                    DisplayImage = admin.DisplayImage,
                    Email = user.Email,
                    DashBoardLanguage = admin.DashBoardLanguage,
                    TrackingPanelLanguage = admin.TrackingPanelLanguage,
                    BranchId = admin.BranchId
                };
            }

            return Ok(adminProfile);
        }


        [HttpPost]
        [Route("UpdateDefaultBranch")]
        public async Task<IActionResult> UpdateDefaultBranch([FromBody] AdminDefaultBranchViewModel adminBranchViewModel)
        {
            var user = await _appUserManager.GetByUserIdAsync(_currentUser.Id);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
            var result = await _adminManager.UpdateDefaultBranchAsync(adminBranchViewModel, user.Id);
            if (result == null)
            {
                return BadRequest(_localizer[Keys.Validation.CouldNotUpdateDefaultBranch]);
            }
            var res = new EntityResult<string>
            {
                result = _localizer[Keys.Messages.DefaultBranchUpdatedsuccessfully]
            };
            return Ok(res);
        }

        [HttpGet]
        [Route("GetDefautBranch")]
        public async Task<IActionResult> GetDefautBranch()
        {
            var user = await _appUserManager.GetByUserIdAsync(_currentUser.Id);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }

            var admin = await _adminManager.GetAsync(_currentUser.Id);
            if (!admin.BranchId.HasValue)
            {
                return Ok();
            }

            var branch = _branchManager.GetbyId(admin.BranchId.GetValueOrDefault());

            return Ok(branch);

        }

    }
}