﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class LocationAccuracyController : Controller
    {
        private readonly ILocationAccuracyManager _locationAccuracyManager;

        public LocationAccuracyController( ILocationAccuracyManager locationAccuracyManager)
        {
            _locationAccuracyManager = locationAccuracyManager;
        }

        // GET: LocationAccuracy

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All LocationAccuracy 
            var AllLocationAccuracy = await _locationAccuracyManager.GetAllAsync<LocationAccuracyViewModel>(true);
            return Ok(AllLocationAccuracy);
        }

        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All LocationAccuracy 
            var AllLocationAccuracy = await _locationAccuracyManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllLocationAccuracy);
        }

    }
}