﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class TeamsController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly ITeamsManager _teamManager;
        private readonly ITeamsManager _teamsManager;

        public TeamsController(
            ITeamsManager teamManager,
            ITeamsManager teamsManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _teamManager = teamManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
            _currentUser = currentUser;
            _teamsManager = teamsManager;
        }


        // GET: Teams

        [Route("GetAll")]
        [HttpGet]
        [Authorize(ManagerPermissions.Team.ReadMyTeam)]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Teams 
            if (_currentUser.UserType == RoleType.Manager)
            {
                if (_currentUser.HasPermission(ManagerPermissions.Team.ReadTeam))
                {
                    var AllTeamss = await _teamManager.GetAllAsync<TeamViewModel>();
                    return Ok(AllTeamss);
                }

                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }

                var ManagerTeams = await _teamManager.GetAllAsync(managerTeamIds);
                return Ok(ManagerTeams);
            }

            var AllTeams = await _teamManager.GetAllAsync<TeamViewModel>();
            return Ok(AllTeams);
        }

        [Route("GetByKeyword")]
        [HttpGet]
        public async Task<ActionResult> GetByKeywordAsync(string SearchKey)
        {
            ///Get All Teams 
            var AllTeams = await _teamManager.GetByKeywordAsync(SearchKey);
            return Ok(AllTeams);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        [Authorize(ManagerPermissions.Team.ReadMyTeam)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                if (_currentUser.HasPermission(ManagerPermissions.Team.ReadTeam))
                {
                    var AllTeamss = await _teamManager.GetAllByPaginationAsync(pagingparametermodel);
                    return Ok(AllTeamss);
                }

                var managerTeamIds = await _teamsManager.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
                var ManagerTeams = await _teamManager.GetAllByPaginationAsync(pagingparametermodel, managerTeamIds);
                return Ok(ManagerTeams);
            }
            var AllTeams = await _teamManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllTeams);
        }

        // GET: Teams/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        [Authorize(ManagerPermissions.Team.ReadTeam)]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            ///Get All Team By id 
            var team = await _teamManager.Get(id);
            return Ok(team);
        }

        // POST: Teams/Create
        [HttpPost]
        [Route("Create")]
        [Authorize(ManagerPermissions.Team.CreateTeam)]
        public async Task<ActionResult> CreateAsync([FromBody] TeamViewModel teamVM)
        {
            var created_team = await _teamManager.AddAsync(teamVM);
            // TODO: Add insert logic here

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Team",
                ActivityType = "Create",
                Description = " Team " + teamVM.Name + " ( " + (await _teamManager.GetAllAsync<TeamViewModel>()).Max(x => x.Id) + " ) has been created ",
                Record_Id = created_team.Id

            };
            var created_log = await _accountLogManager.Create(accountLog);

            return Ok(created_team);
        }


        // POST: Teams/Edit/5
        [HttpPut]
        [Authorize(ManagerPermissions.Team.UpdateTeam)]
        public async Task<ActionResult> Update([FromBody] TeamViewModel teamVM)
        {
            var updated_team = await _teamManager.UpdateAsync(teamVM);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Team",
                ActivityType = "Update",
                Description = " Team " + teamVM.Name + " ( " + teamVM.Id + " ) has been updated ",
                Record_Id = teamVM.Id

            };

            var created_log = await _accountLogManager.Create(accountLog);

            return Ok(updated_team);
        }

        // GET: Teams/Delete/5
        [HttpDelete("Delete/{id}")]
        [Authorize(ManagerPermissions.Team.DeleteTeam)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var teamToDelete = await _teamManager.Get(id);
            if (teamToDelete.Drivers?.Count > 0)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteTeamItHasDrivers]);
            }
            if (teamToDelete.TeamManagers?.Count > 0)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteTeamIHasManager]);
            }
            var result = await _teamManager.SoftDeleteAsync(teamToDelete);
            if (result)
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Team",
                    ActivityType = "Delete",
                    Description = " Team " + teamToDelete.Name + " ( " + teamToDelete.Id + " ) has been deleted ",
                    Record_Id = teamToDelete.Id
                };

                var created_log = await _accountLogManager.Create(accountLog);
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteTeam]);
            }
        }

    }
}