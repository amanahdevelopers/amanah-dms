﻿using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DownloadController : Controller
    {
        [Route("DownloadImportTemplate")]
        public IActionResult DownloadImportTemplate(string name)
        {
            var path = "ImportTemplates";
            string fileName = "Import" + name + "Template.xlsx";
            string filePath = Path.Combine(path, fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }
    }
}