﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.VALIDATOR;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BranchController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly ICurrentUser _currentUser;
        private readonly IMapper _mapper;
        private readonly IBranchManager _branchManager;
        private readonly ILocalizer _localizer;

        public BranchController(
            IMapper mapper,
            IBranchManager branchManager,
            IAccountLogManager accountLogManager,
            ICurrentUser currentUser,
            ILocalizer localizer)
        {
            _mapper = mapper;
            _branchManager = branchManager;
            _accountLogManager = accountLogManager;
            _currentUser = currentUser;
            _localizer = localizer;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerBranchesIds = await _branchManager.GetManagerBranchesAsync();
                if (managerBranchesIds == null || !managerBranchesIds.Any())
                    return Ok();

                var branches = await _branchManager.GetAllAsync<BranchViewModel>(managerBranchesIds);
                return Ok(branches);
            }
            var data = await _branchManager.GetAllAsync<BranchViewModel>(false);
            return Ok(data);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All branches By Pagination 
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerBranchesIds = await _branchManager.GetManagerBranchesAsync();
                if (managerBranchesIds == null || !managerBranchesIds.Any())
                {
                    return Ok();
                }
                var managerBranches = await _branchManager.GetAllByPaginationAsync(pagingparametermodel, managerBranchesIds);
                return Ok(managerBranches);
            }
            var branches = await _branchManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(branches);
        }

        [Route("GetAllByResturantPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByResturantPagination([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All branches By Pagination 
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerBranchesIds = await _branchManager.GetManagerBranchesAsync();
                if (managerBranchesIds == null || !managerBranchesIds.Any())
                {
                    return Ok();
                }
                var managerBranches = await _branchManager.GetBranchesByRestaurantPagging(pagingparametermodel, managerBranchesIds);
                return Ok(managerBranches);
            }
            var branches = await _branchManager.GetBranchesByRestaurantPagging(pagingparametermodel);
            return Ok(branches);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            var data = await _branchManager.Get<BranchViewModel>(id);
            return Ok(data);
        }

        //[Route("[action]")]
        //[HttpGet]
        //public async Task<ActionResult> GetAllByPagination([FromBody]PaginatedItemsViewModel pagingparametermodel)
        //{
        //    var data = await _geoFenceManager.GetAllByPaginationAsync(pagingparametermodel);
        //    return Ok(data);
        //}
        [Route("[action]/{id}")]
        [HttpGet]
        public async Task<ActionResult> Details([FromRoute] int id)
        {
            var driver = await _branchManager.Get(id);
            return Ok(driver);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> Create([FromBody] CreateBranchViewModel input)
        {
            var mappedInput = _mapper.Map<BranchViewModel>(input);
            mappedInput.IsActive = true;
            var validator = new BranchViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                var branchcreated = await _branchManager.AddAsync(mappedInput);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Branch",
                    ActivityType = "Create",
                    Description = input.Name + " ( " + (await _branchManager.GetAllAsync<BranchViewModel>(false)).Max(a => a.Id) + " ) has been created ",
                    Record_Id = input.Id

                };
                await _accountLogManager.Create(accountLog);

                return Ok(branchcreated);
            }
        }

        [HttpPut]
        [Route("[action]")]
        public async Task<ActionResult> Update([FromBody] CreateBranchViewModel input)
        {
            var mappedInput = _mapper.Map<BranchViewModel>(input);
            var validator = new BranchViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Branch",
                    ActivityType = "Update",
                    Description = input.Name + " ( " + input.Id + " ) has been updated ",
                    Record_Id = input.Id

                };
                await _accountLogManager.Create(accountLog);
                return Ok(await _branchManager.UpdateAsync(mappedInput));
            }
        }

        [HttpDelete]
        [Route("[action]/{id}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            var branchToDelete = _branchManager.GetbyId(id);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Branch",
                ActivityType = "Delete",
                Description = branchToDelete.Name + " ( " + branchToDelete.Id + " ) has been deleted ",
                Record_Id = branchToDelete.Id
            };
            await _accountLogManager.Create(accountLog);
            return Ok(await _branchManager.DeleteAsync(id));
        }


        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult> SetActivate(int id, bool isActive, string Reason)
        {
            return Ok(await _branchManager.SetActivate(id, isActive, Reason));
        }

        [Route("GetBranchesBy")]
        [HttpGet]
        public async Task<ActionResult> GetBranchesBy(int resturantId)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerBranchesIds = await _branchManager.GetManagerBranchesAsync();
                if (managerBranchesIds == null || !managerBranchesIds.Any())
                    return Ok();

                var managerBranches = await _branchManager.GetBranchesBy(resturantId, managerBranchesIds);
                return Ok(managerBranches);
            }
            var data = await _branchManager.GetBranchesBy(resturantId);
            return Ok(data);
        }
        [Route("GetBranchesByresturantIdsAndgeoFenceIds")]
        [HttpGet]
        public async Task<ActionResult> GetBranchesByresturantIdsAndgeoFenceIds(string resturantIds, string geoFenceIds)
        {
            if (string.IsNullOrEmpty(resturantIds))
            {
                return BadRequest(_localizer[Keys.Validation.EmptyRestaurantIds]);
            }
            if (string.IsNullOrEmpty(geoFenceIds))
            {
                return BadRequest(_localizer[Keys.Validation.EmptyGeoFenceIds]);
            }

            string[] resturantIdsArraySTring = resturantIds.Split(',');
            int[] resturantIdsArray = Array.ConvertAll(resturantIdsArraySTring, s => int.Parse(s));
            string[] geoFenceIdsArraySTring = geoFenceIds.Split(',');
            int[] geoFenceIdsArray = Array.ConvertAll(geoFenceIdsArraySTring, s => int.Parse(s));
            // int[] resturantIds=new int[];
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerBranchesIds = await _branchManager.GetManagerBranchesAsync();
                if (managerBranchesIds == null || !managerBranchesIds.Any())
                    return Ok();

                var managerBranches = await _branchManager.GetBranchesBy(resturantIdsArray, geoFenceIdsArray, managerBranchesIds);
                return Ok(managerBranches);
            }
            var data = await _branchManager.GetBranchesBy(resturantIdsArray, geoFenceIdsArray);
            return Ok(data);
        }


        // GET: Branches by name
        [Route("GetByName/{name}")]
        [HttpGet]
        public async Task<ActionResult> GetByNameAsync(string name)
        {
            ///Get Branches by name
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerBranchesIds = await _branchManager.GetManagerBranchesAsync();
                if (managerBranchesIds == null || !managerBranchesIds.Any())
                    return Ok();

                var managerBranches = await _branchManager.GetByNameAsync(name, managerBranchesIds);
                return Ok(managerBranches);
            }
            var AllBranches = await _branchManager.GetByNameAsync(name);
            return Ok(AllBranches);
        }
    }
}
