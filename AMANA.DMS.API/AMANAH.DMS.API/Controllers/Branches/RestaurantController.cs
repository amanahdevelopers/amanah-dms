﻿using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.VALIDATOR;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RestaurantController : Controller
    {
        private readonly IAccountLogManager _accountLogManager;
        private readonly ICurrentUser _currentUser;
        public readonly IMapper _mapper;
        public readonly IRestaurantManager _restaurantManager;


        public RestaurantController(
            IMapper mapper,
            IRestaurantManager restaurantManager,
            IAccountLogManager accountLogManager,
            ICurrentUser currentUser)
        {
            _mapper = mapper;
            _restaurantManager = restaurantManager;
            _accountLogManager = accountLogManager;
            _currentUser = currentUser;
        }

        /// <summary>
        /// this API will retrive all restaurant except blocked restaurant 
        /// </summary>
        /// <returns></returns>
        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerResturantsIds = await _restaurantManager.GetManagerRestaurantsAsync();
                if (managerResturantsIds == null || !managerResturantsIds.Any())
                {
                    return Ok();
                }
                var resturants = (await _restaurantManager.GetAllAsync<RestaurantViewModel>(managerResturantsIds))
                    .Where(x => x.IsActive == true).ToArray();
                return Ok(resturants);
            }
            var data = await _restaurantManager.GetAllAsync<RestaurantViewModel>();
            return Ok(data.Where(x => x.IsActive == true).ToList());
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            var data = await _restaurantManager.Get<RestaurantViewModel>(id);
            return Ok(data);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerResturantsIds = await _restaurantManager.GetManagerRestaurantsAsync();
                if (managerResturantsIds == null || !managerResturantsIds.Any())
                {
                    return Ok();
                }

                var resturants = await _restaurantManager.GetAllByPaginationAsync(pagingparametermodel, managerResturantsIds);
                return Ok(resturants);
            }
            var restaurants = await _restaurantManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(restaurants);
        }


        [Route("[action]/{id}")]
        [HttpGet]
        public async Task<ActionResult> Details([FromRoute] int id)
        {
            var driver = await _restaurantManager.Get(id);
            return Ok(driver);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> Create([FromBody] CreateRestaurantViewModel input)
        {
            var mappedInput = _mapper.Map<RestaurantViewModel>(input);
            mappedInput.IsActive = true;
            var validator = new RestaurantViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                await _restaurantManager.AddAsync(mappedInput);
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Restaurant",
                    ActivityType = "Create",
                    Description = mappedInput.Name + " ( " + (await _restaurantManager.GetAllAsync<RestaurantViewModel>()).Max(x => x.Id) + " ) has been created ",
                    Record_Id = mappedInput.Id
                };
                await _accountLogManager.Create(accountLog);

                return Ok();
            }
        }

        [HttpPut]
        [Route("[action]")]
        public async Task<ActionResult> Update([FromBody] CreateRestaurantViewModel input)
        {

            var mappedInput = _mapper.Map<RestaurantViewModel>(input);
            var validator = new RestaurantViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                await _restaurantManager.UpdateAsync(mappedInput);

                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Restaurant",
                    ActivityType = "Update",
                    Description = mappedInput.Name + " ( " + mappedInput.Id + " ) has been updated ",
                    Record_Id = mappedInput.Id

                };
                await _accountLogManager.Create(accountLog);
                return Ok();
            }

        }

        [HttpDelete]
        [Route("[action]/{id}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            var RestaurantToDelete = await _restaurantManager.Get(id);
            await _restaurantManager.DeleteAsync(id);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Restaurant",
                ActivityType = "Delete",
                Description = RestaurantToDelete.Name + " ( " + RestaurantToDelete.Id + " ) has been deleted ",
                Record_Id = RestaurantToDelete.Id

            };
            await _accountLogManager.Create(accountLog);
            return Ok();

        }


        [HttpGet]
        [Route("[action]")]
        public async Task<ActionResult> SetActivate(int id, bool isActive, string Reason)
        {
            var RestaurantToDelete = await _restaurantManager.Get(id);
            await _restaurantManager.SetActivate(id, isActive, Reason);
            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "Restaurant",
                ActivityType = "SetActivate",
                Description = isActive
                    ? RestaurantToDelete.Name + " ( " + RestaurantToDelete.Id + " ) has been actived "
                    : RestaurantToDelete.Name + " ( " + RestaurantToDelete.Id + " ) has been deactived ",
                Record_Id = RestaurantToDelete.Id
            };
            await _accountLogManager.Create(accountLog);

            return Ok();
        }
    }
}
