﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.VALIDATOR;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Utilities.Utilites.GenericListToExcel;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class GeoFenceController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IGeoFenceManager _geoFenceManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly IDriverManager _driverManager;
        private readonly ICurrentUser _currentUser;

        public GeoFenceController(
            IMapper mapper,
            IGeoFenceManager geoFenceManager,
            IAccountLogManager accountLogManager,
            IDriverManager driverManager,
            ICurrentUser currentUser)
        {
            _mapper = mapper;
            _geoFenceManager = geoFenceManager;
            _accountLogManager = accountLogManager;
            _driverManager = driverManager;
            _currentUser = currentUser;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            List<GeoFenceViewModel> geofences;
            if (_currentUser.UserType == RoleType.Manager)
            {
                var managerZonesIds = await _geoFenceManager.GetManagerZonesAsync();
                if (managerZonesIds == null || !managerZonesIds.Any())
                {
                    return Ok();
                }

                geofences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>(managerZonesIds);
            }
            else
            {
                geofences = await _geoFenceManager.GetAllAsync<GeoFenceViewModel>();
            }
            foreach (var geoFence in geofences)
            {
                geoFence.Drivers = await _driverManager.GetDriverIdsByGeofenceIdAsync(geoFence.Id);
            }
            return Ok(geofences);
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            var data = await _geoFenceManager.Get<GeoFenceViewModel>(id);
            return Ok(data);
        }


        [Route("[action]/{id}")]
        [HttpGet]
        public async Task<ActionResult> Details([FromRoute] int id)
        {
            var driver = await _geoFenceManager.Get(id);
            return Ok(driver);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<ActionResult> Create([FromBody] CreateGeoFenceViewModel input)
        {
            var mappedInput = _mapper.Map<GeoFenceViewModel>(input);
            var validator = new GeoFenceViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                await _geoFenceManager.AddAsync(mappedInput);

                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "GeoFence",
                    ActivityType = "Create",
                    Description = mappedInput.Name + "( " + (await _geoFenceManager.GetAllAsync<GeoFenceViewModel>()).Max(x => x.Id) + " )  has been created ",
                    Record_Id = mappedInput.Id

                };
                var created_log = await _accountLogManager.Create(accountLog);
                return Ok();
            }
        }

        [HttpPut]
        [Route("[action]")]
        public async Task<ActionResult> Update([FromBody] CreateGeoFenceViewModel input)
        {

            var mappedInput = _mapper.Map<GeoFenceViewModel>(input);
            var validator = new GeoFenceViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                await _geoFenceManager.UpdateAsync(mappedInput);

                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "GeoFence",
                    ActivityType = "Update",
                    Description = mappedInput.Name + "( " + mappedInput.Id + " )  has been updated ",
                    Record_Id = mappedInput.Id

                };
                await _accountLogManager.Create(accountLog);

                return Ok();
            }
        }

        [HttpDelete]
        [Route("[action]/{id}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            var geoFenceToDelete = await _geoFenceManager.Get(id);

            await _geoFenceManager.DeleteAsync(id);

            AccountLogs accountLog = new AccountLogs()
            {
                TableName = "GeoFence",
                ActivityType = "Delete",
                Description = geoFenceToDelete.Name + "( " + geoFenceToDelete.Id + " )  has been deleted ",
                Record_Id = geoFenceToDelete.Id

            };
            await _accountLogManager.Create(accountLog);

            return Ok();
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel()
        {
            var data = await _geoFenceManager.ExportExcel();
            foreach (var geoFence in data)
            {
                geoFence.DriversCount = (await _driverManager.GetDriverIdsByGeofenceIdAsync(geoFence.GeoFenceId)).Count();
            }
            var path = "ImportTemplates";
            var fileName = "Geofencs.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }
    }
}