﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly ILocalizer _localizer;
        private readonly ISettingsManager _settingManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly IHubContext<NotificationHub> _notificationhubContext;
        private readonly ICurrentUser _currentUser;


        public SettingsController(
            ISettingsManager settingManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer,
            IHubContext<NotificationHub> notificationhubContext,
            ICurrentUser currentUser)


        {
            _settingManager = settingManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
            _notificationhubContext = notificationhubContext;
            _currentUser = currentUser;

        }

        // GET: Settings

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Settings 
            var AllSettings = await _settingManager.GetAllAsync<SettingViewModel>();
            return Ok(AllSettings);
        }
        // GET: Settings

        [Route("GetSettingByGroupId")]
        [HttpGet]
        public async Task<ActionResult> GetSettingByGroupIdAsync(string GroupId)
        {
            ///Get All Settings 
            var AllSettings = await _settingManager.GetSettingByGroupId(GroupId);
            return Ok(AllSettings);
        }


        [Route("GetByKey/{key}")]
        [HttpGet]
        public async Task<ActionResult> GetByKey(string key)
        {
            var setting = await _settingManager.GetSettingByKey(key);
            return Ok(setting);

        }



        // GET: Settings/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var setting = await _settingManager.Get(id);
            return Ok(setting);

        }


        // POST: Settings/Create
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] SettingViewModel settingVM)
        {
            var created_setting = await _settingManager.AddAsync(settingVM);
            if (created_setting.SettingKey == "IsEnableAutoAllocation")
            {
                string isOnOff = created_setting.Value == "true" ? "On" : "Off";
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Setting",
                    ActivityType = "Auto-allocation",
                    Description = "Auto-allocation has been switched ( " + isOnOff + " )  by " + User.Identity.Name,
                    Record_Id = settingVM.Id

                };
                await _accountLogManager.Create(accountLog);
                if (created_setting.SettingKey == "SelectedAutoAllocationMethodName")
                {
                    int autoAllocationType = await GetAutoAllocationTypeCodeAsync(created_setting);
                    await _notificationhubContext.Clients
                        .Group(_currentUser.TenantId)
                        .SendAsync("AutoAllocationSettingsChanged", autoAllocationType);
                }
            }

            return Ok(created_setting);
        }



        // GET: Settings/Edit/5


        // POST: Settings/Edit/5
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] SettingViewModel settingVM)
        {
            bool result = await InternalUpdateAsync(settingVM);

            return Ok(result);
        }

        // GET: Settings/Delete/5
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var settingToDelete = await _settingManager.Get(id);
            var result = await _settingManager.SoftDeleteAsync(settingToDelete);
            // TODO: Add insert logic here

            if (result)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteSetting]);
            }
        }


        // POST: Settings/Create
        [HttpPost]
        [Route("UpdateRange")]
        public async Task<ActionResult> UpdateRange([FromBody] List<SettingViewModel> settingVMList)
        {
            var result = true;
            foreach (var settingVM in settingVMList)
            {
                result = result && await InternalUpdateAsync(settingVM);
            }

            return Ok(result);
        }




        private async Task<int> GetAutoAllocationTypeCodeAsync(SettingViewModel autoallocationSetting)
        {
            var enableAutoallocationSetting = await _settingManager.GetSettingByKey("IsEnableAutoAllocation");
            int autoAllocationType = enableAutoallocationSetting == null
                ? (int)AutoAllocationTypeEnum.Manual
                : enableAutoallocationSetting.Value.ToLower() == "false"
                    ? (int)AutoAllocationTypeEnum.Manual
                    : autoallocationSetting == null
                        ? (int)AutoAllocationTypeEnum.Manual
                        : autoallocationSetting.Value == "FirstInFirstOutMethod"
                            ? (int)AutoAllocationTypeEnum.Fifo
                            : autoallocationSetting.Value == "NearestAvailableMethod"
                                ? (int)AutoAllocationTypeEnum.Nearest
                                : autoallocationSetting.Value == "OneByOneMethod"
                                    ? (int)AutoAllocationTypeEnum.OneByOne
                                    : (int)AutoAllocationTypeEnum.Manual;

            return autoAllocationType;
        }

        //TODO: this need to moved to service, and no need to to all those trips this should be done in single db trip
        private async Task<bool> InternalUpdateAsync(SettingViewModel settingVM)
        {
            var setting = await _settingManager.GetSettingByKey(settingVM.SettingKey);
            bool result;
            if (setting == null)
            {
                setting = await _settingManager.AddAsync(settingVM);
                result = setting != null;
            }
            else
            {
                settingVM.Id = setting.Id;
                result = await _settingManager.UpdateAsync(settingVM);
            }
            if (result)
            {
                if (settingVM.SettingKey == "IsEnableAutoAllocation")
                {
                    string isOnOff = settingVM.Value == "true" ? "On" : "Off";
                    AccountLogs accountLog = new AccountLogs()
                    {
                        TableName = "Setting",
                        ActivityType = "Auto-allocation",
                        Description = "Auto-allocation has been switched ( " + isOnOff + " )  ",
                        Record_Id = settingVM.Id

                    };
                    await _accountLogManager.Create(accountLog);

                }
                if (settingVM.SettingKey == "SelectedAutoAllocationMethodName")
                {
                    int autoAllocationType = await GetAutoAllocationTypeCodeAsync(settingVM);
                    await _notificationhubContext.Clients
                        .Group(_currentUser.TenantId)
                        .SendAsync("AutoAllocationSettingsChanged", autoAllocationType);
                }
            }

            return result;
        }
    }
}
