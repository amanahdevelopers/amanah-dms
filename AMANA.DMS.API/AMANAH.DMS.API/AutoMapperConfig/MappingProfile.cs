﻿using System;
using System.Collections.Generic;
using System.Linq;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Account;
using AMANAH.DMS.BLL.ViewModels.Admin;
using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.BLL.ViewModels.Mobile;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Models.Entities;
using AMANAH.DMS.ViewModel;
using AMANAH.DMS.ViewModels;
using AutoMapper;
using MapsUtilities.Models;
using Microsoft.AspNetCore.Identity;
using Utilities.Utilites.Paging;

namespace AMANAH.DMS.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TaskDriverRequests, TaskDriverRequestViewModel>(MemberList.None);

            CreateMap<TaskDriverRequestViewModel, TaskDriverRequests>(MemberList.None)
                .ForMember(dest => dest.ResponseStatus, opt => opt.MapFrom(src => (int)src.ResponseStatus));

            CreateMap<ApplicationUser, UserViewModel>(MemberList.None)
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + ' ' + src.MiddleName + ' ' + src.LastName));
            CreateMap<IdentityResult, UserManagerResult>(MemberList.None);

            CreateMap<UserDevice, UserDeviceViewModel>(MemberList.None);
            CreateMap<UserDeviceViewModel, UserDevice>(MemberList.None);



            CreateMap<CreateDriverRateViewModel, DriverRateViewModel>(MemberList.None);

            CreateMap<DriverRate, DriverRateViewModel>(MemberList.None);
            CreateMap<DriverRateViewModel, DriverRate>(MemberList.None);



            // Add as many of these lines as you need to map your objects
            CreateMap<RolePrivilgeViewModel, RolePrivilge>(MemberList.None)
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));
            CreateMap<RolePrivilge, RolePrivilgeViewModel>(MemberList.None)
              .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
              .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role));

            CreateMap<PrivilgeViewModel, Privilge>(MemberList.None)
             .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<Privilge, PrivilgeViewModel>()
              .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles));
            CreateMap<ApplicationRoleViewModel, ApplicationRole>(MemberList.None)
              .ForMember(dest => dest.NormalizedName, opt => opt.Ignore())
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges))
              .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>(MemberList.None)
              .ForMember(dest => dest.Privilges, opt => opt.MapFrom(src => src.Privilges));

            CreateMap<ApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());


            CreateMap<Address, AddressViewModel>(MemberList.None);
            CreateMap<AddressViewModel, Address>(MemberList.None);



            CreateMap<ApplicationUser, ApplicationUserViewModel>(MemberList.None)
            .ForMember(dest => dest.Password, opt => opt.Ignore());


            CreateMap<PagedResult<ApplicationUser>, PagedResult<ApplicationUserViewModel>>(MemberList.None);
            CreateMap<EditApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());

            CreateMap<ApplicationUser, EditApplicationUserViewModel>(MemberList.None);
            CreateMap<EditUserProfileViewModel, ApplicationUser>(MemberList.None);

            //-------------  Settings Profiles ---------------------------------//

            CreateMap<Setting, SettingViewModel>(MemberList.None);
            CreateMap<SettingViewModel, Setting>(MemberList.None);

            CreateMap<LocationAccuracy, LocationAccuracyViewModel>(MemberList.None);
            CreateMap<LocationAccuracyViewModel, LocationAccuracy>(MemberList.None);
            CreateMap<Team, TeamViewModel>(MemberList.None)
                 .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.LocationAccuracy.Name));
            CreateMap<TeamViewModel, Team>(MemberList.None)
                 .ForMember(dest => dest.Drivers, opt => opt.Ignore());

            CreateMap<AgentType, AgentTypeViewModel>(MemberList.None);
            CreateMap<AgentTypeViewModel, AgentType>(MemberList.None);


            CreateMap<TransportType, TransportTypeViewModel>(MemberList.None);
            CreateMap<TransportTypeViewModel, TransportType>(MemberList.None);

            CreateMap<Country, CountryViewModel>(MemberList.None)
                              .ForMember(dest => dest.FlagUrl, opt => opt.MapFrom((src) => "CountryFlags/" + src.Flag))


                ;
            CreateMap<CountryViewModel, Country>(MemberList.None);

            CreateMap<Customer, CustomerViewModel>(MemberList.None);
            CreateMap<CustomerViewModel, Customer>(MemberList.None)
                .ForMember(dest => dest.Country, opt => opt.Ignore());
            CreateMap<ImportCustomerViewModel, CustomerViewModel>(MemberList.None)
              .ForMember(dest => dest.Country, opt => opt.Ignore());


            //-------------  Driver Profiles ---------------------------------//
            CreateMap<DriverViewModel, Driver>(MemberList.None);
            CreateMap<Driver, DriverViewModel>(MemberList.None)
              .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
              .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
              .ForMember(dest => dest.AgentTypeName, opt => opt.MapFrom(src => src.AgentType.Name))
              .ForMember(dest => dest.AgentStatusName, opt => opt.MapFrom(src => src.AgentStatus != null ? src.AgentStatusId == 6 ? "Available" : src.AgentStatus.Name : string.Empty))
              .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name))
              .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
              .ForMember(dest => dest.CountryCode, opt => opt.MapFrom(src => src.Country.Code))
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
              .ForMember(dest => dest.DeviceType, opt => opt.MapFrom(src => src.DeviceType))
              .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src.Version))

              .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? (double?)null : src.CurrentLocation.Latitude))
              .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? (double?)null : src.CurrentLocation.Longitude))
              .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Name))
              .ForMember(dest => dest.LocationAccuracyDuration, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Duration))
              .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.BranchId));

            CreateMap<Driver, DriverWithTasksViewModel>(MemberList.None)
                .IncludeBase<Driver, DriverViewModel>()
                .ForMember(dest => dest.AssignedMainTasks, opt => opt.Ignore());


            CreateMap<ImportDriverViewModel, ApplicationUserViewModel>(MemberList.None);
            CreateMap<ImportDriverViewModel, DriverViewModel>(MemberList.None);

            CreateMap<CreateDriverViewModel, ApplicationUserViewModel>(MemberList.None);
            CreateMap<CreateDriverViewModel, DriverViewModel>(MemberList.None);
            CreateMap<EditDriverViewModel, ApplicationUserViewModel>(MemberList.None)
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId));
            ;
            CreateMap<EditDriverViewModel, DriverViewModel>(MemberList.None);
            CreateMap<Driver, DriverForAssignViewModel>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FirstName + " " + src.User.LastName))
              .ForMember(dest => dest.AgentStatusName, opt => opt.MapFrom(src => src.AgentStatus.Name));


            CreateMap<Driver, DriverTeamViewModel>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FirstName + " " + src.User.LastName));
            CreateMap<TeamManagerViewModel, TeamManager>(MemberList.None);
            CreateMap<TeamManager, TeamManagerViewModel>(MemberList.None)
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name));
            CreateMap<ManagerViewModel, Manager>(MemberList.None);
            CreateMap<Manager, ManagerViewModel>(MemberList.None)
              .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
              .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
              .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.User.CountryId))
              .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.User.Country));

            CreateMap<Driver, DriverTransportionTypeViewModel>(MemberList.None)
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name));

            CreateMap<ManagerViewModel, ApplicationUserViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId));
            CreateMap<CreateManagerViewModel, ApplicationUserViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<CreateManagerViewModel, ManagerViewModel>(MemberList.None);




            CreateMap<TaskRoute, TaskRouteViewModel>(MemberList.None);
            CreateMap<TaskRouteViewModel, TaskRoute>(MemberList.None);
            CreateMap<TaskGallary, TaskGallaryViewModel>(MemberList.None);


            CreateMap<Area, AreaViewModel>(MemberList.None);
            CreateMap<AreaViewModel, Area>(MemberList.None);




            CreateMap<PACI, PACIViewModel>(MemberList.None);
            CreateMap<PACIViewModel, PACI>(MemberList.None);

            CreateMap<AddressViewModel, PACI>(MemberList.None)
              .ForMember(dest => dest.AreaName, opt => opt.MapFrom(src => src.Area))
              .ForMember(dest => dest.BlockName, opt => opt.MapFrom(src => src.Block))
              .ForMember(dest => dest.BuildingNumber, opt => opt.MapFrom(src => src.Building))
              .ForMember(dest => dest.FloorNumber, opt => opt.MapFrom(src => src.Floor))
              .ForMember(dest => dest.StreetName, opt => opt.MapFrom(src => src.Street));

            CreateMap<AddressViewModel, PACIViewModel>(MemberList.None)
              .ForMember(dest => dest.AreaName, opt => opt.MapFrom(src => src.Area))
              .ForMember(dest => dest.BlockName, opt => opt.MapFrom(src => src.Block))
              .ForMember(dest => dest.BuildingNumber, opt => opt.MapFrom(src => src.Building))
              .ForMember(dest => dest.FloorNumber, opt => opt.MapFrom(src => src.Floor))
              .ForMember(dest => dest.StreetName, opt => opt.MapFrom(src => src.Street));

            CreateMap<MainTaskStatus, LkpViewModel>(MemberList.None);
            CreateMap<LkpViewModel, MainTaskStatus>(MemberList.None);
            CreateMap<MainTaskType, LkpViewModel>(MemberList.None);
            CreateMap<LkpViewModel, MainTaskType>(MemberList.None);
            CreateMap<TaskType, LkpViewModel>(MemberList.None);
            CreateMap<LkpViewModel, TaskType>(MemberList.None);
            CreateMap<TaskStatus, TaskStatusViewModel>(MemberList.None);
            CreateMap<TaskStatusViewModel, TaskStatus>(MemberList.None);
            CreateMap<MainTask, MainTaskViewModel>(MemberList.None)
              .ForMember(dest => dest.MainTaskStatusName, opt => opt.MapFrom(src => src.MainTaskStatus != null ? src.MainTaskStatus.Name : null))
              .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType != null ? src.MainTaskType.Name : null))
              .ForMember(dest => dest.Settings, opt => new TaskSettingsViewModel());
            CreateMap<MainTaskViewModel, MainTask>(MemberList.None)
              .ForMember(dest => dest.IsCompleted, opt => opt.Ignore())
              .ForMember(dest => dest.IsDelayed, opt => opt.Ignore());
            CreateMap<Tasks, TasksViewModel>(MemberList.None)
                  .ForMember(dest => dest.DelayTime, opt => opt.MapFrom(src => src.DelayTime))
                  .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Name : src.Address))
                  .ForMember(dest => dest.IsTaskReached, opt => opt.MapFrom(src => src.IsTaskReached ?? false))
                  .ForMember(dest => dest.TotalWaitingTime, opt => opt.MapFrom(src => HandleTotalWaitingTime(src)))
                  .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)))
                  .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
                  .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
                  .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Driver.User.FirstName + " " + src.Driver.User.LastName))
                  .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => src.Driver.User.PhoneNumber))
                  .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                  .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                  .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Driver.Team.Name))
                  .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
                  .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => "TaskSignatures/" + src.SignatureURL))
                  .ForMember(dest => dest.DriverRates, opt => opt.MapFrom(src => src.DriverRates));



            CreateMap<Tasks, TaskHistoryDetailsViewModel>(MemberList.None)
                  .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
                  .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
                  .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                  .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Customer.Address))
                  .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
                  .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
                  .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.SignatureURL) ? null : "TaskSignatures/" + src.SignatureURL));

            CreateMap<Tasks, ExportTasksWithoutProgressViewModel>(MemberList.None)
                  .ForMember(dest => dest.Order_Type, opt => opt.MapFrom(src => src.TaskType.Name))
                  .ForMember(dest => dest.Order_Status, opt => opt.MapFrom(src => src.TaskStatus.Name))
                  .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Customer.Name))
                  .ForMember(dest => dest.Order_DateTime, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
                  .ForMember(dest => dest.Driver_Name, opt => opt.MapFrom(src => src.Driver.User.FirstName))
                  //.ForMember(dest => dest.Rating, opt => opt.MapFrom(src => src.Driver.DriverRates.FirstOrDefault().Rate))
                  //.ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Driver.DriverRates.FirstOrDefault().Note))
                  .ForMember(dest => dest.Reference_Image, opt => opt.MapFrom(src => src.Image))
                  .ForMember(dest => dest.Order_Description, opt => opt.MapFrom(src => src.Description));



            CreateMap<TasksViewModel, Tasks>(MemberList.None)
                .ForMember(dest => dest.DelayTime, opt => opt.Ignore())
                .ForMember(dest => dest.Customer, opt => opt.Ignore())
                .ForMember(dest => dest.TaskHistories, opt => opt.Ignore())
                .ForMember(dest => dest.StartDate, opt => opt.Ignore())
                .ForMember(dest => dest.SuccessfulDate, opt => opt.Ignore())
                .ForMember(dest => dest.TotalTime, opt => opt.Ignore())
                 .ForMember(dest => dest.TaskGallaries, opt => opt.Ignore());

            //.ForMember(dest => dest.DriverRates, opt => opt.Ignore());
            CreateMap<TaskHistory, TaskHistoryViewModel>(MemberList.None)
                  .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task != null ? src.Task.TaskTypeId : 0))
                  .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Task != null ? src.Task.Address : String.Empty))
                  .ForMember(dest => dest.FromStatusName, opt => opt.MapFrom(src => src.FromStatus.Name))
                  .ForMember(dest => dest.ToStatusName, opt => opt.MapFrom(src => src.ToStatus.Name))
                  .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy_Id));

            CreateMap<TaskHistory, MapPoint>(MemberList.None);
            CreateMap<TaskRoute, MapPoint>(MemberList.None);

            CreateMap<TaskHistoryViewModel, TaskHistory>(MemberList.None);
            CreateMap<DriverTaskNotification, DriverTaskNotificationViewModel>(MemberList.None);
            CreateMap<DriverTaskNotificationViewModel, DriverTaskNotification>(MemberList.None)
                .ForMember(dest => dest.Task, opt => opt.Ignore());
            CreateMap<DriverTaskNotification, TaskNotificationViewModel>(MemberList.None)
                  .ForMember(dest => dest.ActionStatus, opt => opt.MapFrom(src => src.ActionStatus))
                  .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.Task.TaskType.Name))
                  .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.Task.TaskStatus.Name))
                  .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task.TaskTypeId))
                  .ForMember(dest => dest.TaskStatusId, opt => opt.MapFrom(src => src.Task.TaskStatusId))
                  .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Task.Customer.Name))
                  .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Task.Customer.Address))
                  .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.Task.PickupDate ?? src.Task.DeliveryDate));

            CreateMap<GeoFenceViewModel, GeoFence>(MemberList.None)
                .ForMember(m => m.GeoFenceLocations, src => src.Ignore());
            

            CreateMap<Team, LkpViewModel>(MemberList.None);
            CreateMap<GeoFence, GeoFenceViewModel>(MemberList.None)
                .ForMember(m => m.Locations, src => src.MapFrom(t => t.GeoFenceLocations != null || t.GeoFenceLocations.Count > 0 ? t.GeoFenceLocations.Select(t => new GeoFenceLocationViewModel { Latitude = t.Latitude, Longitude = t.Longitude }).ToList() : new List<GeoFenceLocationViewModel>()));

            CreateMap<CreateGeoFenceViewModel, GeoFenceViewModel>(MemberList.None);

            CreateMap<Branch, BranchViewModel>(MemberList.None)
                .ForMember(m => m.Restaurant, src => src.MapFrom(t => t.Restaurant));


            CreateMap<BranchViewModel, Branch>(MemberList.None).ForMember(m => m.Restaurant, src => src.Ignore());

            //CreateMap<Restaurant, LkpViewModel>(MemberList.None);


            //CreateMap<Branch, BranchViewModel>(MemberList.None).ReverseMap();
            CreateMap<CreateBranchViewModel, BranchViewModel>(MemberList.None);


            CreateMap<Restaurant, RestaurantViewModel>(MemberList.None).ReverseMap();
            CreateMap<CreateRestaurantViewModel, RestaurantViewModel>(MemberList.None);


            CreateMap<GeoFence, LkpViewModel>(MemberList.None);
            CreateMap<Restaurant, LkpViewModel>(MemberList.None);
            CreateMap<Branch, LkpViewModel>(MemberList.None);
            CreateMap<ManagerDispatchingViewModel, ManagerDispatching>(MemberList.None);
            CreateMap<ManagerDispatching, ManagerDispatchingViewModel>(MemberList.None)
                .ForMember(m => m.ManagerName, src => src.MapFrom(t => t.Manager.User.FirstName));


            CreateMap<DriverDeliveryGeoFenceViewModel, DriverDeliveryGeoFence>(MemberList.None);
            CreateMap<DriverDeliveryGeoFence, DriverDeliveryGeoFenceViewModel>(MemberList.None)
                .ForMember(m => m.GeoFenceName, src => src.MapFrom(t => t.GeoFence.Name));

            CreateMap<DriverPickUpGeoFenceViewModel, DriverPickUpGeoFence>(MemberList.None);
            CreateMap<DriverPickUpGeoFence, DriverPickUpGeoFenceViewModel>(MemberList.None)
                .ForMember(m => m.GeoFenceName, src => src.MapFrom(t => t.GeoFence.Name));


            CreateMap<DriverLoginRequest, DriverLoginRequestViewModel>(MemberList.None)
                .ForMember(m => m.DriverName, src => src.MapFrom(t => t.Driver.User.UserName))
                .ForMember(m => m.TeamId, src => src.MapFrom(t => t.Driver.TeamId))
                .ForMember(m => m.TeamName, src => src.MapFrom(t => t.Driver.Team == null ? "" : t.Driver.Team.Name))
                .ForMember(m => m.AgentTypeId, src => src.MapFrom(t => t.Driver.AgentTypeId))
                .ForMember(m => m.AgentTypeName, src => src.MapFrom(t => t.Driver.AgentType == null ? "" : t.Driver.AgentType.Name))
                .ReverseMap();
            CreateMap<CreateDriverLoginRequestViewModel, DriverLoginRequestViewModel>(MemberList.None);
            CreateMap<CreateDriverLoginRequestViewModel, DriverLoginRequest>(MemberList.None);


            CreateMap<AccountLogViewModel, AccountLogs>(MemberList.None);
            CreateMap<AccountLogs, AccountLogViewModel>(MemberList.None);


            CreateMap<AdminFullProfile, AdminProfileViewModel>();
            CreateMap<AdminFullProfile, UserInfoViewModel>();
            CreateMap<AdminViewModel, Admin>().ReverseMap();


            CreateMap<Notification, NotificationViewModel>(MemberList.None);
            CreateMap<NotificationViewModel, Notification>(MemberList.None);

            CreateMap<DriverloginTracking, DriverloginTrackingViewModel>(MemberList.None)
                .ForMember(m => m.DriverName, src => src.MapFrom(t => t.Driver.User.FirstName + " " + t.Driver.User.LastName))
                .ForMember(m => m.LogoutDate, src => src.MapFrom(t => t.LogoutDate.HasValue && t.LogoutDate.Value.Date < new DateTime(2000, 01, 01).Date ? null : t.LogoutDate));
            CreateMap<DriverloginTrackingViewModel, DriverloginTracking>(MemberList.None);

        }

        private static TimeViewModel HandleTotalWaitingTime(Tasks src)
        {
            TimeViewModel waitingTime = null;
            if (src.IsTaskReached.HasValue && src.ReachedTime.HasValue && src.StartDate.HasValue)
            {
                TimeSpan differenceInTime = (src.StartDate.Value - src.ReachedTime.Value);
                waitingTime = new TimeViewModel
                {
                    Time = differenceInTime,
                    Day = differenceInTime.Days,
                    Hour = differenceInTime.Hours,
                    Minute = differenceInTime.Minutes,
                    Second = differenceInTime.Seconds
                };
            }
            return waitingTime;
        }

        private static TimeViewModel HandleTotalEstimationTime(double? estimationTime)
        {
            TimeViewModel EstimationTime = null;
            if (estimationTime.HasValue)
            {
                TimeSpan EstimationTimeSpan = TimeSpan.FromSeconds((double)estimationTime);
                EstimationTime = new TimeViewModel
                {
                    Time = EstimationTimeSpan,
                    Day = EstimationTimeSpan.Days,
                    Hour = EstimationTimeSpan.Hours,
                    Minute = EstimationTimeSpan.Minutes,
                    Second = EstimationTimeSpan.Seconds
                };
            }
            return EstimationTime;
        }
    }
}

