﻿using System;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AMANAH.DMS.API.Authorization;
using AMANAH.DMS.API.Certificates;
using AMANAH.DMS.API.Infrastructure.Authentication;
using AMANAH.DMS.API.Infrastructure.Ioc;
using AMANAH.DMS.API.Infrastructure.Jobs;
using AMANAH.DMS.API.Localization;
using AMANAH.DMS.API.Services;
using AMANAH.DMS.API.SignalRHups;
using AMANAH.DMS.BASE;
using AMANAH.DMS.BASE.Domain.Repositories;
using AMANAH.DMS.BLL.Authorization;
using AMANAH.DMS.BLL.BackgroundServcies;
using AMANAH.DMS.BLL.BLL.IManagers;
using AMANAH.DMS.BLL.BLL.Settings;
using AMANAH.DMS.BLL.ExternalServcies;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.Managers;
using AMANAH.DMS.BLL.Managers.TaskAssignment;
using AMANAH.DMS.BLL.Providers;
using AMANAH.DMS.Context;
using AMANAH.DMS.Entities;
using AMANAH.DMS.Repoistry;
using AMANAH.DMS.Settings;
using AMANAH.DMS.VALIDATOR;
using AutoMapper;
using FluentValidation.AspNetCore;
using Hangfire;
using Hangfire.SqlServer;
using MapsUtilities.MapsManager;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NSwag;
using NSwag.Generation.Processors.Security;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                  builder => builder
                  .AllowAnyMethod()
                  .AllowAnyHeader().AllowCredentials()
                  .SetIsOriginAllowed((x) => true));
            });

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(
                    _configuration["ConnectionString"],
                    sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(5);
                        sqlOptions.MigrationsAssembly("DispatchProduct.Identity.EFCore.MSSQL");
                    })
                    .EnableSensitiveDataLogging(_environment.IsDevelopment());
            });
#if true
            services.AddScoped<DbContext>(serviceProvider => serviceProvider.GetRequiredService<ApplicationDbContext>());
#else
            services.AddTransient<DbContext, ApplicationDbContext>();
#endif
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                //.AddIdentityServer()
                .AddErrorDescriber<LocalizedIdentityErrorDescriber>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ئءؤرلاىةوزظطكمنتالبيسشضصثقفغعهخحجدذألألإإلآآ";
            });
            ConfigureAuthService(services);

            services.Configure<AppSettings>(_configuration);
            services.Configure<EmailSettings>(_configuration.GetSection("EmailSettings"));

            services.AddHangfire((provider, configuration) => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseFilter(ActivatorUtilities.CreateInstance<DMSJobFilterAttribute>(provider))
                .UseSqlServerStorage(_configuration["ConnectionString"], new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                })); ;

            services.AddHangfireServer();

            services.AddMvc()
                .AddNewtonsoftJson(options =>
                                options.SerializerSettings.Converters.Add(new StringEnumConverter()))
                .AddFluentValidation(
                    configuration =>
                    {
                        configuration.RegisterValidatorsFromAssemblyContaining<SettingViewModelValidator>();
                        configuration.RegisterValidatorsFromAssemblyContaining<ManagerManager>();
                        configuration.RegisterValidatorsFromAssemblyContaining<Startup>();
                    })
                .AddDataAnnotationsLocalization(
                    options => options.DataAnnotationLocalizerProvider =
                        (type, factory) => factory.Create(typeof(Translations)));


            services.AddAutoMapper(typeof(Startup));
            services.AddScoped(typeof(IMapsManager), typeof(MapsManager));
            services.AddScoped(typeof(ITaskAutoAssignmentFactory), typeof(TaskAutoAssignmentFactory));
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();
            services.AddSingleton(typeof(IEmailSenderservice), typeof(EmailSenderService));
            services.AddSingleton(typeof(EmailSettings), typeof(EmailSettings));
            services.AddScoped(typeof(IGeoFenceManager), typeof(GeoFenceManager));
            services.AddScoped(typeof(IPushNotificationService), typeof(FirebasePushNotificationService));
            services.AddScoped(typeof(IDriverMonitorService), typeof(DriverMonitorService));
            services.AddScoped(typeof(IAddressProvider), typeof(CachedPACIProvider));
            services.AddScoped(typeof(IAddressProvider), typeof(PACIAddressProvider));
            services.AddScoped(typeof(IAddressProvider), typeof(GoogleAddressProvider));
            services.AddScoped(typeof(IAddressService), typeof(AddressService));

            services.AddScoped(typeof(IPACIService), typeof(PACIService));
            services.AddScoped(typeof(IAreaManager), typeof(AreaManager));
            services.Configure<PACIServiceSettings>(_configuration.GetSection("Address").GetSection("PACIAddressProvider").GetSection("PACIServiceSettings"));

            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddSwaggerGen(config =>
            {
                config.DescribeAllEnumsAsStrings();
                //config.OperationFilter<SupportMultipleEnumValues>();
            });
            services.AddSwaggerDocument(config =>
               {
                   config.PostProcess = document =>
                   {
                       document.Info.Version = "v1";
                       document.Info.Title = "DMS API";
                       document.Info.Description = "";
                       document.Info.TermsOfService = "None";
                   };

                   config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                   config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                       new OpenApiSecurityScheme()
                       {
                           Type = OpenApiSecuritySchemeType.ApiKey,
                           Name = "Authorization",
                           In = OpenApiSecurityApiKeyLocation.Header,
                           Description = "Copy this into the value field: Bearer {token}"
                       }
                   );
               });

            services.AddSignalR();
            services.AddHttpContextAccessor();
            services.AddCurrentUser();
            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                var allPermissions = ManagerPermissions.FlatPermissions
                    .Concat(AgentPermissions.FlatPermissions)
                    .ToArray();
                foreach (var permission in allPermissions)
                {
                    options.AddPolicy(permission, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(permission));
                    });
                }
            });
            services.AddLocalization(
                options =>
                {
                    options.ResourcesPath = "Resources";
                });
            services.AddTransient<ILocalizer, Localizer>();
            IocHelper.RegisterConventinos(services);
        }

        public void Configure(IApplicationBuilder app, IMapper mapper)
        {
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
            app.UseRequestLocalization(options =>
            {
                var arabicCulture = new CultureInfo("ar");
                arabicCulture.DateTimeFormat.Calendar = new GregorianCalendar();
                var englishCulture = new CultureInfo("en");
                var supportedCultures = new[] { englishCulture, arabicCulture };
                options.DefaultRequestCulture = new RequestCulture(supportedCultures.First());
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            app.UseCors("CorsPolicy");

            if (_environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(errorAppBuilder =>
                {
                    errorAppBuilder.Run(async context =>
                    {
                        var exception = context.Features.Get<IExceptionHandlerFeature>()?.Error;
                        var localizer = context.RequestServices.GetRequiredService<ILocalizer>();
                        var logger = context.RequestServices.GetRequiredService<ILogger<ExceptionHandlerMiddleware>>();
                        var (content, status) = exception switch
                        {
                            NotFoundException notFoundException => (notFoundException.Message, HttpStatusCode.NotFound),
                            DomainException domainException => (domainException.Message, HttpStatusCode.BadRequest),
                            UnauthorizedAccessException unauthorizedAccessException =>
                                (Keys.Validation.UnAuthorizedAccess, HttpStatusCode.Unauthorized),
                            _ => (Keys.Validation.GeneralError, HttpStatusCode.InternalServerError),
                        };
                        logger.LogCritical(exception, content);
                        if (!context.Response.HasStarted)
                        {
                            context.Response.StatusCode = (int)status;
                            var result =
                                new
                                {
                                    error = localizer[content]
                                };
                            // TODO: move to an extension method WriteJsonAsync
                            context.Response.ContentType = "application/json; charset=UTF-8";
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                            await context.Response.Body.FlushAsync();
                        }
                    });
                });
            }

            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseStaticFiles(
                new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "front-end"))
                });
            var paths = new[]
            {
                "CountryFlags",
                "DriverImages",
                "TaskSignatures",
                "TasksGallary",
                "TasksImages",
                "DriverSheet",
                "FaildDriverSheet",
                "CustomerSheet",
                "FaildCustomerSheet",
                "ImportTemplates"

            };

            //var fileProviders = paths.Select(path => new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), path)))
            //    .ToArray();
            foreach (var path in paths)
            {
                var fullPath = Path.Combine(Directory.GetCurrentDirectory(), path);
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(fullPath),
                    RequestPath = $"/{path}"
                });
            }
            app.UseHangfireDashboard();
            RecurringJob.AddOrUpdate<IDriverMonitorService>(
                DriverMonitoring => DriverMonitoring.LogoutDriversWithExpirdTokens(), Cron.Minutely);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notificationHub");
                //endpoints.MapHub<DriverHub>("/driverHub");


            });
            app.Use(
                async (context, next) =>
                {
                    var path = context.Request.Path.Value;
                    if (path.StartsWith("/api") || Path.HasExtension(path))
                    {
                        return;
                    }
                    var indexFilePath = Path.Combine(Directory.GetCurrentDirectory(), "front-end", "index.html");
                    var indexFileInfo = new FileInfo(indexFilePath);
                    if (indexFileInfo.Exists)
                    {
                        context.Response.ContentType = "text/html";
                        await context.Response.SendFileAsync(new PhysicalFileInfo(indexFileInfo));
                    }
                });
        }



        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Add(ClaimTypes.NameIdentifier, "sub");
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Add(ClaimTypes.Role, "role");

            //var identityUrl = Configuration.GetValue<string>("IdentityUrl");
            var signingKey = new X509SecurityKey(Certificate.Get());
            var issuer = _configuration["Jwt:Issuer"];
            var audience = _configuration["Jwt:Audience"];
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        RequireExpirationTime = true,
                        RequireSignedTokens = true,
                        ValidateLifetime = true,
#warning Certificate is expired renew or create a new one, certificate should be loaded from disk to allow custom cert on deployment
                        //ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidIssuer = issuer,// authenticationConfig.Jwt.Issuer,
                        ValidAudience = audience,//authenticationConfig.Jwt.Audience,
                        IssuerSigningKey = signingKey// authenticationConfig.Jwt.GetSecurityKey(),

                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnAuthenticationFailed = context =>
                        {
                            return Task.CompletedTask;
                        },
                        OnMessageReceived = context =>
                        {
                            //Supporting SignalR jwt authentication, negotiation send access_token as query string
                            var accessToken = context.Request.Query["access_token"];
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrWhiteSpace(accessToken) &&
                                path.StartsWithSegments("/notificationHub"))
                            {
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        },
                        OnTokenValidated = context =>
                        {
                            // Note : we can add multiple roles or claims by adding new identity to he existing...
                            //context.Principal.AddIdentity(tempIdentity);
                            return Task.CompletedTask;
                        }
                    };
                });
        }
    }
}