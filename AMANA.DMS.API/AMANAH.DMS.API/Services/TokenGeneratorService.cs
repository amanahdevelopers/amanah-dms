﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AMANAH.DMS.API.Certificates;
using AMANAH.DMS.BASE.Authentication;
using AMANAH.DMS.BLL.Managers;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace AMANAH.DMS.API.Services
{
    internal class TokenGeneratorService : ITokenGeneratorService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> _claimsPrincipalFactory;

        public TokenGeneratorService(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IConfiguration configuration,
            IUserClaimsPrincipalFactory<ApplicationUser> claimsPrincipalFactory)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _claimsPrincipalFactory = claimsPrincipalFactory;
        }

        public async Task<string> GenerateJWTTokenAsync(ApplicationUser user, Driver driver)
        {

            var issuer = _configuration.GetValue("Jwt:Issuer", "DMS");
            var audience = _configuration.GetValue("Jwt:Audience", "DMSClient");
            var expiryTime = _configuration.GetValue("Jwt:ExpiryTime", TimeSpan.FromHours(24));
            //var secret = _configuration.GetValue("Jwt:Secret", "DMSClient");
            //var claims = await GetClaims(user, selectedProfile, impersonatingEmail);
            //var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(protectedKey));
            var signingCredentials = new X509SigningCredentials(
                Certificate.Get(),
                SecurityAlgorithms.RsaSha256);
            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: await GetTokenClaimsAsync(user, driver),
                notBefore: DateTime.UtcNow.AddSeconds(1),
                expires: DateTime.UtcNow + expiryTime,
                signingCredentials: signingCredentials);

            var accessToken = new JwtSecurityTokenHandler().WriteToken(token);
            return accessToken;
        }

        private async Task<IEnumerable<Claim>> GetTokenClaimsAsync(ApplicationUser user, Driver driver = default)
        {
            //IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory = null;
            var claimsPrincipal = await _claimsPrincipalFactory.CreateAsync(user);
            var customClaims = new List<Claim>
            {
                //new Claim("AspNet.Identity.SecurityStamp", user.SecurityStamp),
                //new Claim(ClaimTypes.)
                new Claim("Tenant_Id", user.Tenant_Id),
                new Claim("UserType", await GetUserTypeAsync(user)),
            };
            if (driver != default && driver.Id != default)
            {
                customClaims.Add(new Claim("DriverId", driver.Id.ToString()));
            }
            claimsPrincipal.AddIdentity(new ClaimsIdentity(customClaims));
            return claimsPrincipal.Claims;
        }

        private async Task<string> GetUserTypeAsync(ApplicationUser user)
        {
            int userType = 0;
            var UserRoles = await _userManager.GetRolesAsync(user);
            if (UserRoles != null && UserRoles.Any())
            {
                if (UserRoles.Contains("Tenant"))
                {
                    userType = (int)RoleType.Admin;
                }
                else
                {
                    var userRoleName = UserRoles.FirstOrDefault();
                    var UserRole = await _roleManager.FindByNameAsync(userRoleName);
                    if (UserRole.Type == (int)RoleType.Manager)
                    {
                        userType = (int)RoleType.Manager;
                    }
                    else if (UserRole.Type == (int)RoleType.Agent)
                    {
                        userType = (int)RoleType.Agent;
                    }
                }
            }
            return userType.ToString();
        }
    }
}
