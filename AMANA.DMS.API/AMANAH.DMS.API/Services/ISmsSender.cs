﻿using System.Threading.Tasks;

namespace AMANAH.DMS.API.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
