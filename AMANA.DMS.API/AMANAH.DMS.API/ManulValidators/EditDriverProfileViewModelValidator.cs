﻿using AMANAH.DMS.BLL.ViewModels.Driver;
using AMANAH.DMS.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.ManualValidators
{
    public class EditDriverProfileViewModelValidator : AbstractValidator<EditDriverProfileViewModel>
    {
        public EditDriverProfileViewModelValidator(ILocalizer localizer)
        {
            RuleFor(x => x.CurrentPassword).Password(localizer);
            RuleFor(x => x.NewPassword).Password(localizer);
            RuleFor(x => x.NewPassword).NotEqual(x => x.CurrentPassword)
              .WithMessage(localizer[Keys.Validation.NewPasswordIsSameAsOld]);
            RuleFor(x => x.ConfirmPassword).Equal(x => x.NewPassword)
                .When(x => !string.IsNullOrWhiteSpace(x.NewPassword))
                .WithMessage(localizer[Keys.Validation.RetypePasswordMismatch]);
        }
    }
}
