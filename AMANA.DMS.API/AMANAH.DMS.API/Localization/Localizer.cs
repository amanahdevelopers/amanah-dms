﻿using System;
using Microsoft.Extensions.Localization;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.API.Localization
{
    internal class Localizer : ILocalizer
    {
        private readonly IStringLocalizer<Translations> _localizer;

        public Localizer(IStringLocalizer<Translations> localizer)
        {
            _localizer = localizer;
        }

        public string this[string key] => _localizer[key];

        public string Format(string key, params object[] arguments)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }
            return _localizer.GetString(key, arguments);
        }
    }
}
